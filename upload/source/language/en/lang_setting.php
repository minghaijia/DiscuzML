<?php

/**
 *      [Discuz!] (C)2001-2099 Comsenz Inc.
 *      This is NOT a freeware, use is subject to license terms
 *
 *      $Id: lang_template.php by Valery Votintsev at sources.ru
 *
 *      This file is automatically generate
 */

$lang = array (
//---------------------------
//vot: Titles from install_var.php, pre_common_nav table

	'nav_1'		=> 'Portal',//'门户',
	'nav_2'		=> 'Forum',//'论坛',
	'nav_3'		=> 'Groups',//'群组',
	'nav_4'		=> 'Space',//'家园',
	'nav_5'		=> 'Applications',//'应用',
	'nav_6'		=> 'Plugins',//'插件',
	'nav_7'		=> 'Help',//'帮助',
	'nav_8'		=> 'RankList',//'排行榜',
	'nav_9'		=> 'Follows',//'广播',
	'nav_10'	=> 'Guide',//'导读',
	'nav_11'	=> 'Collections',//'淘帖',
	'nav_12'	=> 'Blogs',//'日志',
	'nav_13'	=> 'Albums',//'相册',
	'nav_14'	=> 'Shares',//'分享',
	'nav_15'	=> 'Moods',//'记录',

	'nav_portal'	=> 'Portal',//'门户',
	'nav_blog'	=> 'Blogs',//'日志',
	'nav_album'	=> 'Albums',//'相册',
	'nav_share'	=> 'Shares',//'分享',
	'nav_doing'	=> 'Moods',//'记录',
	'nav_wall'	=> 'Wall',//'留言板',
	'nav_group'	=> 'Groups',//'群组',
	'nav_ranklist'	=> 'RankList',//'排行榜'
	'nav_follow'	=> 'Follow',//'广播',
	'nav_guide'	=> 'Guide',//'导读',
	'nav_collection'	=> 'Collections',//'淘帖',

	'nav_stat'	=> 'Statistics',//'站点统计',
	'nav_report'	=> 'Reports',//'举报',
	'nav_archiver'	=> 'Archive',
	'nav_mobile'	=> 'Mobile',//'手机版',
	'nav_friend'	=> 'Friends',//'好友',
	'nav_thread'	=> 'Threads',//'帖子',
	'nav_favorite'	=> 'Favorites',//'收藏',
	'nav_magic'	=> 'Magic',//'道具',
	'nav_medal'	=> 'Medals',//'勋章',
	'nav_task'	=> 'Tasks',//'任务',
	'nav_feed'	=> 'Feeds',//'动态',

	'nav_sethomepage'	=> '<img src="static/image/common/home.gif" title="Set as Homepage">',//'设为首页',
	'nav_setfavorite'	=> '<img src="static/image/common/fav.gif" title="Add to Favorites">',//'收藏本站',
	'nav_uchome'		=> 'UCHome',//'',

	'nav_darkroom'		=> 'Dark room',

	'idcardtype_choice'	=> 'ID\nPassport\nDriving license',
	'education_choice'	=> 'Doctor\nMaster\nBachelor\nSpecialist\nSchool\nPrimary School\nOther',
	'bloodtype_choice'	=> 'A\nB\nAB\nO\n其它',//'A\nB\nAB\nO\nOther',//tc:'A\nB\nAB\nO\n其它',

// Titles from pre_common_credit_rule table
	'credit_rule_post'		=> 'Publish post',
	'credit_rule_reply'		=> 'Reply post',
	'credit_rule_digest'		=> 'Add Digest',
	'credit_rule_postattach'	=> 'Upload attachment',
	'credit_rule_getattach'		=> 'Download attachment',
	'credit_rule_sendpm'		=> 'Send PM',
	'credit_rule_search'		=> 'Search',
	'credit_rule_promotion_visit'	=> 'Visit Promotion',
	'credit_rule_promotion_register'	=> 'Register by Promotion',
	'credit_rule_tradefinished'	=> 'Trade finished',
	'credit_rule_realemail'		=> 'E-mail verification',
	'credit_rule_setavatar'		=> 'Upload avatar',
	'credit_rule_videophoto'	=> 'Video verification',
	'credit_rule_hotinfo'		=> 'Hot provided',
	'credit_rule_daylogin'		=> 'Daily login',
	'credit_rule_visit'		=> 'Visit other space',
	'credit_rule_poke'		=> 'Greeting',
	'credit_rule_guestbook'		=> 'Write to Guestbook',
	'credit_rule_getguestbook'	=> 'Get Guestbook Message',
	'credit_rule_doing'		=> 'Publish doing',
	'credit_rule_publishblog'	=> 'Publish Blog',
	'credit_rule_joinpoll'		=> 'Poll Vote',
	'credit_rule_createshare'	=> 'Create Share',
	'credit_rule_comment'		=> 'Write Comment',
	'credit_rule_getcomment'	=> 'Get comment',
	'credit_rule_installapp'	=> 'Install application',
	'credit_rule_useapp'		=> 'Use application',
	'credit_rule_click'		=> 'Rate object',
	'credit_rule_modifydomain'	=> 'Modify domain',
	'credit_rule_portalcomment'	=> 'Comment Article',
	'credit_rule_followedcollection'	=> 'Collection followed',

//	'credit_rule_uploadimage'	=> 'Upload image',//
//	'credit_rule_report'		=> 'Report abuse',//
//	'credit_rule_invitefriend'	=> 'Successfully invite friends',//
//	'credit_rule_register'		=> 'Registration',//
//	'credit_rule_realname'		=> 'Real name verification',//
//	'credit_rule_editrealname'	=> 'Modify real name',//
//	'credit_rule_editrealemail'	=> 'Change verified E-mail',//
//	'credit_rule_delavatar'		=> 'Delete avatar',//

// Titles from instal_var for EXTCREDITS
	'extcredits1'		=> 'Reputation',
	'extcredits2'		=> 'Money',
	'extcredits3'		=> 'Contribution',
	'extcredits4'		=> 'extcredits4',
	'extcredits5'		=> 'extcredits5',
	'extcredits6'		=> 'extcredits6',
	'extcredits7'		=> 'extcredits7',
	'extcredits8'		=> 'extcredits8',
// Credits Formula Explanation
	'digestposts'	=> '精华帖数',//'精華帖數',
	'posts'		=> '发帖数',//'發帖數',
	'threads'	=> '主题数',//'主題數',
	'replies'	=> '回帖数',//'回帖數',
	'oltime'	=> '在线时间',//'在線時間(小時)',
	'blogs'		=> '日志数',//'日誌數',
	'friends'	=> '好友数',//'好友數',
	'albums'	=> '相册数',//'相冊數',
	'doings'	=> '记录数',//'記錄數',
	'polls'		=> '投票数',//'投票數',
	'sharings'	=> '分享数',//'分享數',
//	'nav_'		=> '',//'',

);

