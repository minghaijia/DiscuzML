<?php

/**
 *      [Discuz!] (C)2001-2099 Comsenz Inc.
 *      This is NOT a freeware, use is subject to license terms
 *
 *      $Id: lang_admincp_searchindex.php by Valery Votintsev at sources.ru
 *	This file is automatically generate
 *  -///////////////////////////////////////////////////////////////////////////////////////-
 *       Traduction en FRENCH par Andre13- Date du 07 juillet 2013 -
 *           Website: http://www.discuz-france.fr (only translated into language french)
 * Thanks to: Partners Codersclub - Website: http://codersclub.org/discuzx/forum.php (Vot and the team full and Members)
 * Thanks to: Bertrand my brother - Discuz-france.fr 
 *  -////////////////////////////////////////////////////////////////////////////////////////-
 */

$lang = array (
  0 =>
  array (
    'index' =>
    array (
      'Admin Centre Accueil' => 'action=index',  //  'Admin Center Home'
    ),
    'text' =>
    array (
      0 => 'Admin Centre Accueil',//'管理中心首页', //  'Admin Center Home',
    ),
  ),
  1 =>
  array (
    'index' =>
    array (
      'Gestion du Menu Personnalisé' => 'action=misc&operation=custommenu', // 'Custom Menu Management'
    ),
    'text' =>
    array (
      0 => 'Gestion du Menu Personnalisé',//'常用操作管理', // 'Custom Menu Management',
    ),
  ),
  2 =>
  array (
    'index' =>
    array (
      'Site Info' => 'action=setting&operation=basic',
    ),
    'text' =>
    array (
      0 => 'Site Info',
    ),
  ),
  3 =>
  array (
    'index' =>
    array (
      'Contrôle d\'Accès' => 'action=setting&operation=access',  // Access control' 
    ),
    'text' =>
    array (
      0 => 'Contrôle d\'Accès',//'注册与访问',  // 'Access control', 
    ),
  ),
  4 =>
  array (
    'index' =>
    array (
      'Caractéristiques du site' => 'action=setting&operation=functions',  //  'Site features'
    ),
    'text' =>
    array (
      0 => 'Modules du site',//'站点功能',  //  'Site modules',
    ),
  ),
  5 =>
  array (
    'index' =>
    array (
      'Optimisation des Performances' => 'action=setting&operation=cachethread',  //  'Performance Optimization'
    ),
    'text' =>
    array (
      0 => 'Optimisation des Performances',//'性能优化',  //  'Performance Optimization',
    ),
  ),
  6 =>
  array (
    'index' =>
    array (
      'Paramètres SEO' => 'action=setting&operation=seo',  //  'SEO settings'
    ),
    'text' =>
    array (
      0 => 'Paramètres SEO',//'SEO设置',  //  'SEO settings',
    ),
  ),
  7 =>
  array (
    'index' =>
    array (
      'Paramètres du Domaine' => 'action=domain',  //  'Domain settings'
    ),
    'text' =>
    array (
      0 => 'Paramètres du Domaine',//'域名设置', //   'Domain settings',
    ),
  ),
  8 =>
  array (
    'index' =>
    array (
      'Paramètres Suivi' => 'action=setting&operation=follow',//'广播设置' //  'Follow settings' => 'action=setting&operation=follow',
    ),
    'text' =>
    array (
      0 => 'Paramètres Suivi',//'广播设置', //  'Follow settings',
    ),
  ),
  9 =>
  array (
    'index' =>
    array (
      'Paramètres Espace' => 'action=setting&operation=home',  
    ),
    'text' =>
    array (
      0 => 'Paramètres Espace',//'空间设置', //  'Space settings',
    ),
  ),
  10 =>
  array (
    'index' =>
    array (
      'Autorisations Utilisateurs' => 'action=setting&operation=permissions', 
    ),
    'text' =>
    array (
      0 => 'Autorisations Utilisateurs',//'用户权限', //  'User Permissions',
    ),
  ),
  11 =>
  array (
    'index' =>
    array (
      'Paramètres Monétaire' => 'action=setting&operation=credits',  // 'Money Settings'
    ),
    'text' =>
    array (
      0 => 'Paramètres Monétaire',//'积分设置',  // 'Money Settings',
    ),
  ),
  12 =>
  array (
    'index' =>
    array (
      'Paramètres Anti-spam' => 'action=setting&operation=sec',
    ),
    'text' =>
    array (
      0 => 'Paramètres Anti-spam',//'防灌水设置', //  'Anti-spam settings',
    ),
  ),
  13 =>
  array (
    'index' =>
    array (
      'Paramètres Horaires' => 'action=setting&operation=datetime',
    ),
    'text' =>
    array (
      0 => 'Paramètres Horaires',//'时间设置', //  'Time settings',
    ),
  ),
  14 =>
  array (
    'index' =>
    array (
      'Paramètres Téléchargements' => 'action=setting&operation=attach', // 'Upload Settings',
    ),
    'text' =>
    array (
      0 => 'Paramètres Pièces jointe',//'上传设置', //   'Attachment Settings',
    ),
  ),
  15 =>
  array (
    'index' =>
    array (
      'Filigrane' => 'action=setting&operation=imgwater',
    ),
    'text' =>
    array (
      0 => 'Filigrane',//'水印设置', // 'Watermark',
    ),
  ),
  16 =>
  array (
    'index' =>
    array (
      'Taille Pièces jointe' => 'action=misc&operation=attachtype',
    ),
    'text' =>
    array (
      0 => 'Taille Pièces jointe',//'附件类型尺寸',  //  'Attachment size'
    ),
  ),
  17 =>
  array (
    'index' =>
    array (
      'Paramètres de Recherche' => 'action=setting&operation=search',
    ),
    'text' =>
    array (
      0 => 'Paramètres de Recherche',//'搜索设置',  // 'Search Settings', 
    ),
  ),
  18 =>
  array (
    'index' =>
    array (
      'Paramètres Régionaux',// => 'action=district', // 'Region settings',
    ),
    'text' =>
    array (
      0 => 'Paramètres Régionaux',//'地区设置', //  'Region settings',
    ),
  ),
  19 =>
  array (
    'index' =>
    array (
      'Liste de Rang',// => 'action=setting&operation=ranklist', // 'Rank List',
    ),
    'text' =>
    array (
      0 => 'Liste de Rang',//'排行榜设置', // 'Rank List',
    ),
  ),
  20 =>
  array (
    'index' =>
    array (
      'Paramètres Accès Mobiles' => 'action=setting&operation=mobile',
    ),
    'text' =>
    array (
      0 => 'Paramètres Accès Mobiles',//'手机版访问设置', //  'Mobile Access settings', // OU Paramètres d'Accès aux Portables OU Paramètres Accès Portables
    ),
  ),
  21 =>
  array (
    'index' =>
    array (
      'Paramètres de Navigation' => 'action=nav',
    ),
    'text' =>
    array (
      0 => 'Paramètres de Navigation',//'导航设置', //  'Navigation settings',
    ),
  ),
  22 =>
  array (
    'index' =>
    array (
      'Paramètres du Style' => 'action=setting&operation=styles',
    ),
    'text' =>
    array (
      0 => 'Paramètres du Style',//'界面设置', //'Style settings',  
    ),
  ),
  23 =>
  array (
    'index' =>
    array (
      'Gestion du Style' => 'action=styles',
    ),
    'text' =>
    array (
      0 => 'Gestion du Style',//'风格管理',  //  'Style Management',
    ),
  ),
  24 =>
  array (
    'index' =>
    array (
      'Gestion des Modèles' => 'action=templates',
    ),
    'text' =>
    array (
      0 => 'Gestion des Modèles',//'模板管理', //  'Template Management', // OU Gestion des Gabarits
    ),
  ),
  25 =>
  array (
    'index' =>
    array (
      'Gestion des Expressions' => 'action=smilies',
    ),
    'text' =>
    array (
      0 => 'Gestion des Expressions',//'表情管理', //  'Smile management', ***Andre13 says: NE pas confondre avec les Smileys** Gestions des sourires DONC Gestion des Expressions
    ),
  ),
  26 =>
  array (
    'index' =>
    array (
      'Gestion Pourcentage' => 'action=click',
    ),
    'text' =>
    array (
      0 => 'Gestion Pourcentage',//'表态动作', //   'Rate management', // OU Gestions des Taux
    ),
  ),
  27 =>
  array (
    'index' =>
    array (
      'Icônes du Topic' => 'action=misc&operation=stamp',
    ),
    'text' =>
    array (
      0 => 'Icônes du Topic',//'主题鉴定', //   'Thread icons' // OU Icônes de la discussion OU Icônes du Topic/Sujet
    ),
  ),
  28 =>
  array (
    'index' =>
    array (
      'Paramètres Éditeur' => 'action=setting&operation=editor',
    ),
    'text' =>
    array (
      0 => 'Paramètres Éditeur',//'编辑器设置', // 'Editor Settings'
    ),
  ),
  29 =>
  array (
    'index' =>
    array (
      'Listing Icônes En-Ligne' => 'action=misc&operation=onlinelist',
    ),
    'text' =>
    array (
      0 => 'Listing Icônes En-Ligne',//'在线列表图标', //  'Online List icons',
    ),
  ),
  30 =>
  array (
    'index' =>
    array (
      'Modérer' => 'action=moderate',
    ),
    'text' =>
    array (
      0 => 'Modérer',//'内容审核', //  'Moderate',
    ),
  ),
  31 =>
  array (
    'index' =>
    array (
      'Filtre Mauvais Mot' => 'action=misc&operation=censor',
    ),
    'text' =>
    array (
      0 => 'Filtre Mauvais Mot',//'词语过滤',  //   'Bad Word Filter',
    ),
  ),
  32 =>
  array (
    'index' =>
    array (
      'Rapports Utilisateur' => 'action=report',
    ),
    'text' =>
    array (
      0 => 'Rapports Utilisateur',//'用户举报', // 'User Reports',
    ),
  ),
  33 =>
  array (
    'index' =>
    array (
      'Gestion Tag' => 'action=tag',
    ),
    'text' =>
    array (
      0 => 'Gestion Tag',//'标签管理', //'Tag Management',  // Gestion Balise ou étiquette ou tag ???
    ),
  ),
  34 =>
  array (
    'index' =>
    array (
      'Collections' => 'action=collection',//'淘帖管理'
    ),
    'text' =>
    array (
      0 => 'Collections',//'淘帖管理', // 'Collections',
    ),
  ),
  35 =>
  array (
    'index' =>
    array (
      '' => 'action=',
    ),
    'text' =>
    array (
      0 => NULL,
    ),
  ),
  36 =>
  array (
    'index' =>
    array (
      'Gestion Sujet du Forum' => 'action=threads',
    ),
    'text' =>
    array (
      0 => 'Gestion Sujet du Forum',//'论坛主题管理', //  'Forum Thread Management',
    ),
  ),
  37 =>
  array (
    'index' =>
    array (
      'Suppression Post en Vrac' => 'action=prune',
    ),
    'text' =>
    array (
      0 => 'Suppression Post en Vrac',//'论坛批量删帖', //  'Bulk post delete',
    ),
  ),
  38 =>
  array (
    'index' =>
    array (
      'Gestion Pïèce-jointe' => 'action=attach',
    ),
    'text' =>
    array (
      0 => 'Gestion Pïèce-jointe',//'论坛附件管理', //  'Attachment Management',
    ),
  ),
  39 =>
  array (
    'index' =>
    array (
      '' => 'action=',
    ),
    'text' =>
    array (
      0 => NULL,
    ),
  ),
  40 =>
  array (
    'index' =>
    array (
      '' => 'action=',
    ),
    'text' =>
    array (
      0 => NULL,
    ),
  ),
  41 =>
  array (
    'index' =>
    array (
      'Gestion Groupe Topic' => 'action=threads&operation=group',
    ),
    'text' =>
    array (
      0 => 'Gestion Groupe Topic',//'群组主题管理',  //   'Group Thread Management', // OU Gestionnaire du Groupe des Sujets/Topics
    ),
  ),
  42 =>
  array (
    'index' =>
    array (
      'Suppression Groupe de Posts par Lots ' => 'action=prune&operation=group',
    ),
    'text' =>
    array (
      0 => 'Suppression Groupe de Posts par Lots',//'群组批量删帖', //  'Group Posts Batch delete',
    ),
  ),
  43 =>
  array (
    'index' =>
    array (
      'Gestion Pièces jointes du Groupe' => 'action=attach&operation=group',
    ),
    'text' =>
    array (
      0 => 'Gestion Pièces jointes du Groupe',//'群组附件管理', //   'Group Attachment Management',
    ),
  ),
  44 =>
  array (
    'index' =>
    array (
      '' => 'action=',
    ),
    'text' =>
    array (
      0 => NULL,
    ),
  ),
  45 =>
  array (
    'index' =>
    array (
      '' => 'action=',
    ),
    'text' =>
    array (
      0 => NULL,
    ),
  ),
  46 =>
  array (
    'index' =>
    array (
      'Gestion Recyclage Corbeille' => 'action=recyclebin',
    ),
    'text' =>
    array (
      0 => 'Gestion Recyclage Corbeille',//'主题回收站',  //  'Recycle Bin Management', // Gestion Recyclage de la Corbeille
    ),
  ),
  47 =>
  array (
    'index' =>
    array (
      'Réponses Recyclage Corbeille' => 'action=recyclebinpost',
    ),
    'text' =>
    array (
      0 => 'Réponses Recyclage Corbeille',//'回帖回收站',  //  'Replies Recycle Bin',
    ),
  ),
  48 =>
  array (
    'index' =>
    array (
      'Forum/Top Groupe' => 'action=threads&operation=forumstick',
    ),
    'text' =>
    array (
      0 => 'Forum/Top Groupe',//'版块/群组置顶',  //  'Forum/Group Top',
    ),
  ),
  49 =>
  array (
    'index' =>
    array (
      'Gestion Commentaire Publié' => 'action=postcomment',
    ),
    'text' =>
    array (
      0 => 'Gestion Commentaire Publié',//'帖子点评管理',  //  'Post Comment Management', // Gestion de publications des commentaires
    ),
  ),
  50 =>
  array (
    'index' =>
    array (
      '' => 'action=',
    ),
    'text' =>
    array (
      0 => NULL,
    ),
  ),
  51 =>
  array (
    'index' =>
    array (
      '' => 'action=',
    ),
    'text' =>
    array (
      0 => NULL,
    ),
  ),
  52 =>
  array (
    'index' =>
    array (
      'Gestion des Actes' => 'action=doing',
    ),
    'text' =>
    array (
      0 => 'Gestion des Actes',//'记录管理',  //Andre13 says: La Gestion des Actes*(Faits et gestes* OU Agissements*)
    ),
  ),
  53 =>
  array (
    'index' =>
    array (
      'Gestion Blogs' => 'action=blog',
    ),
    'text' =>
    array (
      0 => 'Gestion Blogs',//'日志管理', //  'Blogs Management', // La Gestion des Blogs
    ),
  ),
  54 =>
  array (
    'index' =>
    array (
      'Gestion Flux' => 'action=feed',
    ),
    'text' =>
    array (
      0 => 'Gestion Flux',//'动态管理',  //  'Feed management',
    ),
  ),
  55 =>
  array (
    'index' =>
    array (
      'Albums gérer' => 'action=album',
    ),
    'text' =>
    array (
      0 => 'Albums gérer',//'相册管理', //  'Albums manage',
    ),
  ),
  56 =>
  array (
    'index' =>
    array (
      'Gestion Images' => 'action=pic',
    ),
    'text' =>
    array (
      0 => 'Gestion Images',//'图片管理', //   'Images Management',
    ),
  ),
  57 =>
  array (
    'index' =>
    array (
      'Comments/Gestions Messages' => 'action=comment',
    ),
    'text' =>
    array (
      0 => 'Comments/Gestions Messages',//'评论/留言管理', //  'Comments/Messages Management', // Commentaires/ Gestions des Posts
    ),
  ),
  58 =>
  array (
    'index' =>
    array (
      'Gestion Partage' => 'action=share',
    ),
    'text' =>
    array (
      0 => 'Gestion Partage',//'分享管理', //  'Share Management',
    ),
  ),
  59 =>
  array (
    'index' =>
    array (
      '' => 'action=',
    ),
    'text' =>
    array (
      0 => NULL,
    ),
  ),
  60 =>
  array (
    'index' =>
    array (
      'Gestion Utilisateur' => 'action=members&operation=search',
    ),
    'text' =>
    array (
      0 => 'Gestion Utilisateur',//'用户管理',  //   'User Management',
    ),
  ),
  61 =>
  array (
    'index' =>
    array (
      'Ajouter Utilisateur' => 'action=members&operation=add',
    ),
    'text' =>
    array (
      0 => 'Ajouter Utilisateur',//'添加用户',  //  'Add User',
    ),
  ),
  62 =>
  array (
    'index' =>
    array (
      'Profil Utilisateur' => 'action=members&operation=profile',
    ),
    'text' =>
    array (
      0 => 'Profil Utilisateur',//'用户栏目',  //  'User profile',
    ),
  ),
  63 =>
  array (
    'index' =>
    array (
      'Statistiques' => 'action=members&operation=stat',
    ),
    'text' =>
    array (
      0 => 'Statistiques',//'资料统计',  //  'Statistics', // Stats.
    ),
  ),
  64 =>
  array (
    'index' =>
    array (
      'Envoyer Notification' => 'action=members&operation=newsletter',
    ),
    'text' =>
    array (
      0 => 'Envoyer Notification',//'发送通知', // 'Send notification', // OU Envoyer un avertissement
    ),
  ),
  65 =>
  array (
    'index' =>
    array (
      'Avis Mobile' => 'action=members&operation=newsletter&do=mobile',//'发送手机通知'
    ),
    'text' =>
    array (
      0 => 'Avis Mobile',//'发送手机通知',  // 'Mobile notification', // OU // 'Notification Mobile',
    ),
  ),
  66 =>
  array (
    'index' =>
    array (
      'Tags Utilisateur' => 'action=usertag', //'用户标签'
    ),
    'text' =>
    array (
      0 => 'Tags Utilisateur',//'用户标签' //  'User Tags',
    ),
  ),
  67 =>
  array (
    'index' =>
    array (
      'Utilisateurs Bannis' => 'action=members&operation=ban',
    ),
    'text' =>
    array (
      0 => 'Utilisateurs Bannis',//'禁止用户',  //   'Ban users', // OU Utilisateurs Bans
    ),
  ),
  68 =>
  array (
    'index' =>
    array (
      'IP Ban' => 'action=members&operation=ipban',
    ),
    'text' =>
    array (
      0 => 'IP Ban',//'禁止 IP', // 'Ban IP',
    ),
  ),
  69 =>
  array (
    'index' =>
    array (
      'Points Bonus' => 'action=members&operation=reward',
    ),
    'text' =>
    array (
      0 => 'Points Bonus',//'积分奖惩', // 'Reward points', // Pts. Prime Bonus OU Points Prime OU Points Prime Bonus
    ),
  ),
  70 =>
  array (
    'index' =>
    array (
      'Revoir News Utilisateurs' => 'action=moderate&operation=members',
    ),
    'text' =>
    array (
      0 => 'Revoir News Utilisateurs',//'审核新用户', //   'Review new users',  // Revoir les nouveaux utilisateurs OU Passer en revue tout les utilisateurs
    ),
  ),
  71 =>
  array (
    'index' =>
    array (
      'Gestion du Groupe' => 'action=admingroup',
    ),
    'text' =>
    array (
      0 => 'Gestion du Groupe',//'管理组',  //  'Group Management',  // Gestionnaire du Groupe
    ),
  ),
  72 =>
  array (
    'index' =>
    array (
      'Groupes Utilisateurs' => 'action=usergroups',
    ),
    'text' =>
    array (
      0 => 'Groupes Utilisateurs',//'用户组',  //  'User Groups',
    ),
  ),
  73 =>
  array (
    'index' =>
    array (
      'Les Suivis' => 'action=specialuser&operation=follow',//'推荐关注'
    ),
    'text' =>
    array (
      0 => 'Les Suivis',//'推荐关注', // 'Follows', // Suivis
    ),
  ),
  74 =>
  array (
    'index' =>
    array (
      'Amis par défaut' => 'action=specialuser&operation=defaultuser',
    ),
    'text' =>
    array (
      0 => 'Amis par défaut',//'推荐好友',  //  'Default friends',
    ),
  ),
  75 =>
  array (
    'index' =>
    array (
      '' => 'action=verify&operation=verify',
    ),
    'text' =>
    array (
      0 => NULL,
    ),
  ),
  76 =>
  array (
    'index' =>
    array (
      'Paramètres de Contrôles' => 'action=verify',
    ),
    'text' =>
    array (
      0 => 'Paramètres de Contrôles',//'认证设置', //  'Verification settings', // OU Paramètres/Réglages de Vérifications/Contrôles
    ),
  ),
  77 =>
  array (
    'index' =>
    array (
      '' => 'action=verify&operation=verify&do=1',
    ),
    'text' =>
    array (
      0 => NULL,
    ),
  ),
  78 =>
  array (
    'index' =>
    array (
      '' => 'action=verify&operation=verify&do=2',
    ),
    'text' =>
    array (
      0 => NULL,
    ),
  ),
  79 =>
  array (
    'index' =>
    array (
      'Catégories Portail' => 'action=portalcategory',
    ),
    'text' =>
    array (
      0 => 'Catégories Portail',//'频道栏目',  // 'Portal Categories',
    ),
  ),
  80 =>
  array (
    'index' =>
    array (
      'Gestion Article' => 'action=article',
    ),
    'text' =>
    array (
      0 => 'Gestion Article',//'文章管理',  // 'Article management',
    ),
  ),
  81 =>
  array (
    'index' =>
    array (
      'Gestion Topic' => 'action=topic',
    ),
    'text' =>
    array (
      0 => 'Gestion Topic',//'专题管理', //  'Topic management',
    ),
  ),
  82 =>
  array (
    'index' =>
    array (
      'Gestion Mise en Page' => 'action=diytemplate',
    ),
    'text' =>
    array (
      0 => 'Gestion Mise en Page',//'页面管理', //  'Layout Management', // OU Gestion Disposition
    ),
  ),
  83 =>
  array (
    'index' =>
    array (
      'Gestion des Blocs' => 'action=block',
    ),
    'text' =>
    array (
      0 => 'Gestion des Blocs',//'模块管理',  //  'Block Management',   
    ),
  ),
  84 =>
  array (
    'index' =>
    array (
      'Styles de Blocs' => 'action=blockstyle',
    ),
    'text' =>
    array (
      0 => 'Styles de Blocs',//'模块模板',  //   'Block styles', // OU Modèles du/des Bloc(s)
    ),
  ),
  85 =>
  array (
    'index' =>
    array (
      'Blocs tierces' => 'action=blockxml',
    ),
    'text' =>
    array (
      0 => 'Blocs tierces',//'第三方模块',  //   'Third-party blocks',
    ),
  ),
  86 =>
  array (
    'index' =>
    array (
      'Permissions Portail' => 'action=portalpermission',
    ),
    'text' =>
    array (
      0 => 'Permissions Portail',//'权限列表',  //  'Portal permissions',
    ),
  ),
  87 =>
  array (
    'index' =>
    array (
      'Catégories Blog' => 'action=blogcategory',
    ),
    'text' =>
    array (
      0 => 'Catégories Blog',//'日志分类',  //  'Blog Categories',
    ),
  ),
  88 =>
  array (
    'index' =>
    array (
      'Catégories Album' => 'action=albumcategory',
    ),
    'text' =>
    array (
      0 => 'Catégories Album',//'相册分类',  // 'Album Categories',
    ),
  ),
  89 =>
  array (
    'index' =>
    array (
      'Gestion Forum' => 'action=forums',
    ),
    'text' =>
    array (
      0 => 'Gestion Forum',//'版块管理',  //  'Forum Management',
    ),
  ),
  90 =>
  array (
    'index' =>
    array (
      'Forum Fusion' => 'action=forums&operation=merge',
    ),
    'text' =>
    array (
      0 => 'Forum Fusion',//'版块合并',  //   'Forum merge',
    ),
  ),
  91 =>
  array (
    'index' =>
    array (
      'Types sujet' => 'action=threadtypes',
    ),
    'text' =>
    array (
      0 => 'Types sujet',//'分类信息', //   'Thread types',
    ),
  ),
  92 =>
  array (
    'index' =>
    array (
      'Paramètres Groupe' => 'action=group&operation=setting',
    ),
    'text' =>
    array (
      0 => 'Paramètres Groupe',//'群组设置',  //   'Group settings',
    ),
  ),
  93 =>
  array (
    'index' =>
    array (
      'Catégories Groupe' => 'action=group&operation=type',
    ),
    'text' =>
    array (
      0 => 'Catégories Groupe',//'群组分类',  //  'Group categories',
    ),
  ),
  94 =>
  array (
    'index' =>
    array (
      'Groupe de Gestion' => 'action=group&operation=manage',
    ),
    'text' =>
    array (
      0 => 'Groupe de Gestion',//'群组管理',  //'Group Management',  
    ),
  ),
  95 =>
  array (
    'index' =>
    array (
      'Principaux autorisations du groupe' => 'action=group&operation=userperm',
    ),
    'text' =>
    array (
      0 => 'Principaux autorisations du groupe',//'群主权限',  // 'Main group permissions', 
    ),
  ),
  96 =>
  array (
    'index' =>
    array (
      'Niveau du Groupe' => 'action=group&operation=level',
    ),
    'text' =>
    array (
      0 => 'Niveau du Groupe',//'群组等级',  //   'Group level',
    ),
  ),
  97 =>
  array (
    'index' =>
    array (
      'Modération du Groupe' => 'action=group&operation=mod',
    ),
    'text' =>
    array (
      0 => 'Modération du Groupe',//'审核群组',  //   'Group moderation',
    ),
  ),
  98 =>
  array (
    'index' =>
    array (
      'Annonce du site' => 'action=announce',
    ),
    'text' =>
    array (
      0 => 'Annonce du site',//'站点公告',  //  'Site Announce',
    ),
  ),
  99 =>
  array (
    'index' =>
    array (
      'Publicitaire du Site' => 'action=adv',
    ),
    'text' =>
    array (
      0 => 'Publicitaire du Site',//'站点广告',  //  'Site Advertising', 
    ),
  ),
  100 =>
  array (
    'index' =>
    array (
      'Missions du site' => 'action=tasks',
    ),
    'text' =>
    array (
      0 => 'Missions du site',//'站点任务', //   'Site Tasks',
    ),
  ),
  101 =>
  array (
    'index' =>
    array (
      'Centre de Magie' => 'action=magics',
    ),
    'text' =>
    array (
      0 => 'Centre de Magie',//'道具中心',  //  'Magic Center',
    ),
  ),
  102 =>
  array (
    'index' =>
    array (
      'Centre de Médaille' => 'action=medals',
    ),
    'text' =>
    array (
      0 => 'Centre de Médaille',//'勋章中心',  //   'Medal Center',
    ),
  ),
  103 =>
  array (
    'index' =>
    array (
      'Site Aide' => 'action=faq', 
    ),
    'text' =>
    array (
      0 => 'Site Aide',//'站点帮助', // 'Site Help'
    ),
  ),
  104 =>
  array (
    'index' =>
    array (
      'E-commerce' => 'action=setting&operation=ec',
    ),
    'text' =>
    array (
      0 => 'E-Commerce',//'电子商务',  //'E-Commerce',  
    ),
  ),
  105 =>
  array (
    'index' =>
    array (
      'Liens Amis' => 'action=misc&operation=link',
    ),
    'text' =>
    array (
      0 => 'Liens Amis',//'友情链接',  //  'Friend Links',
    ),
  ),
  106 =>
  array (
    'index' =>
    array (
      'Webmaster Recommande' => 'action=misc&operation=focus',
    ),
    'text' =>
    array (
      0 => 'Webmaster Recommande',//'站长推荐',  // 'Webmaster Recommended', 
    ),
  ),
  107 =>
  array (
    'index' =>
    array (
      'Liens Connexes' => 'action=misc&operation=relatedlink',
    ),
    'text' =>
    array (
      0 => 'Liens Connexes',//'关联链接',  //  'Related Links',
    ),
  ),
  108 =>
  array (
    'index' =>
    array (
      'Recharge carte secrète' => 'action=card',
    ),
    'text' =>
    array (
      0 => 'Recharge carte secrète',//'充值卡密',  //  'Recharge card secret',
    ),
  ),
  109 =>
  array (
    'index' =>
    array (
      '' => 'action=check&operation=index',
    ),
    'text' =>
    array (
      0 => NULL,
    ),
  ),
  110 =>
  array (
    'index' =>
    array (
      '' => 'action=check&operation=basic',
    ),
    'text' =>
    array (
      0 => NULL,
    ),
  ),
  111 =>
  array (
    'index' =>
    array (
      '' => 'action=check&operation=check',
    ),
    'text' =>
    array (
      0 => NULL,
    ),
  ),
  112 =>
  array (
    'index' =>
    array (
      '' => 'action=check&operation=post',
    ),
    'text' =>
    array (
      0 => NULL,
    ),
  ),
  113 =>
  array (
    'index' =>
    array (
      '' => 'action=http://www.kuozhan.net/check.php?do=setting',
    ),
    'text' =>
    array (
      0 => NULL,
    ),
  ),
  114 =>
  array (
    'index' =>
    array (
      '' => 'action=cloud&operation=applist',
    ),
    'text' =>
    array (
      0 => NULL,
    ),
  ),
  115 =>
  array (
    'index' =>
    array (
      '' => 'action=cloud&operation=siteinfo',
    ),
    'text' =>
    array (
      0 => NULL,
    ),
  ),
  116 =>
  array (
    'index' =>
    array (
      '' => 'action=cloud&operation=doctor',
    ),
    'text' =>
    array (
      0 => NULL,
    ),
  ),
  117 =>
  array (
    'index' =>
    array (
      '' => 'action=cloud&operation=connect',
    ),
    'text' =>
    array (
      0 => NULL,
    ),
  ),
  118 =>
  array (
    'index' =>
    array (
      '' => 'action=cloud&operation=manyou',
    ),
    'text' =>
    array (
      0 => NULL,
    ),
  ),
  119 =>
  array (
    'index' =>
    array (
      'Centre Application' => 'action=cloudaddons',//'应用中心'  //'Application Center' 
    ),
    'text' =>
    array (
      0 => 'Centre Application',//'应用中心',  //   'Application Center',
    ),
  ),
  120 =>
  array (
    'index' =>
    array (
      'Modules' => 'action=plugins',//'插件'  
    ),
    'text' =>
    array (
      0 => 'Modules',//'插件',
    ),
  ),
  121 =>
  array (
    'index' =>
    array (
      '' => 'action=plugins&operation=config&do=138',
    ),
    'text' =>
    array (
      0 => NULL,
    ),
  ),
  122 =>
  array (
    'index' =>
    array (
      'MAJ cache' => 'action=tools&operation=updatecache',
    ),
    'text' =>
    array (
      0 => 'MAJ cache',//'更新缓存', //  'Update cache',
    ),
  ),
  123 =>
  array (
    'index' =>
    array (
      'MAJ statistiques' => 'action=counter',
    ),
    'text' =>
    array (
      0 => 'MAJ statistiques',//'更新统计',  //  'Update statistics',
    ),
  ),
  124 =>
  array (
    'index' =>
    array (
      'Opération log' => 'action=logs',
    ),
    'text' =>
    array (
      0 => 'Opération log',//'运行记录',  //  'Operation log',
    ),
  ),
  125 =>
  array (
    'index' =>
    array (
      'Tâches Planifiées' => 'action=misc&operation=cron',
    ),
    'text' =>
    array (
      0 => 'Tâches Planifiées',//'计划任务',  //  'Scheduled Tasks',
    ),
  ),
  126 =>
  array (
    'index' =>
    array (
      'Vérifier Autorisations Fichiers' => 'action=tools&operation=fileperms',
    ),
    'text' =>
    array (
      0 => 'Vérifier Autorisations Fichiers',//'文件权限检查',  //  'Check file permissions',
    ),
  ),
  127 =>
  array (
    'index' =>
    array (
      'Somme de contrôle Fichier' => 'action=checktools&operation=filecheck',
    ),
    'text' =>
    array (
      0 => 'Somme de contrôle Fichier',//'文件校验',  //   'File Checksum',
    ),
  ),
  128 =>
  array (
    'index' =>
    array (
      'Vérifier Crochets' => 'action=checktools&operation=hookcheck',
    ),
    'text' =>
    array (
      0 => 'Vérifier Crochets',//'嵌入点校验',  //  'Check Hooks',
    ),
  ),
  129 =>
  array (
    'index' =>
    array (
      'Equipe Admin' => 'action=founder&operation=perm',
    ),
    'text' =>
    array (
      0 => 'Equipe Admin',//'后台管理团队',  //  'Admin Team',
    ),
  ),
  130 =>
  array (
    'index' =>
    array (
      'Paramètres Mail' => 'action=setting&operation=mail',
    ),
    'text' =>
    array (
      0 => 'Paramètres Mail',//'邮件设置',  //   'Mail Settings',
    ),
  ),
  131 =>
  array (
    'index' =>
    array (
      'Centre de Sécurité' => 'action=patch',
    ),
    'text' =>
    array (
      0 => 'Centre de Sécurité',//'安全中心',  //   'Security Center',
    ),
  ),
  132 =>
  array (
    'index' =>
    array (
      'Paramètres UCenter' => 'action=setting&operation=uc',
    ),
    'text' =>
    array (
      0 => 'Paramètres UCenter',//'UCenter 设置',  //  'UCenter Settings',
    ),
  ),
  133 =>
  array (
    'index' =>
    array (
      'Database' => 'action=db&operation=export',
    ),
    'text' =>
    array (
      0 => 'Database',//'数据库',  //   'Database',
    ),
  ),
  134 =>
  array (
    'index' =>
    array (
      'Optimisation Table Utilisateur' => 'action=membersplit&operation=check',
    ),
    'text' =>
    array (
      0 => 'Optimisation Table Utilisateur',//'用户表优化',  //   'Optimize user table',
    ),
  ),
  135 =>
  array (
    'index' =>
    array (
      'Posts Divisés' => 'action=postsplit&operation=manage',
    ),
    'text' =>
    array (
      0 => 'Posts Divisés',//'帖子分表',  // 'Split posts', 
    ),
  ),
  136 =>
  array (
    'index' =>
    array (
      'Sujet Divisé' => 'action=threadsplit&operation=manage',
    ),
    'text' =>
    array (
      0 => 'Sujet Divisé',//'主题分表',  //  'Split thread',
    ),
  ),
  137 =>
  array (
    'index' =>
    array (
      'MAJ En-ligne' => 'action=upgrade',
    ),
    'text' =>
    array (
      0 => 'MAJ En-ligne',//'在线升级',  //  'Online Upgrade',
    ),
  ),
  138 =>
  array (
    'index' =>
    array (
      'Équipe de Gestion' => 'action=founder&operation=perm',  //  '后台管理团队'
    ),
    'text' =>
    array (
      0 => 'Équipe de Gestion', //  '后台管理团队',
    ),
  ),
  139 =>
  array (
    'index' =>
    array (
      'Paramètres de messagerie' => 'action=setting&operation=mail',  //  '邮件设置'
    ),
    'text' =>
    array (
      0 => 'Paramètres de messagerie', //   '邮件设置',
    ),
  ),
  140 =>
  array (
    'index' =>
    array (
      'Centre de sécurité' => 'action=patch', //  '安全中心'
    ),
    'text' =>
    array (
      0 => 'Centre de sécurité', //  '安全中心',
    ),
  ),
  141 =>
  array (
    'index' =>
    array (
      'UCenter Mis en place' => 'action=setting&operation=uc', //  'UCenter 设置'
    ),
    'text' =>
    array (
      0 => 'UCenter Mis en place', //   'UCenter 设置',
    ),
  ),
  142 =>
  array (
    'index' =>
    array (
      'Database' => 'action=db&operation=export',//'数据库'
    ),
    'text' =>
    array (
      0 => 'Database',//'数据库',
    ),
  ),
  143 =>
  array (
    'index' =>
    array (
      'Tables utilisateur optimisées' => 'action=membersplit&operation=check',  // '用户表优化' 
    ),
    'text' =>
    array (
      0 => 'Tables utilisateur optimisées', //  '用户表优化',
    ),
  ),
  144 =>
  array (
    'index' =>
    array (
      'Messages table de points' => 'action=postsplit&operation=manage',  //  '帖子分表'
    ),
    'text' =>
    array (
      0 => 'Messages table de points', //   '帖子分表',
    ),
  ),
  145 =>
  array (
    'index' =>
    array (
      'Thème sous-table' => 'action=threadsplit&operation=manage',  //  '主题分表'
    ),
    'text' =>
    array (
      0 => 'Thème sous-table', //  '主题分表',
    ),
  ),
  146 =>
  array (
    'index' =>
    array (
      'Mise à jour en ligne' => 'action=upgrade',  //'在线升级'  
    ),
    'text' =>
    array (
      0 => 'Mise à jour en ligne', //   '在线升级',
    ),
  ),
  147 =>
  array (
    'index' =>
    array (
      'Action optimisation' => 'action=optimizer',  //  '优化大师'
    ),
    'text' =>
    array (
      0 => 'Action optimisation', //  '优化大师',
    ),
  ),
  148 =>
  array (
    'index' =>
    array (
      'Groupe de Festion' => 'action=admingroup',//'管理组' //  'Group Management'
      'Gestion des permissions de la discussion' => 'action=admingroup&operation=edit&anchor=threadperm',//'主题管理权限' //'Thread permision management'  
    ),
    'text' =>
    array (
      0 => 'Group Management &raquo; La gestion de la permission de la discussion',//'管理组 &raquo; 主题管理权限',  //'Group Management &raquo; Thread permision management',
      1 => 'admingroup_edit_threadperm',
      2 => 'Gestion des permissions de la discussion',//'主题管理权限', //  'Thread permision management',
      3 => 'admingroup_edit_stick_thread',
      4 => 'Permet de Collez la discussion',//'允许置顶主题', //  'Allow to Stick Thread',
      5 => 'Choisissez si vous voulez permettre de Scotché ou collé les sujets. Si vous activez la fonction de scotch ou colle, Top III sera le site Top et Top II dans la catégorie actuelle',//'设置是否允许置顶管理范围内主题的级别。如果打开全局置顶的功能，置顶 III 将全站置顶，置顶 II 将在当前分类置顶', //   'Set whether to allow to Stick topics. If you enable Stick feature, Top III will be the site Top, and Top II within the current category',
      6 => 'admingroup_edit_digest_thread',
      7 => 'Autoriser Modifier Résumé',//'允许精华主题', //  'Allow Edit Digest',
      8 => 'Choisissez si vous voulez autoriser pour modifier le niveau Résumé de la discussion',//'设置是否允许精华管理范围内主题的级别',  //  'Set whether to allow to edit the thread Digest level',
      9 => 'admingroup_edit_bump_thread',
      10 => 'Autoriser de remonter le sujet',//'允许提升主题',  //  'Allow to bump thread',
      11 => 'Choisissez si vous voulez permettre à la direction des discussions en actions',//'设置是否允许提升管理范围内的主题',  //  'Set whether to allow manageme of bump threads',
      12 => 'admingroup_edit_highlight_thread',
      13 => 'Autoriser mettre en évidence le sujet',//'允许高亮主题',  //  'Allow highlight thread',
      14 => 'Choisissez si vous voulez autoriser les discussions mettent en évidence',//'设置是否允许高亮管理范围内的主题',  //  'Set whether to allow highligh threads',
      15 => 'admingroup_edit_live_thread',
      16 => 'Live Autoriser Thème',  //  '允许直播主题',
      17 => 'Définir opportunité de permettre Live  dans le cadre des sujets de gestion',  //   '设置是否允许直播管理范围内的主题',
      18 => 'admingroup_edit_recommend_thread',
      19 => 'Autoriser de recommander le sujet',//'允许推荐主题',  //  'Allow to recommend thread',
      20 => 'Définir si vous souhaitez autoriser la gestion du fil de la discussion en recommandation (Régler la fonction Recommander dans les paramètres du forum approprié)',//'设置是否允许推荐管理范围内的主题 (要在相应版块的扩展设置里开启推荐主题功能)', // 'Set whether to allow the Recommend thread management (Set the Recommend feature in the appropriate forum settings)',
      21 => 'admingroup_edit_stamp_thread',
      22 => 'Permission d\'ajouter le tampon au sujet',//'允许添加主题图章', // 'Allows to add thread stamp', 
      23 => 'Choisissez si vous voulez autoriser pour ajouter des tampons sur le fil de la discussion du sujet',//'设置是否允许给主题在帖子内容页添加图章', //  'Set whether to allow to add stamps to the thread',
      24 => 'admingroup_edit_stamp_list',
      25 => 'Permet d\'ajouter l\'icône de la discussion',//'允许添加主题图标',  // 'Allows to add thread icon', 
      26 => 'Choisissez si vous voulez autoriser à ajouter icône pour un sujet dans la liste des discussions',//'设置是否允许给主题在帖子列表页添加图标', //  'Set whether to allow to add icon to a thread in the thread list',
      27 => 'admingroup_edit_close_thread',
      28 => 'Autoriser de Fermer le sujet',//'允许关闭主题', //  'Allow to Close thread',
      29 => 'Choisissez si vous voulez laisser Fermez les discussions',//'设置是否允许关闭管理范围内的主题', //   'Set whether to allow to Close threads',
      30 => 'admingroup_edit_move_thread',
      31 => 'Autoriser de déplacer les discussions',//'允许移动主题', //  'Allow to move threads',
      32 => 'Choisissez si vous voulez autoriser de déplacer les discussions',//'设置是否允许移动管理范围内的主题',  //   'Set whether to allow to move threads',
      33 => 'admingroup_edit_edittype_thread',
      34 => 'Autoriser de modifier Type de Sujet',//'允许编辑主题分类', //   'Allow to edit thread type',
      35 => 'Choisissez si vous voulez autoriser la modification du type du sujet de la discussion',//'设置是否允许编辑管理范围内主题的分类',  // 'Set whether to allow editing of the thread type',
      36 => 'admingroup_edit_copy_thread',
      37 => 'Autoriser de dupliquer les discussions',//'允许复制主题',  // 'Allow to copy threads',
      38 => 'Définir si vous souhaitez autoriser la reproduction du sujet de la discussion',//'设置是否允许复制管理范围内的主题',  //  'Set whether to allow the the thread replication',
      39 => 'admingroup_edit_merge_thread',
      40 => 'Autoriser de fusionner les discussions',//'允许合并主题', //  'Allow to merge threads',
      41 => 'Choisissez si vous voulez autoriser le sujet fusionnant',//'设置是否允许合并管理范围内的主题',  // 'Set whether to allow the thread merging',
      42 => 'admingroup_edit_split_thread',
      43 => 'Autoriser de diviser le Sujet',//'允许分割主题', // 'Allow to split thread',
      44 => 'Choisissez si vous voulez autoriser à diviser les discussions',//'设置是否允许分割管理范围内的主题', //  'Set whether to allow to split threads',
      45 => 'admingroup_edit_repair_thread',
      46 => 'Autoriser de réparer les discussions',//'允许修复主题', // 'Allow to repair threads',
      47 => 'Choisissez si vous voulez autoriser la réparation du sujet de la discussion',//'设置是否允许修复管理范围内的主题', // 'Set whether to allow the thread repair',
      48 => 'admingroup_edit_refund',
      49 => 'Autoriser de modifier le remboursement',//'允许强制退款', //  'Allow to edit refund',
      50 => 'Autoriser de mettre fin aux frais du Sujet de la discussion, et retourner au profit de l\'auteur pour les acheteurs. Lorsque le remboursement, le thème ne sera plus exigible, tant que l\'auteur résultant aux propres points suivant une transaction',//'允许将作者发布的收费主题终止，并将作者所获得收益退还给购买者。退款后，该主题将不能再被设置为收费主题，同时作者将自行承担积分交易而产生的损失', // 'Allow to terminate the thread fees, and eturn the author benefit to buyers. When refund, the topic will no longer be chargeable, until the author arising own points by a transaction',
      51 => 'admingroup_edit_edit_poll',
      52 => 'Autoriser de modifier un sondage',//'允许编辑投票', // 'Allow edit Poll',
      53 => 'Que ce soit pour permettre de modifier les options du sondage',//'设置是否允许编辑管理范围内投票主题的选项', // 'Whether to allow edit poll options',
      54 => 'admingroup_edit_remove_reward',
      55 => 'Autoriser supprimer les primes bonus',//'允许移除悬赏', // 'Allow to remove rewards',
      56 => 'Soit pour autoriser de retirer la récompense du montant au rendement du sujet de la discussion',//'设置是否允许移除管理范围内悬赏主题的悬赏金额', // 'Whether to allow to remove the reward within the thread reward amount',
      57 => 'admingroup_edit_edit_activity',
      58 => 'Autoriser pour gérer les membres de l\'événement',//'允许管理活动报名者', // 'Allow to manage event members',
      59 => 'Soit pour autoriser la gestion d\'un des membres de l\'événement',//'设置是否允许管理范围内活动主题的报名者', // 'Whether to allow the management of an event members',
      60 => 'admingroup_edit_edit_trade',
      61 => 'Autoriser de modifier les produits',//'允许编辑商品', // 'Allow edit products',
      62 => 'Soit pour permettre de modifier les produits',//'设置是否允许编辑管理范围内商品主题的商品', // 'Whether to allow edit products',
      63 => 'admingroup_edit_usertag',
      64 => 'Autoriser les utilisateurs d\'ajouter des Tags',//'允许添加用户标签', // 'Allow users to add tags',
      65 => 'Choisissez si vous voulez autoriser les utilisateurs à modifier/gestion des Tags de Sujet',//'设置是否允许编辑管理范围内主题的用户标签', //  'Set whether to allow users to edit/manageme thread tags',
    ),
  ),
  149 =>
  array (
    'index' =>
    array (
      'Groupe de Gestion' => 'action=admingroup',//'管理组' //   'Group Management'
      'Post Privilèges Gestion' => 'action=admingroup&operation=edit&anchor=postperm',//'帖子管理权限'//  'Post management privileges'
    ),
    'text' =>
    array (
      0 => 'Groupe de Gestion &raquo; Post Privilèges Gestion',//'管理组 &raquo; 帖子管理权限',//   'Group Management &raquo; Post management privileges',
      1 => 'admingroup_edit_postperm',
      2 => 'Post Privilèges Gestion',//'帖子管理权限',//  'Post management privileges',
      3 => 'admingroup_edit_edit_post',
      4 => 'Autoriser de modifier les posts',//'允许编辑帖子',//   'Allow to edit posts',
      5 => 'Soit pour autoriser les posts modifier d\'autrui',//'设置是否允许编辑管理范围内的帖子',//  'Whether to allow edit posts of others',
      6 => 'admingroup_edit_warn_post',
      7 => 'Autoriser avertir les utilisateurs',//'允许警告帖子',//  'Allow warn users',
      8 => 'Que ce soit pour permettre d\'avertir les utilisateurs',//'设置是否允许警告管理范围内的帖子',//  'Whether to allow warn users',
      9 => 'admingroup_edit_ban_post',
      10 => 'Autoriser à interdire les posts',//'允许屏蔽帖子',//   'Allow to ban Posts',
      11 => 'Soit pour autoriser à interdire les posts',//'设置是否允许屏蔽管理范围内的帖子',//  'Whether to allow to ban posts',
      12 => 'admingroup_edit_del_post',
      13 => 'Autoriser de supprimer les posts',//'允许删除帖子',//  'Allow to delete posts',
      14 => 'Soit pour autoriser de supprimer les posts',//'设置是否允许删除管理范围内的帖子',//   'Whether to allow to delete posts',
      15 => 'admingroup_edit_stick_post',
      16 => 'Autoriser de Scotcher le post',//'允许置顶回帖',//  'Allow to stick post', 
      17 => 'Soit pour autoriser de scotché les posts dans un sujet',//'设置是否允许置顶管理范围内的回帖',//   'Whether to allow to stick posts within a thread',
      18 => 'admingroup_edit_manage_tag',
      19 => 'Autoriser la gestion de Tag',//'允许管理标签',// 'Allow Tag Management', 
      20 => 'Choisissez si vous voulez autoriser de gérer des Tags',//'设置是否允许管理管理范围内的标签',// 'Set whether to allow to manage tags', 
    ),
  ),
  150 =>
  array (
    'index' =>
    array (
      'Groupe de Gestion' => 'action=admingroup',//'管理组'//'Group Management'  
      'Privilèges Panel Modérateur' => 'action=admingroup&operation=edit&anchor=modcpperm',//'管理面板权限'//  'Moderator panel privileges' 
    ),
    'text' =>
    array (
      0 => 'Groupe de Gestion &raquo; Privilèges Panel Modérateur',//'管理组 &raquo; 管理面板权限',//  'Group Management &raquo; Moderator panel privileges',
      1 => 'admingroup_edit_modcpperm',
      2 => 'Privilèges Panel Modérateur',//'管理面板权限',//  'Moderator panel privileges',
      3 => 'admingroup_edit_mod_post',
      4 => 'Autorise de vérifier les posts',//'允许审核帖子',//  'Allow to verify Posts',
      5 => 'Soit pour autoriser de vérifier de posts des utilisateurs (uniquement quand un premoderation activé dans les paramètres du site)',//'设置是否允许审核用户发表的帖子，只在站点设置需要审核时有效',//   'Whether to allow to verify users posts (only when a premoderation enabled in the site settings)',
      6 => 'admingroup_edit_mod_user',
      7 => 'Permettent de vérifier les inscriptions',//'允许审核用户', //   'Allow to verify registrations',
      8 => 'Soit pour autoriser de vérifier les nouveaux utilisateurs enregistrés. Obligatoire uniquement si une vérification manuelle activée dans les paramètres du site.',//'设置是否允许审核新注册用户，只在站点设置需要人工审核新用户时有效',  //  'Whether to allow verify new registered users. Required only if a manuall verification enabled in the site setttings.', 
      9 => 'admingroup_edit_ban_user',
      10 => 'Autoriser à interdire aux utilisateurs de parler',//'允许禁止用户发言',  //   'Allows to prohibit users to speak',
      11 => 'Choisissez si vous voulez autoriser à interdire aux utilisateurs de parler',//'设置是否允许禁止用户发言',  //  'Set whether to allow to prohibit users to speak',
      12 => 'admingroup_edit_ban_user_visit',
      13 => 'Autoriser bannir des utilisateurs',//'允许禁止用户访问',  // 'Allow to ban users', 
      14 => 'Choisissez si vous voulez autoriser à interdire aux utilisateurs d\'y accéder',//'设置是否允许禁止用户访问',  //   'Set whether to allow to prohibit users to access',
      15 => 'admingroup_edit_ban_ip',
      16 => 'Autoriser ban IP',//'允许禁止 IP',  //   'Allow ban IP',
      17 => 'Soit pour autoriser à interdire l\'IP ou modifier les paramètres IP',//'设置是否允许添加或修改禁止 IP 设置',  //   'Whether to allow to ban IP or modify the IP settings',
      18 => 'admingroup_edit_edit_user',
      19 => 'Allow to edit users',//'允许编辑用户',  //  'Allow to edit users',
      20 => 'Soit pour autoriser de modifier les informations utilisateur',//'设置是否允许编辑用户资料',  //   'Whether to allow to edit the user information',
      21 => 'admingroup_edit_mass_prune',
      22 => 'Autoriser suppression de posts par lots',//'允许批量删帖',  //  'Allow batch delete posts',
      23 => 'Soit pour autoriser de lots les posts supprimer via le panneau modérateur. Modérateur peut massivement supprimer des posts dans la fouchette d\'1 par semaine, Super modérateur peut supprimer des messages dans la gamme de 2 semaines, l\'Administrateur n\'a pas de limites.',//'设置是否允许通过管理面板批量删帖，版主在面板中批量删帖的范围是 1 周内，超级版主删帖范围是 2 周内， 管理员无限制',  //  'Whether to allow to batch delete posts through the moderator panel. Moderator can mass delete posts in range of 1 week, Super moderator can delete posts in range of 2 weeks, Administrator have no limits.',
      24 => 'admingroup_edit_edit_forum',
      25 => 'Autoriser de modifier le Forum',//'允许编辑版块',  //  'Allow to edit Forum',
      26 => 'Que ce soit pour permettre de modifier les informations du forum',//'设置是否允许编辑管理范围内的版块的资料',  // 'Whether to allow edit forum information', 
      27 => 'admingroup_edit_post_announce',
      28 => 'Allow Announcement',//'允许发布公告',  //  
      29 => 'Que ce soit pour permettre de publier l\'Annonce au site',//'设置是否允许发布站点公告',  //   'Whether to allow publish the site announces',
      30 => 'admingroup_edit_clear_recycle',
      31 => 'Autoriser pour supprimer les posts de la Corbeille',//'允许删除回收站的帖子',  //   'Allow to delete posts to the Recycle Bin',
      32 => 'Choisissez si vous voulez autoriser de supprimer les posts par le "Front -> Panel Admin -> Sujet Détritus"',//'设置是否允在“前台->管理面板->主题回收站”中删除主题',  //  'Set whether to allow to delete posts by the "Front -> Admin Panel -> Thread Trash"',
      33 => 'admingroup_edit_view_log',
      34 => 'Autoriser la gestion consulter les logs',//'允许查看管理日志', //  'Allow to view management logs',
      35 => 'Soit pour autoriser de visualiser le journal de gestion',//'设置是否允许查看管理日志',  // 'Whether to allow to view the management log',
    ),
  ),
  151 =>
  array (
    'index' =>
    array (
      'Groupe de Gestion' => 'action=admingroup',//'管理组'// 'action=admingroup',
      'Privilèges Gestion Espace' => 'action=admingroup&operation=edit&anchor=spaceperm',//'空间管理权限' // 'action=admingroup&operation=edit&anchor=spaceperm', 
    ),
    'text' =>
    array (
      0 => 'Groupe de Gestion &raquo; Privilèges Gestion Espace',//'管理组 &raquo; 空间管理权限',// 'Group Management &raquo; Space management privileges',
      1 => 'admingroup_edit_spaceperm',
      2 => 'Privilèges Gestion Espace',//'空间管理权限',// 'Space management privileges',
      3 => 'admingroup_edit_manage_feed',
      4 => 'Autoriser pour gérer le flux',//'允许管理动态(feed)',// 'Allow to manage feeds',
      5 => 'admingroup_edit_manage_doing',
      6 => 'Autoriser pour gérer les actes',//'允许管理记录',// 'Allow to manage doings',
      7 => 'admingroup_edit_manage_share',
      8 => 'Autoriser pour gérer les partages',//'允许管理分享',// 'Allow to manage shares',
      9 => 'admingroup_edit_manage_blog',
      10 => 'Autoriser pour gérer les blogs',//'允许管理日志',// 'Allow to manage blogs',
      11 => 'admingroup_edit_manage_album',
      12 => 'Autoriser pour gérer les albums',//'允许管理相册',// 'Allow to manage albums',
      13 => 'admingroup_edit_manage_comment',
      14 => 'Autoriser Gérer les commentaires',//'允许管理评论',//  'Allow to Manage comments',
      15 => 'admingroup_edit_manage_magiclog',
      16 => 'Autoriser pour gérer les dossiers de magie',//'允许管理道具记录',// 'Allow to manage magic records',
      17 => 'admingroup_edit_manage_report',
      18 => 'Autoriser pour gérer les rapports',//'允许管理举报',// 'Allow to manage reports',
      19 => 'admingroup_edit_manage_hotuser',
      20 => 'Autoriser pour gérer les membres valeur hot',//'允许管理推荐成员',// 'Allow to manage members hot value',
      21 => 'admingroup_edit_manage_defaultuser',
      22 => 'Autoriser pour gérer par défaut des amis',//'允许管理推荐好友',// 'Allow to manage default Friends',
      23 => 'admingroup_edit_manage_videophoto',
      24 => 'Autoriser pour gérer la vérification vidéo',//'允许管理视频认证',//  'Allow to manage video verification',
      25 => 'admingroup_edit_manage_magic',
      26 => 'Autoriser pour gérer la magie',//'允许管理道具',// 'Allow to manage magic',
      27 => 'admingroup_edit_manage_click',
      28 => 'Autoriser pour gérer actions du taux',//'允许管理表态动作',//  'Allow to manage rate actions',
    ),
  ),
  152 =>
  array (
    'index' =>
    array (
      'Groupe de Gestion' => 'action=admingroup',//'管理组'// 'action=admingroup', // 'Group Management' 
      'Autres privilèges' => 'action=admingroup&operation=edit&anchor=otherperm',//'其他权限'// 'action=admingroup&operation=edit&anchor=otherperm', //  'Other privileges'
    ),
    'text' =>
    array (
      0 => 'Groupe de Gestion &raquo; Autres privilèges',//'管理组 &raquo; 其他权限',//  'Group Management &raquo; Other privileges',
      1 => 'admingroup_edit_otherperm',
      2 => 'Autres privilèges',//'其他权限',//  'Other privileges',
      3 => 'admingroup_edit_view_ip',
      4 => 'Autoriser à voir l\'IP',//'允许查看 IP',//  'Allow to View IP',
      5 => 'Soit pour autoriser à Voir l\'IP du membre',//'设置是否允许查看用户 IP',// 'Whether to allow to view user IP',
      6 => 'admingroup_edit_manage_collection',
      7 => 'Autoriser la gestion de collection',//'允许管理淘专辑',//  'Allow collection management',
      8 => 'Définir si vous souhaitez autoriser la gestion de la collection',//'设置是否允许管理淘专辑',// 'Set whether to allow collection management',
      9 => 'admingroup_edit_allow_make_html',
      10 => 'Vous permet de générer des fichiers HTML', // '允许生成HTML文件',
      11 => 'Choisissez si vous voulez autoriser les fichiers HTML générés',// '设置是否允许生成HTML文件',
    ),
  ),
  153 =>
  array (
    'index' =>
    array (
      'Groupe de Gestion' => 'action=admingroup',//'管理组' //  'Group Management'
      'Privilèges Gestion Portail' => 'action=admingroup&operation=edit&anchor=portalperm',//'门户管理权限'// 'Portal management privileges'
    ),
    'text' =>
    array (
      0 => 'Groupe de Gestion &raquo; Portal management privileges',//'管理组 &raquo; 门户管理权限',// 'Group Management &raquo; Portal management privileges',
      1 => 'admingroup_edit_portalperm',
      2 => 'Des privilèges de gestion du portail',//'门户管理权限',// 'Portal management privileges',
      3 => 'admingroup_edit_manage_article',
      4 => 'Allow to manage articles',//'允许管理所有文章',//  'Allow to manage articles',
      5 => 'Choisissez "Oui" De permettre cette gestion des membres du groupe à modifier et supprimer des articles dans toutes les catégories du portail',//'选择“是”则允许此管理组成员管理编辑删除所有频道栏目文章',//  'Select "Yes" to allow for this management group members to edit and delete articles in all the portal categories',
      6 => 'admingroup_edit_add_topic',
      7 => 'Autoriser pour créer le thème',//'允许创建专题',//'Allow to create topic', 
      8 => 'Définir d\'autoriser ou non la création des thèmes',//'设置是否允许创建专题',// 'Set whether to allow the topics creation',
      9 => 'admingroup_edit_manage_topic',
      10 => 'Autoriser pour gérer les sujets',//'允许管理专题',//  'Allow to manage topics',
      11 => 'Choisissez si vous voulez autoriser la modification de tous les sujets',//'设置是否允许编辑所有专题',// 'Set whether to allow editing of all topics',
      12 => 'admingroup_edit_diy',
      13 => 'Autoriser BRICO',//'允许 DIY', // 'Allow DIY',
      14 => 'Choisissez si vous voulez autoriser le BRICO DIY  dans le forum, articles, l\'espace, forum d\'autres pages',//'设置是否允许 DIY 论坛，文章，空间，论坛等页面',//  'Set whether to allow the DIY in forum, articles, space, forum other pages',
    ),
  ),
  154 =>
  array (
    'index' =>
    array (
      'Site Publicitaire' => 'action=adv',//'站点广告'//  'Site Advertising'
      'Espace publicitaire' => 'action=adv&operation=list',//'广告位'// 'Advertising space'
    ),
    'text' =>
    array (
      0 => 'Site Publicitaire &raquo; Espace publicitaire',//'站点广告 &raquo; 广告位',// 'Site Advertising &raquo; Advertising space',
      1 => 'adv_list_tip',
      2 => 'Vous pouvez utiliser "l\'espace publicitaire Personnalisé" n\'importe où sur la page pour ajouter de l\'espace publicitaire. Lors de l\'installation du nouvel espace de publicité, la publicité du script de cet espace sera envoyé à l\'Annuaire source/class/adv/, vous pouvez alors utiliser la liste suivante. Les développeurs de modules, avant de concevoir un nouvel espace publicitaire n\'oubliez pas de lire le contenu de "Discuz bibliothèque technique".',//'你可以使用“自定义广告位”在页面的任意位置添加广告位。安装新的广告位，需将广告位脚本程序上传到 source/class/adv/ 目录，然后即可在以下列表中使用了。插件开发人员在设计新的广告位前请务必仔细阅读《Discuz! 技术文库》中的内容。', //  'You can use the "custom Advertising space" anywhere on the page to add Advertising space. When install the new Advertising space, Advertising space script will be uploaded to the source/class/adv/ directory, then you can use the following list. Plug-in developers, before design a new Advertising space be sure to read the content of "Discuz! Technical Library".',
    ),
  ),
  155 =>
  array (
    'index' =>
    array (
      'Gestion Album' => 'action=album',//'相册管理',// 'Album Management' => 'action=album',
      'Liste récente' => 'action=album',//'最新列表', // 'Recent list' => 'action=album',
    ),
    'text' =>
    array (
      0 => 'Gestion Albums &raquo; Liste récente',//'相册管理 &raquo; 最新列表', //   'Albums Management &raquo; Recent list',
      1 => 'album_tips',
      2 => 'Gestion des lots est utilisée pour supprimer un album. Album supprimé avec des photos à l\'intérieur pour rappeler: cliquez sur la page des résultats de recherche, le bouton Supprimer permet de supprimer les informations directement!',//'批量相册管理用于删除相册使用。连图片一同删除提醒：点击搜索结果页的删除按钮，将会直接删除相关信息！', //   'Batch management is used to delete an album. Album Deleted together with inside pictures to remind: click on the search results page, the Delete button will delete the information directly!',
    ),
  ),
  156 =>
  array (
    'index' =>
    array (
      'Gestion Albums' => 'action=album',//'相册管理', // 'Albums Management' 
      'Recherche' => 'action=album&search=true',//'搜索', //'Search'
    ),
    'text' =>
    array (
      0 => 'Gestion Albums &raquo; Recherche',//'相册管理 &raquo; 搜索', // 'Albums Management &raquo; Search',
      1 => 'album_search_detail',
      2 => 'Voir liste des albums détaillés',//'显示详细相册列表', // 'Show detailed album list',
      3 => 'album_search_perpage',
      4 => 'Articles par page',//'每页显示数', // 'Items per page',
      5 => 'resultsort',
      6 => 'Trier les résultats',//'结果排序', // 'Sort results',
      7 => 'album_search_albumname',
      8 => 'Nom Album',//'相册名', // 'Album name',
      9 => 'album_search_albumid',
      10 => 'Album ID',//'相册 ID', //  'Album ID',
      11 => 'Séparez les IDs d\'album avec une virgule ","',//'多相册 ID 中间请用半角逗号 "," 隔开', //  'Separate multiple Album IDs with a comma ","',
      12 => 'album_search_uid',
      13 => 'UID Utilisateur',//'用户 UID',
      14 => 'Séparer les noms d\'utilisateur par une virgule ","',//'多 UID 中间请用半角逗号 "," 隔开', // 'Separate multiple user IDs with a comma ","',
      15 => 'album_search_user',
      16 => 'Auteur de l\'Album',//'创建相册用户名', // 
      17 => 'Plusieurs noms d\'utilisateurs séparés par une virgule ","',//'多用户名中间请用半角逗号 "," 隔开', // 'Separate multiple user names with a comma ","',
      18 => 'blog_search_friend',
      19 => 'Visualiser les autorisations',//'查看权限', // 'View Permissions',
      20 => 'album_search_time',
      21 => 'Mise à jour Plage horaire',//'更新时间范围', // 'Updated time range',
      22 => 'Format est yyyy-mm-jj',//'格式 yyyy-mm-dd', // 'Format is yyyy-mm-dd',
    ),
  ),
  157 =>
  array (
    'index' =>
    array (
      'Catégories de l\'Album' => 'action=albumcategory',//'相册分类', // 'Album Categories' 
    ),
    'text' =>
    array (
      0 => 'Catégories de l\'Album',//'相册分类', //  'Album Categories',
      1 => 'system_category_stat',
      2 => 'Activer le système de catégories',//'开启系统分类',// 'Enable system Categories',
      3 => 'system_category_required',
      4 => 'Configuration requises des Catégories',//'系统分类必填', //  'System Categories required',
    ),
  ),
  158 =>
  array (
    'index' =>
    array (
      'Site Annonces' => 'action=announce',//'站点公告', //  'Site Announces'
    ),
    'text' =>
    array (
      0 => 'Site Annonces',//'站点公告', //   'Site Announces',
      1 => 'announce_edit',
      2 => 'Modifier Annonce du site',//'站点公告编辑', //  'Edit Site Announce',
      3 => 'start_time',
      4 => 'Heure de départ',//'起始时间', //   'Start time',
      5 => 'end_time',
      6 => 'Heure de fin',//'终止时间', //  'End time', 
      7 => 'announce_type',
      8 => 'Type d\'Annonce',//'公告类型', //  'Announce type',
      9 => 'announce_message',
      10 => 'Message',//'内容', //  'Message',
      11 => '<b>Texte d\'Annonce:</b> L\'Annonce du contenu supporte le BBCode<br /><b>Lien URL:</b> PS\'il vous plaît saisir le lien annonce tel que: http://xxx.xxx.xxx',//'文字公告: 直接输入公告内容，支持 Discuz! 代码网址链接: 请输入公告的链接地址如某个主题地址: http://xxx.xxx.xxx', //  '<b>Announce text:</b> Announce content support BBCode<br /><b>URL link:</b> Please enter the announce link such as: http://xxx.xxx.xxx', 
    ),
  ),
  159 =>
  array (
    'index' =>
    array (
      'Blocs Tierce partie' => 'action=blockxml',//'第三方模块', //  'Third-party blocks' 
      'Recherche' => 'action=blockxml&operation=add',//'搜索', //  'Search' 
    ),
    'text' =>
    array (
      0 => 'Blocs Tierce partie &raquo; Recherche',//'第三方模块 &raquo; 搜索', //   'Third-party blocks &raquo; Search',
      1 => 'blockxml_tips',
      2 => 'Les développeurs de modules, avant de concevoir un nouvel espace publicitaire n\'oubliez pas de lire le contenu de "Discuz bibliothèque technique".',//'插件开发人员在设计新的第三方模块前请务必仔细阅读《Discuz! 技术文库》中的内容。', //  'Plug-in developers, before design a new Advertising space be sure to read the content of "Discuz! Technical Library".',
      3 => 'blockxml_add',
      4 => 'Ajouter Bloc Tierce partie',//'添加 第三方模块', //  'Add third-party block',
      5 => 'blockxml_xmlurl',
      6 => 'URL Tierce partie',//'第三方地址', //  'Third-party URL', 
      7 => 'Entrez l\'URL du bloc de tiers. Tierce partie doit retourner le contenu au format XML bloc',//'输入 第三方模块的 URL 地址，第三方返回的内容必须 XML 模块的格式', //  'Enter the third-party block URL. Third-party must return the content in XML block format',
      8 => 'blockxml_clientid',
      9 => 'Client ID',//'客户端ID', //  'Client ID',
      10 => 'Cet ID est fourni par la source de données',//'此ID由数据源方提供', //  'This ID is provided by the data source',
      11 => 'blockxml_signtype',
      12 => 'Le cryptage de Signature',//'签名加密方式', //  'Signature encryption',
      13 => 'Actuellement pris en charge la méthode de chiffrement MD5, vous ne pouvez pas utiliser une signature, fourni par la source de données',//'目前支持MD5加密方式，也可以不使用签名，由数据源方提供', // 'Currently supported MD5 encryption method, you can not use a signature, provided by the data source',
      14 => 'blockxml_xmlkey',
      15 => 'Clé de Notification',//'通信密钥',  //  'Communication key',
      16 => 'Adresse URL pour obtenir la clé de données à partir. Laissez vide si la source de données ne nécessite pas de clé. Cette clé est fournie par la source de données',//'从 URL 地址获得数据时的密钥，如果数据源不需要密钥可以为空。此密钥由数据源方提供', // 'URL address to obtain the data key from. Leave empty if the data source does not require key. This key is provided by the data source',
    ),
  ),
  160 =>
  array (
    'index' =>
    array (
      'Gestion Blogs' => 'action=blog',//'日志管理',  // 'Blogs Management'  
      'Blogs récents' => 'action=blog',//'最新列表',  // 'Recent blogs' 
    ),
    'text' =>
    array (
      0 => 'Gestion Blogs  &raquo; Blogs récents',//'日志管理 &raquo; 最新列表',  //   'Blogs Management  &raquo; ',
      1 => 'blog_tips',
      2 => 'Gestion des lots est utilisée pour éliminer les Blogs avec tout ce qu\'il répond. Le {x} mot qualificatif peut être utilisé pour ignorer un texte entre deux caractères adjacents, par exemple si vous utilisez "a{1}s{2}s" masque (sans les guillemets), vous pouvez trouver le "ass" et "axsxs" et "axsxxs" et ainsi de suite. Attention: Lorsque vous cliquez sur le bouton Supprimer dans la page de résultats de recherche, l\'information sera supprimé directement!',//'批量日志信息管理用于删除日志使用，并且连同对应的回复一同删除。关键字可以使用限定符 {x} 以限定相邻两字符间可忽略的文字，x 是忽略字符的个数，如 "a{1}s{2}s"(不含引号) 可以搜索到 "ass" 也可搜索到 "axsxs" 和 "axsxxs" 等等。提醒：点击搜索结果页的删除按钮，将会直接删除相关信息！',  //   'Batch management is used to remove the blogs together with all it replies. The {x} keyword qualifier can be used to ignored some text between two adjacent characters, for example if you use "a{1}s{2}s" mask (without the quotes), you can find the "ass" and "axsxs" and "axsxxs" and so on. Warning: When you click on the Delete button in the search results page, the information will be deleted directly!',
    ),
  ),
  161 =>
  array (
    'index' =>
    array (
      'Gestion Blogs' => 'action=blog',//'日志管理',  //  'Blogs Management' 
      'Recherche' => 'action=blog&search=true',//'搜索',  //'Search' 
    ),
    'text' =>
    array (
      0 => 'Gestion Blogs &raquo; Recherche',//'日志管理 &raquo; 搜索',  //  'Blogs Management &raquo; Search',
      1 => 'blog_search_detail',
      2 => 'Afficher la liste des blogs détaillés',//'显示详细日志列表',  //  'Show detailed blog list',
      3 => 'blog_search_perpage',
      4 => 'Articles par page',//'每页显示数',  //  'Items per page', 
      5 => 'resultsort',
      6 => 'Trier les résultats',//'结果排序',  //  'Sort results', 
      7 => 'blog_search_uid',
      8 => 'UID Utilisateur',//'用户 UID',  //  'User UID',
      9 => 'UID Séparez par une virgule ","',//'多 UID 中间请用半角逗号 "," 隔开',  // 'Separate multiple UID with a comma ","',
      10 => 'blog_search_blogid',
      11 => 'Blog ID',//'日志 ID',  //  'Blog ID',
      12 => 'Séparez les IDs de blog par une virgule ","',//'多日志 ID 中间请用半角逗号 "," 隔开',  //  'Separate multiple blog ID with a comma ","',
      13 => 'blog_search_user',
      14 => 'Blog author user name',//'发表日志用户名',  //   'Blog author user name',
      15 => 'Plusieurs noms d\'utilisateurs séparés par une virgule ","',//'多用户名中间请用半角逗号 "," 隔开',  //  'Separate multiple user names with a comma ","',
      16 => 'blog_search_keyword',
      17 => 'Title or Keyword',//'标题或内容关键字',  //'Title or Keyword',  
      18 => 'Plusieurs mots-clés séparés par une virgule ",". Qualificatif de mots clés {x} peut être utilisé.',//'多关键字中间请用半角逗号 "," 隔开，关键词可以用限定符 {x}',  // 'Separate multiple keywords with a comma ",". Keyword qualifier {x} can be used.', 
      19 => 'blog_search_friend',
      20 => 'View Permissions',//'查看权限',  //  'View Permissions',
      21 => 'blog_search_ip',
      22 => 'Posted IP',//'发布IP',  //  'Posted IP',
      23 => 'Joker "*" peut être utilisé, à savoir que "127.0 .*.*" (sans les guillemets), à utiliser avec prudence!',//'通配符 "*" 如 "127.0.*.*"(不含引号)，慎用!!!',  // 'Wildcard "*" may be used, i.e. "127.0 .*.*" (without quotes), use with caution!!!', 
      24 => 'blog_search_lengthlimit',
      25 => 'Minimal content length',//'内容最小长度',  //   'Minimal content length',
      26 => 'Cette fonctionnalité augmentera la charge du serveur',//'本功能会加重服务器负担',  //  'This feature will add the server loading',
      27 => 'blog_search_view',
      28 => 'Vues',//'查看数',  //  'Views',
      29 => 'blog_search_reply',
      30 => 'Réponses',//'回复数',  //  'Replies',
      31 => 'blog_search_hot',
      32 => 'Hot',//'热度',  //  'Hot',
      33 => 'blog_search_time',
      34 => 'Post Time range',//'发表时间范围',  // 'Post Time range', 
      35 => 'Format est aaaa-mm-jj',//'格式 yyyy-mm-dd',  //'Format is yyyy-mm-dd',  
    ),
  ),
  162 =>
  array (
    'index' =>
    array (
      'Blog Catégories' => 'action=blogcategory',//'日志分类', // 'Blog Categories'
    ),
    'text' =>
    array (
      0 => 'Blog Catégories',//'日志分类',// 'Blog Categories',
      1 => 'system_category_stat',
      2 => 'Activer le système de catégories',//'开启系统分类',// 'Enable system Categories',
      3 => 'system_category_required',
      4 => 'Système Categories requises',//'系统分类必填',// 'System Categories required',
    ),
  ),
  163 =>
  array (
    'index' =>
    array (
      'Connexion Corbeille' => 'action=blogrecyclebin', // '日志回收站'   
      'Liste des journaux' => 'action=blogrecyclebin', //  '日志列表' 
    ),
    'text' =>
    array (
      0 => 'Connexion Corbeille &raquo; Liste des journaux', //  '日志回收站 &raquo; 日志列表',
      1 => 'blog_tips',
      2 => 'Gestion de l\'information du journal des lots de grumes suppression utilisée, et supprimé ainsi que les réponses correspondantes. Les mots clés peuvent utiliser des qualificatifs {x} Pour définir entre deux caractères adjacents dans le texte peut être ignoré, x est le nombre de caractères est ignoré, comme "a{1}s{2}s"(Sans les guillemets) peuvent être recherchés "ass" Peut également être recherché "axsxs" et "axsxxs" Et ainsi de suite. Rappel: cliquez sur les résultats de recherche la page bouton Supprimer permet de supprimer les informations pertinentes directement!', //  '批量日志信息管理用于删除日志使用，并且连同对应的回复一同删除。关键字可以使用限定符 {x} 以限定相邻两字符间可忽略的文字，x 是忽略字符的个数，如 "a{1}s{2}s"(不含引号) 可以搜索到 "ass" 也可搜索到 "axsxs" 和 "axsxxs" 等等。提醒：点击搜索结果页的删除按钮，将会直接删除相关信息！',
    ),
  ),
  164 =>
  array (
    'index' =>
    array (
      'Gestion des journaux' => 'action=blog', //  '日志管理'
      'Rechercher' => 'action=blog&search=true', //  '搜索' 
    ),
    'text' =>
    array (
      0 => 'Gestion des journaux &raquo; Rechercher', //  '日志管理 &raquo; 搜索',
      1 => 'blog_search_detail',
      2 => 'Afficher la liste des journaux', //  '显示详细日志列表',
      3 => 'blog_search_perpage',
      4 => 'Nombre par page', //  '每页显示数', 
      5 => 'resultsort',
      6 => 'Trier', // '结果排序',  
      7 => 'blog_search_uid',
      8 => 'UID Utilisateur', //   '用户 UID',
      9 => 'multi- UID Svp. veuillez utiliser une intermédiaire  "," Séparer par des virgules', //  '多 UID 中间请用半角逗号 "," 隔开',
      10 => 'blog_search_blogid',
      11 => 'Connexion ID', //  '日志 ID', 
      12 => 'multiple Connexion ID Svp. veuillez utiliser une intermédiaire "," Séparer par des virgules', //  '多日志 ID 中间请用半角逗号 "," 隔开',
      13 => 'blog_search_user',
      14 => 'Message enregistre le nom utilisateur', //   '发表日志用户名', 
      15 => 'Nom multi-utilisateur Moyen Svp. veuillez utiliser  "," Séparer par des virgules', // '多用户名中间请用半角逗号 "," 隔开', 
      16 => 'blog_search_keyword',
      17 => 'Titre ou mot-clé', //  '标题或内容关键字',
      18 => 'Mots-clés multiples avec par des virgules  veuillez utiliser une intermédiaire "," Séparer par des virgules  Mots-clés séparés vous pouvez utiliser des qualificatifs {x}', //'多关键字中间请用半角逗号 "," 隔开，关键词可以用限定符 {x}',  
      19 => 'blog_search_friend',
      20 => 'Afficher les autorisations', //  '查看权限',
      21 => 'blog_search_ip',
      22 => 'Post IP', //  '发布IP',
      23 => 'Caractère générique "*" comme "127.0.*.*"(Sans les guillemets), prudence!', // '通配符 "*" 如 "127.0.*.*"(不含引号)，慎用!!!',  
      24 => 'blog_search_lengthlimit',
      25 => 'Contenu longueur minimale', // '内容最小长度',  
      26 => 'Cette fonctionnalité va augmenter la charge sur le serveur', //  '本功能会加重服务器负担', 
      27 => 'blog_search_view',
      28 => 'Afficher par nombre', //   '查看数',
      29 => 'blog_search_reply',
      30 => 'Réponses', //  '回复数',
      31 => 'blog_search_hot',
      32 => 'Hot', //  '热度',
      33 => 'blog_search_time',
      34 => 'Poster plage de temps', // '发表时间范围', 
      35 => 'Format aaaa-mm-jj', //  '格式 yyyy-mm-dd',
    ),
  ),
  165 =>
  array (
    'index' =>
    array (
      'Autoriser recharger à la réception' => 'action=card',//'允许前台充值', // 'Allow recharge reception'
    ),
    'text' =>
    array (
      0 => 'Autoriser recharger à la réception',//'允许前台充值', //  'Allow recharge reception',
      1 => 'card_config_open',
      2 => 'Autoriser recharger à la réception',//'允许前台充值',  //  'Allow recharge reception',
      3 => 'Choisissez si vous voulez autoriser la réception de recharge. Si la réception de recharger est désactivée, la génération de fond du code de vérification secrète pour la recharge aura une incidence sur: Global -> Paramètres de protection anti-spam -​​> Définir le code de vérification',//'设置是否允许前台充值，关闭前台充值功能，不影响后台卡密的生成充值卡密验证码开关：全局->防灌水设置->验证码设置', //  'Set whether to allow the recharge reception. If reception of recharge is disabled, the background generation of the secret verification code for recharge will affect: Global -> Spam protection settings -> Set verification code',
    ),
  ),
  166 =>
  array (
    'index' =>
    array (
      'Exporter carte de recharge soutient exporter jusqu\'à 10.000 données secrètes. Pour ouvrir les fichiers XLS utilisent tous les outils disponibles comme Excel.' => 'action=card&operation=manage',//'导出充值卡密最多支持 10000 条数据，导出的 .xls 文件可用 EXCEL 等工具打开。',  // 'Export recharge card supports export up to 10,000 secret data. For open XlS files use any available tools such as EXCEL.' => 'action=card&operation=manage',
    ),
    'text' =>
    array (
      0 => 'Exporter carte de recharge soutient exporter jusqu\'à 10000 données secrètes. Pour ouvrir les fichiers XLS utilisent tous les outils disponibles comme Excel.',//'导出充值卡密最多支持 10000 条数据，导出的 .xls 文件可用 EXCEL 等工具打开。', // 'Export recharge card supports export up to 10000 secret data. For open XlS files use any available tools such as EXCEL.',
      1 => 'card_manage_tips',
      2 => 'Exporter carte de recharge soutient exporter jusqu\'à 10000 données secrètes. Pour ouvrir les fichiers XLS utilisent tous les outils disponibles comme Excel.',//'导出充值卡密最多支持 10000 条数据，导出的 .xls 文件可用 EXCEL 等工具打开。', // 'Export recharge card supports export up to 10000 secret data. For open XlS files use any available tools such as EXCEL.',
    ),
  ),
  167 =>
  array (
    'index' =>
    array (
      'Si une catégorie du secret de la carte retirée, toutes les cartes sous son régime seront automatiquement inclus dans la "Catégorie par Défaut"' => 'action=card&operation=type',//'卡密分类删除时，其下所有卡片将自动纳入“默认分类”', //  'If a card secret category removed, all the cards under it will be automatically included in the "Default Category"' => 'action=card&operation=type',
    ),
    'text' =>
    array (
      0 => 'Si une catégorie du secret de la carte retirée, toutes les cartes sous son régime seront automatiquement inclus dans la "Catégorie par Défaut"',//'卡密分类删除时，其下所有卡片将自动纳入“默认分类”', //  'If a card secret category removed, all the cards under it will be automatically included in the "Default Category"',
      1 => 'card_type_tips',
      2 => 'Si une catégorie du secret de la carte retirée, toutes les cartes sous son régime seront automatiquement inclus dans la "Catégorie par Défaut"',//'卡密分类删除时，其下所有卡片将自动纳入“默认分类”', // 'If a card secret category removed, all the cards under it will be automatically included in the "Default Category"', 
    ),
  ),
  168 =>
  array (
    'index' =>
    array (
      'Lorsque le secret de la carte est généré des règles simples ou petit nombre de feuilles à générer peuvent provoquer numéro généré réel de feuilles sera inférieur au nombre de feuilles par défaut.' => 'admin.php?action=card&operation=make',//'生成卡密时，简单的规则或较小的生成张数可能导致实际生成张数小于预设生成张数。', //  'When the card secret is generated, simple rules or small number of sheets to generate may cause actual generated number of sheets will less than the default number of sheets.' => 'admin.php?action=card&operation=make',
    ),
    'text' =>
    array (
      0 => 'Lorsque le secret de la carte est généré des règles simples ou petit nombre de feuilles à générer peuvent provoquer numéro généré réel de feuilles sera inférieur au nombre de feuilles par défaut.',//'生成卡密时，简单的规则或较小的生成张数可能导致实际生成张数小于预设生成张数。', // 'When the card secret is generated, simple rules or small number of sheets to generate may cause actual generated number of sheets will less than the default number of sheets.',
      1 => 'card_make_tips',
      2 => 'Lorsque le secret de la carte est généré des règles simples ou petit nombre de feuilles à générer peuvent provoquer numéro généré réel de feuilles sera inférieur au nombre de feuilles par défaut.',//'生成卡密时，简单的规则或较小的生成张数可能导致实际生成张数小于预设生成张数。', // 'When the card secret is generated, simple rules or small number of sheets to generate may cause actual generated number of sheets will less than the default number of sheets.',
      3 => 'card_make_rule',
      4 => 'Règles de génération Carte secrètes',//'卡密生成规则', // 'Card secret generation rules',
      5 => '"@" représente tous les caractères aléatoires en anglais, "#" représente n\'importe quel nombre aléatoire, "*" représente n\'importe quel caractère anglais ou un chiffre. Exemples de règles secrètes Carte: «DZ2011 @ @ @ @ # # # # # *****". Note: Je règle est trop petit, il va provoquer le secret sécurité de la carte sera faible ou les secrets générés peut être répété. secrètes trop dur peut entraîner des cartes en double ou la génération échoué. Afin d\'éviter la duplication de la carte, ne pas utiliser de caractères spéciaux chinois ou autre dans les règles. Le mieux est d\'utiliser des chiffres aléatoires, mais pas moins de 8 chiffres.',//'"@"代表任意随机英文字符，"#"代表任意随机数字，"*"代表任意英文或数字卡密规则样本："DZ2011@@@@@#####*****"注意：规则位数过小会造成卡密生成重复概率增大，过多的重复卡密会造成卡密生成终止卡密规则中不能带有中文及其他特殊符号为了避免卡密重复，随机位数最好不要少于8位', //  '"@" represents any random characters in English, "#" Represents any random number, "*" stands for any English character or a digit. Card secret sample rules: "DZ2011@@@@#####*****". Note: I a rule is too small, it will cause the card secret security will be low or the secrets generated may be repeated. Too hard secret may result in duplicate cards or the generation failed. In order to avoid duplication of card, do not use any Chinese or other special characters in the Rules. The best is to use random digits, but not less than 8 digits.',
      6 => 'card_type',
      7 => 'Cartes de type secrètes',//'卡密分类', // 'Cards secret type',
      8 => 'card_make_num',
      9 => 'Générer le nombre de feuilles',//'生成张数',//'Generate the number of sheets',
      10 => 'Définissez le nombre de feuilles secrètes de cartes obtenu',//'设置本次生成的卡密张数', //  'Set the resulting number of card secret sheets',
      11 => 'card_make_extcredits',
      12 => 'Points quantité et le type',//'积分数额及类型', // 'Points amount and type',
      13 => 'Fixer le montant et le type de points pour générer les cartes de recharge secrète',//'设置本次生成充值卡密可充得的积分数额与类型',  //  'Set the amount and type of points to generate secret recharge card',
      14 => 'card_make_price',
      15 => 'Le prix de la carte actuelle',//'实际面值', // 'The actual card price',
      16 => 'Unité monétaire (USD), le prix réel imprimé sur la carte, utilisé pour informer l\'utilisateur sur la carte coût réel',//'单位(元)，实际面值常用在卡片印刷，用作告知用户面值以衡量卡密价值', //  'Currency unit (USD), the actual price printed on the card, used to inform the user about the card real cost',
      17 => 'card_make_cleardateline',
      18 => 'valable jusqu\'au',//'有效期至', // 'Valid until',
      19 => 'Par défaut, la carte est valide pour un an. Quand une heure valide de la carte est réglé de 24:00 a échoué, le statut de la carte changée en "expiré".',//'默认有效期为一年卡密会在设定时间的24:00失效，状态变更为“已过期”',// 'By default the card is valid for one year. When a card valid time is set of 24:00 failed, the card status changed to "expired".',
      20 => 'card_make_description',
      21 => 'Description',//'备注',  //  'Description',
      22 => 'Lorsque la description de la carte est entré, il apparaîtra dans la "construire Connexion"',//'为本次生成的卡密添加备注说明，备注将显示在“生成记录”中', // 'When the card description entered, it will appear in the "Build Log"',
    ),
  ),
  169 =>
  array (
    'index' =>
    array (
      'Gestion des notes' => 'action=click',//'表态管理', // 'Rating management' 
    ),
    'text' =>
    array (
      0 => 'Gestion des notes',//'表态管理', // 'Rating management',
      1 => 'click_edit_tips',
      2 => 'Cette fonction est utilisée pour régler le blog / images / articles de mesures tarifaires. S\'il vous plaît remplir le nom de la note et le nom du fichier image et les fichiers d\'image correspondant téléchargés sur le statique / image / click / répertoire. Attention: Le fonctionnement du site depuis quelques temps, moyen de changer l\'ordre des icônes aura une incidence sur les résultats de chaque position de type avant d\'ouvrir jusqu\'à huit actions.',//'本功能用于设置日志/图片/文章表态动作，动作图片中请填写图片文件名，并将相应图片文件上传到 static/image/click/ 目录中。警告：站点运营一段时间后，中途改变各表情的顺序将会影响之前的表态结果每个类型最多启用8个动作', //  'This function is used to set the blog/images/articles rate actions. Please fill out the rating name and image file name, and the corresponding image files uploaded to the static/image/click/directory. Warning: The site operation for some times, way to change the icon order will affect the results of each type position before opening up to eight actions.',
    ),
  ),
  170 =>
  array (
    'index' =>
    array (
      'Collections' => 'action=collection',//'淘帖管理' // 'Collections' 
    ),
    'text' =>
    array (
      0 => 'Collections',//'淘帖管理',//  'Collections',
      1 => 'collection_ctid',
      2 => 'ID de la Collection',//'淘专辑 ID',//  'Collection ID',
      3 => 'collection_comment_message',
      4 => 'Texte du Commentaire',//'评论内容',//  'Comment text',
      5 => 'collection_comment_cid',
      6 => 'ID Commentaire',//'评论 ID',
      7 => 'collection_comment_username',
      8 => 'Commenter Nom Utilisateur',//'评论人用户名',  //  'Comment poster name',
      9 => 'collection_comment_uid',
      10 => 'Commentaire poster UID',//'评论人 UID',  
      11 => 'collection_comment_rate',
      12 => 'La note est supérieure à',//'评分大于', //  'Rating is greater than',
      13 => 'collection_comment_useip',
      14 => 'Adresse IP',//'IP 地址', //  'IP address',
      15 => 'threads_search_time',
      16 => 'Plage de temps du Post',//'发表时间范围', // 'Post Time range',
      17 => 'Format est aaaa-mm-jj. Définir à 0 pour aucune restrictions',//'格式 yyyy-mm-dd，不限制请输入 0', //  'Format is yyyy-mm-dd. Set to 0 for no restrictions',
      18 => 'feed_search_perpage',
      19 => 'Sujets par page',//'每页显示数',  //  'Threads per page',
      20 => 'collection_name',
      21 => 'Nom de la Collection',//'淘专辑名称', // 'Collection name',
      22 => 'collection_ctid',
      23 => 'Collection ID',//'淘专辑 ID', //  'Collection ID',
      24 => 'collection_username',
      25 => 'Collection Auteur',//'淘专辑作者', //  'Collection author',
      26 => 'collection_uid',
      27 => 'Collection UID Auteur ',//'淘专辑作者 UID', // 'Collection author UID',
      28 => 'feed_search_perpage',
      29 => 'Articles par page',//'每页显示数', // 'Items per page',
      30 => 'collection_recommend_index_autonumber',
      31 => 'Accueil Nombre recommandée', //  '首页推荐数量', 
      32 => 'Mise en place le réglage effectué, recommander un nombre spécifié d\'album à la maison populaire, mis à "0" pour désactiver la fonction de recommandation. Lorsque vous Mise en place manuellement la quantité recommandée n\'atteint pas le nombre, le système proposera automatiquement de l\'album.', //  '设置后，会在首页推荐指定数量的热门专辑，设为“0”则关闭推荐功能。当手动推荐数量未达到设置的数量时，系统会自动推荐专辑。', 
    ),
  ),
  171 =>
  array (
    'index' =>
    array (
      'Gestion Commentaire' => 'action=comment',//'留言管理',  //  'Comment Management'
      'Commentaires/Réactions' => 'action=comment',//'评论/留言', //  'Comments/Feedback'
    ),
    'text' =>
    array (
      0 => 'Gestion Commentaire &raquo; Commentaires/Réactions',//'留言管理 &raquo; 评论/留言', //   'Comment Management &raquo; Comments/Feedback',
      1 => 'comment_tips',
      2 => 'Gestion des messages par lots est utilisée pour supprimer des messages. Attention: Lorsque vous cliquez sur le bouton Supprimer dans la page de résultats de recherche, l\'information sera effacé directement!',//'批量留言管理用于删除留言使用。提醒：点击搜索结果页的删除按钮，将会直接删除相关信息！', //  'Batch message management is used to delete messages. Warning: When you click the Delete button in the search results page, the information will deletet directly!',
      3 => 'comment_search_detail',
      4 => 'Afficher la liste des commentaires détaillés',//'显示详细留言列表', //  'Show detailed comment list',
      5 => 'comment_search_perpage',
      6 => 'Articles par page',//'每页显示数', //  'Items per page',
      7 => 'comment_idtype',
      8 => 'Type de Commentaire',//'评论类型', //   'Comment Type',
      9 => 'comment_search_id',
      10 => 'ID du Commentaire',//'留言 ID', //  
      11 => 'Séparez les différentes commentaire ID par une virgule ","',//'多留言 ID 中间请用半角逗号 "," 隔开', //  'Separate multiple comment ID by a comma ","',
      12 => 'comment_search_author',
      13 => 'Nom Auteur',//'评论者', //  'Author name',
      14 => 'Plusieurs auteurs séparés par une virgule &quot;,&quot;.',//'多评论者名中间请用半角逗号 "," 隔开', // 'Separate multiple authors by comma &quot;,&quot;.', 
      15 => 'comment_search_authorid',
      16 => 'Auteur ID',//'评论者 ID',
      17 => 'UID Séparez par une virgule ","',//'多评论者 ID 中间请用半角逗号 "," 隔开', //  'Separate multiple UID with a comma ","',
      18 => 'comment_search_uid',
      19 => 'UID Auteur Commentaire',//'被评论者 UID',
      20 => 'UID Séparez par une virgule ","',//'多 UID 中间请用半角逗号 "," 隔开', // 'Separate multiple UID with a comma ","', 
      21 => 'comment_search_message',
      22 => 'Commentaire texte',//'评论内容', //  'Comment text',
      23 => 'comment_search_ip',
      24 => 'IP Posté',//'发布IP', // 'Posted IP',  
      25 => 'Joker "*" peut être utilisé, par exemple "127.0. *. *" (Sans les guillemets), à utiliser avec prudence!',//'通配符 "*" 如 "127.0.*.*"(不含引号)，慎用!!!', //  'Wildcard "*" may be used, i.e. "127.0 .*.*" (without quotes), use with caution!!!',
      26 => 'comment_search_time',
      27 => 'Commentaire plage de temps',//'评论时间范围', // 'Comment time range', 
      28 => 'Format est aaaa-mm-jj',//'格式 yyyy-mm-dd', //   'Format is yyyy-mm-dd',
    ),
  ),
  172 =>
  array (
    'index' =>
    array (
      'Gestion de Commentaire' => 'action=comment',//'留言管理', //   'Comment Management' 
      'Article Commentaires' => 'action=comment&operation=article',//'文章评论'// 'Article Comments' 
      'Sujet commentaires' => 'action=comment&operation=topic',//'专题评论'//  'Topic comments'
    ),
    'text' =>
    array (
      0 => 'Gestion de Commentaire &raquo; Article Commentaires &raquo; Sujet commentaires',//       0 => 'Comment Management &raquo; 文章评论 &raquo; 专题评论',
      1 => 'comment_',
      2 => '',
      3 => 'comment_search_perpage',
      4 => 'Articles par page',//'每页显示数',  // 'Items per page',
      5 => 'comment_search_message',
      6 => 'Texte Commentaire',//'评论内容', // 'Comment text',
      7 => 'comment_search_author',
      8 => 'Nom Auteur',//'评论者', //  'Author name',
      9 => 'Plusieurs auteurs séparés par une virgule &quot;,&quot;.',//'多评论者名中间请用半角逗号 "," 隔开', //  'Separate multiple authors by comma &quot;,&quot;.',
      10 => 'comment_search_authorid',
      11 => 'Author ID',//'评论者 ID', //  'Author ID', 
      12 => 'UID Séparez par une virgule ","',//'多评论者 ID 中间请用半角逗号 "," 隔开', //  'Separate multiple UID with a comma ","',
      13 => 'comment_search_time',
      14 => 'Commentaire plage de temps',//'评论时间范围', //  'Comment time range',
      15 => 'Format est aaaa-mm-jj',//'格式 yyyy-mm-dd', // 'Format est yyyy-mm-dd',
    ),
  ),
  173 =>
  array (
    'index' =>
    array (
      'MAJ Statistiques' => 'action=counter',//'更新统计'  // 'Update Statistics'  
    ),
    'text' =>
    array (
      0 => 'MAJ Statistiques',//'更新统计',  //   'Update Statistics',
      1 => 'counter_tips',
      2 => 'Les données suivantes ne sont pas normales. Vous pouvez utiliser cette fonction pour mettre à jour les données pour corriger l\'Etat. S\'il vous plaît essayez d\'éviter l\'heure de pointe de membres l\'accès.
				Chaque cycle jour le nombre: Réglez la valeur dans une fourchette raisonnable. Ne doit pas être trop grand, sinon il augmenter la pression de la charge du serveur
				',
    ),
  ),
  174 =>
  array (
    'index' =>
    array (
      'Database' => 'action=db&operation=export',//'数据库', // 'Database' 
      'Sauvegarde' => 'action=db&operation=export',//'备份', // 'Backup'
    ),
    'text' =>
    array (
      0 => 'Sauvegarde &raquo; Database',//'数据库 &raquo; 备份', //  'Database &raquo; Backup',
      1 => 'db_export_type',
      2 => 'Type de Sauvegarde',//'数据备份类型', //  'Backup type',
      3 => 'db_export_method',
      4 => 'Méthode de Sauvegarde',//'数据备份方式', //  'Backup Method',
      5 => 'db_export_options',
      6 => 'Options de Sauvegarde',//'数据备份选项', // 'Backup Options',
      7 => 'db_export_options_extended_insert',
      8 => 'Utilisez le mode d\'insertion étendue',//'使用扩展插入(Extended Insert)方式', //  'Use Extended Insert mode',
      9 => 'db_export_options_sql_compatible',
      10 => 'Ajouter "Créer table" déclaration',//'建表语句格式', // 'Add "Create table" statement',
      11 => 'db_export_options_charset',
      12 => 'Définir Jeu de caractères',//'强制字符集', //  'Force character set',
      13 => 'db_export_usehex',
      14 => 'Hexadecimal format',//'十六进制方式', // 'Hexadecimal format',
      15 => 'db_export_usezip',
      16 => 'Compresser Fichiers de Sauvegarde',//'压缩备份文件', //'Compress Backup file', 
      17 => 'db_export_filename',
      18 => 'Nom du Fichier de la Sauvegarde',//'备份文件名', // 'Backup file name',
      19 => 'db_import_tips',
      20 => 'Lorsque cette fonction rétablit les données de sauvegarde, il va réécrire toutes les données curent dans une base de données. Avant la reprise, il est recommandé de fermer le forum et ouvrir le forum après l\'achèvement de la restauration. Pour restaurer les données de la Discuz! Version moins de X2, allez dans le répertoire d\'installation de l\'utilitaire de trouver le fichier restore.php, puis télécharger le fichier restore.php dans le dossier du programme dans le répertoire de données. Pour des raisons de sécurité, svp. merci de n\'oubliez pas de retirer le fichier restore.php après la récupération de données réussie. Vous pouvez consulter la sauvegarde des données à la sauvegarde détails du fichier de log, supprimer les sauvegardes obsolètes, et d\'importer la sauvegarde nécessaire.',//'本功能在恢复备份数据的同时，将全部覆盖原有数据，请确定恢复前已将论坛关闭，恢复全部完成后可以将论坛重新开放。恢复数据前请在Discuz! X2安装文件目录下utility文件夹内找到 restore.php 文件，然后将 restore.php 文件上传到程序文件夹data目录下。为了您站点的安全，成功恢复数据后请务必及时删除 restore.php 文件。您可以在数据备份记录处查看站点的备份文件的详细信息，删除过期的备份,并导入需要的备份。', // 'When this function restored the data from backup, it will REWRITE all the curent data in a database. Before the recovery it is recommended to shut down the forum, and open the forum after completion of the restore. For restore data of the Discuz! version less than X2, go to the utility installation directory to find the restore.php file, and then upload file restore.php to the program folder under the data directory. For security reason, please be sure to remove the restore.php file after the successful data recovery. You can view the data backup at the backup log file details, delete outdated backups, and import the necessary backup.',
      21 => 'db_import',
      22 => 'Récupération de données',//'数据恢复', // 'Data Recovery',
    ),
  ),
  175 =>
  array (
    'index' =>
    array (
      'Database' => 'action=db&operation=export',//'数据库',  //  'Database' 
      'MAJ' => 'action=db&operation=runquery',//'升级' //   'Upgrade' 
    ),
    'text' =>
    array (
      0 => 'Database &raquo; MAJ',//'数据库 &raquo; 升级', //  'Database &raquo; Upgrade',
      1 => 'db_runquery_tips',
      2 => 'Vous pouvez modifier le /source/admincp/admincp_quickquery.php d\'ajouter une fonctionnalité pour étendre ses opérations SQL communs. Pour des raisons de sécurité, le Discuz! Backoffice désactivée par défaut d\'exécution des instructions SQL directement, et vous ne pouvez utiliser les requêtes SQL communs. Si vous souhaitez mettre à jour pour écrire des instructions SQL personnalisés gratuits, vous devez modifier le config/config_global.php et régler le $_config[admincp][runquery] variable à 1.',//'你可以通过修改 /source/admincp/admincp_quickquery.php 来添加常用 SQL 操作进行功能扩充。出于安全考虑，Discuz! 后台默认情况下禁止 SQL 语句直接执行，只能使用常用 SQL 当中的内容，如果你想自己随意书写 SQL 升级语句，需要将 config/config_global.php 当中的 $_config[admincp][runquery] 设置修改为 1。', //  'You can modify the /source/admincp/admincp_quickquery.php to add a functionality to expand common SQL operations. For security reasons, the Discuz! Backoffice by default disabled SQL statements executing directly, and you can only use the common SQL queries. If you wish to upgrade to write free custom SQL statements, you need to edit the config/config_global.php and set the $_config[admincp][runquery] variable to 1.', 
      3 => 'db_runquery_simply',
      4 => 'Mise à niveau instruction SQL commune',//'常用 SQL 升级语句', //  'Upgrade common SQL statement',
      5 => 'db_runquery_sql',
      6 => 'Discuz! Mise à niveau de base de données - Veuillez coller dans la déclaration de mise à niveau de base de données suivant',//'Discuz! 数据库升级 - 请将数据库升级语句粘贴在下面', //  'Discuz! Database upgrade - Please paste in the following database upgrade statement',
    ),
  ),
  176 =>
  array (
    'index' =>
    array (
      'Database' => 'action=db&operation=export',//'数据库',
      'Optimisation' => 'action=db&operation=optimize',//'优化', //  'Optimization'
    ),
    'text' =>
    array (
      0 => 'Optimisation &raquo; Database',//'数据库 &raquo; 优化',
      1 => 'db_optimize_tips',
      2 => 'Optimisation Database peut supprimer la fragmentation des fichiers de données, les enregistrements serrées, et ainsi améliorer la lecture et l\'écriture en vitesse.',//'数据表优化可以去除数据文件中的碎片，使记录排列紧密，提高读写速度。', //   'Database optimization can remove the data file fragmentation, tightly packed records, and so improve reading and writing speed.',
    ),
  ),
  177 =>
  array (
    'index' =>
    array (
      'Les paramètres régionaux' => 'action=district',//'地区设置',  //  'Region settings'
    ),
    'text' =>
    array (
      0 => 'Les paramètres régionaux',//'地区设置', //  'Region settings',
      1 => 'district_tips',
      2 => 'Vous pouvez modifier ar ajouter les données régionales. Modifier ou supprimer le besoin d\'opération pour cliquer sur le bouton "Soumettre ou Envoyer" pour prendre effet.',//'你可以自己编辑地区数据添加，编辑或删除操作后需要点击“提交”按钮才生效',  //   'You can edit ar add the regional data. Edit or delete operation need to click "submit" button for take effect.',
    ),
  ),
  178 =>
  array (
    'index' =>
    array (
      'Gestion Actes' => 'action=doing',//'记录管理'  //  'Doings Management'
    ),
    'text' =>
    array (
      0 => 'Gestion Actes',//'记录管理', //   'Doings Management',
      1 => 'doing_tips',
      2 => 'Gestion Batch  son action qui sert à supprimer faits et gestes OU Actes ainsi que les réponses correspondantes. Qualificatif de mots clés {x} peut être utilisé au texte ignoré entre deux caractères adjacents, voici "x" est un nombre de caractères ignorés. À savoir "a {1} s {2} s" (sans les guillemets) peut rechercher "ass" et "axsxs" et "axsxxs" et ainsi de suite. Attention: Cliquez sur le bouton Supprimer dans les résultats de recherche va supprimer les informations directement!',//'批量记录信息管理用于删除记录使用，并且连同对应的回复一同删除。关键字可以使用限定符 {x} 以限定相邻两字符间可忽略的文字，x 是忽略字符的个数，如 "a{1}s{2}s"(不含引号) 可以搜索到 "ass" 也可搜索到 "axsxs" 和 "axsxxs" 等等。提醒：点击搜索结果页的删除按钮，将会直接删除相关信息！', //   'Batch doing management is used to delete doings along with the corresponding replies. Keyword qualifier {x} can be used to ignored text between two adjacent characters, here "x" is a number of ignored characters. I.e. "a{1}s{2}s" (without the quotes) can search for "ass" and "axsxs" and "axsxxs" and so on. Warning: Click on the Delete button in the search results will delete the information directly!',
    ),
  ),
  179 =>
  array (
    'index' =>
    array (
      'Gestions des Actes' => 'action=doing',//'记录管理', //  Doings Management'
      'Recherche' => 'action=doing&search=true',//'搜索', //  'Search'
    ),
    'text' =>
    array (
      0 => 'Gestions des Actes  &raquo; Recherche',//'记录管理 &raquo; 搜索', //  'Doings Management &raquo; Search',
      1 => 'doing_search_detail',
      2 => 'Voir acte liste détaillée',//'显示详细记录列表', //  'Show detailed doing list',
      3 => 'doing_search_perpage',
      4 => 'Articles par page',//'每页显示数', //  'Items per page',
      5 => 'doing_search_user',
      6 => 'Acte nom de l\'auteur',//'发表记录用户名', //  'Doing author name',
      7 => 'Plusieurs noms d\'utilisateurs séparés par une virgule ","',//'多用户名中间请用半角逗号 "," 隔开', // 'Separate multiple user names by a comma ","',
      8 => 'doing_search_ip',
      9 => 'Acte Publié IP',//'发表记录 IP ',  // 'Published doing IP ', 
      10 => 'Joker "*" peut être utilisé, par exemple "127.0. *. *" (Sans les guillemets), à utiliser avec prudence!',//'通配符 "*" 如 "127.0.*.*"(不含引号)，慎用!!!', // 'Wildcard "*" may be used, i.e. "127.0 .*.*" (without quotes), use with caution!!!',
      11 => 'doing_search_keyword',
      12 => 'Mots-clés',//'内容关键字',  //   'Keywords',
      13 => 'Plusieurs mots-clés séparés par une virgule ",", les mots clés qualificatif {x} peuvent être utilisés.',//'多关键字中间请用半角逗号 "," 隔开，关键词可以用限定符 {x}', //  'Separate multiple keywords with a comma ",", keywords qualifier {x} can be used.',
      14 => 'doing_search_lengthlimit',
      15 => 'La longueur du contenu minimal',//'内容最小长度', // 'Minimal content length',
      16 => 'Cette fonctionnalité va ajouter la charge du serveur',//'本功能会加重服务器负担', // 'This feature will add the server loading', 
      17 => 'doing_search_time',
      18 => 'Post Plage de temps',//'发表时间范围', // 'Post Time range',
      19 => 'Format est aaaa-mm-jj',//'格式 yyyy-mm-dd', // 'Format is yyyy-mm-dd',
    ),
  ),
  180 =>
  array (
    'index' =>
    array (
      'Paramètres du domaine' => 'action=domain',//'域名设置',  //  'Domain settings' 
      'Domaine d\'application' => 'domain&operation=app',//'应用域名', //  'Application Domain'
    ),
    'text' =>
    array (
      0 => 'Paramètres du domaine &raquo; Domaine d\'application',//'域名设置 &raquo; 应用域名', //  'Domain settings &raquo; Application Domain',
      1 => 'setting_domain_app_tips',
      2 => 'Domaine de liaison respectif ne peut pas être la même, Domaine pas besoin d\'ajouter "http:// au début et "/" à la fin, par exemple: portal.comsenz.com peut être ouvert comme un nom de domaine, le besoin de configurer le nom de domaine par défaut, sinon il va créer plus de problèmes lors de l\'ouverture de l\'entrée lors du multi-domaine . S\'il vous plaît Modifiez la valeur CookieDomain dans config / config_global.php de fixer le réglage à la portée des cookies.',//'各自绑定的域名不能相同，域名不需要添加“http://”，也不要以“/”结尾，例如：portal.comsenz.com任意开启一项域名，需要配置默认域名，否则会造成多入口问题当开启多域名时，请在 config/config_global.php 中修改 cookiedomain 值来设置 cookie 作用域',  //   'Respective binding domain can not be the same, Domain not need to add "http:// at the beginning and "/" at the end, For example: portal.comsenz.com can be opened as a domain name, Need to configure the default domain name, Otherwise it will create more problems when opening the entrance when the multi-domain. Please Modify the cookiedomain value in config/config_global.php to set the cookie scope.',
    ),
  ),
  181 =>
  array (
    'index' =>
    array (
      'Paramètres du domaine' => 'action=domain',//'域名设置',  //   'Domain settings' => 'action=domain',
      'Définissez le nom de domaine racine' => 'domain&operation=root',//'根域名设置', //  'Set the root domain name' => 'domain&operation=root',
    ),
    'text' =>
    array (
      0 => 'Paramètres du domaine &raquo; Définissez le nom de domaine racine',//'域名设置 &raquo; 根域名设置', //  'Domain settings &raquo; Set the root domain name',
      1 => 'setting_domain_root_tips',
      2 => 'Pour espace personnel, groupe, forum, le sujet, le portail peut être défini un sous-domaine du nom de domaine racine. Exemple: pour l\'application utilisateur à XXX.comsenz.com, remplissez le domaine racine: comsenz.com pour l\'espace personnel, groupes Après avoir réglé les deux domaines, la racine après, doivent également ouvrir les paramètres de base dans le commutateur de sous-domaine correspondant, utilisateurs peut appliquer l\'endroit approprié pour se lier à un sous-domaine nom, aussi limitée par la longueur de la catégorie des groupes d\'utilisateurs permettent nom-domaine si elle est liée à la catégorie, besoin de configurer leur propre dans l\'opération de liaison de l\'environnement liée à la manière différente n\'est pas recommandé pour changer le domaine racine au même domaine racine',//'可以为个人空间、群组、版块、专题、频道设置一个二级域名的根域名。例：用户申请XXX.comsenz.com,根域名填：comsenz.com个人空间、群组设置完二级域名的根后，还需要在基本设置中开启相应的二级域名开关，用户才能在相应的地方申请绑定二级域名，同时受限于用户组的二级域名长度频道启用二级域名如果是绑定目录的，需要自已在环境中配置相关的绑定操作不建议中途将不同的根域变更为相同的根域', //  'For personal space, Group, forum, topic, portal can be set a subdomain from the root domain name. Example: for  the user application at XXX.comsenz.com, Fill the root domain: comsenz.com for personal space, groups After setting the two domain, the root after, Also need to open the basic settings in the corresponding subdomain switch, Users can apply the appropriate place to bind to subdomain name, Also limited by the length of the user group category enable subdomain name if it is bound to the category, Need to configure their own in the environment bind operation related to the different way is not recommended to change the root domain to the same root domain',
    ),
  ),
  182 =>
  array (
    'index' =>
    array (
      'Paramètres du domaine' => 'action=domain',//'域名设置',  // 'Domain settings'
      'Réglages de base' => 'domain&operation=base',//'基本设置', //  'Basic settings'
    ),
    'text' =>
    array (
      0 => 'Paramètres du domaine &raquo; Réglages de base',//'域名设置 &raquo; 基本设置', //  'Domain settings &raquo; Base settings',
      1 => 'setting_domain_base_tips',
      2 => 'Après le nom de sous-domaine est réservé, on ne peut utiliser ce nom sous-domaine d\'application ou l\'espace personnel. Après que le groupe réglé sur un sous-domaine nom Nécessité de définir les paramètres de domaine racine de groupe, maison du domaine racine',//'保留二级域名设置后所有人都无法申请或使用该域名个人空间、群组的二级域名开启后需要到根域名设置中设置群组、家园的根域名', //  'After the subdomain name is reserved, no one can use this subdomain name for application or personal space. After the group set to subdomain name Need to set the group root domain settings, Home of the root domain',
      3 => 'setting_domain_allow_space',
      4 => 'Prévoyez suffisamment d\'espace sur le nom sous-domaine',//'开启个人空间二级域名',  //'Allow Space on subdomain name', 
      5 => 'Si est activé, vous devez définir le nom de domaine racine, sous-domaine nom et groupe d\'utilisateurs d\'ouvrir seulement après la longueur de demandes de noms de domaine',//'开启后需在设置根域名、以及用户组域名长度后方能开启域名申请', //   'If is turned on, you need to set the root domain name, subdomain name and user group open only after the length of domain name applications',
      6 => 'setting_domain_allow_group',
      7 => 'Activer le groupe dans les noms de sous-domaines',//'开启群组二级域名',  // 'Enable group in subdomain names',
      8 => 'Si est activé, vous devez définir le nom de domaine racine, sous-domaine nom et groupe d\'utilisateurs d\'ouvrir seulement après la longueur de demandes de noms de domaine',//'开启后需在设置根域名、以及用户组域名长度后方能开启域名申请',  //   'If is turned on, youneed to set the root domain name, subdomain name and user group open only after the length of domain name applications',
      9 => 'setting_domain_hold_domain',
      10 => 'Les noms de sous-domaines réservés',//'保留二级域名', // 'Reserved subdomain names',
      11 => 'Séparez plusieurs sous-domaines par "|", vous pouvez utiliser le joker "*".',//'多个之间用 | 隔开，可以使用通配符* ', // 'Separate multiple subdomains by "|", you can use the wildcard "*".',
    ),
  ),
  183 =>
  array (
    'index' =>
    array (
      'Paramètres E-Commerce' => 'action=setting&operation=ec',//'电子商务', // 'E-Commerce Settings'
      'Alipay' => 'action=ec&operation=alipay',//'支付宝', // 'Alipay'
    ),
    'text' =>
    array (
      0 => 'E-Commerce &raquo; Alipay',//'电子商务 &raquo; 支付宝', // 'E-Commerce &raquo; Alipay',
      1 => 'ec_alipay_tips',
      2 => '"Alipay" (http://www.alipay.com) la Chine est importante plate-forme de paiement en ligne, la meilleure création mondiale d\'Alibaba entreprises B2B pour Discuz! Offrir des crédits d\'achat et autres forums B2C, C2C plateforme de trading. Vous avez seulement besoin d\'une configuration simple, vous pouvez rendre le contenu du forum et de la popularité, en plus de revenus publicitaires vraiment devenir une source importante de profits à l\'extérieur, afin de réaliser forum opération de grande envergure. Comme il s\'agit d\'opérations de trésorerie, afin d\'éviter un mauvais fonctionnement causé des pertes financières, veuillez utiliser les points d\'Alipay, au début de la fonction de transaction (ne comprend pas Alipay fonction du bouton), assurez-vous de lire attentivement le «manuel d\'utilisation» dans la partie E-commerce de fois pleinement comprise et acceptée par les processus de reconnaissance correspondantes, puis utiliser les paramètres pertinents. Vous pouvez mettre en place pour permettre aux utilisateurs de payer au moyen d\'argent en ligne, vous ressourcer compte des points de transaction pour l\'achat de contenu de poste, la permission de groupe d\'utilisateurs à l\'achat, les points de transfert ou groupes d\'utilisateurs de mettre à niveau les capacités. Les points Alipay capacités de transaction, doivent «définir des points» dans l\'intégration de l\'ouverture du commerce, et définit la stratégie intégrale correspondant à répondre aux besoins des différentes occasions. Assurez-vous de configurer votre compte Alipay collection, sinon paiement se traduira par l\'utilisation intégrale en temps réel ne peut pas être crédité sur le compte, ce qui entraîne un grand nombre de traitement manuel de l\'information requise de l\'ordre. En Discuz de plus! Site officiel ou un avis officiel en dehors du forum, Discuz! Fournir Alipay frais de service de 1,5% par frais de transaction. S\'il vous plaît noter les dernières préoccupations liés aux entreprises, des politiques ou des changements de procédé, des ajustements à Discuz! Site Web du Forum officiel ou les informations prévaudront. Vous utilisez un service Alipay est construit sur une base entièrement volontaire, dans Discuz de plus! Malveillants en raison des facteurs subjectifs autres que des pertes en capital, Beijing Sing-ups Technology Co., Ltd ne pas utiliser cette fonctionnalité pour toute perte découlant de la responsabilité. Email conseil aux entreprises Alipay pour le 6688@taobao.com; Alipay numéro de téléphone du service à la clientèle est +86-0571-88156688.',//'“支付宝”(http://www.alipay.com)是中国领先的网上支付平台，由全球最佳 B2B 公司阿里巴巴公司创建，为 Discuz! 用户提供积分购买及论坛 B2C、C2C 交易平台。你只需进行简单的设置，即可使论坛内容和人气，真成为除广告收入外的重要利润来源，从而实现论坛的规模化经营。由于涉及现金交易，为避免因操作不当而造成的资金损失，请在开始使用支付宝积分交易功能(不包含支付宝按钮功能)前，务必仔细阅读《用户使用说明书》中有关电子商务的部分，当确认完全理解和接受相关流程及使用方法后再进行相关设置。你可以设置允许用户通过现金在线支付的方式，为其交易积分账户充值，用于购买帖子内容、购买用户组权限、积分转账或用户组升级等功能。支付宝积分交易功能，需在“积分设置”中启用交易积分，并同时设置相应的积分策略以满足不同场合的需要。请务必正确设置你的收款支付宝账号，否则将造成用户付款后积分无法实时到账，造成大量需要人工处理的订单信息。除 Discuz! 官方网站或官方论坛另行通知以外，Discuz! 提供的支付宝支付服务每笔交易收取 1.5% 的手续费。请及时关注相关业务的最新通知，各项政策或流程的变更、调整，以 Discuz! 官方网站或官方论坛提供的信息为准。你使用支付宝服务是建立在完全自愿的基础上，除 Discuz! 因主观恶意的因素造成的资金损失以外，北京康盛新创科技有限责任公司不对因使用此功能造成的任何损失承担责任。支付宝业务咨询 Email 为 6688@taobao.com；支付宝客户服务电话为 +86-0571-88156688。', // '"Alipay" (http://www.alipay.com) is China leading online payment platform, the world best creation of Alibaba B2B companies for Discuz! Offer credits to purchase and forums B2C, C2C trading platform. You only need a simple setup, you can make the forum content and popularity, in addition to advertising revenue really become an important source of profits outside, in order to achieve large-scale operation forum. As it involves cash transactions, in order to avoid improper operation caused financial losses, please use Alipay points at the beginning of the transaction function (does not include Alipay button function), be sure to carefully read the "user manual" in the e-commerce part of when fully understood and accepted by the relevant recognition processes and then use the relevant settings. You can set up to allow users to pay by way of cash online, recharge their account transaction points for the purchase of post content, the user group permission to purchase, transfer points or groups of users to upgrade capabilities. Alipay points transaction capabilities, need to "set points" in the opening trade integration, and sets the corresponding integral strategy to meet the needs of different occasions. Be sure to set your collection Alipay account, otherwise payment will result in the user real-time integration can not be credited into account, resulting in a large number of required manual processing of order information. In addition Discuz! Official website or official notice outside the forum, Discuz! Provide Alipay 1.5% service charge per transaction fee. Please notice the latest business-related concerns, policies, or process changes, adjustments to Discuz! Official Forum website or the information shall prevail. You use Alipay service is built on a completely voluntary basis, in addition Discuz! Malicious due to the subjective factors other than capital losses, Beijing Sing-ups Technology Co., Ltd. does not use this feature for any loss resulting from liability. Email Alipay business consulting for the 6688@taobao.com; Alipay customer service telephone number is +86-0571-88156688.', 
      3 => 'ec_alipay',
      4 => 'Paramètres E-Commerce Alipay',//'支付宝设置', // 'Alipay settings',
      5 => 'ec_alipay_account',
      6 => 'Alipay compte débiteurs',//'收款支付宝账号', //  'Alipay account receivable',
      7 => 'Si vous ouvrez une conversion ou des capacités de transaction, s\'il vous plaît remplir un compte Alipay réelle et effective pour l\'utilisateur de recevoir des paiements en espèces liés à échanger des transactions en points. Comme le numéro de compte n\'est pas valide ou incorrect code de sécurité, se traduira par l\'utilisateur n\'est pas correctement rémunérés pour leurs comptes recharger automatiquement des points ou des points de son compte de négociation normale rechargent automatiquement, ou effectuer des opérations normales. Si vous n\'avez pas Alipay compte, s\'il vous plaît cliquez ici pour vous inscrire',//'如果开启兑换或交易功能，请填写真实有效的支付宝账号，用于收取用户以现金兑换交易积分的相关款项。如账号无效或安全码有误，将导致用户支付后无法正确对其积分账户自动充值，或进行正常的交易对其积分账户自动充值，或进行正常的交易。如你没有支付宝帐号，请点击这里注册',  //   'If you open a conversion or transaction capabilities, please fill out a real and effective Alipay account for the user to receive cash payments related to exchange transactions in points. Such as account number is invalid or incorrect security code, will result in the user not properly paid for their accounts automatically recharge points, or points of its normal trading account automatically recharge, or carry out normal transactions. If you do not Alipay account, please click here to register',
      8 => 'ec_alipay_check',
      9 => 'Test de paiement',//'支付测试',  //   'Payment test',
      10 => 'Ce test permettra de simuler 0,01 yuans afin soumis à l\'essai, si le paiement a lieu après la présentation d\'un succès, l\'interface montre le fonctionnement de votre Alipay site correctement',//'本测试将模拟提交 1 元人民币的订单进行测试，如果提交后成功出现付款界面，说明你站点的支付宝功能可以正常使用', // 'This test will simulate 0.01 yuan order submitted for testing, if the payment occurs after the submission of a successful, interface shows your site Alipay function properly',
      11 => 'ec_contract',
      12 => 'Les paramètres d\'abonnés Alipay',//'支付宝签约用户设置', //'Alipay subscriber settings', 
      13 => 'ec_alipay_partner',
      14 => 'Partner id (PID)',//'合作者身份 (PID)', //  'Partner id (PID)',
      15 => 'Alipay abonné Remplissez ici votre partenaire Alipay attribué à l\'identité de la redevance d\'abonnement que vous avez payé et le trésor de l\'accord de signature officielle prévaudra. Si vous n\'avez pas encore signé, s\'il vous plaît cliquez ici pour vous; au moment de signer le contrat si le modèle dans le conflit, veuillez consulter 0571-88158090',//'支付宝签约用户请在此处填写支付宝分配给您的合作者身份，签约用户的手续费按照您与支付宝官方的签约协议为准。如果您还未签约，请点击这里签约；如果已签约,请点击这里获取PID、Key;如果在签约时出现合同模板冲突，请咨询0571-88158090', //  'Alipay Subscriber Please fill in here your Alipay partner assigned to the identity of the subscriber fee paid by you and treasure the official signing agreement shall prevail. If you have not yet signed, please click here to sign; when signing the contract if the template in the conflict, please consult 0571-88158090', 
      16 => 'ec_alipay_securitycode',
      17 => 'Sécurité des transactions code de vérification (Clé)',//'交易安全校验码 (key)', // 'Transaction security check code (key)',
      18 => 'Alipay Les utilisateurs peuvent s\'inscrire ici attribué à remplir votre sécurité Alipay code de vérification de la transaction, ce code vous pouvez vérifier à payer au trésor dans les services officiels d\'affaires à cocher',//'支付宝签约用户可以在此处填写支付宝分配给你的交易安全校验码，此校验码你可以到支付宝官方的商家服务功能处查看', // 'Alipay Users can sign here assigned to completing your transaction security Alipay check code, this code you can check to pay for treasure at the official check business services', 
      19 => 'ec_alipay_creditdirectpay',
      20 => 'Avec une interface de paiement direct en temps réel',//'使用纯即时到帐接口', //  'With a direct real-time payment interfaces',
      21 => 'Si vous signez le contrat contient instant pur arriver interface (pas la double interface standard) peut choisir ce, alors que l\'intégration ne peut recharger instantanée de paiement de crédit',//'如果你的签约协议中包含纯即时到帐接口（不是标准双接口）可以选择此项，让积分充值只能使用即时到账方式付款',  //   'If you sign the agreement contains pure instant arrive interface (not the standard dual-interface) can choose this, so that integration can only recharge Instant Credit Payment',
    ),
  ),
  184 =>
  array (
    'index' =>
    array (
      'Paramètres E-Commerce' => 'action=setting&operation=ec',//'电子商务',  //  'E-Commerce Settings'
      'Tenpay' => 'action=ec&operation=tenpay',//'财付通'  //  'Tenpay'
    ),
    'text' =>
    array (
      0 => 'E-Commerce &raquo; TenPay',//'电子商务 &raquo; 财付通', //   'E-Commerce &raquo; TenPay',
      1 => 'ec_tenpay_tips',
      2 => '"TenPay" (http://www.tenpay.com) is the founder of leading in China Tencent online payment platform, Discuz! Offer credits to purchase trading platform. To enable integration recharge Prompt arrival, please use the TenPay Instant Credit Enterprise Edition and open trade; otherwise, only need to open an "intermediary secured transactions" button. As it involves cash transactions, in order to avoid improper operation caused financial losses, please use the start transaction function TenPay points (does not include money paid through the button function), be sure to read the "user manual" in the e-commerce part, when fully understood and accepted by the relevant recognition processes and then use the relevant settings. You can set up to allow users to pay by way of cash online, recharge their account transaction points for the purchase of post content, the user group permission to purchase, transfer points or groups of users to upgrade capabilities. TenPay integral transaction capabilities, need to "set points" in the opening trade integration, and sets the corresponding integral strategy to meet the needs of different occasions. Be sure to set your collection TenPay account, otherwise payment will result in the user real-time integration can not be credited into account, resulting in a large number of required manual processing of order information. In addition Discuz! Official website or official notice outside the forum, Discuz! Provided TenPay payment service charge of 1% per transaction fee. Please notice the latest business-related concerns, policies, or process changes, adjustments to Discuz! Official Forum website or the information shall prevail. TenPay service you use is based entirely on a voluntary basis, in addition Discuz! Because of subjective bad faith of the financial losses caused by factors outside the Beijing Sing-ups Technology Co., Ltd. does not use this feature for any loss resulting from liability. TenPay Service Hotline :0755 -83762288-2 (24 hour service hotline)',//'“财付通”(http://www.tenpay.com)是腾讯公司创办的中国领先的在线支付平台，为 Discuz! 用户提供积分购买交易平台。如需启用积分充值即时到账，请使用财付通企业版并开通即时到账交易；否则，只需要开通“中介担保交易”即可。 //   '"TenPay" (http://www.tenpay.com) is the founder of leading in China Tencent online payment platform, Discuz! Offer credits to purchase trading platform. To enable integration recharge Prompt arrival, please use the TenPay Instant Credit Enterprise Edition and open trade; otherwise, only need to open an "intermediary secured transactions" button. As it involves cash transactions, in order to avoid improper operation caused financial losses, please use the start transaction function TenPay points (does not include money paid through the button function), be sure to read the "user manual" in the e-commerce part, when fully understood and accepted by the relevant recognition processes and then use the relevant settings. You can set up to allow users to pay by way of cash online, recharge their account transaction points for the purchase of post content, the user group permission to purchase, transfer points or groups of users to upgrade capabilities. TenPay integral transaction capabilities, need to "set points" in the opening trade integration, and sets the corresponding integral strategy to meet the needs of different occasions. Be sure to set your collection TenPay account, otherwise payment will result in the user real-time integration can not be credited into account, resulting in a large number of required manual processing of order information. In addition Discuz! Official website or official notice outside the forum, Discuz! Provided TenPay payment service charge of 1% per transaction fee. Please notice the latest business-related concerns, policies, or process changes, adjustments to Discuz! Official Forum website or the information shall prevail. TenPay service you use is based entirely on a voluntary basis, in addition Discuz! Because of subjective bad faith of the financial losses caused by factors outside the Beijing Sing-ups Technology Co., Ltd. does not use this feature for any loss resulting from liability. TenPay Service Hotline :0755 -83762288-2 (24 hour service hotline)',
//由于涉及现金交易，为避免因操作不当而造成的资金损失，请在开始使用财付通积分交易功能(不包含财付通按钮功能)前，务必仔细阅读《用户使用说明书》中有关电子商务的部分，当确认完全理解和接受相关流程及使用方法后再进行相关设置。你可以设置允许用户通过现金在线支付的方式，为其交易积分账户充值，用于购买帖子内容、购买用户组权限、积分转账或用户组升级等功能。财付通积分交易功能，需在“积分设置”中启用交易积分，并同时设置相应的积分策略以满足不同场合的需要。请务必正确设置你的收款财付通账号，否则将造成用户付款后积分无法实时到账，造成大量需要人工处理的订单信息。除 Discuz! 官方网站或官方论坛另行通知以外，Discuz! 提供的财付通支付服务每笔交易收取 1% 的手续费。请及时关注相关业务的最新通知，各项政策或流程的变更、调整，以 Discuz! 官方网站或官方论坛提供的信息为准。你使用财付通服务是建立在完全自愿的基础上，除 Discuz! 因主观恶意的因素造成的资金损失以外，北京康盛新创科技有限责任公司不对因使用此功能造成的任何损失承担责任。财付通服务热线：0755-83762288-2(全天24小时服务热线)', //  
      3 => 'ec_tenpay_opentrans',
      4 => 'Les paramètres de transactions sécurisées',//'担保交易设置', //   'Secured transactions settings',
      5 => 'ec_tenpay_opentrans_chnid',
      6 => 'Transactions sécurisées Compte débiteurs',//'担保交易收款账号', //  'Secured transactions receivable Account',
      7 => 'A ouvert un compte TenPay des droits de transaction sécurisé, si vous n\'avez pas l\'autorisation d\'ouvrir des transactions sécurisées, s\'il vous plaît cliquez ici pour vous inscrire',//'开通了担保交易权限的财付通账号，如你没有开通担保交易权限，请点击这里申请', // 'Opened a secured transaction rights TenPay account, if you do not have permission to open secured transactions, please click here to apply',  
      8 => 'ec_tenpay_opentrans_key',
      9 => 'Clé de compte de transaction sécurisée',//'担保交易账号密钥', //  'Secured transaction account key',
      10 => 'ec_tenpay',
      11 => 'Paramètres Invite',//'即时到账设置',  //   'Prompt arrival settings',
      12 => 'ec_tenpay_direct',
      13 => 'Des points est activé recharger crédit instantané',//'是否启用积分充值即时到账', //  'Is enabled points recharge Instant Credit',
      14 => 'ec_tenpay_bargainor',
      15 => 'TenPay instantanée crédit Marchand No.',//'财付通即时到账商户号', //  'TenPay Instant Credit Merchant No.',
      16 => 'Si vous ouvrez la fonctionnalité d\'échange, s\'il vous plaît remplir un compte réel et efficace TenPay Enterprise pour l\'utilisateur de recevoir des paiements en espèces liés à échanger des transactions en points. Comme le numéro de compte n\'est pas valide ou la mauvaise touche se traduira par l\'utilisateur n\'est pas correctement rémunérés pour leurs points de recharge de compte automatiquement. Si vous n\'avez pas TenPay compte, s\'il vous plaît cliquez ici pour vous inscrire',//'如果开启兑换功能，请填写真实有效的财付通企业版账号，用于收取用户以现金兑换交易积分的相关款项。如账号无效或密钥有误，将导致用户支付后无法正确对其积分账户自动充值。如你没有财付通帐号，请点击这里注册', //  'If you open the Exchange functionality, please fill out a real and effective TenPay Enterprise account for the user to receive cash payments related to exchange transactions in points. Such as account number is invalid or the wrong key will result in the user not properly paid for their points account automatically recharge. If you do not TenPay account, please click here to register',
      17 => 'ec_tenpay_key',
      18 => 'Clé de crédit instantané',//'即时到账密钥', //  'Instant Credit Key',
      19 => 'Le chiffre correspondant à l\'entreprise, d\'une longueur de 32 lettres majuscules et minuscules, des chiffres, peut se remettre en TenPay',//'与商户号对应的密钥，由长度为32的大小写字母、数字组成，可在财付通后台获取', //  'The corresponding number key with the business, from a length of 32 uppercase and lowercase letters, numbers, can get back in TenPay',
      20 => 'ec_tenpay_check',
//vot      21 => 'Payment test',//'支付测试',
//vot      22 => 'This test will simulate one yuan of orders submitted for testing, if the payment occurs after the submission of a successful interface shows your site TenPay function properly',//'本测试将模拟提交 1 元人民币的订单进行测试，如果提交后成功出现付款界面，说明你站点的财付通功能可以正常使用',
    ),
  ),
  185 =>
  array (
    'index' =>
    array (
      'Paramètres E-Commerce' => 'action=setting&operation=ec',//'电子商务', // 'E-Commerce Settings'
      'Points ordres de recharge' => 'action=ec&operation=orders',//'积分充值订单',  // 'Points recharge orders'
    ),
    'text' =>
    array (
      0 => 'E-Commerce &raquo; Points ordres de recharge',//'电子商务 &raquo; 积分充值订单', //  'E-Commerce &raquo; Points recharge orders',
      1 => 'ec_orders_tips',
      2 => 'Veuillez commencer à utiliser la fonctionnalité de gestion des ordres de transaction Alipay intégrale (non incluses fonction du bouton Alipay), assurez-vous de lire le "manuel d\'utilisation" dans le cadre de e-commerce, lorsqu\'il sera pleinement comprise et acceptée par les processus de comptabilisation, puis d\'utiliser les opérations connexes . Si vos utilisateurs ne sont pas automatiquement répercutées dans leurs comptes de paiement des points de recharge en ligne, probablement à cause de votre compte PayPal pour payer l\'interface de notification est mal réglé, ou votre site Web ne peut pas être remarqué Alipay raison d\'un accès normal du système. Vous pouvez vous connecter interface de gestion de transaction Alipay, en comparant les détails des transactions liées à confirmer manuellement la commande. Renseignements pour la commande de 60 jours, de sorte que vous pouvez consulter et gérer les ordres dans les 60 jours de l\'information, des informations sur plus de 60 jours sera automatiquement supprimé.',//'请在开始使用支付宝积分交易订单管理功能(不包含支付宝按钮功能)前，务必仔细阅读《用户使用说明书》中有关电子商务的部分，当确认完全理解和接受相关流程及使用方法后再进行相关操作。如果你的用户反映在线支付后无法自动为其积分账户充值，可能是由于你的支付宝账户的通知接口设置有误，或你的网站无法被支付宝通知系统正常访问所致。你可以登录支付宝交易管理界面，通过比对交易详情人工确认相关订单。订单信息保留 60 天，因此你只能查询和管理 60 天之内的订单信息，超过 60 天的信息将被自动删除。', //   'Please start using Alipay integral transaction order management functionality (not included Alipay button function), be sure to read the "user manual" in the part on e-commerce, when fully understood and accepted by the relevant recognition processes and then to use related operations. If your users are not automatically reflected in their online payment accounts recharge points, probably due to your PayPal account to pay the notification interface is set incorrectly, or your website can not be Alipay notice due to normal system access. You can log Alipay transaction management interface, by comparing the details of the transactions related to manually confirm order. Order Information for 60 days, so you can check and manage the orders within 60 days of information, information on more than 60 days will be automatically deleted.',
      3 => 'ec_orders_search',
      4 => 'Points rechargement recherche de commande',//'积分充值订单搜索', // 'Points recharge order search', 
      5 => 'ec_orders_search_status',
      6 => 'Suivi de commande',//'订单状态', // 'Order Status',
      7 => 'ec_orders_search_id',
      8 => 'No. de commande.',//'订单号', //   'order No.',
      9 => 'ec_orders_search_users',
      10 => 'Identifiant de paiement (plusieurs noms d\'utilisateur separete par une virgule ",")',//'付款用户名(多个用户名间请用半角逗号 "," 隔开)', //  'Payment username (separete multiple user names with a comma ",")',
      11 => 'ec_orders_search_buyer',
      12 => 'Paiement Alipay compte utilisateur',//'付款用户的支付宝账号', //  'Payment User Alipay account',
      13 => 'ec_orders_search_admin',
      14 => 'Remplir manuellement un administrateur',//'人工补单管理员', //  'Manually fill one administrator',
      15 => 'ec_orders_search_submit_date',
      16 => 'Ordre plage de temps de dépôt (aaaa-mm-jj)',//'订单提交时间范围(yyyy-mm-dd)', //  'Order submission time range (yyyy-mm-dd)',
      17 => 'ec_orders_search_confirm_date',
      18 => 'Ordre plage de temps de confirmation (format aaaa-mm-jj)',//'订单确认时间范围(格式 yyyy-mm-dd)', //  'Order confirmation time range (format yyyy-mm-dd)',
    ),
  ),
  186 =>
  array (
    'index' =>
    array (
      'Paramètres E-Commerce' => 'action=setting&operation=ec',//'电子商务', //  'E-Commerce Settings'
      'Règles de crédit' => 'action=ec&operation=credit',//'诚信规则', //  'Credit Rules'
    ), 
    'text' =>
    array (
      0 => 'E-Commerce &raquo; Règles de crédit',//'电子商务 &raquo; 诚信规则', //   'E-Commerce &raquo; Credit Rules',
      1 => 'ec_credit_tips',
      2 => 'Vous pouvez modifier des images dans static/image/traderank/ répertoire, et concevoir votre propre icône de style du site',//'你可以修改 static/image/traderank/ 目录下的图片，设计适合自己站点风格的图标', //   'You can modify images in static/image/traderank/ directory, and design your own site style icon',
      3 => 'ec_credit',
      4 => 'Règles de crédit',//'诚信规则', //   'Credit Rules',
      5 => 'ec_credit_maxcreditspermonth',
      6 => 'Le maximum de points par mois entre les acheteurs et les vendeurs de la même évaluation',//'每个自然月中，相同买家和卖家之间的评价计分最大值', //  Maximum points per month between buyers and sellers of the same evaluation
      7 => 'À la transaction créer calcul du temps, au-delà de la portée de points des règles de l\'évaluation aura pas de points',//'以交易创建的时间计算，超出计分规则范围的评价将不计分', //  'At the transaction create time calculation, beyond the points scope of the rules of the evaluation will not be points',
    ),
  ),
  187 =>
  array (
    'index' =>
    array (
      'Gestion flux' => 'action=feed',//'动态管理',  //  'Feed Management'
    ),
    'text' =>
    array (
      0 => 'Gestion flux',//'动态管理', //  'Feed Management',
      1 => 'feed_global_title',
      2 => 'Titre du Flux',//'动态标题',//  'Feed title',
      3 => 'HTML prises en charge, telles que: &lt;b&gt;bold&lt;/b&gt; et &lt;font color=red&gt;color&lt;/font&gt;',//'支持html，例如：加粗&lt;b&gt;&lt;/b&gt;，变色 &lt;font color=red&gt;&lt;/font&gt;',  // 'Html supported, such as: &lt;b&gt;bold&lt;/b&gt; and &lt;font color=red&gt;color&lt;/font&gt;', 
      4 => 'feed_global_body_general',
      5 => 'Contenu du Flux',//'动态备注',//  'Feed content',
      6 => 'HTML prises en charge',//'支持html',//  'Html supported',
      7 => 'feed_global_image_1',
      8 => 'Image 1 adresse',//''第1张图片地址', //   'Image 1 address',
      9 => 'feed_global_image_1_link',
      10 => 'Image 1 lien URL',//'第1张图片链接', //  'Image 1 link URL',
      11 => 'feed_global_image_2',
      12 => 'Image 2 adresse',//'第2张图片地址', //   'Image 2 address',
      13 => 'feed_global_image_2_link',
      14 => 'Image 2 lien URL',//'第2张图片链接', //  'Image 2 link URL',
      15 => 'feed_global_image_3',
      16 => 'Image 3 adresse',//'第3张图片地址',  //   'Image 3 address',
      17 => 'feed_global_image_3_link',
      18 => 'Image 3 lien URL',//'第3张图片链接', //  'Image 3 link URL',
      19 => 'feed_global_image_4',
      20 => 'Image 4 adresse',//'第4张图片地址', //   'Image 4 address',
      21 => 'feed_global_image_4_link',
      22 => 'Image 4 lien URL',//'第4张图片链接', // 'Image 4 link URL', 
      23 => 'feed_global_dateline',
      24 => 'Temps de publication',//'发布时间', //   'Publish time',
      25 => '(Format: aaaa-mm-jj H: i) Vous pouvez remplir une date et une heure ultérieures, cette dynamique à l\'avenir avant la date d\'arrivée, il a été démontré dans la première',//'(格式：yyyy-mm-dd H:i)你可以填写一个将来的日期和时间，那么这条动态会在这个将来的日期到来之前，一直显示在第一位', //  '(Format: yyyy-mm-dd H: i) You can fill out a future date and time, then this dynamic in the future before the arrival date, has been shown in the first',
      26 => 'feed_global_hot',
      27 => 'Flux valeur Hot',//'动态热度',  //   'Feed Hot value',
    ),
  ),
  188 =>
  array (
    'index' =>
    array (
      'Forums' => 'action=forums',//'版块管理', //  'Forums'
      'Modifier Forum' => 'action=forums&operation=edit',//'编辑版块', // 'Edit Forum' 
    ),
    'text' =>
    array (
      0 => 'Forums &raquo; Modifier Forum',//'版块管理 &raquo; 编辑版块', //  'Forums &raquo; Edit Forum',
      1 => 'forums_edit_basic_cat_name',
      2 => 'Nom de la catégorie',//'分区名称', //  'Category name',
      3 => 'forums_edit_basic_cat_name_color',
      4 => 'Nom de la catégorie couleur',//'分区名称颜色', //   'Category Name Color',
      5 => 'forums_edit_basic_cat_style',
      6 => 'Système de style de la catégorie (aura une incidence dans la catégorie des sous-forums)',//'分区风格方案(会影响该分区下的子版块)', //  'Category style scheme (will affect under the category sub-forums)',
      7 => 'Utilisez le schéma de style pour des visites dans la catégorie de forum et sous-forums (sous-forums peuvent avoir un différent du style de la catégorie, et peuvent être envoyés séparément)',//'访问者进入分区版块及子版块(子版块不同于分区风格，可单独设置)所使用的风格方案', //  Use the style scheme for visits into the forum category and sub-Forums (sub-forums can have a different from the category style, and can be set separately)
      8 => 'forums_edit_extend_forum_horizontal',
      9 => 'Index du Forum sous-forums inférieure horizontale',//'论坛首页下级子版块横排', //'Forum Index Lower horizontal sub-forums',  
      10 => 'Quand l\'Accueil des forums mis à l\'horizontale et le numéro de la diminution sous-forums par ligne définie à 0, le arrangés selon le régime normal',//'设置进入论坛首页时下级子版块横排时每行版块数量，如果设置为 0，则按正常方式排列', // When Forum Home set into the horizontal and number of lower sub-forums per line set to 0, the arranged according to the normal scheme
      11 => 'forums_edit_extend_cat_sub_horizontal',
      12 => 'Afficher Catégorie sous-forums inférieure horizontale',//'分区下级子版块横排', //   'Show Category lower sub-forums horizontally',
      13 => 'Lorsque la catégorie numéro horizontale des sous-forums inférieure par ligne définie à 0, la catégorie organisé selon le régime normal',//'设置进入分区时，下级子版块横排时每行版块数量，如果设置为 0，则按正常方式排列', // When the category horizontal number of lower sub-forums per line set to 0, the category arranged according to the normal scheme
      14 => 'forums_edit_extend_domain',
      15 => 'Forum domaine stationnaire',//'绑定域名', //  'Forum parked domain',
      16 => 'Après avoir réglé le domaine racine, où domaine lié à prendre effet, définir le nom de domaine racine',//'根域名设置完后，此处域名绑定才能生效，设置根域名',
      17 => 'forums_edit_extend_domain',
      18 => 'Forum domaine stationnaire',//'绑定域名', //  'Forum parked domain',
      19 => 'Après avoir réglé le domaine racine, où domaine lié à prendre effet, définir le nom de domaine racine',//'根域名设置完后，此处域名绑定才能生效，设置根域名', // After setting the root domain, where domain bound to take effect, set the root domain name
      20 => 'forums_cat_display',
      21 => 'Afficher la catégorie',//'显示分区', //  'Display category',
      22 => 'Si vous sélectionnez "Non", la catégorie sera temporairement masqué, mais les utilisateurs peuvent toujours accéder à cette zone via l\'URL du Forum',//'选择“否”将暂时将分区隐藏不显示，但分区内容仍将保留，且用户仍可通过 URL 访问到此分区及其版块', // 'If select "No", the category will be temporarily hidden, but users can still access this area through the Forum URL',
      23 => 'forums_edit_basic_shownav',
      24 => 'Dans l\'écran de navigation',//   '在导航显示',
      25 => 'Choisissez si vous souhaitez afficher la navigation principale', //   '选择是否在主导航中显示',
      26 => 'setting_seo_forum_tips',
      27 => '{bbname} = Nom du Site (Applications: Toutes les positions); {forum} = actuel nom du forum (Application: En plus de l\'Accueil); {fup} = parent nom du forum (Application: Sous-forum page Liste de thèmes et de la page du contenu du post); {fgroup} = Nom de la catégorie (Application: En plus de l\'Accueil à l\'extérieur); {subject} = Titre de rubrique (Application: pages poste de contenu); {summary} = Résumé des sujets (Application: pages poste de contenu); {tags} = Étiquettes thématiques (Application: pages poste de contenu); {page} = Sub Page (Application: pages de la liste de sujet, pages poste de contenus)',//'站点名称&nbsp;{bbname}（应用范围：所有位置）当前版块名称&nbsp;{forum}（应用范围：除首页以外）一级版块名称&nbsp;{fup}（应用范围：子版块主题列表页和帖子内容页）分区名称&nbsp;{fgroup}（应用范围：除首页以外）帖子标题&nbsp;{subject}（应用范围：帖子内容页）主题摘要&nbsp;{summary}（应用范围：帖子内容页）主题标签&nbsp;{tags}（应用范围：帖子内容页）分页数&nbsp;{page}（应用范围：主题列表页、帖子内容页）', // '{bbname} = Site Name (Applications: All positions); {forum} = current forum name (Application: In addition to home); {fup} = parent forum name (Application: Sub-forum Topic List page and post content page); {fgroup} = Category name (Application: In addition to outside home); {subject} = Topic title (Application: post content pages); {summary} = Topic summary (Application: post content pages); {tags} = Topic tags (Application: post content pages); {page} = Sub page (Application: Topic List pages, post contents pages)',
      28 => 'forums_edit_basic_seotitle',
      29 => 'title',
      30 => 'forums_edit_basic_keyword',
      31 => 'keywords',
      29 => 'Mots clés de référencement utilisés dans la balise meta keyword. Plusieurs mots-clés séparés par une virgule ","',//'keywords用于搜索引擎优化，放在 meta 的 keyword 标签中，多个关键字间请用半角逗号 "," 隔开', // 'SEO keywords used in the meta keyword tag. Separate multiple keywords with a comma ","',
      33 => 'forums_edit_basic_seodescription',
      34 => 'description',
      35 => 'Description SEO utilisé dans balise meta description.',//'description用于搜索引擎优化，放在 meta 的 description 标签中', // SEO description used in meta description tag.
    ),
  ),
  189 =>
  array (
    'index' =>
    array (
      'Forums' => 'action=forums',//'版块管理' //   'Forums' 
      'Réglages de base' => 'action=forums&operation=edit&anchor=basic',//'基本设置'//  'Basic settings'
    ),
    'text' =>
    array (
      0 => 'Forums &raquo Réglages de base',//'版块管理 &raquo; 基本设置',//   'Forums &raquo Base settings',
      1 => 'forums_edit_tips',
      2 => 'Les paramètres suivants ne sont pas héritées, qui n\'est efficace que dans le forum actuel n\'aura pas d\'impact sur​les sous-forums inférieurs.',//'以下设置没有继承性，即仅对当前版块有效，不会对下级子版块产生影响。', //  'The following settings are not inherited, that is effective only to the current forum will not impact on the lower sub-forums.',
      3 => 'forums_edit_basic',
      4 => 'Réglages de base',//'基本设置',  //  'Basic settings',
      5 => 'forums_edit_basic_name',
      6 => 'Nom du Forum',//'版块名称', //  'Forum Name',
      7 => 'forums_edit_base_name_color',
      8 => 'Couleur Nom Forum',//'版块名称颜色', // 'Forum name color',  
      9 => 'forums_edit_basic_icon',
      10 => 'Icône du Forum',//'版块图标', //  'Forum Icon',
      11 => 'forums_edit_basic_icon_width',
      12 => 'Largeur Icône, px',//'图标宽度 (单位: px)', //  'Icon width, px',
      13 => 'forums_edit_basic_banner',
      14 => 'Bandeau supérieur',//'顶部图片', //  'Top banner',
      15 => 'forums_edit_basic_display', 
      16 => 'Affichage Forum',//'显示版块', //  'Display Forum',
      17 => 'Sélectionnez l\'option "invisible" pour masquer temporairement le forum dans la liste, mais le contenu du forum restera, et l\'utilisateur peut toujours accéder directement à ce forum par l\'URL d\'un fid',//'选择“不显示”将暂时将版块隐藏不显示，但版块内容仍将保留，且用户仍可通过直接提供带有 fid 的 URL 访问到此版块', //   'Select the "invisible" for temporarily hide the forum in the list, but the forum content will remain, and the user can still directly access to this forum by the URL with a fid',
      18 => 'forums_edit_basic_shownav',
      19 => 'Dans l\'écran de navigation', //  '在导航显示',
      20 => 'Choisissez si vous souhaitez afficher la navigation principale', //  '选择是否在主导航中显示', 
      21 => 'forums_edit_basic_up',
      22 => 'Forum Parent',//'上级版块', //  'Parent forum',
      23 => 'Forum parent ou Catégorie du forum ',//'本版块的上级版块或分类',  // 'Parent Forum or category for this Forum ',
      24 => 'forums_edit_basic_redirect',
      25 => 'Forum URL de redirection',//'版块转向 URL', //  'Forum redirect URL',
      26 => 'Si vous définissez cette URL (comme http://www.discuz.com), l\'utilisateur clique sur le Forum sera redirigé vers cette URL. Pour ne pas utiliser cette fonctionnalité laissé vide.',//'如果设置转向 URL(例如 http://www.discuz.com)，用户点击本分版块将进入转向中设置的 URL。一旦设定将无法进入版块页面，请确认是否需要使用此功能，留空为不设置转向 URL', // 'If you set this URL (such as http://www.discuz.com), the user click to the Forum will be redirected to this URL. For not use this feature left it blank.',
      27 => 'forums_edit_basic_description',
      28 => 'Description Forum',//'版块简介', //  'Forum description',
      29 => 'Entrez une brève description du Forum, qui sera affiché sous le nom du forum.<br>BB-Code pris en charge.',//'将显示于版块名称的下面，提供对本版块的简短描述支持内置的 Discuz! 代码', //  'Enter a brief description of the Forum, that will be shown below the forum name.<br>BB-Code supported.',
      30 => 'forums_edit_basic_rules',
      31 => 'Règles du Forum',//'本版块规则', // 'Forum Rules',
      32 => 'Afficher une liste de pages liées aux règles actuel sur le forum, laisser vide pour ne pas montrer, BB-Code pris en charge.',//'显示于主题列表页的当前版块规则，留空为不显示支持内置的 Discuz! 代码', // 'Show a list of pages related to the current Forum rules, leave blank to not show, BB-Code supported.',
      33 => 'forums_edit_basic_keys',
      34 => 'Alias du forum',//'绑定别名', //  'Forum Alias',
      35 => 'Lorsque le site est utilisé URL statiques, les alias du forum peuvent être utilisés pour accéder à ce forum. À savoir Ii un alias fixés à "développeur", puis le forum URL sera changé en "http://127.0.0.1/Discuz!X/tools/language_template/forum-developer-1.html". Remarque: Les alias peuvent contenir que des lettres ou des chiffres, et ne peuvent pas être pur numérique.',//'绑定后当站点开启 URL 静态化后可通过别名访问此版块，如设置别名为“developer”那么版块的 URL 将变为“http://127.0.0.1/Discuz!X/tools/language_template/forum-developer-1.html”。注意：别名中只能包含字母或数字，且不能是纯数字', //  'When the site is used static URL, the forum alias can be used to access this forum. I.e. Ii an alias set to "developer", then the forum URL will changed to "http://127.0.0.1/Discuz!X/tools/language_template/forum-developer-1.html". Note: The alias can contain only letters or numbers, and can not be pure digital.',
      36 => 'forums_edit_extend_domain',
      37 => 'Forum domaine stationnaire',//'绑定域名', // 'Forum parked domain',
      38 => 'Après avoir réglé le domaine racine, où domaine lié à prendre effet, définir le nom de domaine racine',//'根域名设置完后，此处域名绑定才能生效，设置根域名', // 'After setting the root domain, where domain bound to take effect, set the root domain name',
      39 => 'forums_edit_extend_domain',
      40 => 'Forum domaine stationnaire',//'绑定域名', // 'Forum parked domain',
      41 => 'Après avoir réglé le domaine racine, où domaine lié à prendre effet, définir le nom de domaine racine',//'根域名设置完后，此处域名绑定才能生效，设置根域名', // 'After setting the root domain, where domain bound to take effect, set the root domain name',
      42 => 'setting_seo_forum_tips',
      43 => '{bbname} = Nom du site (Applications: Toutes les positions); {forum} = actuel nom du forum (Application: En plus de l\'Accueil); {fup} = parent nom du forum (Application: Sub-forum Page Liste de thèmes et de la page du contenu du post); {fgroup} = Nom de la catégorie (Application: En plus de l\'Accueil externe); {subject} = Titre de rubrique (Application: pages poste de contenu); {summary} = Résumé des sujets (Application: pages poste de contenu); {tags} = Étiquettes thématiques (Application: pages poste de contenu); {page} = Sub page (Application: les pages de liste de sujet, pages poste de contenus)',//'站点名称&nbsp;{bbname}（应用范围：所有位置）当前版块名称&nbsp;{forum}（应用范围：除首页以外）一级版块名称&nbsp;{fup}（应用范围：子版块主题列表页和帖子内容页）分区名称&nbsp;{fgroup}（应用范围：除首页以外）帖子标题&nbsp;{subject}（应用范围：帖子内容页）主题摘要&nbsp;{summary}（应用范围：帖子内容页）主题标签&nbsp;{tags}（应用范围：帖子内容页）分页数&nbsp;{page}（应用范围：主题列表页、帖子内容页）', //  '{bbname} = Site Name (Applications: All positions); {forum} = current forum name (Application: In addition to home); {fup} = parent forum name (Application: Sub-forum Topic List page and post content page); {fgroup} = Category name (Application: In addition to outside home); {subject} = Topic title (Application: post content pages); {summary} = Topic summary (Application: post content pages); {tags} = Topic tags (Application: post content pages); {page} = Sub page (Application: Topic List pages, post contents pages)',
      44 => 'forums_edit_basic_seotitle',
      45 => 'title',
      46 => 'forums_edit_basic_keyword',
      47 => 'Keywords',
      48 => 'Mots clés de référencement utilisés dans la balise meta keyword. Plusieurs mots-clés séparés par une virgule ","',//'keywords用于搜索引擎优化，放在 meta 的 keyword 标签中，多个关键字间请用半角逗号 "," 隔开', //  'SEO keywords used in the meta keyword tag. Separate multiple keywords with a comma ","',
      49 => 'forums_edit_basic_seodescription',
      50 => 'description',
      51 => 'Description SEO utilisé dans la description meta tag.',//'description用于搜索引擎优化，放在 meta 的 description 标签中', //  'SEO description used in meta description tag.',
    ),
  ),
  190 =>
  array (
    'index' =>
    array (
      'Forums' => 'action=forums',//'版块管理',  //  'Forums' 
      'Paramètres avancés' => 'action=forums&operation=edit&anchor=extend',//'扩展设置',  //  'Extended settings'
    ),
    'text' =>
    array (
      0 => 'Forums &raquo; Paramètres avancés',//'版块管理 &raquo; 扩展设置', // 'Forums &raquo; Extended settings',
      1 => 'forums_edit_tips',
      2 => 'Les paramètres suivants ne sont pas héritées, qui n\'est efficace que dans le forum actuel n\'aura pas d\'impact sur ​​les sous-forums inférieurs.',//'以下设置没有继承性，即仅对当前版块有效，不会对下级子版块产生影响。', // 'The following settings are not inherited, that is effective only to the current forum will not impact on the lower sub-forums.',
      3 => 'forums_edit_extend',
      4 => 'Paramètres avancés',//'扩展设置', // 'Extended settings',
      5 => 'forums_edit_extend_style',
      6 => 'Style du Forum',//'风格方案', // 'Forum style',
      7 => 'Entrez le style utilisé par les visiteurs à ce forum',//'访问者进入本版块所使用的风格方案', // 'Enter the style used by visitors in this Forum',
      8 => 'forums_edit_extend_sub_horizontal',
      9 => 'Afficher plus bas sous-forums horizontalement',//'论坛版块下级子版块横排', // 'Show lower sub-forums horizontally',
      10 => 'Lorsque le nombre horizontal forum des sous-forums inférieure par ligne définie à 0, la catégorie organisé selon le régime normal',//'设置进入论坛版块时下级子版块横排时每行版块数量，如果设置为 0，则按正常方式排列', //  'When the forum horizontal number of lower sub-forums per line set to 0, the category arranged according to the normal scheme',
      11 => 'forums_edit_extend_subforumsindex',
      12 => 'Afficher les sous-forums dans le Forum d\'accueil',//'本版块在首页显示下级子版块', //   'Show sub-Forums in the Forum Home',
      13 => 'Accueil du forum au bas de la liste indique le nom du forum et les liens sous-forum inférieurs (le cas échéant). Remarque: Cette fonction ne considère pas les enfants forums autorisations pour un visiteur. Sélectionnez "Par Défaut" pour utiliser paramètres généraux.',//'首页版块列表中在版块简介下方显示下级子版块名字和链接(如果存在的话)。注意: 本功能不考虑子版块特殊浏览权限的情况，只要存在即会被显示出来。选择“默认”，将使用全局设置', //  'Forum Home at the bottom of the list shows the forum name and the lower sub-Forum links (if present). Note: This feature does not consider the child forums permissions for a visitor. Select "Default" to use global settings.',
      14 => 'forums_edit_extend_simple',
      15 => 'Afficher uniquement les sous-forums niveaux inférieurs',//'只显示下级子版块',//   'Show only lower-level sub-Forums',
      16 => 'Sélectionnez "Oui" pour Afficher les autres forums similaires à des catégories, sans boutons post et ainsi de suite.',//'选择“是”将不显示本版块的主题列表、发帖按钮等等，类似于一个分类', // 'Select "Yes" for show forums similar to categories, without post buttons and so on.',
      17 => 'forums_edit_extend_sub_horizontal',
      18 => 'Afficher plus bas sous-forums horizontalement',//'论坛版块下级子版块横排',//   'Show lower sub-forums horizontally',
      19 => 'Lorsque le nombre horizontal forum des sous-forums inférieure par ligne définie à 0, la catégorie organisé selon le régime normal',//'设置进入论坛版块时下级子版块横排时每行版块数量，如果设置为 0，则按正常方式排列', // When the forum horizontal number of lower sub-forums per line set to 0, the category arranged according to the normal scheme
      20 => 'forums_edit_extend_subforumsindex',
      21 => 'Afficher les sous-forums dans le Forum d\'accueil',//'本版块在首页显示下级子版块', // 'Show sub-Forums in the Forum Home',
      22 => 'Accueil du forum au bas de la liste indique le nom du forum et les liens sous-forum inférieurs (le cas échéant). Remarque: Cette fonction ne considère pas les enfants forums autorisations pour un visiteur. Sélectionnez "Par Défaut" pour utiliser paramètres généraux.',//'首页版块列表中在版块简介下方显示下级子版块名字和链接(如果存在的话)。注意: 本功能不考虑子版块特殊浏览权限的情况，只要存在即会被显示出来。选择“默认”，将使用全局设置',  //  'Forum Home at the bottom of the list shows the forum name and the lower sub-Forum links (if present). Note: This feature does not consider the child forums permissions for a visitor. Select "Default" to use global settings.', 
      23 => 'forums_edit_extend_simple',
      24 => 'Afficher uniquement les sous-forums niveaux inférieurs',//'只显示下级子版块', // 'Show only lower-level sub-Forums',
      25 => 'Sélectionnez "Oui" pour Afficher les autres forums similaires à des catégories, sans boutons post et ainsi de suite.',//'选择“是”将不显示本版块的主题列表、发帖按钮等等，类似于一个分类', //  Select "Yes" for show forums similar to categories, without post buttons and so on.
      26 => 'forums_edit_extend_widthauto',
      27 => 'Largeur style pour ce forum',//'本版块宽窄风格', //  'Width style for this forum',
      28 => 'Si vous définissez le style d\'affichage du forum (version large ou version étroite), les utilisateurs ne seront pas en mesure de changer le style de la largeur du forum. Ce paramètre hérite des paramètres globaux par défaut.',//'设置本版块的显示风格是宽版还是窄版，设置后用户将无法自由切换宽窄风格。默认表示继承全局设置', //  'If you set the forum display style (wide version or narrow version), users will not be able to switch the forum width style. This setting inherits the default global settings.',
      29 => 'forums_edit_extend_picstyle',
      30 => 'Activer le mode liste d\'images',//'开启图片列表模式', // 'Enable the image list mode',
      31 => 'Des images seront affichées dans une liste de sujet. Couverture du sujet peut être réglé à la largeur et la hauteur unifiée',//'主题列表将以图片方式显示，主题封面可统一设置宽高', // 'Images will displayed in a topis list. Topic cover can be set to unified width and height',
      32 => 'forums_edit_extend_allowside',
      33 => 'Afficher la barre latérale',//'显示边栏', //  'Show Sidebar',
      34 => 'Sélectionnez "Oui" pour afficher le contenu des sections de la barre latérale à l\'accueil.',//'选择“是”版块首页侧边将显示聚合本版内容的信息', // 'Select "Yes" for show the sidebar section contents at Home.',
      35 => 'forums_edit_extend_recommend_top',
      36 => 'Autoriser Scotché global',//'显示全局置顶和分类置顶的主题', // 'Allow global stick',
      37 => 'Marqué comme des fils de discussions scotchés  seront affichés dans le top globale et sous-forum top',//'是否在本版显示全局置顶和分版置顶', //  'Marked as Sticked threads will be displayed in the global top and sub-forum top',
      38 => 'forums_edit_extend_defaultorderfield',
      39 => 'Champ de tri par défaut de cette discussion',//'主题默认排序字段', //  'Thread default sort field',
      40 => 'Définir la liste des champs de tri par défaut. La valeur par défaut est "temps de réponse". Tout ajout de champs de tri va augmenter la charge du serveur.',//'设置版块的主题列表默认按照哪个字段进行排序显示。默认为“回复时间”，除默认设置外其他排序方式会加重服务器负担', // 'Set the list of default sort fields. The default is "Reply time". Any addition of sort fields will increase the server loading.',
      41 => 'forums_edit_extend_defaultorder',
      42 => 'Discussion ordre de tri par défaut',//'主题默认排序方式',// 'Thread default sort order',
      43 => 'Définir l\'ordre de tri par défaut des threads. La valeur par défaut est "décroissante". Toute adjonction à défaut va augmenter la charge du serveur.',//'设置版块的主题列表默认排序的方式。默认为“按降序排列”，除默认设置外其他排序方式会加重服务器负担', //  'Set the threads default sort order. The default is "Descending". Any addition to default will increase the server loading.',
      44 => 'forums_edit_extend_reply_background',
      45 => 'Image de fond de la boîte de réponse rapide', //   '快捷回复框背景图片',
      46 => 'Situé dans la zone de réponse rapide affichera l\'image', //   '设置后将在快速回复框中展现该图片',
      47 => 'forums_edit_extend_threadcache',
      48 => 'Mémoire Cache du fil de la discussion',//'页面缓存系数', // 'Thread Cache',
      49 => 'Cette fonctionnalité est utile pour diminuer la charge de gros serveurs avec beaucoup de visites de clients en utilisant un cache temporaire. Le coefficient de cache varie de 0 à 100, la valeur proposée est de 20 à 40. Mettre à 0 pour désactiver la mise en cache. Dans le cas de l\'espace disque autorisé, le facteur approprié pour augmenter la mémoire cache, le cache peut améliorer l\'efficacité. Note: Accédez aux paramètres du forum: Global -> Paramètres -> Paramètres d\'optimisation du cache, et d\'ajuster le temps de la maintenance.',//'此功能可以将游客经常访问的主题临时缓存起来，缓解大型服务器压力。系数范围 0～100，建议数值 20 ～ 40，0 为关闭缓存。在磁盘空间允许的情况下，适当调高缓存系数，可以提高缓存效果注：版块设置完毕后请到 全局 -> 优化设置 -> 站点页面缓存设置，调整其缓存时间', // 'This feature is usefull for decrease the loading of large servers with a lot of guest visits by using a temporary cache. The cache coefficient ranges from 0 to 100, the proposed value is from 20 to 40. Set to 0 to turn off caching. In the case of disk space allowed, the appropriate factor to increase the cache, the cache can improve the effectiveness. Note: Go to the forum settings: Global -> Optimization settings -> Cache settings, and adjust the buffer time.',
      50 => 'forums_edit_extend_relatedgroup',
      51 => 'Groupes connexes/Forums',//'关联群组/版块', // 'Related Groups/Forums',
      52 => 'Remplissez les fids de groupe/forum associés, séparés par une virgule ",". Page du forum affichera une liste de sujets liés avec le groupe/forum.',//'填写被关联群组/版块的fid，以半角逗号 "," 隔开。版块的主题列表页将显示被关联群组/版块的主题', //  'Fill in the associated group/forum fids, separated by a comma ",". Forum page will display a list of topics related with the group/forum.',
      53 => 'forums_edit_extend_edit_rules',
      54 => 'Permettre aux modérateurs à éditer ces Règles du forum',//'允许版主修改本版块规则', //  'Allow moderators to edit this Forum rules',
      55 => 'Ces paramètres permettent de modifier les règles du forum de super Modérateurs et animateurs',//'设置是否允许超级版主和版主修改本版规则', //  'This settings allowed edit the forum rules to Super Moderators and Moderators',
      56 => 'forums_edit_extend_disablecollect',
      57 => 'Désactivez Collections',//'禁止淘帖', // 'Disable Collections',
      58 => 'Sélectionnez "OUI" pour empêcher les utilisateurs de l\'utilisation des collections',//'选择“是”将禁止用户淘帖本版主题', // 'Select "Yes" to prevent users from using collections',
      59 => 'forums_edit_extend_recommend',
      60 => 'Permettre de recommander les discussions',//'是否开启推荐主题功能', //  'Enable to recommend threads',
      61 => 'Ce paramètre activer/désactiver la fonction "recommander"',//'设置是否开启推荐主题功能', // 'This setting enable/disable the "recommend" function',
      62 => 'forums_edit_extend_recommend_sort',
      63 => 'Modérateur méthode recommandée',//'版主推荐主题方式', //  'Moderator Recommended method',
      64 => 'Automatique: la liste des sujets recommandée généré selon des thèmes niveau recommandé.<br />Manuel: liste recommandée généré par le modérateur manuellement.<br />Semi-automatique: similaire à la méthode manuelle, lorsque la liste des articles générer manuellement, et que s\'affiche un certain nombre d\'articles, il remplira automatiquement en conformité avec les règles recommandées par le thème de nombre de taux.',//'自动生成则按照推荐主题规则设定生成推荐列表。手动生成则由版主自行推荐生成推荐列表。半自动生成则和手动方式类似，当手动生成的列表条数不足设置的显示条数时，将按照推荐主题规则自动补足相差条数', //  'Automatic: recommended topics list generated in accordance with topics recommended level.<br />Manual: recommended list generated by the moderator manually.<br />Semi-automatic: similar to manual method, when a list of articles generate manually, and than displayed a specified number of articles, it will automatically fill in accordance with the rules recommended by the theme Number of rates.',
      65 => 'forums_edit_extend_recommend_orderby',
      66 => 'Ordre recommandé discussions par',//'推荐主题规则',//  'Order recommended threads by',
      67 => 'Cet ordre n\'est efficace que pour le mode automatique et semi-automatique de génération de fils de discussions recommandés. Utilisé uniquement descendant Ordre toujours.',//'推荐主题规则，只对自动和半自动生成推荐主题列表有效，此规则均为降序排列',// 'This ordering is eefective only for automatic and semi-automatic mode of recommended threads generation. Used only Descending order allways.',
      68 => 'forums_edit_extend_recommend_num',
      69 => 'Nombre de sujets recommandés pour voir',//'推荐主题显示数量',// 'Number of recommended topics to show',
      70 => 'Définissez un nombre de fils recommandées affichées. Par Défaut est de 10, pas plus de 20.',//'推荐主题显示的数量，默认为 10 条，建议不要超过 20 条',// 'Set a number of recommended threads displayed. Default is 10, not more than 20.',
      71 => 'forums_edit_extend_recommend_imagenum',
      72 => 'Nombre d\'icônes de pièce jointe applicables pour les discussions recommandés',//'推荐主题图片附件显示数量',//  'Number of attachment icons for Recommended Threads',
      73 => 'Combien icônes de pièces jointes pour afficher à un sujet marqué comme recommandé. Par Défaut est de 5. Mettre à 0 pour désactiver les icônes recommandées. Ne pas utiliser plus de 10.',//'推荐主题中图片附件显示的数量，默认为 5 条。设置为 0 则不显示，建议不要超过 10 条', // 'How many attachment icons to show for a thread marked as Recommended. Default is 5. Set to 0 for disble recommended icons. Do not use more than 10.',
      74 => 'forums_edit_extend_recommend_imagesize',
      75 => 'Recommandé la taille des icônes',//'推荐主题图片附件显示大小',//  'Recommended icon size',
      76 => 'Taille d\'une rubrique images jointes recommandée, la valeur par défaut est de 300 x 250 px.',//'推荐主题中图片附件显示的大小，默认为 300 x 250', // 'Recommended size for a topic attached images, the default is 300 x 250 px.',
      77 => 'forums_edit_extend_recommend_maxlength',
      78 => 'Longueur du titre',//'标题最大字节数', //  'Title length',
      79 => 'Définir le titre longueur max, si un titre est plus long que cette valeur, il sera taillé automatiquement, 0 est désactiver',//'设置当标题长度超过本设定时，是否将标题自动缩减到本设定中的字节数，0 为不自动缩减', //  'Set the title max length, if a title is longer than this valuse, it will be cutted automatically, 0 is disable',
      80 => 'forums_edit_extend_recommend_cachelife',
      81 => 'Durée du cache en mémoire (en secondes)',//'数据缓存时间(秒)',// 'Cache time (seconds)',
      82 => 'Comme une sorte d\'opération de récupération est relativement coûteuse, il est recommandé de définir la valeur à 900',//'由于一些排序检索操作比较耗费资源，建议设置为 900 的数值', //  'As some sort of retrieval operation is relatively expensive, it is recommended to set the value to 900',
      83 => 'forums_edit_extend_recommend_dateline',
      84 => 'Recommandé temps réel',//'推荐主题时间段', // 'Recommended live time',
      85 => 'Définissez la période de temps à partir de ce moment, pour générer automatiquement une liste de sujets recommandés en heures. Mettre à 0 pour tout le temps',//'按照设置的排序方式，取从访问推荐主题列表这一刻往前推送时间段内的主题，自动生成推荐主题列表，单位为小时，设置成0为所有时段', //  'Set the time period from this moment for automatically generate a list of recommended topics in hours. Set to 0 for all the time',
    ),
  ),
  191 =>
  array (
    'index' =>
    array (
      'Forums' => 'action=forums',//'版块管理', // 'Forums'
      'Options du post' => 'action=forums&operation=edit&anchor=posts',//'帖子选项', // 'Post options'
    ),
    'text' =>
    array (
      0 => 'Forums &raquo; Post options',//'版块管理 &raquo; 帖子选项', //  'Forums &raquo; Post options',
      1 => 'forums_edit_tips',
      2 => 'Les paramètres suivants ne sont pas héritées, qui n\'est efficace que dans le forum actuel n\'aura pas d\'impact sur​les sous-forums inférieurs.',//'以下设置没有继承性，即仅对当前版块有效，不会对下级子版块产生影响。', // 'The following settings are not inherited, that is effective only to the current forum will not impact on the lower sub-forums.',
      3 => 'forums_edit_posts',
      4 => 'Post options',//'帖子选项',// 'Post options',
      5 => 'forums_edit_posts_modposts',
      6 => 'Post premoderation',//'发帖审核',// 'Post premoderation',
      7 => 'Si sélectionné "Oui", il faudra à tout nouveau post de l\'utilisateur doit être examiné par les modérateurs et administrateurs avant que le poste est affiché. Après son tour sur cette fonction, vous pouvez définir le groupe d\'utilisateurs qui les messages doivent être vérifiés, ainsi que les groupes de gestion, qui peuvent effectuer l\'audit ',//'选择“是”将使用户在本版发表的帖子待版主或管理员审查通过后才显示出来，打开此功能后，你可以在用户组中设定哪些组发帖可不经审核，也可以在管理组中设定哪些组可以审核别人的帖子', // 'If selected "Yes", it will require to all new users post must be reviewed by moderators or administrators before the post is shown. After turn on this feature, you can set the user group which posts must be audited, and also management groups, which can perform the auditing',
      8 => 'forums_edit_posts_alloweditpost',
      9 => 'Laisser éditer les messages',//'允许编辑帖子', // 'Allow to edit posts',
      10 => 'Sélectionnez "Oui" pour permettre aux utilisateurs de modifier leurs propres messages',//'选择“是”将允许用户编辑本版发表的帖子',//  'Select "Yes" for allow users to edit their own posts',
      11 => 'forums_edit_posts_recyclebin',
      12 => 'Corbeille',//'回收站',// 'Recycle Bin',
      13 => 'Si la fonction de la Corbeille est activée, tous les sujets et les réponses supprimés seront déplacés dans la corbeille, et ne seront pas directement supprimés',//'是否在本版启用回收站功能，打开此功能后，所有被删除主题和回帖将被放在回收站中，而不会被直接删除', //  'If the Recycle Bin function is enabled, all deleted topics and replies will be moved into the recycle bin, and will not be directly deleted',
      14 => 'forums_edit_posts_html',
      15 => 'Permettre d\'utiliser le code HTML',//'允许使用 HTML 代码',// 'Allow to use HTML code',
      16 => 'NOTE: Si cela est permis, il apportera des risques de sécurité graves. S\'il vous plaît utiliser avec prudence!',//'注意: 选择“是”将不屏蔽帖子中的任何代码，将带来严重的安全隐患，请慎用', // 'NOTE: If this enabled, it will bring serious security risks. Please use with caution!',
      17 => 'forums_edit_posts_bbcode',
      18 => 'Autoriser l\'utilisation des BB-Code',//'允许使用 Discuz! 代码', // 'Allow to use BB-Code',
      19 => 'BB-code est un code simplifiée et sans danger pour le formatage du contenu',//'Discuz! 代码是一种简化和安全的页面格式代码', //  'BB-Code is a simplified and safe code for content formatting',
      20 => 'forums_edit_posts_imgcode',
      21 => 'Autoriser l\'utilisation Code [img]',//'允许使用 [img] 代码', // 'Allow to use [img] Code',
      22 => 'Le code [img] permet aux utilisateurs d\'afficher des images à partir d\'autres sites',//'允许 [img] 代码作者将可以在帖子插入其他网站的图片并显示',//  'The [img] code enable users to post images from other sites',
      23 => 'forums_edit_posts_mediacode',
      24 => 'Autoriser d\'utiliser le code multimédia',//'允许使用多媒体代码', //  'Allow to use multimedia code',
      25 => 'Autoriser l\'utilisation [audio] [Médias] [flash] et autres codes multimédias, les auteurs pourront insérer des fichiers multimédias dans le poste et l\'afficher',//'允许 [audio] [media] [flash] 等多媒体代码后，作者将可以在帖子插入多媒体文件并显示', // 'Allow to use [audio] [media] [flash] and other multimedia codes, authors will be able to insert multimedia files in the post and display it',
      26 => 'forums_edit_posts_smilies',
      27 => 'Autoriser l\'utilisation humeurs',//'允许使用表情', //  'Allow to use smiles',
      28 => 'Cette fonctionnalité permet de convertir les émoticônes textuelles, telles que ":)" humeurs en images.',//'表情提供对表情符号，如“:)”的解析，使之作为图片显示', // 'This feature enable to convert textual emoticons, such as ":)" to smile images.',
      29 => 'forums_edit_posts_jammer',
      30 => 'Activer le contenu brouilleur',//'启用内容干扰码',// 'Enable content jammer',
      31 => 'Sélectionnez "Oui" pour ajouter une chaîne aléatoire perturbatrice ou brouilleur dans le contenu du message, de sorte que le visiteur ne peut pas copier le contenu original. Remarque: Cette fonction va légèrement augmenter la charge du serveur.',//'选择“是”将在帖子内容中增加随机的干扰字串，使得访问者无法复制原始内容。注意: 本功能会轻微加重服务器负担', //  'Select "Yes" for add a disturbance random string into the post content, so the visitor can not copy the original content. Note: This feature will slightly increase the server loading.',
      32 => 'forums_edit_posts_anonymous',
      33 => 'Autoriser annonce anonyme',//'允许匿名发帖', // 'Allow anonymous posting',
      34 => 'Que ce soit pour permettre aux utilisateurs de publier les discussions et les réponses aussi anonyme. Anonyme affichage est différent des invités affichés, les utilisateurs anonymes devez vous connecter avant de poster, et les modérateurs et administrateurs peuvent visualiser le véritable auteur.',//'是否允许用户在本版匿名发表主题和回复，只要用户组或本版块允许，用户均可使用匿名发帖功能。匿名发帖不同于游客发帖，用户需要登录后才可使用，版主和管理员可以查看真实作者', //'Whether to allow users to publish threads and replies as anonymous. Anonymous posting is different from the guests posted, anonymous users need to log in before posting, and moderators and administrators can view the real author.',
      35 => 'forums_edit_posts_disablethumb',
      36 => 'Désactiver pour créer les images jointes miniatures',//'禁用图片附件添加缩略图', //  'Disable to create the attached images thumbnails',
      37 => 'Sélectionnez "Oui" ne créera pas de vignettes auto-générées dans ce forum, même si les paramètres globaux activer cette fonction. Sélectionnez "Non" pour utiliser les paramètres par défaut du système pour décider s\'il convient d\'ajouter une vignette ou non',//'选择“是”将不对本版块上传的图片附件自动缩略图，即便全局设置中开启了此项功能。选择“否”为按照系统默认设置决定是否添加缩略图', // 'Select "Yes" will not create auto-generated thumbnails in this forum, even if the global settings enable this feature. Select "No" for use the system default settings to decide whether to add a thumbnail or not',
      38 => 'forums_edit_posts_disablewatermark',
      39 => 'Désactiver le filigrane',//'禁用图片附件添加水印', // 'Disable watermark', 
      40 => 'Sélectionnez "Oui" pour désactiver la création automatique du filigrane pour le transfert à ce forum d\'images, même si activer cette fonction. les paramètres globaux Choisissez "Non" pour procédez aux paramètres par défaut du système afin de décider si vous souhaitez ajouter un filigrane ou pas',//'选择“是”将不对本版块上传的图片附件自动添加水印，即便全局设置中开启了此项功能。选择“否”为按照系统默认设置决定是否添加水印', // 'Select "Yes" for disable automatical creating of watrmark for uploaded to this forum images, even if the global settings enable this feature. Select "No" to follow the system default settings to decide whether to add a watermark or not',
      41 => 'forums_edit_posts_allowpostspecial',
      42 => 'Autoriser de poster des fils de discussions spéciaux',//'允许发布的特殊主题', // 'Allow to post special threads',
      43 => 'forums_edit_posts_threadplugin',
      44 => 'Plugin pour permettre de publier des discussions spéciales',//'允许发布的扩展特殊主题', // 'Plugin for allow to publish special threads',
      45 => 'Définir l\'extension afin de permettre de publier des discussions spéciales',//'设置本版允许发布哪些其他扩展的特殊主题', //  'Set the extension to allow publish special threads',
      46 => 'forums_edit_posts_allowspecialonly',
      47 => 'Autoriser de ne publier que des discussions spéciales',//'只允许发布特殊类型主题', // 'Allow to publish only special threads',
      48 => 'Ces paramètres désactivent la publications de posts autres que des sujets spéciaux',//'设置本版是否只允许发布特殊类型主题', //  'This settings disable to publish other threads than special',
      49 => 'forums_edit_posts_autoclose',
      50 => 'Fermeture de la discussion  automatique',//'主题自动关闭', // 'Thread auto close',
      51 => 'Définir si le sujet peut être fermé automatiquement après un certain temps, d\'interdire réponses des utilisateurs simples.',//'设置主题是否在某时间后自动关闭，禁止普通用户回复', // 'Set if the thread can be automatically closed after a certain time, to prohibit ordinary users replies.',
      52 => 'forums_edit_posts_autoclose_time',
      53 => 'Temps de fermeture automatique (en jours)',//'自动关闭时间(天)', // 'Auto-close time (days)',
      54 => 'Entrez un nombre de jours après la création ou la dernière réponse lorsqu\'un sujet sera fermé automatiquement pour les utilisateurs ordinaires (ils ne peuvent pas répondre). Ce paramètre prend effet que lorsque le "close-Auto" fonctionnalité est active.',//'本设定必须在“主题自动关闭”功能打开时才生效，主题依据自动关闭的设定: 在发表后若干天、或被最后回复后若干天被自动转入关闭状态，从而使普通用户无法回复', //  'Enter a number of days after creating or last reply when a thread will closed automatically for ordinary users (they can not reply). This setting take effect only when the "Auto-close" feature enabled.',
      55 => 'forums_edit_posts_attach_ext',
      56 => 'Activer le type de pièce jointe',//'允许附件类型', //  'Enabled attachment type',
      57 => 'Complétez l\'activation pour télécharger des extensions en pièces jointes, séparées de plusieurs extensions par une virgule ",". Ce paramètre est prioritaire sur les paramètres du groupe d\'utilisateurs. Laissez ce champ vide pour permettre les types de pièces jointes en conformité avec les paramètres du groupe d\'utilisateurs.',//'设置允许上传的附件扩展名，多个扩展名之间用半角逗号 "," 隔开。本设置的优先级高于用户组，留空为按照用户组允许的附件类型设定',//  'Fill in the enabled to upload attachment extensions, separate multiple extensions by comma ",". This setting takes precedence over the user group settings. Leave it blank to allow for the attachment types in accordance with the user group settings.',
      58 => 'forums_edit_posts_allowfeed',
      59 => 'Activer la diffusion & flux',//'允许发送广播和动态',// 'Enable broadcast & feeds',
      60 => 'Si est activé, ce qui permet aux utilisateurs d\'envoyer et de la diffusion des flux par défaut. Diffusion d\'envoyer l\'utilisateur peut supprimer l\'affichage. Pour Privacy Forum a recommandé de mettre au No.',//'开启后默认允许用户发送广播和动态，广播是否发送用户可以在发帖时去掉，隐私版块建议设为否。', //  'If is turned on, this allows users to send broadcast and feeds by default. Broadcast whether to send the user can remove the posting. For Privacy Forum it recommended to set to No.',
      61 => 'forums_edit_posts_commentitem',
      62 => 'Préréglage de la liste générale du Commentaire',//'普通主题点评预置观点', // 'Preset the General Comment List',
      63 => 'Entrez un commentaire par ligne. Si une ligne vide présente dans une liste, il sera affiché un élément vide dans le fond du menu déroulant comme un autre point de vue. Ce paramètre prend effet uniquement si les commentaires activation à tous. S\'il est vide, le paramètre global "communs fil de discussion prédéfinis les Commentaires "seront utilisés".',//'每个观点一行。如存在空行，空行下方的内容将显示在下拉菜单中作为备选观点。只有开启帖子点评功能后本设置才生效，如留空表示使用全局的“普通主题点评预置观点”设置', //  'Enter one comment per line. If an empty line presents in a list, it will be displayed an empty item in a bottom of the drop-down menu as an alternative point of view. This setting take effect only if comments enabled at all. If empty, the global setting "Common thread preset Comments" will be used.',
      64 => 'forums_edit_posts_noantitheft',
      65 => 'Fermer Anti-collecte', //  '关闭防采集',
      66 => 'Désactiver la fonction anti-collecte peut sauver une petite quantité de ressources du serveur, la zone de la classe d\'irrigation au spam proposé hors Anti-collecte.', //   '关闭防采集功能可节省少量服务器资源，建议灌水类版块关闭防采集功能。', 
      67 => 'forums_edit_posts_noforumhidewater',
      68 => 'Fermer cacher décalcomanie', //  '关闭隐藏水帖', 
      69 => 'Extraits peuvent être activés individuellement hors fonction décalcomanie cachée de la section de la classe d\'irrigation au spam.', //  '可单独关闭本版的隐藏水帖功能，适用于灌水类版块。', 
      70 => 'forums_edit_posts_noforumrecommend',
      71 => 'Fermer Recommander réponse', //   '关闭推荐回复', 
      72 => 'Extraits peuvent être activés individuellement hors fonction de réponse recommandée pour la section de la classe d\'irrigation au spam.', //  '可单独关闭本版的推荐回复功能，适用于灌水类版块。',
    ),
  ),
  192 =>
  array (
    'index' =>
    array (
      'Gestion du Forum' => 'action=forums',//'版块管理' // 'Forum Management'
      'Types de pièces jointes' => 'action=forums&operation=edit&anchor=attachtype',//'附件类型' // 'Attach type'
    ),
    'text' =>
    array (
      0 => 'Gestion du Forum &raquo; Types de pièces jointes',//'版块管理 &raquo; 附件类型', // 'Forum Management &raquo; Attachment types',
      1 => 'forums_edit_attachtype_tips',
      2 => 'Cette fonctionnalité permet de limiter la taille maximale des pièces jointes de type spécifié sur le forum. Lorsque l\'ensemble de taille inférieure à la taille maximale autorisée pour le groupe d\'utilisateurs, la limite du type spécifié de taille des pièces jointes prévaudra ce paramètre. Vous pouvez définir la taille maximale de certains types de pièces jointes, fixées à 0 pour désactiver le chargement de certains type de pièce jointe. Laissez vide pour utiliser le réglage du format de type de pièce jointe globale.',//'本功能可限定本版块某特定类型附件的最大尺寸，当这里设定的尺寸小于用户组允许的最大尺寸时，指定类型的附件尺寸限制将按本设定为准。你可以设置某类附件最大尺寸为 0 以整体禁止这类附件被上传。此处设置留空则使用全局的“附件类型尺寸”设置。', // 'This feature can limit the maximum size of specified type attachments at the forum. When the set of size less than the maximum size allowed for the user group, the attachment size limit of the specified type will prevail this setting. You can set the maximum size of certain types of attachments, set to 0 for disable uploading of some attachment type. Leave empty to use the global attachment type size setting.',
    ),
  ),
  193 =>
  array (
    'index' =>
    array (
      'Forums' => 'action=forums',//'版块管理', // 'Forums' 
      'Étendue stratégie d\'échange de points' => 'action=forums&operation=edit&anchor=credits',//'扩展积分增减策略', // 'Extended points exchange strategy'
    ),
    'text' =>
    array (
      0 => 'Gestion des Forum &raquo; Extended points exchange strategy',//'版块管理 &raquo; 扩展积分增减策略', // 'Forums &raquo; Extended points exchange strategy', 
      1 => 'forums_edit_tips',
      2 => 'Les paramètres suivants ne sont pas héritées, qui n\'est efficace que dans le forum actuel n\'aura pas d\'impact sur​les sous-forums inférieurs.',//'以下设置没有继承性，即仅对当前版块有效，不会对下级子版块产生影响。', // 'The following settings are not inherited, that is effective only to the current forum will not impact on the lower sub-forums.',
      3 => 'forums_edit_credits_policy',
      4 => 'Étendue stratégie d\'échange de points',//'扩展积分增减策略', //  'Extended points exchange strategy',
    ),
  ),
  194 =>
  array (
    'index' =>
    array (
      'Forums' => 'action=forums',//'版块管理', //  'Forums'
      'Types config de topic' => 'action=forums&operation=edit&anchor=threadtypes',//'主题分类', // 'Thread types config'
    ),
    'text' =>
    array (
      0 => 'Forums &raquo; Thread types config',//'版块管理 &raquo; 主题分类', // 'Forums &raquo; Thread types config',
      1 => 'forums_edit_tips',
      2 => 'Les paramètres suivants ne sont pas héritées, qui n\'est efficace que dans le forum actuel n\'aura pas d\'impact sur les sous-forums inférieurs.',//'以下设置没有继承性，即仅对当前版块有效，不会对下级子版块产生影响。', //  The following settings are not inherited, that is effective only to the current forum will not impact on the lower sub-forums.
      3 => 'forums_edit_threadtypes_config',
      4 => 'Types config de topic',//'主题分类', // 'Thread types config',
      5 => 'forums_edit_threadtypes_status',
      6 => 'Activer les types de sujets',//'启用主题分类',// 'Enable thread types',
      7 => 'Réglez activé dans ce forum types de sujets. Vous devez régler les options de types appropriés pour activer cette fonctionnalité',//'设置是否在本版块启用主题分类功能，你需要同时设定相应的分类选项，才能启用本功能', //  'Set enabled in this forum topic types. You need to set the appropriate types options to enable this feature',
      8 => 'forums_edit_threadtypes_required',
      9 => 'Catégorie sujet désiré',//'发帖必须归类', //  'Thread category Required',
      10 => 'Que ce soit pour forcer l\'utilisateur à choisir une catégorie quand une nouvelle discussion est publié.',//'是否强制用户发表新主题时必须选择分类', // 'Whether to force user to choose a category when a new thread published.',
      11 => 'forums_edit_threadtypes_listable',
      12 => 'Autoriser Parcourir par catégorie',//'允许按类别浏览', //  'Allow Browse by category',
      13 => 'Les utilisateurs peuvent parcourir le contenu par discussion Filtre des catégories',//'用户是否可以按照主题分类筛选浏览内容', //  'Users can browse content by thread Category Filter',
      14 => 'forums_edit_threadtypes_prefix',
      15 => 'Catégorie préfixe',//'类别前缀',// 'Category Prefix',
      16 => 'Ce préfixe est affiché avant le nom de la catégorie',//'是否在主题前面显示分类的名称', // 'This prefix is displayed before the category name',
      17 => 'forums_edit_threadtypes',
      18 => 'Types de Sujets',//'主题分类', // 'Thread types',
      19 => 'Vous pouvez activer un des types existants ou ajouter un nouveau type de thème à utiliser dans le forum. Dans le cas de nombreux types, nous recommandons d\'utiliser uniquement des types importants en utilisant le mode "plat de panneau d\'affichage", mais d\'autres types pouvons utiliser l\'approche "listes déroulantes"',//'你可以启用已有的主题分类或添加新主题分类应用于本版块，在分类很多的情况下，建议只有重要的分类使用“平板显示”方式，更多的分类使用“下拉显示”方式', //  'You can enable an existing types or add a new topic type to use in the forum. In case of many types, we recommend to use only important types using the "flat panel display" mode, but additional types may use the "Drop down" approach',
    ),
  ),
  195 =>
  array (
    'index' =>
    array (
      'Forums' => 'action=forums',//'版块管理', // 'Forums' 
      'Catégories de topics' => 'action=forums&operation=edit&anchor=threadsorts',//'分类信息', // 'Thread categories'
    ),
    'text' =>
    array (
      0 => 'Forums &raquo; Thread categories',//'版块管理 &raquo; 分类信息', // 'Forums &raquo; Thread categories',
      1 => 'forums_edit_tips',
      2 => 'Les paramètres suivants ne sont pas héritées, qui n\'est efficace que dans le forum actuel n\'aura pas d\'impact sur les sous-forums inférieurs.',//'以下设置没有继承性，即仅对当前版块有效，不会对下级子版块产生影响。',// 'The following settings are not inherited, that is effective only to the current forum will not impact on the lower sub-forums.',
      3 => 'forums_edit_threadsorts',
      4 => 'Catégories de topics',//'分类信息',// 'Thread categories',
      5 => 'forums_edit_threadsorts_status',
      6 => 'Activer catégories du forum',//'启用分类信息',// 'Enable forum categories',
      7 => 'forums_edit_threadtypes_required',
      8 => 'Catégorie Sujet Important',//'发帖必须归类',// 'Thread category Required',
      9 => 'Que ce soit pour forcer un utilisateur à choisir la catégorie quand une nouvelle discussion est publié.',//'是否强制用户发表新主题时必须选择分类',// 'Whether to force a user to choose category when a new thread published.',
      10 => 'forums_edit_threadtypes_prefix',
      11 => 'Catégorie préfixe',//'类别前缀',// 'Category Prefix',
      12 => 'Ce préfixe est affiché avant le nom de la catégorie',//'是否在主题前面显示分类的名称',//  'This prefix is displayed before the category name',
      13 => 'forums_edit_threadsorts_default',
      14 => 'Activer pour montrer des catégories par défaut',//'启用默认显示分类', // 'Enable to show default categories',
      15 => 'Si activée choisir les catégories ci-dessous',//'是否启用默认显示分类，如果启用请在下面的分类信息里面选择', // 'If enabled choose the categories below',
    ),
  ),
  196 =>
  array (
    'index' =>
    array (
      'Forums' => 'action=forums',//'版块管理', //  'Forums' 
      'Forum permissions' => 'action=forums&operation=edit&anchor=perm',//'版块权限', //  'Forum permissions'
    ),
    'text' =>
    array (
      0 => 'Forums &raquo; Forum permissions',//'版块管理 &raquo; 版块权限', // 'Forums &raquo; Forum permissions',
      1 => 'forums_edit_tips',
      2 => 'Les paramètres suivants ne sont pas héritées, qui n\'est efficace que dans le forum actuel n\'aura pas d\'impact sur les sous-forums de niveaux inférieurs.',//'以下设置没有继承性，即仅对当前版块有效，不会对下级子版块产生影响。',// 'The following settings are not inherited, that is effective only to the current forum will not impact on the lower sub-forums.',
      3 => 'forums_edit_perm_forum',
      4 => 'Forum permissions',//'版块权限', //'Forum permissions', 
      5 => 'Si toutes les Permissions sélectionnées, les significations suivantes: afficher les pages, tous les groupes d\'utilisateur est autorisé à consulter les messages du forum, de nouveaux sujets, en plus de parcourir le groupe d\'utilisateurs ont l\'autorisation d\'affichage; Laissez une réponse, en plus de naviguer, le groupe d\'utilisateurs les autorisations nécessaires pour répondre, afficher des pièces jointes, tous les groupes d\'utilisateurs des autorisations de télécharger de / visualiser les pièces jointes, télécharger des pièces jointes, en plus de parcourir, le groupe utilisateur est autorisé à télécharger des pièces jointes; Télécharger des images, en plus de parcourir, le groupe utilisateur a la permission pour télécharger des images.',//'某权限如果全部未选则表示如下含义:　　浏览版块，全部用户组具有浏览版块帖子权限；发新话题，除游客以外的用户组具有发帖权限；发表回复，除游客以外的用户组具有回复权限；查看附件，全部用户组具有下载/查看附件权限；上传附件，除游客以外的用户组具有上传附件权限；上传图片，除游客以外的用户组具有上传图片权限', //  'If not all permissions selected, the following meanings: View pages, all user group has permission to browse the forum posts; New topics, in addition to browse the user group have posting permission; Leave a response, in addition to browse, the user group have permissions to reply; View attachments, all the user groups have permissions to download/view attachments; Upload attachments, in addition to browse, the user group has permission to upload attachments; Upload images, in addition to browse, the user group has permission to upload images.',
      6 => 'forums_edit_perm_passwd',
      7 => 'Accès Mot de passe',//'访问密码', // 'Access Password',
      8 => 'Lorsque vous définissez un mot de passe, l\'utilisateur doit entrer le mot de passe pour accéder à ce forum',//'当你设置密码后，用户必须输入密码才可以访问到此版块',// 'When you set a password, the user must enter password to access this forum',
      9 => 'forums_edit_perm_users',
      10 => 'Accès pour les utilisateurs spécifiés',//'访问用户', // 'Access for specified users',
      11 => 'Activer accès uniquement pour les utilisateurs spécifiés. Saisissez un Nom d\'utilisateur par ligne',//'限定只有列表中的用户可以访问本版块，每行填写一个用户名', //  'Enable acces only for specified users. Enter one user name per line',
      12 => 'forums_edit_perm_medal',
      13 => 'Accès médailles en posséssion',//'拥有勋章', // 'Access for medal having',
      14 => 'Pour l\'accès de ce forum un utilisateur doit avoir une médaille spécifique',//'用户必须拥有指定的勋章才可访问此版块', // 'For access this Forum a user must have a specified medal',
      15 => 'forums_edit_perm_forum',
      16 => 'Permissions Forum',//'版块权限', //  'Forum permissions',
      17 => 'Si toutes les Permissions sélectionnées, les significations suivantes: afficher les pages, tous les groupes d\'utilisateur est autorisé à naviguer les messages du forum, de nouveaux sujets, en plus de naviguer le groupe d\'utilisateurs ont l\'autorisation d\'affichage; Laissez une réponse, en plus de naviguer, le groupe d\'utilisateurs les autorisations nécessaires pour répondre, afficher des pièces jointes, tous les groupes d\'utilisateurs des autorisations de télécharger de / visualiser les pièces jointes, télécharger des pièces jointes, en plus de parcourir, les groupe utilisateur est autorisé à télécharger des pièces jointes; Télécharger des images, en plus de parcourir, le groupe utilisateur a la permission pour télécharger des images.',//'某权限如果全部未选则表示如下含义:　　浏览版块，全部用户组具有浏览版块帖子权限；发新话题，除游客以外的用户组具有发帖权限；发表回复，除游客以外的用户组具有回复权限；查看附件，全部用户组具有下载/查看附件权限；上传附件，除游客以外的用户组具有上传附件权限；上传图片，除游客以外的用户组具有上传图片权限',//  'If not all permissions selected, the following meanings: View pages, all user group has permission to browse the forum posts; New topics, in addition to browse the user group have posting permission; Leave a response, in addition to browse, the user group have permissions to reply; View attachments, all the user groups have permissions to download/view attachments; Upload attachments, in addition to browse, the user group has permission to upload attachments; Upload images, in addition to browse, the user group has permission to upload images.',
      18 => 'forums_edit_perm_formula',
      19 => 'Permissions formula',//'权限表达式设置', // 'Permissions formula',
      20 => 'Lorsque vous définissez la formule d\'autorisation, uniquement calculés avec ce membres d\'expression peuvent naviguer sur le forum. Par exemple, "posts > 100 et extcredits1 > 10" ressources "Nombre de Posts de l\'utilisateur > 100 et l\'utilisateur Prestige > 10". Le format de date est "{YMD}", tel que "{2009-10-1}". IP format: "{xxxx}", vous pouvez saisir l\'adresse exacte, ou seulement le début IP, tel que"{10.0.0.1 }", "{ 192.168.0}"',//'当你设定了权限表达式后，只有符合此表达式的会员才可以浏览本版块。如 "posts > 100 and extcredits1 > 10" 表示 "发帖数 > 100 并且 威望 > 10"日期格式 "{Y-M-D}"，如 "{2009-10-1}"。IP 格式 "{x.x.x.x}"，既可输入完整地址，也可只输入 IP 开头，如 "{10.0.0.1}"、"{192.168.0}"', //  'When you set the permission formula, only calculated with this expression members can browse the forum. For example, "posts > 100 and extcredits1 > 10" means "Number of User Posts > 100 and User Prestige > 10". Date format is "{YMD}", such as "{2009-10-1}". IP format: "{xxxx}", you can enter the full address, or only the IP beginning, such as "{10.0.0.1 }", "{ 192.168.0}"',
      21 => 'forums_edit_perm_spview',
      22 => 'Groupes d\'utilisateurs illimités',//'不受限制的用户组', //  'Unlimited user groups',
      23 => 'forums_edit_perm_formulapermmessage',
      24 => 'Aucune Autorisation D\4invite ',//'无权限访问时的提示信息', //'No permission prompt', 
      25 => 'Invite personnalisé environ aucun droit d\'accès à l\'information. Si les autorisations ne suffisent pas, une expression de la formule sera affiché',//'自定义无权限时的提示信息，如不填写则无权限时将显示权限表达式的公式',// 'Custom prompt about no rights to access the information. If permissions not enough, a formula expression will be displayed',
    ),
  ),
  197 =>
  array (
    'index' =>
    array (
      'Admin équipe' => 'action=founder',//'后台管理团队', //  'Admin Team'
    ),
    'text' =>
    array (
      0 => 'Admin Permissions d\'équipe',//'后台管理团队', //'Admin Team permissions',
      1 => 'home_security_founder',
      2 => 'Vous pouvez créer une variété de rôles de l\'équipe affectée à votre équipe de gestion du travail. Pour chaque membre, qu\'ils gèrent de différentes affaires des sites, par exemple "sous-chef" peut effectuer toutes les autorisations de fond, à l\'exclusion du "Capacité du Fondateur (webmaster)".',//'你可以制定多种团队职务分配给你网站管理团队的各个成员，让他们管理网站的不同事务“副站长”拥有除“创始人(站长)”专有权限以外的所有后台权限，仅次于“创始人(站长)”',// 'You can create a variety of team roles assigned to your job site management team. For each member, let them manage of different sitese affairs, for example "deputy chief" can perform all the background permissions, excluding the "founder (webmaster)" competence.',
    ),
  ),
  198 =>
  array (
    'index' =>
    array (
      'Paramètres du Groupe' => 'action=group&operation=setting',//'群组设置', //'Group Settings'
    ),
    'text' =>
    array (
      0 => 'Paramètres du Groupe',//'群组设置',  // 'Group Settings',
      1 => 'groups_setting_basic',
      2 => 'Réglages de base',//'基本设置',  // 'Basic settings',
      3 => 'groups_setting_basic_status',
      4 => 'Désactiver les fonctions du Groupe',//'是否开启群组功能', //  'Disable Group Functions',
      5 => 'groups_setting_basic_mod',
      6 => 'Nouveaux groupes Premoderate',//'审核新群组', // 'Premoderate new groups',
      7 => 'Si l\'Administrateur nécessité de vérifier de nouveaux groupes créés',//'管理员建立群组时不需要审核', // 'If Administrator need to audit new created groups',
      8 => 'groups_setting_basic_iconsize',
      9 => 'Groupe icône taille du fichier (en Ko)',//'群组图标文件大小(单位：KB)',
      10 => 'Définissez la taille maximale d\'un fichier icône du groupe, Défini sur 0 ou laisser vide pour ne pas limiter',//'设置群组图标文件的最大尺寸，0 或留空为不限制', // 'Set the maximum size of a group icon file, Set to 0 or leave blank for not limit',
      11 => 'groups_setting_basic_recommend',
      12 => 'Groupes recommandés',//'推荐群组', //  'Recommended Groups',
      13 => 'Remplissez ID du groupe pour recommander dans la page d\'Accueil, séparés par une virgule ",". Au maximum de 8 groupes, tels qu\' inférieure à la plupart des points seront automatiquement ajouter au groupe. exemple:"23,56,983"',//'填写要推荐到群组首页的群组 ID，以半角逗号 "," 隔开，最多显示 8 个，如不足会自动补充积分最高的群组。例：“23,56,983”', //  'Fill in group ID to recommend at the home page, separated with a comma ",". Maximum 8 groups, such as less than the most points will automatically add the group. Example: "23,56,983"',
      14 => 'groups_setting_admingroup',
      15 => 'Sélectionnez le groupe Gestionnaire des utilisateurs (cet utilisateur aura des privilèges administratifs au sein du groupe et au forum correspondant)',//'选择管理用户组(在群组中具有与论坛中相对应的管理权限)', //  'Select the User Group Manager (this user will have administrative privileges in the group and corresponding forum)',
      16 => 'forums_edit_posts_allowfeed',
      17 => 'Activer la diffusion et des flux',//'允许发送广播和动态', // 'Enable broadcast & feeds',
      18 => 'Si est activé, ce qui permet aux utilisateurs d\'envoyer et de la diffusion des flux par défaut. Diffusion d\'envoyer , l\'utilisateur peut supprimer le dépôt. Pour le Forum sur la confidentialité a recommandé de fixer à Non.',//'开启后默认允许用户发送广播和动态，广播是否发送用户可以在发帖时去掉，隐私版块建议设为否。', //  'If is turned on, this allows users to send broadcast and feeds by default. Broadcast whether to send the user can remove the posting. For Privacy Forum it recommended to set to No.',
    ),
  ),
  199 =>
  array (
    'index' =>
    array (
      'newgroup_userperm' => 'action=group&operation=userperm',
    ),
    'text' =>
    array (
      0 => 'newgroup_userperm',
      1 => 'admingroup_edit_threadperm',
      2 => 'Autorisations  Gestion du Topic',//'主题管理权限',  //  'Thread management permissions',
      3 => 'admingroup_edit_digest_thread',
      4 => 'Autoriser Sujet Résumé',//'允许精华主题', //  'Allow thread digest',
      5 => 'Réglez autorisations Type de Résumé',//'设置是否允许精华管理范围内主题的级别', //'Set allowed digest type', 
      6 => 'admingroup_edit_postperm',
      7 => 'Post des privilèges de gestion',//'帖子管理权限', //  'Post management privileges',
      8 => 'admingroup_edit_modcpperm',
      9 => 'Privilèges Panel Modérateur',//'管理面板权限', // 'Moderator panel privileges',
      10 => 'group_userperm_others',
      11 => 'Autres Permissions',//'其它权限', // 'Other permissions',
    ),
  ),
  200 =>
  array (
    'index' =>
    array (
      'Niveau du Groupe' => 'action=group&operation=level',//'群组等级',  // 'Group Level'
    ),
    'text' =>
    array (
      0 => 'Niveau du Groupe',//'群组等级',  // 'Group Level',
      1 => 'group_level_tips',
      2 => 'Ne pas ajouter beaucoup de niveaux du Groupe. Quand un groupe est autorisé à du post mais l\'utilisateur n\'a pas assez de points, comme utilisateur verra une mise en garde, possible parce que les principaux points débroussailles le groupe. Niveau par défaut taille des icônes est de 25 X 25 px.',//'群组等级不宜设置过多。当允许群组内发帖等操作影响用户积分时应谨慎，有可能通过群主刷积分。默认等级图标大小是25 X 25像素。', //  'Do not add a lot of Group levels. When a group is allowed to post but the user has no enough points, such user will see a caution, possible because of the group main brush points. Default level icon size is 25 X 25 px.',
    ),
  ),
  201 =>
  array (
    'index' =>
    array (
      'Groupe de Gestion' => 'action=group&operation=manage',//'群组管理',  // 'Group Management'
    ),
    'text' =>
    array (
      0 => 'Groupe de Gestion',//'群组管理', //  'Group Management',
      1 => 'groups_manage_name',
      2 => 'Titre',//'群组名称', //  'Title',
      3 => 'groups_manage_id',
      4 => 'Groupe ID',//'群组 ID', //  'Group ID',
      5 => 'groups_editgroup_category',
      6 => 'Catégorie du Groupe',//'群组分类', // 'Group Category',
      7 => 'groups_manage_membercount',
      8 => 'Nombre de Membres',//'成员数', //  'Number of Members',
      9 => 'groups_manage_threadcount',
      10 => 'Nombre de Sujets',//'主题数', //  'Number of Threads',
      11 => 'groups_manage_replycount',
      12 => 'Réponses',//'回复数', // 'Replies',
      13 => 'groups_manage_createtime',
      14 => 'Temps de publication',//'创建时间', // 'Publish time',
      15 => 'groups_manage_updatetime',
      16 => 'Mettre à jour le temps',//'最后更新时间', // 'Update time',
      17 => 'groups_manage_founder',
      18 => 'Propriétaire',//'创建者', // 'Owner',
      19 => 'groups_manage_founder_uid',
      20 => 'UID Propriétaire',//'创建者 UID', // 'Owner UID',
    ),
  ),
  202 =>
  array (
    'index' =>
    array (
      'Magies' => 'action=magics',//'道具中心', // 'Magics'
    ),
    'text' =>
    array (
      0 => 'Magies',//'道具中心', // 'Magics',
      1 => 'magics_tips',
      2 => 'Pour installer une nouvelle magie, le script magique doit être chargé à la source /class/ magic / directory et puis vous pouvez l\'utiliser dans la liste suivante. Avant de concevoir de nouveaux magie Plug-in développeurs ont à lire le contenu "Discuz! Technique de la bibliothèque".',//'安装新的道具，需将道具脚本程序上传到 source/class/magic/ 目录，然后即可在以下列表中使用了。插件开发人员在设计新的道具前请务必仔细阅读《Discuz! 技术文库》中的内容。',    //  'For install a new magic, the magic script must be uploaded to source/class/magic/ directory, and then you can use it in the following list. Before designing of new magic Plug-in developers have to read the "Discuz! Technical Library" content.',
      3 => 'magics_config_open',
      4 => 'Activé Centre Magique',//'是否打开道具中心', // 'Enable Magic center',
      5 => 'Que ce soit pour autoriser les fonctions Centre Magique',//'是否开启道具中心功能', //  'Whether to allow the Magic Center functions',
      6 => 'magics_config_discount',
      7 => 'Réduction de la magie de rappel',//'道具回收价格折扣', // 'Magic return discount',
      8 => 'Réglez le retour magique au prix du système avec une réduction, en pourcentage du prix initial. Par exemple, si la valeur est de 85, alors le système va fixer le prix de retour à 85% du prix de la magie originale. Il est recommandé de ne pas dépasser 100, pour prévenir efficacement les points .. Vide ou 0 indique que la fonction de retour magie est désactivé.',//'设置道具系统回收价格折扣，此值为百分比，例如如果此值为 85，则系统将以道具原价 85% 的价格回收，建议不超过 100，有效防止刷积分。留空或者 0 表示不开启道具回收功能',   //  'Set the magic return to system price with a discount, as a percentage of original price. For example, if the value is 85, then the system will set the return price to 85% of original magic price. It is recommended not to exceed 100, to effectively prevent points brush. Blank or 0 indicates that the magic return function is disabled.',
    ),
  ),
  203 =>
  array (
    'index' =>
    array (
      'Génère tous' => 'action=makehtml&operation=all', // '生成全部'    
    ),
    'text' =>
    array (
      0 => 'Génère tous', //   '生成全部', // 
      1 => 'makehtml_tips_all',
      2 => 'Générer l\'heure de début spécifiée après l\'article publié fichier HTML généré après l\'heure de début spécifiée publié des articles canaux HTML des fichiers HTML les fichiers générés portail d\'accueil', // '生成指定起始时间以后发布的文章的HTML文件生成指定起始时间以后发布过文章的频道HTML文件生成门户首页的HTML文件',  
      3 => 'start_time',
      4 => 'Heure de départ',  // '起始时间', 
    ),
  ),
  204 =>
  array (
    'index' =>
    array (
      'html' => 'action=makehtml&operation=makehtmlsetting',
      'Génère HTML' => 'action=makehtml&operation=makehtmlsetting',  //'生成HTML'   
    ),
    'text' =>
    array (
      0 => 'html &raquo; Génère HTML', //  'html &raquo; 生成HTML',
      1 => 'setting_functions_makehtml',
      2 => 'Génère HTML', // '生成HTML', 
      3 => 'setting_functions_makehtml_extendname',
      4 => 'Extension de fichier statique', //  '静态文件扩展名', 
      5 => 'Tous extension de fichier statique, la valeur par défaut est html', //  '所有静态文件的扩展名，默认为html',
      6 => 'setting_functions_makehtml_articlehtmldir',
      7 => 'Article HTML magasin de répertoire racine', //  '文章HTML存放根目录',
      8 => 'Article fichiers statiques stockées dans le répertoire racine, sous-répertoire avec la même liste de canal de la racine du site a commencé à la fin n\'ont pas besoin d\'ajouter/, Est vide puis stocké dans un dossier sous le canal actuel', //  '文章静态文件存放的根目录，子目录与频道目录相同，从网站的根目录开始，结尾不需要添加/，为空则存放在当前频道的文件夹下',
      9 => 'setting_functions_makehtml_htmldirformat',
      10 => 'Structure du répertoire', // '目录结构', 
      11 => 'Répertoire de stockage de documents structure de répertoire subordonné de fichier statique, 0:/Ym/, 1:/Ym/d/, 2:/Y/m/, 3:/Y/m/d/', // '静态文件文件存放目录的下级目录结构, 0:/Ym/, 1:/Ym/d/, 2:/Y/m/, 3:/Y/m/d/',  
      12 => 'setting_functions_makehtml_topichtmldir',
      13 => 'Répertoire de stockage HTML thématique', //  '专题HTML存放目录',
      14 => 'Répertoire de stockage des fichiers HTML thématique, démarre à partir du répertoire racine du site, à la fin n\'a pas besoin d\'ajouter/, Un fichier HTML vide n\'est pas généré sujets', //  '专题HTML文件存放目录，从网站的根目录开始，结尾不需要添加/，为空则不生成专题HTML文件',
      15 => 'setting_functions_makehtml_indexname',
      16 => 'Accueil Portail nom de fichier statique', //  '门户首页静态文件名',
      17 => 'Génère un portail statiques Accueil nom de fichier par défaut à l\'index, sans extension', // '生成门户首页的静态文件名，默认为index，不带扩展名', 
    ),
  ),
  205 =>
  array (
    'index' =>
    array (
    ),
    'text' =>
    array (
      0 => 'setting_functions_makehtml_cleanhtml',
      1 => 'Besoin de nettoyer le code HTML', //  '需要清理的HTML',
      2 => 'Choisir nécessaire pour nettoyer le code HTML', //  '选择需要要清理的HTML',
    ),
  ),
  206 =>
  array (
    'index' =>
    array (
      'Médailles' => 'action=medals',//'勋章中心', // 'Medals'
    ),
    'text' =>
    array (
      0 => 'Médailles',//'勋章中心', //  'Medals',
      1 => 'medals_tips',
      2 => 'Cette fonction peut être utilisée pour définir les informations de médailles décernées par les utilisateurs. S\'il vous plaît remplir le nom du fichier image de la médaille, et télécharger le fichier image correspondant à la static/image/common/ directory.',//'本功能用于设置可以颁发给用户的勋章信息，勋章图片中请填写图片文件名，并将相应图片文件上传到 static/image/common/ 目录中。', //  'This feature can be used to set the medals information awarded by users. Please fill out the Medal image file name, and upload the corresponding image file to the static/image/common/ directory.',
    ),
  ),
  207 =>
  array (
    'index' =>
    array (
      'Alias' => 'action=members&operation=repeat',//'马甲', // 'Aliases' 
    ),
    'text' =>
    array (
      0 => 'Alias',//'马甲', //  'Aliases',
      1 => 'members_search_repeatuser',
      2 => 'Nom d\'utilisateur',//'用户名',// 'User name',
      3 => 'members_search_uid',
      4 => 'UID Utilisateur',//'用户 UID',//  'User UID',
      5 => 'members_search_repeatip',
      6 => 'Adresse IP',//'IP 地址',// 'IP address',
    ),
  ),
  208 =>
  array (
    'index' =>
    array (
      'Ajouter Utilisateur' => 'action=members&operation=add',//'添加用户',// 'Add User'
    ),
    'text' =>
    array (
      0 => 'Ajouter Utilisateur',//'添加用户', //  'Add User',
      1 => 'username', 
      2 => 'Nom d\'utilisateur',//'用户名', //  'User name',
      3 => 'password',
      4 => 'Mot de Passe',//'密　码', //  'Password',
      5 => 'email',
      6 => 'Email',
      7 => 'usergroup',
      8 => 'Groupe Utilisateur',//'用户组', //  'User Group',
      9 => 'members_add_email_notify',
      10 => 'Envoyer une notification à l\'adresse ci-dessus',//'发送通知到上述地址', //  'Send notification to the above address',
    ),
  ),
  209 =>
  array (
    'index' =>
    array (
      'Modifier les membres des groupes d\'utilisateurs' => 'action=members&operation=group',//'编辑会员用户组', //  'Edit user group members'
    ),
    'text' =>
    array (
      0 => 'Modifier les membres des groupes d\'utilisateurs',//'编辑会员用户组', //  'Edit user group members',
      1 => 'usergroup',
      2 => 'Groupe Utilisateur',//'用户组', //   'User Group',
      3 => 'members_group_group',
      4 => 'Groupe Utilisateur respectif',//'所属用户组', // 'Respective user group',
      5 => 'members_group_related_adminid',
      6 => 'Groupes de gestion respectifs',//'所属管理组',//  'Respective management groups',
      7 => 'members_group_validity',
      8 => 'Expiration des groupes  Utilisateurs',//'用户组有效期',// 'User group expiration', 
      9 => 'Pour régler l\'expiration du groupe d\'utilisateur actuel, entrez la date limite de groupe d\'utilisateurs. Laissez ce champ vide pour aucune restriction de date',//'如需设定当前用户组的有效期，请输入用户组截止日期，留空为不做过期限制',  //  'For setting the expiration of the current user group, enter the user group deadline. Leave it blank for no date restrictions',
      10 => 'members_group_orig_adminid',
      11 => 'Après expiration de changer l\'Utilisateur du groupe',//'过期后用户组变为',// 'After expired change the user group to',
      12 => 'members_group_orig_groupid',
      13 => 'Après expiration de changer le groupe de gestion',//'过期后管理组变为', //  'After expired change the management group to',
      14 => 'members_group_extended',
      15 => 'Groupe Utilisateurs élargies',//'扩展用户组', // 'Extended user group',
      16 => 'Note: format valide est aaaa-mm-jj. Si laissé vide, le groupe d\'utilisateurs par défaut n\'expire pas automatiquement',//'注意: 有效期格式 yyyy-mm-dd，如果留空，则默认该扩展用户组不自动过期',  //  'Note: valid format is yyyy-mm-dd. If left blank, the default user group does not expired automatically',
      17 => 'members_edit_reason',
      18 => 'Modifier la raison',//'变更理由',  //   'Edit reason',
      19 => 'members_group_ban_reason',
      20 => 'Raison du Ban',//'禁止/解禁用户的理由',  // 'Ban reason',
      21 => 'Si vous entrez le motif de fonctionnement lorsque interdit un groupe d\'utilisateurs ou à un utilisateur, le système enregistre cette raison dans le journal pour un visionnage ultérieur',//'如果你通过用户组设定禁止或解除禁止该用户，请输入操作理由，系统将把理由记录在用户禁止记录中，以供日后查看',  // 'If you enter the operation reason when banned a user group or a user, the system will record this reason into the log for later viewing',
    ),
  ),
  210 =>
  array (
    'index' =>
    array (
      'Modifier les points de l\'utilisateur' => 'action=members&operation=credit',//'编辑用户积分',  //  'Edit User points'
    ),
    'text' =>
    array (
      0 => 'Modifier les points de l\'utilisateur',//'编辑用户积分', // 'Edit User points',
      1 => 'members_credit_tips',
      2 => 'Discuz! avoir huit types  étendus paramètres de points utilisateur, et ont permis pour modifier uniquement en matière d\'intégration. Modification des points utilisateur donnera lieu à modifier le score total utilisateur et le niveau utilisateur normal. Alors svp veuillez définir soigneusement les points.',//'Discuz! 支持对用户 8 种扩展积分的设置，只有被启用的积分才允许你进行编辑。修改用户的某项积分会造成该用户总积分的变化，从引起普通会员等级的变化，因此请仔细设置各项积分。',  //  'Discuz! have eight kinds of extended user points settings, and enabled to edit only in the integration. Modifying the user points will result in changing the user total score and the normal user level. So please set the points carefully.',
      3 => 'members_edit_reason',
      4 => 'Modifier la raison',//'变更理由', // 'Edit reason',
      5 => 'members_credit_reason',
      6 => 'Raison de modifications des points utilisateur',//'修改用户积分的理由', //  'Modify user points reason',
      7 => 'Si vous avez modifié les points personnels, veuillez saisir les raisons du fonctionnement, le système permettra d\'économiser la raison enregistrés dans un journal pour les visualiser plus tard',//'如果你修改了用户的积分资料，请输入操作理由，系统将把理由记录在用户评分记录中，以供日后查看',  // If you modified the user points, please enter the operation reason, the system will save the reason recorded in a log for later viewing
    ),
  ),
  211 =>
  array (
    'index' =>
    array (
      'Modifier les autorisations des utilisateurs' => 'action=members&operation=access',//'编辑用户权限', //  'Edit User Permissions'
    ),
    'text' =>
    array (
      0 => 'Modifier les autorisations des utilisateurs',//'编辑用户权限',  //  'Edit User Permissions',
      1 => 'members_access_tips',
      2 => 'Droits d\'accès au forum sont basés sur les autorisations d\'utilisateur et les autorisations du forum. Si vous réglez un état de non-défaut, le groupe d\'utilisateurs ne sera pas fixer des limites. Pour supprimer un utilisateur des privilèges dans un décor de bord, vous avez seulement besoin de ses permissions de ce forum dans tous les paramètres par défaut, un utilisateur peut modifier les permissions de ce forum, il vous suffit d\'ajouter ses paramètres d\'autorisation',//'用户在版块中的权限是基于用户和站点间的权限设定，如果设置为非默认状态，将不受用户组的设定限制。删除某个用户在某版块的特殊权限设置，你只需要将他在这个版块中的权限全部设置为默认即可修改某个用户在这个版块的权限，你只需要重新添加他的权限设置即可',  // 'Users access rights in the forum are based on the user permissions and the forum permissions. If you set a non-default state, the user group will not set limits. For remove a user privileges in a board setting, you only need to his permissions in this forum in all settings to default a user can modify the permissions in this forum, you only need to add his permissions settings',
      3 => 'members_access_add_forum',
      4 => 'Select Forum',//'选择版块', //   'Select Forum',
      5 => 'Sélectionner un forum pour définir les autorisations. Les paramètres d\'autorisation suivants affecte les autorisations d\'utilisateur uniquement dans ce forum',//'请选择要设置的版块。以下权限设置仅仅影响该用户在此版块的权限',  //  'Select a Forum to set permissions. The following permission settings affects the user permissions only in this Forum',
      6 => 'members_access_add_',
      7 => '',
    ),
  ),
  212 =>
  array (
    'index' =>
    array (
      'Modifier utilisateur' => 'action=members&operation=edit',//'编辑用户', //  Edit User' 
    ),
    'text' =>
    array (
      0 => 'Modifier utilisateur',//'编辑用户', //  'Edit User',
      1 => 'members_edit_username',
      2 => 'Nom d\'utilisateur',//'用户名', //  'User name',
      3 => 'members_edit_avatar',
      4 => 'Avatar',//'头像', //  'Avatar',
      5 => 'members_edit_statistics',
      6 => 'Statistiques',//'统计信息', //   'Statistics',
      7 => 'members_edit_password',
      8 => 'Nouveau mot de passe',//'新密码', // 'New password', 
      9 => 'Laissez ce champ vide, si vous ne voulez pas changer le mot de passe actuel',//'如果不更改密码此处请留空', //'Leave blank, if you do not want to change the current password',  
      10 => 'members_edit_unbind',
      11 => 'Retirer liaison de compte QQ',//'解除QQ帐号绑定状态', //  'Remove QQ account binding',
      12 => 'Si le compte de l\'utilisateur QQ est perdu ou volé, vous pouvez supprimer ici la liaison de compte QQ',//'如当前用户的QQ帐号丢失或者被盗，可以在这里解除QQ帐号的绑定',  // 'If the current user QQ account is lost or stolen, you can remove here the QQ account binding',
      13 => 'members_edit_uinblack',
      14 => 'Ban QQ compte',//'封禁QQ帐号', //  'Ban QQ account',
      15 => 'Lorsque le compte courant du QQ du l\'utilisateur est banni, le compte de QQ interdit ne peut être utilisé pour la connexion à ce site',//'把当前用户的QQ帐号封锁，封禁后此QQ帐号无法再登录本站点', //  'When the current user QQ account is banned, the banned QQ account can not be used for login to this site',
      16 => 'members_edit_clearquestion',
      17 => 'Retirer question de sécurité utilisateur',//'清除用户安全提问', // 'Remove user security question',
      18 => 'Sélectionnez "Oui" pour effacer des questions de sécurité des utilisateurs, afin que l\'utilisateur n\'aura pas besoin de répondre à des questions de sécurité lors de la connexion. Sélectionnez "Non" pour ne pas changer les paramètres d\'interrogation de sécurité des utilisateurs',//'选择“是”将清除用户安全提问，该用户将不需要回答安全提问即可登录；选择“否”为不改变用户的安全提问设置', // 'Select "Yes" to clear the user security questions, so the user will not need to answer security questions when login. Select "No" for not change the user security question settings', 
      19 => 'members_edit_status',
      20 => 'Verrouiller utilisateur actuel',//'锁定当前用户',  //  'Lock the current user',
      21 => 'members_edit_email',
      22 => 'Email',
      23 => 'members_edit_email_emailstatus',
      24 => 'Courriel état activé',//'邮箱激活状态', //   'Mailbox activated status',
      25 => 'members_edit_posts',
      26 => 'Nombre de post',//'发帖数', //  'Post number',
      27 => 'members_edit_digestposts',
      28 => 'Digests',//'精华帖数', //  'Digests', 
      29 => 'members_edit_regip',
      30 => 'Reg IP',//'注册 IP', //   'Reg IP',
      31 => 'members_edit_regdate',
      32 => 'Date d\'inscription',//'注册时间', // 'Registration date', 
      33 => 'members_edit_lastvisit',
      34 => 'Dernière Visite',//'上次访问', // 'Last Visit', 
      35 => 'members_edit_lastip',
      36 => 'Dernière IP',//'上次访问 IP', //   'Last IP',
      37 => 'members_edit_addsize',
      38 => 'Ajoutez de la capacité de l\'espace supplémentaire',//'额外附件容量', //  'Add extra space capacity',
      39 => 'members_edit_addfriend',
      40 => 'Nombre d\'amis supplémentaires',//'额外好友数', //  'Number of additional friends',
      41 => 'members_edit_timeoffset',
      42 => 'Décalage horaire',//'时差设定', //  'Time Offset',
      43 => 'Heure locale de l\'utilisateur décalé par rapport à GMT. Définir 9999 pour utiliser la durée par défaut de réglage de décalage.',//'用户本地时间与 GMT 标准的时间差，设置 9999 即为默认时差，和站点设置保持一致', // 'User local time offset from GMT. Set 9999 to use the default time offset setting.',
      44 => 'members_edit_invisible',
      45 => 'Activer le mode invisible',//'隐身登录', //  'Enable invisible mode',
      46 => 'members_edit_option',
      47 => 'Forum Options',//'论坛选项', // 'Forum Options',
      48 => 'members_edit_cstatus',
      49 => 'Statut personnalisé',//'自定义头衔',
      50 => 'members_edit_signature',
      51 => 'Signature',//'签名',
      52 => 'members_profile',
      53 => 'Champs Profil utilisateur',//'用户栏目', // 'User profile fields', 
    ),
  ),
  213 =>
  array (
    'index' =>
    array (
      'Rechercher des membres' => 'action=members&operation=search',//'用户管理', //  'Search Members'
    ),
    'text' =>
    array (
      0 => 'Rechercher des membres',//'用户管理', //  'Search Members',
      1 => 'members_search_table',
      2 => 'Table utilisateur',//'用户表', //   'User table',
      3 => 'Lorsque les données de l\'utilisateur mis à jour, les données relatives à l\'utilisateur dans la table archive n\'est pas mis à jour',//'更新用户数据时，存档表中的用户相关数据不会更新', //  'When the user data updated, User-related data in the archive table is not updated',
      4 => 'members_search_user',
      5 => 'Nom d\'utilisateur',//'用户名',  //  'User name',
      6 => 'Vous pouvez utiliser le caractère générique *. Plusieurs noms d\'utilisateurs séparés par une virgule ","',//'可使用通配符 *，多个用户名用半角逗号 "," 隔开', // 'You can use the wildcard *. Separate multiple user names with a comma ","',
      7 => 'members_search_uid',
      8 => 'UID Utilisateur',//'用户 UID', //  'User UID',
      9 => 'members_search_group',
      10 => 'Groupe utilisateur principal',//'主用户组', //   'Main user group',
      11 => 'Ce paramètre permet aux groupes d\'utilisateurs impliqués dans la recherche, vous pouvez maintenir enfoncée la touche CTRL pour choix multiple',//'设置允许参与搜索的用户组，可以按住 CTRL 多选', // 'This setting allow the user groups involved in the search, you can hold down the CTRL key for multiple choice',
      12 => 'members_search_medal',
      13 => 'Médailles',//'勋章', //   'Medals',
      14 => 'Définir la médaille spécifique que les utilisateurs disposent. Maintenez la touche CTRL enfoncée pour choix multiple',//'设置拥有指定勋章的用户，可以按住 CTRL 多选', //  'Set the specified Medal that users have. Hold down the CTRL for multiple choice',
      15 => 'members_search_usertag',
      16 => 'Tags Utilisateur',//'用户标签', //  'User Tags',
      17 => 'Définir les tags des utilisateurs spécifiés. Maintenez enfoncée la touche CTRL pour choix multiple',//'设置指定标签的用户，可以按住 CTRL 多选', // 'Set specified user tags. Hold down the CTRL for multiple choice',
      18 => 'members_search_conisbind',
      19 => 'QQ est consolidé',//'是否绑定QQ', //  'is QQ bound',
      20 => 'members_search_uinblacklist',
      21 => 'est QQ bloqué',//'QQ帐号是否被封', //  'is QQ blocked',
      22 => 'members_search_online',
      23 => 'est en-ligne',//'是否在线', //'is online',  
      24 => 'members_search_lockstatus',
      25 => 'est banni',//'是否锁定', //   'is banned',
      26 => 'members_search_emailstatus',
      27 => 'Email est vérifié',//'是否通过Email认证', //   'is Email verified',
      28 => 'members_search_avatarstatus',
      29 => 'Avatar est chargé',//'是否有头像', //  'is Avatar uploaded',
      30 => 'members_search_email',
      31 => 'Email',
      32 => 'Vous pouvez utiliser le caractère générique *',//'可使用通配符 *', //   'You can use the wildcard *',
      33 => 'members_search_friendsrange',
      34 => 'Nombre de plage des amis',//'好友数介于', //  'Number of friends range',
      35 => 'members_search_postsrange',
      36 => 'Posts plage',//'发帖数介于', //  'Posts range',
      37 => 'members_search_regip',
      38 => 'Inscription IP commencer par',//'注册 IP 开始于',  //  'Register IP begin with',
      39 => 'Comme 192.168, le joker * peut être utilisé',//'如 192.168，可使用通配符 *', //  'Such as 192.168, the wildcard * can be used',
      40 => 'members_search_lastip',
      41 => 'Dernière visite IP commencer par',//'上次访问 IP 开始于',  //   'Last visit IP begin with',
      42 => 'Comme 192.168, le joker * peut être utilisé',//'如 192.168，可使用通配符 *', // 'Such as 192.168, the wildcard * can be used',
      43 => 'members_search_oltimerange',
      44 => 'Utilisateur temps en ligne (heures)',//'用户在线时间(单位小时)', //  'User online time (hours)',
      45 => 'members_search_regdaterange',
      46 => 'Inscription intervalle de dates',//'注册日期介于', // 'Registration date range',
      47 => 'members_search_lastvisitrange',
      48 => 'Dernier plage horaire de visite',//'最后访问时间介于', // 'Last visit time range',
      49 => 'members_search_lastpostrange',
      50 => 'Dernière plage horaire de poste',//'最后发帖时间介于', //  'Last post time range',
      51 => 'members_search_group_fid',
      52 => 'Groupe ID (ID de groupe multiple séparé par des virgules)',//'群组 ID(多个群组逗号分隔)', // 'Group ID (separate multiple group ID by commas)',
      53 => 'members_search_verify',
      54 => 'Approuver',//'认证', //  'Approve',
      55 => 'members_search_birthday',
      56 => 'Anniversaire',//'用户生日', //  'Birthday',
    ),
  ),
  214 =>
  array (
    'index' =>
    array (
      'Sous-table Utilisateur' => 'action=membersplit&operation=check',//'用户分表' //  'User sub-table'
    ),
    'text' =>
    array (
      0 => 'Sous-table Utilisateur',//'用户分表', //  'User sub-table',
      1 => 'membersplit_check_tips',
      2 => 'Utilisation de l\'optimisation utilisateur peut améliorer sensiblement les performances du site. En raison de l\'opération de l\'examen physique est lent, les résultats de l\'examen physique est mis en cache une fois par jour',//'通过用户优化可以大幅度提高网站的性能。因体检操作较慢，所以体检的结果会缓存一天的时间', // 'Using the user optimization can significantly improve the site performance. Because of the physical examination operation is slow, the results of the physical examination is cached once per day',
    ),
  ),
  215 =>
  array (
    'index' =>
    array (
      'Sous-table Utilisateur' => 'action=membersplit&operation=check',//'用户分表' //   'User sub-table'
    ),
    'text' =>
    array (
      0 => 'Sous-table Utilisateur',//'用户分表',  //  'User sub-table',
      1 => 'membersplit_tips',
      2 => 'Utilisation de l\'optimisation utilisateur peut améliorer sensiblement les performances du site. Lorsque le nombre d\'utilisateurs est important, l\'optimisation peut améliorer les performances de plus de 30%, nous recommandons vivement d\'effectuer l\'optimisation de la table user. Selon les performances du serveur, des ajustements appropriés à l\'optimisation en une seule étape du numéro. Il est recommandé de fermer le site avant l\'optimisation!',//'通过用户优化可以大幅度提高网站的性能。当可优化用户数大于30%以上时，我们强烈建议进行用户表优化操作请根据服务器性能，适当调整单步优化数。建议在关闭站点下进行优化操作。',  //  'Using the user optimization can significantly improve the site performance. When the number of users is large, the optimizing can improve performance more than 30%, we strongly recommend to perform user table optimization. Depending on server performance, Appropriate adjustments to single-step optimization of the number. It is recommended to close the site before optimization!', 
    ),
  ),
  216 =>
  array (
    'index' =>
    array (
      'Liste des icônes Online' => 'action=misc&operation=onlinelist',//'在线列表图标', //'Online List icons' 
    ),
    'text' =>
    array (
      0 => 'Liste des icônes Online',//'在线列表图标',  // 'Online List icons', 
      1 => 'misc_onlinelist_tips',
      2 => 'Cette fonction est utilisée pour personnaliser les membres de la légende en ligne. Il n\'est efficace que si une fonction de liste en ligne est activée. Legend au vide pour le groupe d\'utilisateurs ne fait pas de distinction entre le groupe d\'utilisateurs, tous les groupes d\'utilisateurs n\'ont pas la distinction entre l\'uniforme inclus la première ligne de l\'article à des " Utilisateurs ordinaires". Svp. veuillez remplir dans la légende du groupe nom du fichier image de l\'utilisateur, et télécharger le fichier image correspondant au static/image/common/ directory.',//'本功能用于自定义首页及主题列表页显示的在线会员分组及图例，只在在线列表功能打开时有效。用户组图例处空白为不区分该组用户，所有未区分的用户组将统一归入第一行的“普通用户”项。用户组图例中请填写图片文件名，并将相应图片文件上传到 static/image/common/ 目录中。',   //   'This function is used to customize the members online legend. It is effective only if a online list function is enabled. Legend at the blank for the user group does not distinguish between the group of users, all user groups did not distinguish between the uniform included the first line of "ordinary users" item. Please fill in the user group legend image file name, and upload the corresponding image file to the static/image/common/ directory.',
    ),
  ),
  217 =>
  array (
    'index' =>
    array (
      'Liens' => 'action=misc&operation=link',//'友情链接',  // 'Links' 
    ),
    'text' =>
    array (
      0 => 'Liens',//'友情链接',  // 'Links',
      1 => 'misc_link_tips',
      2 => 'Si vous ne souhaitez pas afficher les liens sur la page d\'accueil du forum, vous pouvez désactiver les "Paramètres d\'interface". Si la description du texte du lien n\'est pas terminée, elle sera affichée dans une forme compacte. Ami Liens regroupés et affichés uniquement dans le bloc de bricolage.',//'如果你不想在论坛首页显示友情链接，可以在“界面设置”中关闭。未填写文字说明的项目将以紧凑型显示。  //  If you do not want to show links at the forum home page, you can turn off the "interface settings". If the link text description is not completed, it will be displayed in a compact form. Friend Links grouped and displayed only in DIY block.
    ),
  ),
  218 =>
  array (
    'index' =>
    array (
      'Liens connexes' => 'action=misc&operation=relatedlink',//'关联链接',  // 'Related Links'
    ),
    'text' =>
    array (
      0 => 'Liens connexes',//'关联链接',  // 'Related Links',
      1 => 'misc_relatedlink_tips',
      2 => 'Automatiquement affichés dans une plage définie du contenu connexe.',//'在指定范围内出现的相关文字自动加上链接。',  // 'Displayed automatically in a specified range of related content.',
    ),
  ),
  219 =>
  array (
    'index' =>
    array (
      'Paramètres de l\'éditeur' => 'action=setting&operation=editor',//'编辑器设置',  // 'Editor Settings'
      'BB-Code' => 'action=setting&operation=bbcode',//'Discuz! 代码',  // 'BB-Code'
    ),
    'text' =>
    array (
      0 => 'Paramètres de l\'éditeur &raquo; BB-Code',//'编辑器设置 &raquo; Discuz! 代码',  //  'Editor Settings &raquo; BB-Code',
      1 => 'misc_bbcode_edit_tips',
      2 => 'Bouton BB-code personnalisé est disponible dans l\'affichage uniquement si le fichier icône est réglé, et un groupe d\'utilisateurs correspondant avoir une permission "Autoriser l\'utilisation Code BB personnalisé".',//'只有在自定义 Discuz! 代码可用并设置了图标文件时，具有“允许使用自定义 Discuz! 代码”权限的用户组在发帖时才会看到相应自定义 Discuz! 代码按钮。', //  'Custom BB-Code button is available in posting only if the icon file is set, and a corresopnding user group have a permission "Allow to use custom BB-Code".',
    ),
  ),
  220 =>
  array (
    'index' =>
    array (
      'Filtre illicite Mot' => 'action=misc&operation=censor',//'词语过滤',  // 'Bad Word Filter' 
    ),
    'text' =>
    array (
      0 => 'Filtre illicite Mot',//'词语过滤',  //  'Bad Word Filter',
      1 => 'misc_censor_tips',
      2 => 'Qualificatif de mots clés {x} peut être utilisé au texte ignoré entre deux caractères adjacents, voici "x" est un nombre de caractères ignorés. À savoir"a{1}s{2}s" (sans les guillemets) peuvent rechercher des "ass" et "axsxs" et "axsxxs" et ainsi de suite. Pour les caractères chinois du GBK ou Big-5 encofing chaque caractère chinois équivaut à 2 octets; Pour encodage UTF-8 chaque caractère chinois équivaut à 3 octets. Si yo ne veulent pas diminuer l\'efficacité du programme, s\'il vous plaît ne pas mettre trop de filtrer le contenu. Si vous voulez examiner et de vérifier le contenu du forum de manière efficace, vous pouvez modifier ou supprimer ou ajouter vos propres articles sur le filtre. Si un mauvais mot commence et fini avec "/" (sans les guillemets), il est signifie qu\'il s\'agit d\'un format d\'expression régulière, et si la référence à "(n)" peut être utilisé pour remplacer le contenu à des sub-patterns, tels en tant que un motif "/1\\d{10}([^\\d]+|$)/" peut être remplacé par "téléphone portable: (1)".',//'替换前的内容可以使用限定符 {x} 以限定相邻两字符间可忽略的文字，x 是忽略的字节数。如 "a{1}s{2}s"(不含引号) 可以过滤 "ass" 也可过滤 "axsxs" 和 "axsxxs" 等等。对于中文字符，若使用 GBK、Big-5 版本，每个中文字符相当于 2 个字节；若使用 UTF-8 版本，每个中文字符相当于 3 个字节。为不影响程序效率，请不要设置过多不需要的过滤内容。审核只对论坛中的内容有效如果你不是论坛管理员，你将只能对自己添加的项目进行编辑或删除。不良词语如果以"/"(不含引号)开头和结尾则表示格式为正则表达式，这时替换内容可用"(n)"引用正则中的子模式，如"/1\\d{10}([^\\d]+|$)/"替换为"手机(1)"。', //  'Keyword qualifier {x} can be used to ignored text between two adjacent characters, here "x" is a number of ignored characters. I.e. "a{1}s{2}s" (without the quotes) can search for "ass" and "axsxs" and "axsxxs" and so on. For Chinese characters of GBK or Big-5 encofing each Chinese character is equivalent to 2 bytes; For UTF-8 encoding each Chinese character is equivalent to 3 bytes. If yo do not want to decrease the efficiency of the program, please do not set too much to filter content. If you want to effectively review and verify the forum contents, you can edit or delete or add your own items to the filter. If a bad word starts and finished with "/" (without quotes), it is means that it is a regular expression format, and so the reference to "(n)" may be used for replace the content to the sub-patterns, such as a pattern "/1\\d{10}([^\\d]+|$)/" may be replaced with "Mobile phone: (1)".',
      3 => 'misc_censor_batch_add_tips',
      4 => 'Chaque ligne représente une règle du filtre, mauvais mot et des mots de remplacement séparés par le "=" entre; Pour désactiver un texte sans aucun remplacement, remplacer le contenu correspondant à{BANNED}; Si certains mots dans un poste exigent le poste ne sera pas directement représentée, mais doivent être signalés pour la prévisualisation manuelle par un modérateur, remplacez le contenu correspondant à {MOD}. Si vous voulez juste pour remplacer directement le mot avec **, vous pouvez uniquement entrer ces mots, par exemple: toobadnobadbadword=goodsexword={BANNED}; Pour soutenir Exportation / Importation de mots dans une catégorie, vous pouvez choisir de ne pas préciser la catégorie d\'importation. La catégorie par défaut n\'est pas automatiquement incluse',//'每行一组过滤词语，不良词语和替换词语之间使用“=”进行分割；如需禁止发布包含某个词语的文字，而不是替换过滤，请将其对应的替换内容设置为{BANNED}即可；如需当用户发布包含某个词语的文字时，自动标记为需要人工审核，而不直接显示或替换过滤，请将其对应的替换内容设置为{MOD}即可(审核只对论坛中的内容有效)。如果只是想将某个词语直接替换成 **，则只输入词语即可；例如：toobadnobadbadword=goodsexword={BANNED}导出/导入时支持词语分类导入，导入时不带有分类则可以通过选择指定导入分类。没有分类时自动归入默认分类', //   'Each line represents a one filter rule, bad word and replacement words separated by the "=" between; For disable some text without any replacement, replace the corresponding content with {BANNED}; If some words in a post require the post will not directly shown but must be flagged for manual preview by a moderator, replace the corresponding content to {MOD}. If you just want to directly replacea word with **, you can only enter this words; For example: toobadnobadbadword=goodsexword={BANNED}; For support Export/import of words into a category, you can not choose to specify import category. The default category is not automatically included',
      5 => 'misc_censor_wordtype_tips',
      6 => 'Par défaut, le filtre est intégré dans la catégorie par défaut. Lorsque vous supprimez une catégorie, des filtre de mots seront automatiquement incorporées dans la catégorie par défaut.',//'默认分类为内置分类，当删除一组分类时，词语过滤将会自动编入默认分类中。',   // 'By default the filter is built-in into the default category. When you remove a category, the word filter will be automatically incorporated into the default category.', 
    ),
  ),
  221 =>
  array (
    'index' =>
    array (
      'Tampons de la discussion' => 'action=misc&operation=stamp',//'主题鉴定',  // 'Thread stamps'
      'Listing des Tampons' => 'action=misc&operation=stamp&anchor=list',//'图章',  //  'Stamp List'
    ),
    'text' =>
    array (
      0 => 'Tampons de la discussion &raquo; Listing des Tampons',//'主题鉴定 &raquo; 图章',  //  'Thread stamps &raquo; Stamp List',
      1 => 'misc_stamp_listtips',
      2 => 'Tampon d\'identification affiché sur la page de contenu et de post sur la page de liste des discussions. Le système réserve 100 postes pour les images d\'identification de fil, pour l\'ID d\'image de 0 à 99 respectivement. Ne pas modifier l\'ID d\'image!, Cela provoquera un dérèglement des fils timbrées. Quand un tampon associé à l\'ensemble deopération, le timbre exécute automatiquement l\'action correspondante pour le fil, quand un tampon est associé à l\'icône, l\'icône sera montré automatiquement lorsque le tampon est ajouté dans le fil. La même icône de tampon ne peut pas être associé à plus d\'une opération.',//'鉴定图章显示在帖子内容页，鉴定图标显示在主题列表页。系统预留了 100 个主题鉴定图片位置，分别为图片 ID 0 至 99。不要随意修改图片 ID，否则将会导致主题中已使用的图章、图标错乱。当图章设置了关联操作后，图章会在执行相应操作时被自动添加到主题中；当图章设置了关联图标后，图标会在添加图章时自动添加到主题列表中。关联操作不可与关联图标同时设置。',  //  'Identification stamp displayed on the post content page and on the thread list page. The system reserves 100 positions for the thread identification images, for the image ID from 0 to 99 respectively. Do not modify the image ID!, it will cause a disordering of the stamped threads. When a stamp associated with the operation set, the stamp will automatically perform the corresponding action for the thread; When a stamp is associated with the icon, the icon will be automatically shown when the stamp is added to the thread. The same stamp icon can not be associated with more than one operation.',
    ),
  ),
  222 =>
  array (
    'index' =>
    array (
      'Tampons des Sujets' => 'action=misc&operation=stamp',//'主题鉴定',  // 'Thread stamps'
      'Icônes' => 'action=misc&operation=stamp&anchor=llist',//'图标',  // 'Icons'
    ),
    'text' =>
    array (
      0 => 'Tampons des Sujets &raquo; Icônes',//'主题鉴定 &raquo; 图标',  // 'Thread stamps &raquo; Icons',
      1 => 'misc_stamp_listtips',
      2 => 'Tampon d\'identification affiché sur la page de contenu et de post sur ​​la page de liste des discussions. Le système réserve 100 postes pour les images d\'identification de fil, pour l\'ID d\'image de 0 à 99 respectivement. Ne pas modifier l\'ID d\'image!, Cela provoquera un dérèglement des fils timbrées. Quand un tampon associé à l\'ensemble deopération, le timbre exécute automatiquement l\'action correspondante pour le fil, quand un tampon est associé à l\'icône, l\'icône sera montré automatiquement lorsque le tampon est ajouté dans le fil. La même icône de tampon ne peut pas être associé à plus d\'une opération.',//'鉴定图章显示在帖子内容页，鉴定图标显示在主题列表页。系统预留了 100 个主题鉴定图片位置，分别为图片 ID 0 至 99。不要随意修改图片 ID，否则将会导致主题中已使用的图章、图标错乱。当图章设置了关联操作后，图章会在执行相应操作时被自动添加到主题中；当图章设置了关联图标后，图标会在添加图章时自动添加到主题列表中。关联操作不可与关联图标同时设置。',   //   'Identification stamp displayed on the post content page and on the thread list page. The system reserves 100 positions for the thread identification images, for the image ID from 0 to 99 respectively. Do not modify the image ID!, it will cause a disordering of the stamped threads. When a stamp associated with the operation set, the stamp will automatically perform the corresponding action for the thread; When a stamp is associated with the icon, the icon will be automatically shown when the stamp is added to the thread. The same stamp icon can not be associated with more than one operation.',
    ),
  ),
  223 =>
  array (
    'index' =>
    array (
      'Tampons des Sujets' => 'action=misc&operation=stamp',//'主题鉴定',  // 'Thread stamps'
      'Ajouter' => 'action=misc&operation=stamp&anchor=add',//'添加',  // 'Add'
    ),
    'text' =>
    array (
      0 => 'Tampons des Sujets &raquo; Ajouter',//'主题鉴定 &raquo; 添加',  //  'Thread stamps &raquo; Add',
      1 => 'misc_stamp_addtips',
      2 => 'Vous pouvez ajouter un timbre et choisissez une icône du fichier pour le timbre. Nom de l\'image doit être rempli, pas d\'image de nom ne sera ajouté.',//'可以选择是添加成图章还是图标。图片名称必需填写，没有名称的图片不会被添加。',   //    'You can add a stamp and choose an icon filename for the stamp. Image name must be filled out, no name image will not be added.',
    ),
  ),
  224 =>
  array (
    'index' =>
    array (
      'Fichier attaché Taille Type' => 'action=misc&operation=attachtype',//'附件类型尺寸',  // 'Attachment type size' 
    ),
    'text' =>
    array (
      0 => 'Fichier attaché Taille Type',//'附件类型尺寸',  //  'Attachment type size',
      1 => 'misc_attachtype_tips',
      2 => 'Cette fonctionnalité permet de limiter la taille maximale pour un type de pièce jointe particulier. Lorsque ce paramètre est plus petit que des groupe taille maximale de l\'utilisateur, ce paramètre prévaudra des limites de taille de type précis de pièces jointes. Vous pouvez définir la taille maximale de certains types de pièces jointes à zéro pour interdire de telles pièces jointes ajouts à tous.',//'本功能可限定某特定类型附件的最大尺寸，当这里设定的尺寸小于用户组允许的最大尺寸时，指定类型的附件尺寸限制将按本设定为准。你可以设置某类附件最大尺寸为 0 以整体禁止这类附件被上传。',  //   'This feature can limit the maximum size for a particular attachment type. When this setting is smaller than the user group maximum size, this setting will prevail the specified attachment type size limits. You can set the maximum size of certain attachment types to zero for prohibit such attachments uploads at all.',
    ),
  ),
  225 =>
  array (
    'index' =>
    array (
      'Cron Planificateur' => 'action=misc&operation=cron',//'计划任务',  // 'Cron Scheduler'
    ),
    'text' =>
    array (
      0 => 'Cron Planificateur',//'计划任务',  // 'Cron Scheduler',
      1 => 'misc_cron_tips',
      2 => 'Discuz! Task Planificateur système effectue automatiquement certaines tâches dans un temps spécifique, vous pouvez également facilement étendre les fonctionnalités du site sheduler. Le planificateur de tâches est étroitement associée aux fonctionnalités de base du système, si ce paramètre est incorrect, cela peut provoquer des site ne sera pas visible, ou peut ne fonctionne pas correctement, alors assurez-vous de bien comprendre les propriétés des tâches planifiées, et de savoir clairement ce qui est fait, quel genre de conséquences sera effectué lorsque vous ajoutez ou modifiez une tâche sheduled! Cette catégorie est en pleine conformité avec les réglages de l\'heure par défaut du serveur, mais ne repose pas sur un utilisateur ou un administrateur du site changé réglage de l\'heure.',//'计划任务是 Discuz! 提供的一项使系统在规定时间自动执行某些特定任务的功能，在需要的情况下，你也可以方便的将其用于站点功能的扩展。计划任务是与系统核心紧密关联的功能特性，不当的设置可能造成站点功能的隐患，严重时可能导致站点无法正常运行，因此请务必仅在你对计划任务特性十分了解，并明确知道正在做什么、有什么样后果的时候才自行添加或修改任务项目。此处和其他功能不同，本功能中完全按照站点系统默认时差对时间进行设定和显示，而不会依据某一用户或管理员的时差设定而改变显示或设置的时间值。', //   'Discuz! Task Scheduler system performs certain tasks automatically in a specified time, you can also easily extend the capabilities of the site sheduler. The task scheduler is closely associated with the core system features, so if this settings is improper, it may result the site will not visible, or may does not work properly, so make sure you fully understand the properties of scheduled tasks, and clearly know what is being done, what kind of consequences will performed when you add or modify any sheduled task! This feature is in full accordance with the server default time settings, but not based on a user or a site administrator changed time setting.',
    ),
  ),
  226 =>
  array (
    'index' =>
    array (
      'Sujets Hot' => 'action=misc&operation=focus',//'站长推荐',  // 'Hot Threads'
      'Gérer' => 'action=misc&operation=focus',//'管理',  // 'Manage'
    ),
    'text' =>
    array (
      0 => 'Sujets Hot &raquo; Gérer',//'站长推荐 &raquo; 管理',  // 'Hot Threads &raquo; Manage',
      1 => 'misc_focus_tips',
      2 => 'Une fenêtre flottante "Webmaster recommandé" sera affiché dans des coin inférieur droit de la page de contenu de poste. Vous pouvez ajouter manuellement certains sujets précis, ou choisir quelques sujets à recommandé par l\'animateur. Maximale est de 10 articles, qui seront affichés de manière aléatoire.',//'站长推荐将以浮窗的形式显示在帖子内容页面右下角。你可以手动添加一些指定的主题，或者选择一些版主推荐的主题。最多 10 条，每次随机显示其中 1 条。',  //  'A floating window "Webmaster recommended" will be displayed in the lower right corner of the post content page. You can manually add some specific topics, or select some topics to recommended by moderator. Maximum is 10 items, that will be displayed randomly.',
      3 => 'admin',
      4 => 'Manage',//'管理',  //  'Manage',
    ),
  ),
  227 =>
  array (
    'index' =>
    array (
      'Sujets Hot' => 'action=misc&operation=focus',//'站长推荐',  //   'Hot Threads'
      'Ajouter' => 'action=misc&operation=focus&do=add',//'添加',  //  'Add'
    ),
    'text' =>
    array (
      0 => 'Sujets Hot &raquo; Ajouter',//'站长推荐 &raquo; 添加',  //  'Hot Threads &raquo; Add',
      1 => 'misc_focus_handadd',
      2 => 'Ajouter manuellement',//'手动添加',  //  'Manually add',
      3 => 'misc_focus_handurl',
      4 => 'Lien URL suggéré',//'推荐链接地址',  //  'Recommended link URL',
      5 => 'misc_focus_handsubject',
      6 => 'Titre du lien recommandée',//'推荐标题',  // 'Recommended link title',
      7 => 'misc_focus_handsummary',
      8 => 'Description du lien recommandée',//'推荐内容',  //  'Recommended link description',
      9 => 'Le code HTML pris en charge, la longueur maximale est de 150 caractères',//'此处支持 HTML 代码，建议少于150个字符',  //  'HTML code supported, Max length is 150 characters',
      10 => 'misc_focus_handimg',
      11 => 'Ajouter image',//'附加图片',  // 'Add image',
      12 => 'Entrer le chemin absolu de l\'image',//'请填写图片的绝对路径',  //  'Enter the image absolute path',
      13 => 'misc_focus_position',
      14 => 'Où placer le bloc recommandée Webmestre',//'站长推荐投放区域',  // 'Where to place the Webmaster recommended block',
    ),
  ),
  228 =>
  array (
    'index' =>
    array (
      'Sujets Hot' => 'action=misc&operation=focus',//'站长推荐',  // 'Hot Threads'
      'Paramètres' => 'action=misc&operation=focus&do=config',//'设置',  // 'Settings'
    ),
    'text' =>
    array (
      0 => 'Sujets Hot &raquo; Paramètres',//'站长推荐 &raquo; 设置',  //'Hot Threads &raquo; Settings', 
      1 => 'config',
      2 => 'Paramètres',//'设置',  // 'Settings',
      3 => 'misc_focus_area_title',
      4 => 'Webmaster titre du bloc recommandée',//'站长推荐区域标题',  //  'Webmaster recommended block title',
      5 => 'Par défaut est &quot;Recommended by Webmaster&quot;, vous pouvez mettre ce que vous voulez',//'默认为“站长推荐”，你可以设置为你希望的标题', //Default is &quot;Recommended by Webmaster&quot;, you can set what you want
      6 => 'misc_focus_area_cookie',
      7 => 'Réglez le temps de rafraîchissement pop-up (en heures)',//'设置关闭后可再次弹出的时间(小时)',  // 'Set the pop-up refresh time (in hours)',
      8 => '0 signifie l\'actualisation des pop up à chaque fois. 1 signifie que des pop-up seront mises à jour toutes les heures. La valeur par défaut est 1, vous pouvez définir le temps que vous souhaitez actualiser des pop-up.',//'0表示再刷新页面就会弹出。1表示1小时后刷新页面才会出现。默认为1，可以设置为你希望弹出的时间     //  '0 means the refresh the pop up every time. 1 means the pop-up will be refreshed every one hour. The default is 1, you can set the time you want to refresh the pop-up.',
//',
    ),
  ),
  229 =>
  array (
    'index' =>
    array (
      'Gestion des Images' => 'action=pic',//'图片管理',  //'Images Management'
    ),
    'text' =>
    array (
      0 => 'Gestion des Images',//'图片管理',  //  'Images Management',
      1 => 'pic_tips',
      2 => 'Gestion des lots est utilisé pour supprimer des images utilisées. Attention: Lorsque vous cliquez sur le bouton Supprimer à la page de résultats de recherche, l\'information sera supprimé directement!',//'批量图片管理用于删除图片使用。连图片评论一同删除提醒：点击搜索结果页的删除按钮，将会直接删除相关信息！', //   'Batch management is used to delete used images. Warning: When you click the Delete button at the search results page, the information will be deleted directly!',
    ),
  ),
  230 =>
  array (
    'index' =>
    array (
      'Gestion des Images' => 'action=pic',//'图片管理',  // 'Images Management'
      'Recherche' => 'action=pic&search=true',//'搜索',  //  'Search' 
    ),
    'text' =>
    array (
      0 => 'Gestion des Images &raquo; Recherche',//'图片管理 &raquo; 搜索',  // 'Images Management &raquo; Search',
      1 => 'pic_search_detail',
      2 => 'Afficher la liste d\`une image détaillée',//'显示详细图片列表',  //  'Show detailed image list',
      3 => 'pic_search_perpage',
      4 => 'Articles par page',//'每页显示数',  //  'Items per page',
      5 => 'resultsort',
      6 => 'Trier les résultats',//'结果排序',  // 'Sort results',
      7 => 'pic_search_albumid',
      8 => 'Où le ID de l\'album',//'所在相册 ID',  // 'Where the album ID', 
      9 => 'Séparez les ID de l\'album par une virgule ","',//'多相册 ID 中间，请用半角逗号 "," 隔开',  //  'Separate multiple album ID with a comma ","',
      10 => 'pic_search_user',
      11 => 'Affiché image Nom de l\'utilisateur',//'发表图片用户名',  // 'Posted image User Name',
      12 => 'Plusieurs noms d\'utilisateurs séparés par une virgule ","',//'多所有者中间请用半角逗号 "," 隔开',  //  'Separate multiple user names with a comma ","',
      13 => 'pic_search_picid',
      14 => 'Image ID',//'图片 ID',  // 'Image ID',
      15 => 'Séparez plusieurs ID d\'images avec une virgule ","',//'多图片 ID 中间请用半角逗号 "," 隔开',  // 'Separate multiple image ID with a comma ","',
      16 => 'pic_search_title',
      17 => 'Titre de l\'image',//'图片标题',  // 'Image Title',
      18 => 'pic_search_ip',
      19 => 'Affiché IP',//'发布IP',  //  'Posted IP',
      20 => 'Joker "*" peut être utilisé, par exemple "127.0. *. *" (Sans les guillemets), à utiliser avec prudence!',//'通配符 "*" 如 "127.0.*.*"(不含引号)，慎用!!!',  //  'Wildcard "*" may be used, i.e. "127.0 .*.*" (without quotes), use with caution!!!',
      21 => 'pic_search_hot',
      22 => 'Hot',//'热度',  // 'Hot',
      23 => 'pic_search_time',
      24 => 'Téléchargez plage horaire',//'上传时间范围',  // 'Upload time range',
      25 => 'Format est aaaa-mm-jj',//'格式 yyyy-mm-dd',  // 'Format is yyyy-mm-dd',
    ),
  ),
  231 =>
  array (
    'index' =>
    array (
      'Gestion Post Commentaire' => 'action=postcomment',//'帖子点评管理',  //  'Post Comment Management'
    ),
    'text' =>
    array (
      0 => 'Gestion Post Commentaire',//'帖子点评管理',  // 'Post Comment Management',
      1 => 'postcomment_tips',
      2 => 'Gestion de Commentaire par lots est utilisée pour supprimer poster des commentaires. Attention: Cliquez sur le bouton Supprimer dans la page de résultats de recherche va supprimer les informations directement!',//'批量点评管理用于删除帖子点评。提醒：点击搜索结果页的删除按钮，将会直接删除相关信息！', //   'Batch Comment management is used to delete post Comments. Warning: Click on the Delete button in the search results page will delete the information directly!',
    ),
  ),
  232 =>
  array (
    'index' =>
    array (
      'Gestion Post Commentaire' => 'action=postcomment',//'帖子点评管理',  //  'Post Comment Management'
      'Recherche' => 'action=postcomment&search=true',//'搜索',  //  'Search'
    ),
    'text' =>
    array (
      0 => 'Gestion Post Commentaire &raquo; Recherche',//'帖子点评管理 &raquo; 搜索',  // 'Post Comment Management &raquo; Search',
      1 => 'postcomment_search_detail',
      2 => 'Afficher liste des commentaires détaillés',//'显示详细点评列表',  // 'Show detailed comment list',
      3 => 'comment_search_perpage',
      4 => 'Articles par page',//'每页显示数',  // 'Items per page',
      5 => 'postcomment_content',
      6 => 'Commenter le contenu',//'点评内容',  //  'Comment content',
      7 => 'postcomment_search_tid',
      8 => 'Commentaires pour ID du Sujet',//'被点评主题 ID',  // 'Comments for thread ID',
      9 => 'Rechercher tous les commentaires pour désigné ID du Sujet (y compris les postes de fil et des réponses), ID Séparez par une virgule","',//'指定主题的所有点评(包括主题帖和回帖)，多 ID 中间请用半角逗号 "," 隔开', // 'Search all comments for designated thread ID (including the thread posts and replies), Separate multiple ID with a comma ","',
      10 => 'postcomment_search_pid',
      11 => 'Commentaire pour l\'ID du Post',//'被点评帖子 ID',  // 
      12 => 'Séparer ID du post multiples par une virgule ","',//'指定帖子的点评，多 ID 中间请用半角逗号 "," 隔开',  // 'Separate multiple post ID with a comma ","',
      13 => 'postcomment_search_author',
      14 => 'Commenter nom de l\'auteur',//'点评者',  // 
      15 => 'Plusieurs noms d\'auteurs séparés par une virgule ","',//'多点评者名中间请用半角逗号 "," 隔开',  // 'Separate multiple author names with a comma ","',
      16 => 'postcomment_search_authorid',
      17 => 'Commentaire Auteur ID',//'点评者 ID',  // 'Comment author ID',
      18 => 'Séparez plusieurs ID de l\'auteur par une virgule ","',//'多点评者 ID 中间请用半角逗号 "," 隔开',  // 'Separate multiple author ID with a comma ","',
      19 => 'comment_search_ip',
      20 => 'Affiché IP',//'发布IP',  //  'Posted IP',
      21 => 'Joker "*" peut être utilisé, par exemple "127.0. *. *" (Sans les guillemets), à utiliser avec prudence!',//'通配符 "*" 如 "127.0.*.*"(不含引号)，慎用!!!', // 'Wildcard "*" may be used, i.e. "127.0 .*.*" (without quotes), use with caution!!!',
      22 => 'postcomment_search_time',
      23 => 'Commenter plage horaire',//'点评时间范围',  // 'Comment time range',
      24 => 'Format est aaaa-mm-jj',//'格式 yyyy-mm-dd',  //  'Format is yyyy-mm-dd',
    ),
  ),
  233 =>
  array (
    'index' =>
    array (
      'Posts brisés' => 'action=postsplit&operation=manage',//'帖子分表',  // 'Split Posts'
      'Gestion Table' => 'action=postsplit&operation=manage',//'分表管理',  // 'Table management' 
    ),
    'text' =>
    array (
      0 => 'Posts brisés &raquo; Gestion Table',//'帖子分表 &raquo; 分表管理',  // 'Split Posts &raquo; Table management',
      1 => 'postsplit_manage_tips',
      2 => 'Pour éviter les problèmes, vous devez fermer le site avant la scission poste. Avant le brisement du post, il est recommandé de préparer la sauvegarde de base de données. L\'optimisation de la table passé un long moment, et pendant cette période, vous ne devez pas redémarrer le serveur ou arrêter le service MySQL. Vous devez être conserver au moins 300M pour optimiser les données forum_post (Table principal). La fonction de post-scission placera les données postales dans différentes tables pour augmenter l\'efficacité du site en cas d\'un grand nombre de messages.',//'进行帖子分表前一定要先关闭站点，为防止分表操作过程中出现问题，必须做好数据库备份后再进行此操作帖子数据转移完毕会对源帖子表进行表优化操作，优化表所耗时间比较长，在这期间绝对不能重起服务器或者停止MySQL服务forum_post(主表)必须保留300M的数据帖子分表功能可以将帖子分在不同的数据表中，适合帖子数很多并且运行效率受到影响的站点使用', // 'To prevent problems, you must shut down the site before the post splitting. Before the post splitting it is recommended to prepare the Database backup. The table optimization spent a long time, and during this period you must not restart the server or stop the MySQL service. You have to be preserve at less 300M for optimizing the forum_post (main table) data. The post split feature will place the post data into different tables for increase the site efficiency in case of a lot of posts.',
    ),
  ),
  234 =>
  array (
    'index' =>
    array (
      'Posts brisés' => 'action=postsplit&operation=manage',//'帖子分表',  // 'Split Posts'
      'Gestion Table' => 'action=postsplit&operation=manage',//'分表管理',  // 'Table management'
    ),
    'text' =>
    array (
      0 => 'Posts brisés &raquo; Gestion Table',//'帖子分表 &raquo; 分表管理',  //  'Split Posts &raquo; Table management',
      1 => 'postsplit_manage_tips',
      2 => 'Pour éviter les problèmes, vous devez fermer le site avant la scission poste. Avant le brisement du post, il est recommandé de préparer la sauvegarde de base de données. L\'optimisation de la table passé un long moment, et pendant cette période, vous ne devez pas redémarrer le serveur ou arrêter le service MySQL. Vous devez être conserver au moins 300M pour optimiser les données forum_post (Table principal). La fonction de post-scission placera les données postales dans différentes tables pour augmenter l\'efficacité du site en cas d\'un grand nombre de messages.',//'进行帖子分表前一定要先关闭站点，为防止分表操作过程中出现问题，必须做好数据库备份后再进行此操作帖子数据转移完毕会对源帖子表进行表优化操作，优化表所耗时间比较长，在这期间绝对不能重起服务器或者停止MySQL服务forum_post(主表)必须保留300M的数据帖子分表功能可以将帖子分在不同的数据表中，适合帖子数很多并且运行效率受到影响的站点使用',  //   'To prevent problems, you must shut down the site before the post splitting. Before the post splitting it is recommended to prepare the Database backup. The table optimization spent a long time, and during this period you must not restart the server or stop the MySQL service. You have to be preserve at less 300M for optimizing the forum_post (main table) data. The post split feature will place the post data into different tables for increase the site efficiency in case of a lot of posts.',
    ),
  ),
  235 =>
  array (
    'index' =>
    array (
      'Effacer Post En vrac' => 'action=prune',//'论坛批量删帖',  // 'Bulk post delete'
    ),
    'text' =>
    array (
      0 => 'Effacer Post En vrac',//'论坛批量删帖',  // 'Bulk post delete',
      1 => 'prune_tips',
      2 => 'Poste en vrac supprimer utilisé uniquement pour supprimer les messages illégaux. Si vous avez besoin de lot supprimer l\'historique Rapport Haut, S\'il vous plaît utiliser la gestion des lots de fil. Pour utiliser cette fonction, vous devez fournir l\'heure de départ de début et heure de fin, le nom du poste de l\'utilisateur, post IP et les mots clés du contenu. Au moins deux conditions sont nécessaires, sinon il ne sera pas en mesure de fonctionner. Si vos conditions de l\'après suppression de rencontrer le premier message du fil, ou un fil sans réponses, tout le fil sera supprimé. Si vous n\'êtes pas un administrateur de forum, vous pouvez supprimer les messages que dans une plage de temps de peu de temps pour le présent. Super Moderator peut supprimer des messages dans l\'intervalle de temps de deux semaines (14 jours); modérateur peut supprimer les messages dans l\'intervalle de temps d\'une semaine (7 jours) et seulement pour les forums dans sa juridiction. Qualificatif de mots clés {x} peut être utilisé au texte ignoré entre deux caractères adjacents, voici "x" est un nombre de caractères ignorés. À savoir "a {1}s {2}s" (sans les guillemets) peut rechercher "ass" et "axsxs" et "axsxxs" et ainsi de suite.',//'批量删帖仅用于删除违规帖子使用，如你需要批量删除历史旧帖，请使用批量主题管理功能。使用此功能，需同时提供起始时间和终止时间其中之一，及发帖用户名、发帖 IP、内容关键字其中之一，至少共二项必要的条件，否则将无法进行操作。如果符合删除条件的帖子为主题第一帖，或该主题无回复，将删除整个主题。如果你不是论坛管理员，你只能删除从某一时间到现在的范围内的帖子。超级版主的删帖的时间范围为二星期(14 天)；版主的时间范围为一星期(7 天)且只能删除所管辖论坛内的帖子。关键字可以使用限定符 {x} 以限定相邻两字符间可忽略的文字，x 是忽略字符的个数，如 "a{1}s{2}s"(不含引号) 可以搜索到 "ass" 也可搜索到 "axsxs" 和 "axsxxs" 等等。', //  'Bulk post delete used only for delete illegal posts. If you need to batch delete the history Report Top, Please use the thread batch management. For using this feature, you have to provide the post start time and end time, post user name, post IP and content keywords. At least two conditions are necessary, otherwise it will not be able to operate. If your conditions of the post deleting meet the thread first post, or a thread without replies, the entire thread will be removed. If you are not a forum administrator, you can delete posts only in a time range from some time to the present. Super Moderator can delete posts in the time range of two weeks (14 days); Moderator can remove posts in the time range of one week (7 days) and only for the forums in his jurisdiction. Keyword qualifier {x} can be used to ignored text between two adjacent characters, here "x" is a number of ignored characters. I.e. "a{1}s{2}s" (without the quotes) can search for "ass" and "axsxs" and "axsxxs" and so on.',
      3 => 'prune_search_detail',
      4 => 'Afficher la liste de post détaillé',//'显示详细帖子列表',  // 'Show detailed post list',
      5 => 'prune_search_select_postsplit',
      6 => 'Choisir la répartition du post de la sous-table',//'选择帖子分表',  // 'Select the post split sub-table',
      7 => 'prune_search_forum',
      8 => 'Recherche dans les forums',//'所在版块',  // 'Search in Forums',
      9 => 'prune_search_perpage',
      10 => 'Articles par page',//'每页显示数',  // 'Items per page',
      11 => 'prune_search_time',
      12 => 'Post Time range',//'发表时间范围',  // 'Post Time range',
      13 => 'Format est aaaa-mm-jj. Définir à 0 pour aucune restrictions',//'格式 yyyy-mm-dd，不限制请输入 0',  // 'Format is yyyy-mm-dd. Set to 0 for no restrictions',
      14 => 'prune_search_user',
      15 => 'Post nom d\'Auteur',//'发帖用户名',  // 'Post author name',
      16 => 'Séparez les noms par une virgule ","',//'多用户名中间请用半角逗号 "," 隔开',  // 'Separate multiple names with a comma ","',
      17 => 'prune_search_ip',
      18 => 'Post IP ',//'发帖 IP ',  //  'Poster IP ',
      19 => 'Joker "*" peut être utilisé, par exemple "127.0. *. *" (Sans les guillemets), à utiliser avec prudence!',//'通配符 "*" 如 "127.0.*.*"(不含引号)，慎用!!!', //  'Wildcard "*" may be used, i.e. "127.0 .*.*" (without quotes), use with caution!!!',
      20 => 'prune_search_keyword',
      21 => 'Mots-clés',//'内容关键字',  // 'Keywords',
      22 => 'Plusieurs mots-clés séparés par une virgule ",", les mots clés qualificatif {x} peuvent être utilisés.',//'多关键字中间请用半角逗号 "," 隔开，关键词可以用限定符 {x}', // 'Separate multiple keywords with a comma ",", keywords qualifier {x} can be used.',
      23 => 'prune_search_lengthlimit',
      24 => 'La longueur du contenu minimal',//'内容最小长度',  // 'Minimal content length',
      25 => 'Cette fonction va ajouter la charge du serveur',//'本功能会加重服务器负担',  // 'This feature will add the server loading',
    ),
  ),
  236 =>
  array (
    'index' =>
    array (
      'Gestion Corbeille' => 'action=recyclebin',//'主题回收站',  // 'Recycle Bin Management'
      'Recherche' => 'action=recyclebin&operation=search',//'搜索',  //  'Search'
    ),
    'text' =>
    array (
      0 => 'Gestion Corbeille &raquo; Recherche',//'主题回收站 &raquo; 搜索',  // 'Recycle Bin Management &raquo; Search',
      1 => 'recyclebin_search',  
      2 => 'Rechercher les posts pertinents pour suppression',//'搜索符合条件的被删帖子',  //  'Search relevant posts for delete',
      3 => 'recyclebin_search_forum',
      4 => 'Recherche dans les forums',//'所在版块',  // 'Search in Forums',
      5 => 'recyclebin_search_author',
      6 => 'Nom de l\'Auteur',//'原帖作者',  // 'Author name',
      7 => 'Séparez les noms par une virgule ","',//'多个用户名间请用半角逗号 "," 隔开',  //  'Separate multiple names with a comma ","',
      8 => 'recyclebin_search_keyword',
      9 => 'Mots-clés',//'标题关键字',  // 'Keywords', 
      10 => 'Séparez les mots clés par une virgule ","',//'多关键字中间请用半角逗号 "," 隔开',  //  'Separate multiple keywords by a comma ","',
      11 => 'recyclebin_search_admin',
      12 => 'Supprimé par le gestionnaire',//'删帖管理员',  // 'Deleted by manager',
      13 => 'Plusieurs noms d\'utilisateurs séparés par une virgule ","',//'多个用户名间请用半角逗号 "," 隔开',  // 'Separate multiple user names by a comma ","',
      14 => 'recyclebin_search_post_time',
      15 => 'Affichage intervalle de temps',//'帖子发表时间范围',  // 'Posting time range',
      16 => 'Format est aaaa-mm-jj, laisser vide pour aucune restrictions',//'格式 yyyy-mm-dd，不限制请留空',  // 'Format yyyy-mm-dd, leave blank for no restrictions',
      17 => 'recyclebin_search_mod_time',
      18 => 'Supprimé dans la plage horaire',//'删帖时间范围', //  'Deleted in time range',
      19 => 'Format est aaaa-mm-jj, laisser vide pour aucune restrictions',//'格式 yyyy-mm-dd，不限制请留空',  // 'Format yyyy-mm-dd, leave blank for no restrictions',
      20 => 'recyclebin_search_security_thread',
      21 => 'Ne contient que les discussions de traitement de sécurité Tencent',//'仅包含腾讯安全处理的主题',  // 'Contains only the Tencent security handling threads',
    ),
  ),
  237 =>
  array (
    'index' =>
    array (
      'Discussion Corbeille' => 'action=recyclebin',//'主题回收站',  // 'Thread Recycle Bin' 
      'Clean-up' => 'action=recyclebin&operation=clean',//'清理',  // 'Clean-up'
    ),
    'text' =>
    array (
      0 => 'Discussion Corbeille &raquo; Clean-up',//'主题回收站 &raquo; 清理',  //  'Thread Recycle Bin &raquo; Clean-up',
      1 => 'recyclebin_clean',
      2 => 'En vrac nettoyage du fil de la discussion de la Corbeille',//'批量清空回收站',  //  'Bulk clean-up the thread Recycle Bin',
      3 => 'recyclebin_clean_days',
      4 => 'Nettoyez le fil de la discussion de la Corbeille plus de (jours)',//'清空多少天以前的回收站帖子',  // 'Clean up the thread Recycle Bin older than (days)',
      5 => 'Mis à 0 pour TOUT nettoyer',//'0 为清空全部',  //  'Set to 0 for clean ALL',
    ),
  ),
  238 =>
  array (
    'index' =>
    array (
      'Posts Corbeille' => 'action=recyclebinpost',//'回帖回收站',  //  'Posts Recycle Bin'
      'Recherche' => 'action=recyclebinpost&operation=search',//'搜索',  // 'Search'
    ),
    'text' =>
    array (
      0 => 'Posts Corbeille &raquo; Recherche',//'回帖回收站 &raquo; 搜索',  // 'Posts Recycle Bin &raquo; Search',
      1 => 'recyclebinpost_search',
      2 => 'Recherche de posts liés à supprimer',//'搜索符合条件的被删回帖',  // 'Searching for related posts to be deleted',
      3 => 'recyclebinpost_search_forum',
      4 => 'Recherche dans les forums',//'所在版块',  //  'Search in Forums',
      5 => 'recyclebinpost_search_author',
      6 => 'Nom de l\'Auteur',//'原帖作者',  // 
      7 => 'Plusieurs noms d\'Utilisateurs séparés par une virgule ","',//'多个用户名间请用半角逗号 "," 隔开',  // 'Separate multiple user names by a comma ","',
      8 => 'recyclebinpost_search_keyword',
      9 => 'Mots-clés',//'标题关键字',  //  'Keywords',
      10 => 'Séparez les mots clés par une virgule ","',//'多关键字中间请用半角逗号 "," 隔开',  // 'Separate multiple keywords by a comma ","',
      11 => 'recyclebin_search_post_time',
      12 => 'Affichage Plage horaire',//'帖子发表时间范围',  // 'Posting time range',
      13 => 'Format est yyyy-mm-dd, laisser vide pour aucune restrictions',//'格式 yyyy-mm-dd，不限制请留空',  // 'Format yyyy-mm-dd, leave blank for no restrictions',
      14 => 'postsplit',
      15 => 'Posts Brisés',//'帖子分表',  // 'Split Posts',
      16 => 'recyclebin_search_security_thread',
      17 => 'Ne contient que les discussions de traitement de sécurité Tencent',//'仅包含腾讯安全处理的主题',  // 'Contains only the Tencent security handling threads',
    ),
  ),
  239 =>
  array (
    'index' =>
    array (
      'Posts Corbeille' => 'action=recyclebinpost',//'回帖回收站',  //  'Posts Recycle Bin'
      'Clean-up' => 'action=recyclebinpost&operation=clean',//'清理',  //  'Clean-up' 
    ),
    'text' =>
    array (
      0 => 'Posts Corbeille &raquo; Clean-up',//'回帖回收站 &raquo; 清理',  // 'Posts Recycle Bin &raquo; Clean-up',
      1 => 'recyclebinpost_clean',  
      2 => 'En vrac Nettoyez le Post Corbeille',//'批量清空回帖回收站',  // 'Bulk Clean the Post Recycle Bin',
      3 => 'recyclebinpost_clean_days',
      4 => 'Nettoyez le Post Corbeille Pour de combien de jours avant',//'清空多少天以前的回帖回收站帖子',  // 'Clean the Post Recycle Binfor how many days before',
      5 => 'Mis à 0 pour TOUT nettoyer',//'0 为清空全部',  // 'Set to 0 for clean ALL',
    ),
  ),
  240 =>
  array (
    'index' =>
    array (
      'Rapports d\'Utilisateurs' => 'action=report',//'用户举报',  //'User Reports' 
    ),
    'text' =>
    array (
      0 => 'Rapports d\'Utilisateurs',//'用户举报',  // 'User Reports',
      1 => 'report_tips',  
      2 => 'Selon le rapport de la situation réelle, de déclarer personne addition et soustraction des points, définir des points aux utilisateurs de signaler l\'utilisation de primes et de punitions de primes bonus et de punitions lorsque les rapports premiers informateurs.',//'可以根据举报真实情况，给举报者加减积分，设置用户举报奖惩使用的积分多人举报时奖惩第一个举报人。',  //  'According to report the real situation, to report person addition and subtraction points, set points users to report the use of rewards and punishments than rewards and punishments when the first reports informants.',
    ),
  ),
  241 =>
  array (
    'index' =>
    array (
      'Réglages de Base' => 'action=setting&operation=basic', // 'Base Settings'
    ),
    'text' =>
    array (
      0 => 'Réglages de Base',//'站点信息',  // 'Base Settings',
      1 => 'setting_basic_bbname',
      2 => 'Nom du Site',//'站点名称',  // 'Site Name',
      3 => 'Le nom du site sera affiché dans le titre de la fenêtre du navigateur, etc.',//'站点名称，将显示在浏览器窗口标题等位置',  // 'The site name will be displayed in the browser window title, etc.',
      4 => 'setting_basic_sitename',
      5 => 'Nom du Site',//'网站名称', //  'Site Name',
      6 => 'Ce nom du site apparaît dans le bas de page de la page Contact & Services',//'网站名称，将显示在页面底部的联系方式处', // 'This site name will appear in the page bottom of Contact & Services page',
      7 => 'setting_basic_siteurl',
      8 => 'URL du Site',//'网站 URL',  // 'Site URL',
      9 => 'L\'URL du Site, sera présenté comme un lien en bas de page',//'网站 URL，将作为链接显示在页面底部',  // 'The site URL, will shown as a link in the page bottom',
      10 => 'setting_basic_adminemail',
      11 => 'E-mail Administrateur ',//'管理员邮箱',  // 'Administrator Email',
      12 => 'Courriel de l\'Administrateur est utilisé comme adresse de l\'expéditeur dans un système de messages e-mail.',//'管理员 E-mail，将作为系统发邮件的时候的发件人地址', //  'Administrator E-mail is used as a sender address in a system e-mail messages.',
      13 => 'setting_basic_site_qq',
      14 => 'QQ numéro de service client en ligne', //  'QQ在线客服号码', 
      15 => 'Je mis mon statut en ligne QQ', //   '设置我的QQ在线状态', 
      16 => 'setting_basic_icp',
      17 => 'Les informations d\'enregistrement du site Web',//'网站备案信息代码',  // 'Web site registration information',
      18 => 'Le responsable de la compagnie information d\'enregistrement est affiché en bas de page. Laissez ce champ vide si aucune information.',//'页面底部可以显示 ICP 备案信息，如果网站已备案，在此输入你的授权码，它将显示在页面底部，如果没有请留空',  //   'The official company registration info is displayed at the page bottom. Leave blank if no such info.',
      19 => 'setting_basic_boardlicensed',
      20 => 'Voir lien informations de licence',//'显示授权信息链接',  //  'Show license information link',
      21 => 'Choisir &quot;Oui&quot; pour l\'affichage  au pied de page un lien pour les professionnels les utilisateurs autorisés, le lien pointera vers le Discuz! Site officiel, les utilisateurs peuvent utiliser ce lien pour vérifier leur Discuz! qui est commercialement sous licence.',//'选择“是”将在页脚显示商业授权用户链接，链接将指向 Discuz! 官方网站，用户可通过此链接验证其所使用的 Discuz! 是否经过商业授权',  //  'Select &quot;Yes&quot; for display in the footer a link for business authorized users, the link will point to the Discuz! Official website, users can use this link to verify their Discuz! is commercially licensed.',
      22 => 'setting_basic_stat',
      23 => 'Tiers Site code statistique',//'网站第三方统计代码',  //  'Third-party site statistical code',
      24 => 'Afficher les statistiques tiers en bas de page',//'页面底部可以显示第三方统计',  //  'Display third-party statistics at the page bottom',
      25 => 'setting_basic_bbclosed',
      26 => 'Fermer le Site',//'关闭站点',  // 'Close the site',
      27 => 'Fermer temporairement le site. L\'Accès sera désactivé pour tous les visiteurs à l\'exception des Administrateurs.',//'暂时将站点关闭，其他人无法访问，但不影响管理员访问', // 'Close temporarily the site. Access will be disabled for all visitors except of administrators.',
      28 => 'setting_basic_bbclosed',
      29 => 'Fermer le Site',//'关闭站点',  //  'Close the site',
      30 => 'Fermer temporairement le site. L\'Accès sera désactivé pour tous les visiteurs à l\'exception des Administrateurs.',//'暂时将站点关闭，其他人无法访问，但不影响管理员访问', // 'Close temporarily the site. Access will be disabled for all visitors except of administrators.',
      31 => 'setting_basic_closedreason',
      32 => 'Motif de fermeture',//'关闭站点的原因',  //  'Close Reason',
      33 => 'Un message concernant pourquoi le site est fermé',//'站点关闭时出现的提示信息',  // 'A message about why the site shut is down',
      34 => 'setting_basic_bbclosed_activation',
      35 => 'Le site est fermé. Aller à UCenter pour l\'activer.',//'站点关闭时允许 UCenter 中的用户激活',  // 'The site is closed. Go to UCenter for activate it.',
    ),
  ),
  242 =>
  array (
    'index' =>
    array (
      'Paramètres Espace' => 'action=setting&operation=home',//'空间设置',  //  'Space settings'
      'Paramètres de Base' => 'action=setting&operation=home&anchor=base',//'基本设置', //  'Basic settings'
    ),
    'text' =>
    array (
      0 => 'Paramètres Espace &raquo; Paramètres de Base',//'空间设置 &raquo; 基本设置', //  'Space settings &raquo; Base settings',
      1 => 'setting_follow_base_default_follow_retain_day',
      2 => 'Diffusion du délai d\'archivage',//'广播存档时间', //   'Broadcast archive time',
      3 => 'Diffusions anciens supérieurs au nombre de jours seront déplacés vers la table archive (unité: jours)',//'广播超过多少天移入存档表(单位：天)',  //  'Broadcastings older than this number of days will be moved to the archive table (unit: days)',
      4 => 'setting_follow_base_default_view_profile',
      5 => 'Par défaut Voir le profil',//'默认查看个人资料', //   'Default View Profile',
      6 => 'Ce réglage est efficace pour l\'Administrateur, après l\'activation de l\'Administrateur afficher la page de profil utilisateur par défaut',//'该设置对管理员有效，开启后默认管理员查看将直接查看个人资料页面',  //  'This setting is effective for administrator, after enabling the administrator will view the user profile page by default',
    ),
  ),
  243 =>
  array (
    'index' =>
    array (
      'Paramètres Espace' => 'action=setting&operation=home',//'空间设置',  //  'Space settings' 
      'Confidentialité' => 'action=setting&operation=home&anchor=privacy',//'隐私设置',  //   'Privacy' 
    ),
    'text' =>
    array (
      0 => 'Paramètres Espace &raquo; Confidentialité',//'空间设置 &raquo; 基本设置', //  'Space settings &raquo; Base settings',
      1 => 'setting_home_base_feedday',
      2 => 'Nombre de jours de stockage des flux dynamique personnelle',//'动态保留天数', //  'Number of days to store personal dynamic feeds',
      3 => 'Combien de temps pour stocker les dynamiques personnelles (par défaut est de 7 jours). Après ce nombre de jours tout dynamique personnelle sera nettoyé, ce qui peut assurer l\'efficacité du MySQL. Non recommandé de fixer à beaucoup de temps.',//'(默认为 7)个人动态的保留天数。超过该天数的个人动态会被清理掉，从而可以保证MySQL的效率。建议不要设置太长',  //   'How long to store personal dynamics (default is 7 days). After this number of days all personal dynamic will be cleaned, which can ensure the efficiency of MySQL. Not recommended to set too long.',
      4 => 'setting_home_base_feedmaxnum',
      5 => 'Nombre de flux dynamiques affichés à l\'Accueil',//'首页动态显示数', //   'Number of dynamics displayed at Home',
      6 => 'Combien de flux dynamiques à afficher dans la page d\'Accueil (par défaut est de  100). Pour assurer l\'efficacité du MySQL, il est recommandé pour régler pas trop élevé, mais d\'au moins pas  inférieure à 50.',//'(默认为 100)首页显示的好友动态将从这些数目的事件中进行合并显示。建议不要设置太多，从而可以保证MySQL的效率，但最少不能低于50',   //   'How many dynamics to display in the Home page (default is 100). For ensure the efficiency of MySQL, recommended to set not too much, but at least not less than 50.',
      7 => 'setting_home_base_feedhotday',
      8 => 'Jours éventail de Hot a recommandé à afficher',//'热点推荐的天数范围', // 'Days range of hot recommended to show', 
      9 => 'Définissez le nombre de jours pour afficher hot spot a recommandé à la page d\'accueil (par défaut est de 2 jours).',//'(单位天，默认为 2天)设置首页热点推荐选择的天数范围',  // 'Set the number of days to show recommended hot spot at the home page (default is 2 days).', 
      10 => 'setting_home_base_feedhotmin',
      11 => 'La valeur Hot minimale pour afficher à la page d\'Accueil',//'热点推荐的最小热度值', //   'Minimum hot value to show at home page',
      12 => 'Réglez la valeur a recommandé Hot lorsque apparaîtra en page d\'Accueil.',//'设置当发布的信息热度值超过多少后，才会显示在推荐里面', //   'Set the hot value when recommended will appear at home page.',
      13 => 'setting_home_base_feedtargetblank',
      14 => 'Ouvrir un lien flux dynamique dans une nouvelle fenêtre',//'动态链接新窗口打开', //  'Open a dynamic link in a new window',
      15 => 'Si ce paramètre est réglé, tous les liens flux dynamiques s\'ouvriront dans une nouvelle fenêtre.',//'如果选择是，用户阅读动态的时候，所有的链接都会在新窗口打开', //   'If this is set, all the feed links will open in a new window.',
      16 => 'setting_home_base_showallfriendnum',
      17 => 'Nombre de flux Amis pour afficher à l\'Accueil',//'首页动态默认标签设置', //   'Number of friend feeds to show at Home',
      18 => 'Combien de flux d\'Amis à afficher. Tous les autres peuvent être présentés dans Nouvelles des Amis. Ce paramètre prend le même effet dans les blogs et albums de liste.',//'好友数小于 该值的时候显示所有人的动态，超过这个数值显示好友动态。在日志、相册列表页面，该参数同样生效',  //   'How many friends feeds to show. All others may be shown in Friends News. This setting take the same effect in blogs and albums list page.',
      19 => 'setting_home_base_feedhotnum',
      20 => 'Nombre de  flux dynamiques  Hot représentés à l\'Accueil',//'首页显示热点的数目', //   'Number of hot feeds shown at home',
      21 => 'Par défaut est de 3. Si la valeur est sur  0, puis les messages hots ne seront pas affichés. Ne pas dépasser le maximum à 10. Le système obtiendra automatiquement 10 points Hots Les posts Hots, d\'où le numéro un du hots pot s\'affiche toujours, et le points Hots restants seront affichés de manière aléatoire.',//'(默认为 3)为0，则首页不显示热点推荐。最大不要超过10个。系统会自动获取10个热点，其中，排名第一的热点会固定显示，其余热点会随机显示',  //   'Default is 3. If set to 0, then hotspots wil not displayed. Do not exceed the maximum 10. The system get automatically 10 hot hotspots, from which the number one hotspot will be displayed allways, and the remaining hotspots will displayed randomly.',
      22 => 'setting_home_base_maxpage',
      23 => 'Liste nombre max. de sous-pages',//'列表最大分页数', //  'List max number of sub-pages',
      24 => 'Permet aux utilisateurs d\'afficher les sous-pages pas plus de cette valeur (par défaut 100). Plus grand nombre de sous-pages empêcher le serveur d\'avoir une plus grande surcharge.',//'(默认为 100)允许用户查看的最大分页数。当用户查看的分页数越大的时候，对服务器的负载压力就越大',  //  'Allows users to view the not more sub-pages than this value (default 100). More greater number of sub-pages will cause the server greater overloading.',
      25 => 'setting_home_base_sendmailday',
      26 => 'Jours hors connexion avant l\E-mail de préavis',//'邮件通知更新天数', //  'Days offline before e-mail notice',
      27 => 'Nombre de jours, lorsque l\'utilisateur ne se connecte plus, ensuite, après le système vous enverra un message de notification. Mis à 0 pour désactiver la notification par e-mail. Activé cette fonctionnalité sera légèrement augmenter la charge du serveur.',//'(默认 0) 单位：天，当用户多少天没有登录站点的时候才会给其发送邮件通知；设置为0，则不启用邮件通知功能；启用本功能将会轻微增加服务器负载',  // 'Number of days, when the user does not log in, after then the system will send a notification message. Set to 0 for disable e-mail notification. Enabled this feature will slightly increase the server loading.', 
      28 => 'setting_home_base_recycle_bin',
      29 => 'Ouvrir le journal Corbeille', //  '开启日志回收站',
      30 => 'Ouvrir cette fonction, tous les journaux seront placés dans la corbeille, et ne seront pas supprimés directement', //  '打开此功能后，所有被日志将被放在回收站中，而不会被直接删除',  //  
      31 => 'setting_home_base_groupnum',
      32 => 'Nombre d\'Amis des catégories',//'好友用户组个数',  //   'Number of friends categories',
      33 => 'Réglez combien d\'Amis des catégories chaque utilisateur peut avoir (valeur par défaut est 8).',//'(默认为 8)设置每个用户最多拥有的好友用户组个数',  //  'Set how many friends categories each user can have (default is 8).',
      34 => 'setting_home_base_default_',
      35 => '',
      36 => 'setting_home_base_default_doing',
      37 => 'Conseils par défaut pour le contenu effectué',//'默认记录提示内容',  //'Default tips for the doing content',  
      38 => 'L\'une des instructions énumérées ci-dessus seront affichés de manière aléatoire dans la zone à faire',//'一行为一个提示内容，这些内容将在记录发表框中显示，随机从这些默认提示中选取一行做为提示内容',   //  'One of the listed here prompts will be shown randomly in the doing box',
    ),
  ),
  244 =>
  array (
    'index' =>
    array (
      'Paramètres Espace' => 'action=setting&operation=home',//'空间设置',  //  'Space settings' 
      'Confidentialité' => 'action=setting&operation=home&anchor=privacy',//'隐私设置',  //   'Privacy' 
    ),
    'text' =>
    array (
      0 => 'Paramètres Espace &raquo; Confidentialité',//'空间设置 &raquo; 隐私设置',  //  'Space settings &raquo; Privacy', 
      1 => 'setting_home_privacy_new_user',
      2 => 'Les nouveaux paramètres de confidentialité par défaut utilisateur',//'新用户默认隐私设置',  //   'New user default privacy settings', 
      3 => 'setting_home_privacy_view_index',
      4 => 'Accueil Espace Personnel',//'个人空间首页',  //   'Personal Space Home',
      5 => 'setting_home_privacy_view_friend',
      6 => 'List Amis',//'好友列表',  //   'Friend list',
      7 => 'setting_home_privacy_view_wall',
      8 => 'Mur',//'留言板',  //   'Wall',
      9 => 'setting_home_privacy_view_feed',
      10 => 'Flux',//'动态',  // 'Feeds',  
      11 => 'Non visibles à tous les utilisateurs, lors de l\'Accès à la page de flux',//'非“全站用户可见”同时限制了动态首页的游客访问',  //  'Not visible to all users, when accessing the Feed page', 
      12 => 'setting_home_privacy_view_doing',
      13 => 'Actes',//'记录',  //'Doings',   
      14 => 'Toutefois une liste des actes peut apparaître dans d\'autres pages du site.',//'在全站的记录列表中可能会出现记录信息。 ',  //    'However a list of doings may appear in other pages of the site.',
      15 => 'setting_home_privacy_view_blog',
      16 => 'Blogs',//'日志',  //   
      17 => 'Parcourir les autorisations connexes doivent être réglés séparément pour chaque blog.',//'相关浏览权限需要在每篇日志中单独设置方可完全生效',  //   'Browse related permissions must be set separately for each blog.', 
      18 => 'setting_home_privacy_view_album',
      19 => 'Albums',//'相册',  // 'Albums', 
      20 => 'Voir droits d\'Albums connexes doivent être réglés individuellement pour chaque album pleinement en vigueur.',//'相关浏览权限需要在每个相册中单独设置方可完全生效',  // 'View related album rights need to be set individually for each album in full effect.',  
      21 => 'setting_home_privacy_view_share',
      22 => 'Partages',//'分享',  //  'Shares',
      23 => 'Toutefois, les actions peuvent apparaître dans d\'autres pages du site. ',//'在全站的分享列表中可能会出现分享信息。 ',  //'However, shares may appear in other site pages. ',   
      24 => 'setting_home_privacy_default_feed',
      25 => 'Par défaut les flux dynamiques  les paramètres de publication',//'默认动态发布设置',  //   'Default feeds publishing settings',
    ),
  ),
  245 =>
  array (
    'index' =>
    array (
      'Enregistrement et le contrôle d\'Accès' => 'action=setting&operation=access',    //  'Registration and access control' 
      'Enregistrement' => 'action=setting&operation=access&anchor=register',  //   'Registration' 
    ),
    'text' =>
    array (
      0 => 'Enregistrement et le contrôle d\'Accès &raquo; Enregistrement',//'注册与访问控制 &raquo; 注册',  //   'Registration and access control &raquo; Registration',
      1 => 'setting_access_register_status',
      2 => 'Autoriser l\'enregistrement pour les nouveaux utilisateurs',//'允许新用户注册',  //  'Allow registration for new users', 
      3 => 'Les paramètres pour permettre aux visiteurs du site de créer un nouveau compte. Vous pouvez choisir d\'enregistrer selon l\'approche de la demande sur le site',//'设置是否允许游客注册成为站点会员，你可以根据站点需求选择注册方式',  // 'Settings to allow the site visitors to register a new account. You can choose to register under the site demand approach',  
      4 => 'setting_access_register_invite_buyprompt',
      5 => 'Instructions pour l\'inscription des invités',//'邀请注册说明',  //   'Invited registration instructions', 
      6 => 'setting_access_register_invite_buy',
      7 => 'Permet le rechargement afin d\'acheter le code d\'invitation',//'允许充值购买邀请码',  //   'Allow recharge to buy invitation code',
      8 => 'Permettre aux visiteurs pour l\'achats prépayées du code d\'invitation d\'inscription en ligne. Elle est limitée à clôturer les inscriptions général efficacement. Avant d\'ouvrir cette fonctionnalité assurer que votre e-mail est normalement fonctionnelle.',//'允许游客通过在线充值购买注册邀请码。仅限于关闭普通注册时有效。开启前请确认您的电子商务及发送邮件功能可正常使用',  //   'Allow visitors to purchase online prepaid registration invitation code. It is limited to close General registration effectively. Before open this feature sure your e-mail is function normal.',
      9 => 'setting_access_register_invite_buyprice',
      10 => 'Code Invitation Prix (EURO)',//'邀请码单价(元)',  //   'Invitation code Price (USD)',
      11 => 'setting_access_register_invite_credit',
      12 => 'Inviter des points de fidélité utilisées pour l\'élargissement',//'邀请新注册会员奖励所用扩展积分',  //   'Invite reward points used to expand',
      13 => 'Enregistré utilisé invitation points de fidélité d\'extension, y compris les incitations d\'inviter et d\'être inviter aux quelconques points bonus',//'邀请新注册会员奖励所用的扩展积分，包括奖励邀请人和被邀请人的奖励积分',  //   
      14 => 'setting_access_register_invite_addcredit',
      15 => 'Être invité à un nombre de points de fidélité',//'被邀请人奖励积分数量',  //   'Be invited to one the number of reward points',
      16 => 'Après une inscription réussie par le code d\'invitation, points de fidélité seront invités à élargir le nombre de personnes',//'通过邀请码注册成功后，奖励被邀请人的扩展积分数量',  //    'After successful registration through invitation code, reward points to be invited to expand the number of people',
      17 => 'setting_access_register_invite_invitedcredit',
      18 => 'Montant des points de fidélité pour y inviter des personnes',//'邀请人奖励积分数量',  //   'Amount of reward points to invite people',
      19 => 'Après une inscription réussie par le code d\'invitation, les personnes invités recevront le nombre de points de fidélité.',//'通过邀请码注册成功后，奖励邀请人的扩展积分数量',  //   'After successful registration through invitation code, people invited will receive the number of reward points.',
      20 => 'setting_access_register_invite_group',
      21 => 'Groupe utilisateur initialement enregistré des utilisateurs invités',//'邀请注册用户初始用户组',  //   'Initial user group for registered by invitation users',
      22 => 'Définir un Groupe utilisateur par défaut pour les nouveaux membres invités',//'被邀请人的初始用户组',  //  'Set a default user group for new invited members',  
      23 => 'setting_access_register_invite_areawhite',
      24 => 'Absence d\'une liste de codes pour les zones réglementées d\'invitations',//'不受邀请码限制的地区列表',  //   'Without an invitation code restricted area list',
      25 => 'Quand un utilisateur se trouve dans cette liste d\'adresse, l\'enregistrement est activé sans un code d\'invitation. Saisissez une région par ligne, comme "Beijing" (sans les guillemets), laisser vide pour les limites non établies. Remarque: Pour déterminer la région correcte par votre adresse IP, veuillez télécharger le fichier de base de données d\'adresses IP qqwry.dat, puis le télécharger à  "data/ipdata/" répertoire et renommer à wry.dat. Également supprimer le fichier tinyipdata.dat.',//'当用户处于本列表中的地址时，注册时不受邀请码限制。每个地区一行，例如 "北京"(不含引号)，留空为不设置。注意：如要正确无误的判断您 IP 地址所在的地区，请到网上下载 qqwry.dat IP 地址库文件上传到 "data/ipdata/" 目录下更名为 wry.dat，同时删除 tinyipdata.dat 文件',  //  'When a user is in this list of address, registration is enabled without an invitation code. Enter one region per line, such as "Beijing" (without the quotes), leave it blank for not set limits. Note: To determine correct region by your IP address, please download the qqwry.dat IP address database file and then upload it to the "data/ipdata/" directory and rename to wry.dat. Also delete the tinyipdata.dat file.',
      26 => 'setting_access_register_invite_ipwhite',
      27 => 'Liste IP restreinte pour le code d\'invitation',//'不受邀请码限制的 IP 列表',  //  'Restricted IP list for invitation code',  
      28 => 'Quand un utilisateur se trouve dans cette liste d\'adresses IP, une inscription est disponible sans un code d\'invitation. Entrez une adresse IP par ligne. Vous pouvez saisir l\'adresse complète, ou seulement le début IP, par exemple, "192.168." (Sans les guillemets) pour correspondre à toutes les adresses de la gamme de 192.168.0.0 ~ 192.168.255.255. Laissez ce champ vide pour ne pas définir.',//'当用户处于本列表中的 IP 地址时，注册时不受邀请码限制。每个 IP 一行，既可输入完整地址，也可只输入 IP 开头，例如 "192.168."(不含引号) 可匹配 192.168.0.0～192.168.255.255 范围内的所有地址，留空为不设置',  //  'When a user is in this IP address list, a registration is available without an invitation code. Enter one IP per line. You can enter the full address, or only the IP beginning, for example, "192.168." (Without quotation marks) to match all addresses from the range of 192.168.0.0 ~ 192.168.255.255. Leave it blank to not set.',
      29 => 'setting_access_register_regclosemessage',
      30 => 'Fermer le message d\'enregistrement',//'关闭注册提示信息',  //  'Close registration message', 
      31 => 'Une raison pour laquelle l\'enregistrement sur le site est fermé',//'当站点关闭注册时的提示信息',  //    'A reason why the site registration is closed',
      32 => 'setting_access_register_name',
      33 => 'Nom d\'inscription:',//'注册地址:',  // 'Registration name:',  
      34 => 'Réglez le site en place adresse mod, par défaut "Register", changer le nom du logiciel d\'irrigation (spam) permet d\'éviter différent de celui de Discuz! X versions avant de modifier ce paramètre, ne modifiez pas le nom du fichier, seule cette modification peut être',//'设置站点注册地址 mod 值，默认为“register”，修改名称有利于防止灌水软件，有别于 Discuz! X 之前的版本更改了此设置，不需要修改任何文件名称，只在此修改即可',
      35 => 'setting_access_register_send_register_url',
      36 => 'Envoyer le lien d\'inscription par E-mail',//'通过邮件发送注册链接',  //   'Send registration link via e-mail', 
      37 => 'Après avoir activé le système enverra une URL d\'inscription à la messagerie de l\'utilisateur. Les utilisateurs sont arrivés à cette URL est deviennent automatiquement des enregistrés. Également a recommandé de faire les mêmes paramètres dans UCenter: permettre l\'enregistrement de compte que par boîte courriel. Faites attention à: serveur UC - Paramètres de messagerie - Assurez-vous que l\'E-mail envoyé est activée.',//'开启后系统会发一条注册的地址到用户的邮箱，从该地址链接过来的允许注册，同时建议UCenter中开启一个邮箱只允许注册一个帐户注意：只有在站长 - 邮件设置中完成邮件设置，确保邮件能发送成功下可以开启该功能 ',  //  'After enabling the system will send a registration URL to the user mailbox. Users came to this URL became registered. Also recommended to make the same settings in UCenter: Enable account registration only by mailbox. Pay attention to: UC server - Mail settings - Ensure that the E-mail sent is enabled.',
      38 => 'setting_access_register_forge_email',
      39 => 'Désinscription email (obligatoire)', //  '取消注册邮箱必填',     
      40 => 'Ouvrir si l\'utilisateur ne remplit pas l\'email d\'inscription va générer automatiquement une adresse électronique aléatoire', //   '开启后如果用户不填写注册邮箱，将自动生成一个随机邮箱地址',
      41 => 'setting_access_register_link_name',
      42 => 'Enregistrer texte du lien',//'注册链接文字',  //  'Register link text', 
      43 => 'Définir le texte du lien pour la page d\'inscription du site, par défaut est &quot;Inscription&quot;',//'设置站点注册页的链接文字，默认为“立即注册”',  //  'Set the link text for the site registration page, the default is &quot;Register&quot;', 
      44 => 'setting_access_register_censoruser',
      45 => 'Mots désactivés dans le Profil',//'用户信息保留关键字',   //  'Disabled words in Profile', 
      46 => 'Les mots que les utilisateurs ne peuvent pas utiliser leurs informations de l\'utilisateur. Une ligne pour chaque mot-clé, vous pouvez utiliser un joker &quot;*&quot;, par exemple &quot;*modérateur*&quot; (sans les guillemets).',//'用户在其用户信息中无法使用这些关键字。每个关键字一行，可使用通配符 "*" 如 "*版主*"(不含引号)',¨ //  'The words that users can not use in their user information. One Line for each keyword, you can use a wildcard &quot;*&quot;, i.e. &quot;*moderator*&quot; (without quotation marks).',
      47 => 'setting_access_register_pwlength',
      48 => 'Longueur minimale du mot de passe',//'密码最小长度',  //  'Minimal password length', 
      49 => 'Réglez la longueur du mot de passe le plus court activée pour l\'enregistrement de nouveaux utilisateurs, la valeur 0 pour aucune limite',//'新用户注册时密码最小长度，0或不填为不限制',  //  'Set the shortest password length enabled for new user registration, set to 0 for no limits',  
      50 => 'setting_access_register_strongpw',
      51 => 'Mot de passe complexité Brutale',//'强制密码复杂度',  //  'Force password complexity', 
      52 => 'Les utilisateurs de regisration doivent utiliser des types de caractères sélectionnés par mot de passe. Ddo pas  choisir un mot de passe sans restriction',//'新用户注册时密码中必须存在所选字符类型，不选则为无限制',  //  'Users at regisration must use in the password selected character types. Ddo not choose for unrestricted password',
      53 => 'setting_access_register_verify',
      54 => 'New user registration validation',//'新用户注册验证',  //    'New user registration validation',
      55 => 'Choisir &quot;Non&quot; si les utilisateurs peuvent directement s\'inscrire, choisir&quot;Email Verification&quot; pour l\'envoi d\'un courriel de vérification afin de confirmer l\'enregistrement; select &quot;validation manuelle&quot; pour l\'Administrateur approuver manuellement l\'enregistrement de nouveaux utilisateurs.',//'选择“无”用户可直接注册成功；选择“Email 验证”将向用户注册 Email 发送一封验证邮件以确认邮箱的有效性（开启“通过邮件发送注册链接”该设置自动通过验证）；选择“人工审核”将由管理员人工逐个确定是否允许新用户注册',  //  'Select &quot;No&quot; if users can directly register; select &quot;Email Verification&quot; for sending a verification email to confirm the Registration; select &quot;manual validation&quot; for administrator manually approve new user registration.',
      56 => 'setting_access_register_verify_areawhite',
      57 => 'Location &quot;White List&quot;',//'不受新用户注册验证限制的地区列表',  //  'Location &quot;White List&quot;',  
      58 => 'Lorsque l\'utilisateur est à partir de cette liste de lieux, ne pas effectuer l\'examen d\'inscription. Entrez une ligne par chaque région, tels que "Pékin" (sans les guillemets), ou laisser vide pour ne pas définir',//'当用户处于本列表中的地址时，直接注册成功，无需新用户注册验证。每个地区一行，例如 "北京"(不含引号)，留空为不设置',  //  'When the user is from this location list, do not perform the registration examination. Enter one line per each region, such as "Beijing" (without the quotes), or leave blank for not set',
      59 => 'setting_access_register_verify_ipwhite',
      60 => 'IP Liste blanche (pas de restrictions)',//'不受新用户注册验证限制的 IP 列表',  //  'IP White list (no restrictions)', 
      61 => 'Lorsque l\'utilisateur se trouve dans cette liste des adresses IP, l\'enregistrement sans examen. Une IP par ligne, vous pouvez saisir une adresse complète, ou seulement le début , soit &quot;192.168.&quot; (Sans les guillemets) à toutes les adresses dans la plage de 192.168.0.0 ~ 192.168.255.255. Laissez vide pour ne pas définir.',//'当用户处于本列表中的 IP 地址时，直接注册成功，无需新用户注册验证。每个 IP 一行，既可输入完整地址，也可只输入 IP 开头，例如 "192.168."(不含引号) 可匹配 192.168.0.0～192.168.255.255 范围内的所有地址，留空为不设置',    //    'When the user is in this list of IP addresses, the registration without examination. One IP per line, you can enter a full address, or only the IP beginning, i.e. &quot;192.168.&quot; (Without quotation marks) to match all addresses in the range of 192.168.0.0 ~ 192.168.255.255. Leave blank to not set.',
      62 => 'setting_access_register_maildomain',
      63 => 'Restriction de domaine de courrier recommandé', //  '注册邮箱域名限制',
      64 => 'Choisir "Non" directement désactiver cette fonction, choisissez "mode liste blanche", seulement de limiter la liste de domaine de messagerie peuvent être enregistrées, choisissez "mode liste noire" pour restreindre la liste des e-mail nom de domaine ne peut être enregistré.', //  '选择“无”直接关闭本功能；选择“白名单模式”，只有限制名单中的邮箱域名可以注册；选择“黑名单模式”，限制名单中的邮箱域名不能注册。',  
      65 => 'setting_access_register_maildomain_list',
      66 => 'la liste de restriction', //  '限制名单',  //   
      67 => 'Lorsque le nom de boîte aux lettres dans la liste des adresses, selon les options ci-dessus en conséquence restreindre les opérations. Entrez la restriction de domaine e-mail peut être, par exemple, à l\'abri xxx@xxx.com Domaine, entrez simplement xxx.com. un nom de domaine par ligne.', //  '当邮箱的域名处于本列表中的地址时，将根据上述选项进行相应限制操作。输入要限制的邮箱域名即可，例如屏蔽 xxx@xxx.com 的域，只需输入 xxx.com。每行一个域名。',   
      68 => 'setting_access_register_ctrl',
      69 => 'Inscription limite d\'intervalle pour la même IP (heures)',//'同一 IP 注册间隔限制(小时)',  //  'Registration interval limit for the same IP (hours)', 
      70 => 'Intervalle de temps minimum pour la même IP peut enregistrer un compte, 0 pour aucune limite',//'同一 IP 在本时间间隔内将只能注册一个帐号，0 为不限制',  //   'The minimum time interval for the same IP can register an account, 0 for no limits',
      71 => 'setting_access_register_floodctrl',
      72 => 'Le nombre maximum d\'inscriptions activé la même adresse IP en 24 heures',//'同一 IP 在 24 小时允许注册的最大次数',  //    'Maximum number of registrations enabled for the same IP in 24 hours',
      73 => 'Limiter le nombre de tentatives d\'enregistrement à partir de la même adresse IP dans les 24 heures. Valeur recommandée dans la gamme de 30 à 100. Réglez 0 pour aucune limite.',//'同一 IP 地址在 24 小时内尝试注册的次数限制，建议在 30 - 100 范围内取值，0 为不限制',  //   'Restrict the number of attempts to register from the same IP address within 24 hours. Recommended value in range of 30 - 100. Set 0 for no limits.',
      74 => 'setting_access_register_ipctrl_time',
      75 => 'Limiter l\'enregistrement à partir de la même IP en un intervalle de temps (heures):',//'限时注册IP注册间隔限制(小时):',  //  'Limit registration from same IP in a time interval (hours):', 
      76 => 'Les utilisateurs de cette liste d\'adresses IP seront limitées à enregistrer seulement un nouveau compte à partir de la même adresse IP en cet intervalle de temps',//'用户处于限时注册的 IP 列表中的 IP 地址时，同一 IP 在本时间间隔内将只能注册一个帐号',  //  'Users from this IP list will be limited to register only one new account from the same IP address in this interval of time', 
      77 => 'setting_access_register_ipctrl',
      78 => 'Liste d\'adresses IPs limitées pour le registre',//'限时注册的 IP 列表',  //   'Limited IP list for register',
      79 => 'Lorsque l\'utilisateur se trouve dans cette liste d\'adresses IP, il a permis de créer un compte seulement dans les 72 heures. Une IP par ligne. Vous pouvez entrer une adresse IP complète ou juste le début , soit &quot;192.168.&quot; (Without quotation marks) to match all addresses in the range of 192.168.0.0 ~ 192.168.255.255. Leave blank for no limits.',//'当用户处于本列表中的 IP 地址时，在限时注册IP注册间隔限制内将至多只允许注册一个帐号。每个 IP 一行，既可输入完整地址，也可只输入 IP 开头，例如 "192.168."(不含引号) 可匹配 192.168.0.0～192.168.255.255 范围内的所有地址，留空为不设置',    //    'When the user is in thistlist of IP addresses, he enabled to register an account only in 72 hours. One IP per line. You can enter a full IP address or just the IP beginning, i.e. &quot;192.168.&quot; (Without quotation marks) to match all addresses in the range of 192.168.0.0 ~ 192.168.255.255. Leave blank for no limits.',
      80 => 'setting_access_register_welcomemsg',
      81 => 'Envoyer Bienvenue message à l\'utilisateur enregistré par courriel',//'发送欢迎信息',  //   'Send Welcome message to registered user by Email',
      82 => 'Envoyer automatiquement un message de bienvenue au nouvel utilisateur enregistré',//'可选择是否自动向新注册用户发送一条欢迎信息',  //  'Automatically send a welcome message to new registered user',  
      83 => 'setting_access_register_welcomemsgtitle',
      84 => 'Bienvenue titre du message',//'欢迎信息标题',  // 'Welcome message title',   
      85 => 'Le système vous enverra un message de bienvenue à ce titre. Ne supporte pas le HTML, pas plus de 75 caractères. ',//'系统发送的欢迎信息的标题，不支持 HTML，不超过 75 字节。 ',  //   'The system will send a welcome message with this title. Does not support HTML, not longer than 75 characters. ',
      86 => 'setting_access_register_welcomemsgtxt',
      87 => 'Bienvenue contenu du message',//'欢迎信息内容',  //   'Welcome message content',
      88 => 'Le système envoie un message de bienvenue à ce contenu. Titre et contenu prend en charge la substitution de variable. Utilisez les variables suivantes: {username}: Utilisateur, {time}: heure transmises, {sitename}: Nom du site, {bbname}: Nom du site, {adminemail}: E-mail Administrateur. ',//'系统发送的欢迎信息的内容。标题内容均支持变量替换，可以使用如下变量:{username} : 用户名{time} : 发送时间{sitename} : 站点名称{bbname} : 站点名称{adminemail} : 管理员 Email',     //   'The system send a welcome message with this content. Title and content supports variable substitution. Use the following variables: {username}: User, {time}: Time Sent, {sitename}: Site name, {bbname}: Site name, {adminemail}: Administrator Email. ',
      89 => 'setting_access_register_bbrules',
      90 => 'Voir le site Conditions générales d\'utilisation',//'显示网站服务条款',  //   'Show the site Terms of Service', 
      91 => 'Voir Modalités de service au nouvel utilisateur à l\'inscription.',//'新用户注册时显示网站服务条款',  // 'Show Terms of Service to new user in registration.',  
      92 => 'setting_access_register_bbruleforce',
      93 => 'S\'il faut forcer nos Conditions générales d\'utilisation',//'是否强制显示网站服务条款',  //   'Whether to force the site Terms of Service',
      94 => 'Cette option pour l\'enregistrement de l\'utilisateur, les premiers termes complets de service verront le site, vous devez accepter avant qu\'ils puissent continuer à être enregistré',//'选择是则在用户注册时，首先将看到网站服务条款全文，必须同意才可以继续注册',  // 'This option in for the user registration, the first full terms of service will see the site, you must agree to before they can continue to be registered', 
      95 => 'setting_access_register_bbrulestxt',
      96 => 'Termes de contenu du service',//'服务条款内容',  //   'Terms of Service Content',
      97 => 'Entrez les termes de contenu du service',//'网站服务条款的详细内容',  //    'Enter the Terms of Service content',
    ),
  ),
  246 =>
  array (
    'index' =>
    array (
      'Enregistrement et contrôle d\'Accès' => 'action=setting&operation=access',//'注册与访问控制',  //   'Registration and access control'
      'Contrôle d\'Accès' => 'action=setting&operation=access&anchor=access',//'访问控制', //    'Access Control'
    ),
    'text' =>
    array (
      0 => 'Enregistrement et contrôle d\'Accès &raquo; Contrôle d\'Accès',//'注册与访问控制 &raquo; 访问控制', //  'Registration and access control &raquo; Access Control',
      1 => 'setting_access_access_newbiespan',
      2 => 'Période d\'aprobation Newbie (heures)',//'新手见习期限(分钟)', //  'Newbie Aprobation period (hours)',
      3 => 'Nouveaux utilisateurs enregistrés ne seront pas autorisés à poster un court message dans ce délai, à l\'exclusion des Administrateurs et Modérateurs. Réglez 0 pour aucune limite',//'新注册用户在本期限内将无法发帖和短消息，不影响版主和管理员，0 为不限制',  //    'New registered users will not allowed to post a short messages in this period, excluding administrators and moderators. Set 0 for no limit',
      4 => 'setting_access_access_ipaccess',
      5 => 'Liste IP blanche pour accéder au site',//'允许访问站点的 IP 列表',  //  'White IP list for access the site',
      6 => 'Seuls les utilisateurs de cette liste d\'adresses IP peuvent accéder au site, lorsque d\'autres utilisateurs en dehors de cette liste seront interdites d\'accès. Attention: si l\'Administrateur n\'est pas dans cette liste, il ne sera pas en mesure d\'accéder aussi! Assurez-vous d\'utiliser cette fonction avec précaution. Une IP par ligne, vous pouvez entrer l\'adresse IP complète ou seulement le début , soit &quot;192.168.&quot; (Sans les guillemets) à toutes les adresses dans la gamme de 192.168.0.0 ~ 192.168.255.255. Laissez ce champ vide pour permettre l\'accès de tous les IPs.',//'只有当用户处于本列表中的 IP 地址时才可以访问本站点，列表以外的地址访问将视为 IP 被禁止，仅适用于诸如企业、学校内部站点等极个别场合。本功能对管理员没有特例，如果管理员不在此列表范围内将同样不能登录，请务必慎重使用本功能。每个 IP 一行，既可输入完整地址，也可只输入 IP 开头，例如 "192.168."(不含引号) 可匹配 192.168.0.0～192.168.255.255 范围内的所有地址，留空为所有 IP 除明确禁止的以外均可访问',  // 'Only users from this list of IP addresses can access the site,  when other users outside this list will be prohibited to access. Attention: if the administrator is not in this list, he will not able to access too! Make sure to use this feature carefully. One IP per line, you can enter the full IP address, or only the IP beginning, i.e. &quot;192.168.&quot; (Without quotation marks) to match all addresses in the range of 192.168.0.0 ~ 192.168.255.255. Leave blank for enable access from all IP.',  
      7 => 'setting_access_access_adminipaccess',
      8 => 'Autoriser l\'accès à l\'admin-centrale pour la liste IPs',//'允许访问管理中心的 IP 列表',  //  'Allow access to admin-center for the IP list',
      9 => 'Permettre l\'accès au centrale administrateur uniquement lorsque l\'Administrateur (Super modérateurs et animateurs non inclus!) Est dans cette liste d\'adresses IP. Autres adresses IP ne seront pas en mesure d\'accéder, mais peut encore visiter le site de l\'interface utilisateur frontal. Assurez-vous d\'utiliser cette fonction avec précaution. Une IP par ligne, vous pouvez entrer l\'adresse IP complète ou seulement le début , soit &quot;192.168.&quot; (Sans les guillemets) pour correspondre à toutes les adresses de la gamme de 192.168.0.0 ~ 192.168.255.255. Laissez ce champ vide pour aucune limite.',//'只有当用户处于本列表中的 IP 地址时才可以访问站点管理中心，列表以外的地址访问将无法访问，但仍可访问站点前端用户界面，请务必慎重使用本功能。每个 IP 一行，既可输入完整地址，也可只输入 IP 开头，例如 "192.168."(不含引号) 可匹配 192.168.0.0～192.168.255.255 范围内的所有地址，留空为所有 IP 除明确禁止的以外均可访问管理中心',  //   'Enable access to the admin center only when the administrator (super moderators and moderators not included!) is in this list of IP addresses. Other IP addresses will not be able to access, but can still visit the site front-end user interface. Be sure to use this feature carefully. One IP per line, you can enter the full IP address or only the IP beginning, i.e. &quot;192.168.&quot; (Without quotation marks) to match all addresses from the range of 192.168.0.0 ~ 192.168.255.255. Leave blank for no limits.',
      10 => 'setting_access_access_domainwhitelist',
      11 => 'Liste des noms de domaine Autoriser',//'允许发表的域名列表',  //  'Allow list of domain names',
      12 => 'Lorsque la publication est au groupe URL de l\'interdiction de l\'utilisateur, ou l\'URL n\'est pas résolu, le nom de domaine dans la liste sous l\'URL peut toujours être affiché, l\'analyse normale, un nom de domaine par ligne, le nom par défaut pour le site peut être affiché.',//'当用户组禁止发表 URL ，或者 URL 不解析时，处于本列表中的域名下的 URL 仍然可以正常发表，正常解析，每行一个域名，默认为本站域名可以正常发表',  //    'When the user group publication ban URL, or URL does not resolve, the domain name in the list under the URL can still be posted, the normal analysis, a domain name per line, the default name for the site can be posted.',
    ),
  ),
  247 =>
  array (
    'index' =>
    array (
      'Réglages du style' => 'action=setting&operation=styles',//'界面设置',  //  'Style settings' 
      'Global' => 'action=setting&operation=styles&anchor=global',//'全局',  //  'Global' 
    ),
    'text' =>
    array (
      0 => 'Réglages du style &raquo; Global',//'界面设置 &raquo; 全局',  //  'Style settings &raquo; Global',
      1 => 'setting_tips',
      2 => 'Les options marquées par la ligne pointillée rouge sont liés à l\'efficacité du système, la capacité de charge et la consommation des ressources (peut améliorer ou réduire l\'efficacité). Il est recommandé de procéder à des ajustements en fonction de la situation de votre serveur.',//'以红色虚线标示的选项，表示该选项和系统效率、负载能力与资源消耗有关(提高效率、或降低效率)，建议依据自身服务器情况进行调整。',  //   'Options marked by red dotted line are related to the system efficiency, load capacity and resource consumption (can improve or reduce the efficiency). It is recommended to make adjustments according to your server situation.',
      3 => 'setting_styles_global',
      4 => 'Global',//'全局',  //  'Global',
      5 => 'setting_styles_global_styleid',
      6 => 'Style par défaut du site',//'站点默认风格',  // 'Site default style', 
      7 => 'Site par Défaut d\'interface de style, les visiteurs et les membres seront utiliser ce style par défaut.',//'站点默认的界面风格，游客和使用默认风格的会员将以此风格显示',  // 'Default site interface style, visitors and members will use this style as default.', 
      8 => 'setting_styles_global_home_style',
      9 => 'Afficher style espace',//'家园展示风格',  //   'Space show style',
      10 => 'Sélectionnez le style d\'affichage pour les garçons, des albums photo, le partage, le mur, les aliments',//'选择日志、相册、分享、记录、广播展示风格',  //  'Select the display style for bogs, photo albums, sharing, wall, feeds', 
      11 => 'setting_styles_global_homepage_style',
      12 => 'Page d\'accueil personnelle skin par défaut',//'个人主页默认皮肤',  //  'Personal home page default skin',
      13 => 'Sélectionnez le style espace d\'affichage par défaut ',//'选择个人主页默认展示风格',  //  'Select the space default display style',
      14 => 'setting_styles_global_navsubhover',
      15 => 'Afficher navigation secondaire d\'en-tête',//'二级导航显示方式',  //  'Display header secondary navigation', 
      16 => 'Définir pour afficher la page en-tête de la seconde navigation. S\'il est défini à Afficher toute la navigation principale, navigation secondaire, ce sera quand la souris se déplace à l\'autre lorsque afficher le menu principal de leur texte en haut de la navigation secondaire ',//'设置二级导航的显示方式。如果设置为显示所有主导航的二级导航，那么会当鼠标移动到其他主导航文字上方的时候显示它们的二级导航',  //    'Set to display the page header of the second navigation. If set to show all the main navigation, secondary navigation, it will be when the mouse moves to the other when the main navigation display their text at the top of the secondary navigation ',
      17 => 'setting_styles_index_allowwidthauto',
      18 => 'Le style de la largeur du Forum',//'论坛宽窄风格',  //   'Forum width style',
      19 => 'Style d\'affichage Forum peut être réglé sur le style large ou étroite. Chaque forum peut être réglé indépendamment',//'设置论坛的显示风格是宽版还是窄版。每个版块中还可以进行独立的设置',  //   'Forum display style may be set to Wide or Narrow style. Each forum can be set independently',
      20 => 'setting_styles_index_switchwidthauto',
      21 => 'Autoriser les utilisateurs à changer la largeur du  style',//'允许用户自由切换',  //   'Allow users to switch width style',
      22 => 'Choisissez si vous voulez autoriser aux utilisateurs de passer librement la largeur du style',//'设置是否允许用户自由切换宽窄风格',  //  'Set whether to allow users to freely switch width style',
      23 => 'setting_styles_global_allowfloatwin',
      24 => 'Activer la fenêtre flottante',//'启用浮动窗口',  //  'Enable the floating window',
      25 => 'Définir ce contenu est affiché dans une fenêtre flottante',//'设置以浮动方式显示的窗口',  //  'Set whar content be displayed in a floating window',
      26 => 'setting_styles_global_showfjump',
      27 => 'Zone d\'affichage de navigation',  //  '显示版块导航',  //  'Zone d\'affichage de navigation', 
      28 => 'Configure s\'il faut afficher le raccourci de navigation dans cette zone',  //   '设置是否在快捷导航中显示版块导航',    
      29 => 'setting_styles_global_creditnotice',
      30 => 'Points Notification',//'积分变动提示',  //  'Points Notification',
      31 => 'Afficher l\'invite pop-up à l\'utilisateur lorsque le montant de points d\'utilisateur a été modifié par n\'importe quelle opération.',//'当用户在站点的操作产生积分变动时，将弹出提示信息告知用户', //  'Display pop-up prompt to user when the user points amount was changed by any operation.',
      32 => 'setting_styles_global_showusercard',
      33 => 'Afficher la carte de visite de l\'utilisateur',//'显示用户名片',  //  'Show the user business card',
      34 => 'Définir cette option pour afficher la carte de visite de l\'utilisateur lorsque la souris passe sur le nom d\'utilisateur',//'设置当鼠标移动到用户名位置上时是否显示他的名片',    // 'Set this to display the user business card when the mouse moves over the user name',   
      35 => 'setting_styles_global_anonymoustext',
      36 => 'Anonyme surnom de l\'utilisateur',//'匿名用户的昵称',  //  'Anonymous user nickname',
      37 => 'Définissez le nom de pseudo utilisateur anonyme affiché en poste ou commentaire. Tels que: "Utilisateur du site" ou "Anonyme"',//'设置帖子或评论留言中的匿名用户显示的文字。如:本站网友',   //  'Set the anonymous user pseudo name displayed in post or comment. Such as: "Site user" or "Anonymous"',  
    ),
  ),
  248 =>
  array (
    'index' =>
    array (
      'Réglages du style' => 'action=setting&operation=styles',//'界面设置', //   'Style settings'
      'Accueil du Forum' => 'action=setting&operation=styles&anchor=index',//'论坛首页',  //  'Forum Home'
    ),
    'text' =>
    array (
      0 => 'Réglages du style &raquo; Accueil du Forum',//'界面设置 &raquo; 论坛首页',  //   'Style settings &raquo; Forum Home',
      1 => 'setting_styles_index',
      2 => 'Accueil Forum',//'论坛首页',   //  'Forum Home',
      3 => 'setting_styles_index_indexhot_status',
      4 => 'Forum Discussions hot',//'论坛热点',  //  'Forum hot threads',
      5 => 'Choisissez si vous voulez afficher les discussions hot à l\'Accueil du forum',//'设置是否显示全论坛的热点主题',  // 'Set whether to display hot threads at the forum home', 
      6 => 'setting_styles_index_indexhot_limit',
      7 => 'Nombre de Forum hots à afficher',//'论坛热点显示数量',  //  'Number of Forum hots to show',
      8 => 'Définissez le nombre de discussions du forum hot, la valeur par défaut est de 10',//'设置论坛热点条目数，默认值 10 条',  //  'Set the number of forum hot threads, the default value of 10',
      9 => 'setting_styles_index_indexhot_days',
      10 => 'Jours pour Afficher Forum hot',//'论坛热点天数',  //  'Days for Forum hot show',
      11 => 'Définir combien de jours à afficher le fil de discussion Hot, la valeur par défaut est de 7 jours',//'设置显示多少天内的主题，默认值 7 天',  //  'Set how many days to display the hot thread, the default value is 7 days',
      12 => 'setting_styles_index_indexhot_expiration',
      13 => 'Mis à jour du Forum durée hots (secondes)',//'论坛热点更新周期(秒)',  //  'Update forum hots period (seconds)',
      14 => 'Définissez la fréquence de mise à jour de la liste rouge du forum, la valeur par défaut est de 900 s.',//'设置论坛热点在多长时间更新一次，默认值 900',  //  'Set how often to update the forum hot list, the default value is 900 s.',
      15 => 'setting_styles_index_indexhot_messagecut',
      16 => 'Forum longueur du contenu du de la discussion hot',//'论坛热点内容截取文字长度',  //   'Forum hot thread content length',
      17 => 'Réglez la longueur raccourcie du contenu du fil de la discussion Hot, la valeur par défaut est de 200 caractères',//'设置论坛热点内容的文字长度，默认值 200 个字',  //'Set the shortened length of the hot thread content, the default is 200 characters',  
      18 => 'setting_styles_index_subforumsindex',
      19 => 'Afficher les sous-forums à l\'Accueil',//'显示版块的下级子版块',  // 'Show sub-Forums at Home', 
      20 => 'Liste du Forum au bas d\'Accueil affiche le nom du forum et sous-Forum Liens (si présent). Remarque: Cette fonction ne considère pas les droits du forum enfant, mais il sera affiché.',//'设置是否在版块描述下方显示下级子版块名字和链接(如果存在的话)。注意: 本功能不考虑子版块特殊浏览权限的情况，只要存在即会被显示出来',  //   'Forum list at the Home bottom shows the forum name and sub-Forum links (if present). Note: This feature does not consider the child forum rights, however it will be displayed.',
      21 => 'setting_styles_index_forumlinkstatus',
      22 => 'Afficher les liens à l\'Accueil',//'显示友情链接',  //   'Show Links at Home',
      23 => 'S\'il faut afficher les liens au Forum d\'Accueil',//'设置是否显示友情链接',  //  'Whether to display links at Forum Home', 
      24 => 'setting_styles_index_forumallowside',
      25 => 'Afficher la barre latérale',//'显示边栏',  //   'Show Sidebar',
      26 => 'Choisir &quot;Oui&quot; pour afficher les informations de la barre latérale à l\'Accueil du Forum',//'选择“是”论坛首页侧边将显示聚合论坛内容的信息',  //  'Select &quot;Yes&quot; to display the sidebar information at the Forum Home', 
      27 => 'setting_styles_index_whosonline',
      28 => 'Afficher les utilisateurs en ligne',//'显示在线用户',  //  'Show online users',
      29 => 'Afficher utilisateurs en ligne à l\'Accueil du Forum et chaque page du forum',//'设置是否在论坛首页底部和论坛版块边栏显示在线会员列表',  //   'Display online users at Forum Home and each Forum page',
      30 => 'setting_styles_index_whosonline_contract',
      31 => 'Afficher la liste en ligne sous forme de vignettes',//'缩略显示在线列表',  // 'Display online list as thumbnails', 
      32 => 'Si vous ouvrez la liste des options n\'apparaîtra que les utilisateurs en ligne, sans plus de détails. Ensuite, les membres peuvent ouvrir manuellement la liste des utilisateurs en ligne (Si le plus grand en ligne plus de 500, le système affiche automatiquement une liste des abréviations)',//'如果打开该选项，在线列表将只显示在线用户数，不显示详情，此时会员可手动打开在线用户列表(最大在线超过 500 人系统将自动缩略显示在线列表)',  //  'If you open the option list will only appear online users without details. Then members can manually open the online users list (If the largest online more than 500, the system will automatically display a list of abbreviations)',  
      33 => 'setting_styles_index_online_more_members',
      34 => 'Voir &quot;Beaucoup plus En-ligne&quot;',//'最多显示在线人数',  //   'Show &quot;More online&quot;',
      35 => 'Ce réglage n\'est efficace que lorsque l\'affichage internautes activés. Mettre à 0 pour aucune limite.',//'此设置只有在显示在线用户启用时才有效。设置为 0 则为不限制',  //  'This setting is effective only when display online users enabled. Set to 0 for no limits.',
      36 => 'setting_styles_index_hideprivate',
      37 => 'Masquer les utilisateurs qui n\'ont pas accès au Forum',//'隐藏无权访问的版块',  //  'Hide users who not have access to the Forum',
      38 => 'Ne pas inscrire les utilisateurs qui n\'ontont pas accès actuellement au Forum',//'不在列表中显示当前用户无权访问的版块', //   'Do not list users not have access to the Forum currently',
      39 => 'setting_styles_index_showfollowcollection',
      40 => 'Accueil affiche le nombre d\'utilisateurs abonnés à Amoy album', //  '首页显示用户订阅的淘专辑数量',  
      41 => 'Affiché en nombre des utilisateurs inscrits à Amoy album', //  '在首页显示用户订阅的淘专辑数量', 
      42 => 'setting_styles_index_disfixednv',
      43 => 'Fermez la navigation supérieure fixée', //  '关闭顶部导航固定',
      44 => 'Lorsque le défilement vers le haut de l\'écran, apparaît immédiatement dans le menu principal en haut de l\'écran', //  '当向上滚动屏幕时，主导航将即时出现在屏幕的顶部',
    ),
  ),
  249 =>
  array (
    'index' =>
    array (
      'Réglages du style' => 'action=setting&operation=styles',//'界面设置', //  'Style settings'
      'Liste de discussion' => 'action=setting&operation=styles&anchor=forumdisplay',//'主题列表页', //  'Thread List' 
    ),
    'text' =>
    array (
      0 => 'Réglages du style &raquo; Liste de discussion',//'界面设置 &raquo; 主题列表页', //  'Style settings &raquo; Thread List',
      1 => 'setting_tips',
      2 => 'Les options marquées par la ligne pointillée rouge sont liés à l\'efficacité du système, la capacité de charge et la consommation des ressources (peut améliorer ou réduire l\'efficacité). Il est recommandé de procéder à des ajustements en fonction de la situation de votre serveur.',//'以红色虚线标示的选项，表示该选项和系统效率、负载能力与资源消耗有关(提高效率、或降低效率)，建议依据自身服务器情况进行调整。',  //    'Options marked by red dotted line are related to the system efficiency, load capacity and resource consumption (can improve or reduce the efficiency). It is recommended to make adjustments according to your server situation.',
      3 => 'setting_styles_forumdisplay',
      4 => 'Liste de discussion',//'主题列表页', //  'Thread List',
      5 => 'setting_styles_forumdisplay_tpp',
      6 => 'Sujets par page',//'每页显示主题数', //  'Threads per page',
      7 => 'Définissez le nombre de discussions énumérés par chaque page',//'主题列表中每页显示主题数目', // 'Set the number of threads listed per each page', 
      8 => 'setting_styles_forumdisplay_threadmaxpages',
      9 => 'Plus grand nombre de pages dans la liste de discussion',//'主题列表最大页数', //   'Largest number of pages in thread list',
      10 => 'Définissez le nombre maximum de pages dans la liste des sujets que l\'utilisateur peut faire défiler jusqu\'à. Recommandé pour définir la valeur par défaut 1000, et pas plus de 2500. Réglez 0 pour aucune limite.',//'主题列表中用户可以翻阅到的最大页数，建议设置为默认值 1000，或在不超过 2500 范围内取值，0 为不限制',  //   'Set the maximum number of pages in topic list that a user can scroll to. Recommended to set the default value 1000, and not more than 2500. Set 0 for no limit.',
      11 => 'setting_styles_forumdisplay_leftsidewidth',
      12 => 'Forum de côté gauche largeur navigation',//'左侧版块导航宽度', //   'Left side forum navigation width',
      13 => 'Réglez la largeur de la navigation du gauche Forum de. Mettre à 0 pour ne pas afficher le Forum de de navigation dans la barre latérale',//'设置左侧版块导航的宽度，设置为 0 或者版块开启了边栏时不显示此导航',  // Set the width of the left forum navigation. Set to 0 for do not display the forum navigation on the sidebar
      14 => 'setting_styles_forumdisplay_leftsideopen',
      15 => 'Développez le forum barre latérale gauche par défaut',//'左侧版块默认展开', //  'Expand the forum left sidebar by default',
      16 => 'Réglez le Forum de gauche barre latérale de navigation est développée par défaut ou non',//'设置左侧版块导航是否默认全部展开', //   'Set the forum left sidebar navigation is expanded by default or not',
      17 => 'setting_styles_forumdisplay_globalstick',
      18 => 'Activer Scotché global',//'启用全局置顶', //  'Enable Global Stick',
      19 => 'Choisir &quot;Oui&quot;. Remarque de types: Scotché-III - le fil de discussion sera affiché dans tous les forums; Scotché II - Le sujet sera affiché dans un forum en cours. Remarque: Cette fonction va augmenter la charge du serveur.',//'选择“是”，置顶 III 的主题将在全部论坛显示，置顶 II 的主题将在当前分区中显示。注意: 本功能会加重服务器负担',  //    'Select &quot;Yes&quot;. Note of types: Stick-III - the thread will be displayed in all forums; Stick II - Thread will be displayed in a current forum only. Note: This feature will increase the server loading.',
      20 => 'setting_styles_forumdisplay_targetblank',
      21 => '"Nouvelle Fenêtre" est cochée par défaut',//'“新窗”是否默认勾选', //  '"New window" is checked by default',
      22 => 'Choisir "Oui" pour ouvrir les liens dans la page du liste des discussions dans une nouvelle fenêtre. La valeur par défaut est "Non".',//'选择“是”则默认情况下主题列表页主题链接在新窗口打开，默认为“否”',  //  'Select "Yes" for open links at the the thread list page in a new window. The default is "No".',
      23 => 'setting_styles_forumdisplay_stick',
      24 => 'Scotchés les types de discussions',//'置顶主题的标识', //   'Stick threads types',
      25 => 'Entrez types de scotch activé: Activé lettre anglais seulement &quot;I&quot; (max. 3 caractères)! Les types distincts par une virgule, le niveau de l\'ordre de haut en bas.',//'以英文状态下的逗号分割，只有三项有效，级别依次为从高至低',  //  'Enter enabled stick types: Enabled only English letter &quot;I&quot; (max 3 characters)! Separate types by comma, order level from high to low.',
      26 => 'setting_styles_forumdisplay_part',
      27 => 'Discussions Hot Séparez les autres sujets',//'显示主题列表分割带', //  'Separate Hot threads from other threads',
      28 => 'Choisir &quot;Non&quot; pour Discussions hot non séparés des autres liste des discussions',//'选择“否”，在主题列表页将不显示置顶主题与普通主题之间的分割带(版块主题那一空行)',  // 'Select &quot;No&quot; for not separate hot threads from other thread list',
      29 => 'setting_styles_forumdisplay_visitedforums',
      30 => 'Number of recent visited forums to show',//'显示最近访问版块数量', //  'Number of recent visited forums to show',
      31 => 'Définissez la taille de la liste déroulante des derniers forums visités. Recommende valeur de 0 à 30, 0 pour désactiver cette fonction.',//'设置在版块列表和帖子浏览中显示最近访问过的版块数量，建议设置为 10 以内，0 为关闭此功能',  //  'Set the size of of drop-down list of last visited Forums. Recommende value from 0 to 30, 0 to disable this feature.',
      32 => 'setting_styles_forumdisplay_fastpost',
      33 => 'Post rapide',//'快速发帖', //   'Quick post',
      34 => 'Afficher sous forme de petit post en bas',//'底部显示快速发帖表单', //  'Show the bottom quick post form',
      35 => 'setting_styles_forumdisplay_fastsmilies',
      36 => 'Afficher sourires recommandées',//'显示推荐表情', //   'Show recommended smiles',
      37 => 'Afficher sourires recommandées à droite du formulaire de message rapide. Sourires recommandées doivent être mis "Gestion Sourires"',//'在快速发帖右侧显示推荐表情。推荐的表情需要到“表情管理”中设置',  //  'Show recommended smiles on the right of the quick post form. Recommended smiles needs to be set in "Smile management"',
      38 => 'setting_styles_forumdisplay_forumpicstyle_thumbwidth',
      39 => 'Discussion largeur de couverture',//'主题封面宽度', // 'Thread Cover width', 
      40 => 'Lorsqu\'un forum est réglé sur le mode de la liste d\'image, réglez-le sur 0 ou ne pas remplir pour un usage largeur par défaut (214px). Cliquez ici pour reconstruire la largeur et la hauteur recommandée de la couverture du sujet.',//'当版块开启图片列表模式时有效，0或不填 将使用默认值(214)。变更宽高建议重建主题封面，点此重建',  //   'When a forum is set to Image list mode, set this to 0 or do not fill for use default width (214px). Click here to reconstruct the recommended width and height of thread cover.',
      41 => 'setting_styles_forumdisplay_forumpicstyle_thumbheight',
      42 => 'Discussion hauteur de couverture',//'主题封面高度', //   'Thread cover height',
      43 => 'Ceci est efficace lorsque la liste de forum est ouvert en mode image. Mettre à 0 ou vide pour utiliser la valeur par défaut (160)',//'当版块开启图片列表模式时有效，0或不填 将使用默认值(160)',  //   'This is effective when the forum list is open in image mode. Set to 0 or blank to use the default value (160)',
      44 => 'setting_styles_forumdisplay_forumpicstyle_thumbnum',
      45 => 'Afficher le nombre d\'images de couverture',//'封面图显示数量', //  'Show the number of cover images',
      46 => 'Combien d\'images par page à afficher lorsqu\' un Forum est réglé sur le mode de liste d\'images. Mettre à 0 ou ne pas remplir pour utiliser le mode de liste ordinaire',//'当版块开启图片列表模式时有效，0或不填 将和普通列表模式每页显示主题数一致',  //   'How many images per page to show when a Forum is set to image list mode. Set to 0 or do not fill for use the regular list mode',
      47 => 'setting_styles_forumdisplay_newbie',
      48 => 'Icône nouveau post',//'新人帖图标',  //   'New post icon',
      49 => 'Cette icône s\'affiche lorsque l\'utilisateur a émis le premier fil de discussion, laisser vide pour ne pas afficher l\'icône',//'用户发第一篇主题时显示的图标，为空则不打图标',  //  'This icon is displayed when the user issued the first thread, leave empty for not show the icon',
      50 => 'setting_styles_forumdisplay_disfixednv_forumdisplay',
      51 => 'Fermez la navigation supérieure fixée', //  '关闭顶部导航固定',
      52 => 'Lorsque le défilement vers le haut de l\'écran, apparaît immédiatement dans le menu principal en haut de l\'écran', //  '当向上滚动屏幕时，主导航将即时出现在屏幕的顶部',    
      53 => 'setting_styles_forumdisplay_threadpreview',
      54 => 'Fermer Extrait thématique', //  '关闭主题预览',
      55 => 'Fermer ne sera pas dans la liste des rubriques page de contenu Posts Extrait', //  '关闭后将不可在主题列表页预览帖子内容',
    ),
  ),
  250 =>
  array (
    'index' =>
    array (
      'Réglages de style' => 'action=setting&operation=styles',//'界面设置',  //'Style settings'   
      'Afficher les paramètres de la discussion' => 'action=setting&operation=styles&anchor=viewthread',//'帖子内容页',  //  'View thread settings'
    ),
    'text' =>
    array (
      0 => 'Réglages de style &raquo; Afficher les paramètres de la discussion',//'界面设置 &raquo; 帖子内容页',  //  'Style settings &raquo; View thread settings',
      1 => 'nav_setting_viewthread',
      2 => 'Afficher les paramètres de la discussion',//'帖子内容页',  //   'View thread settings',
      3 => 'setting_styles_viewthread_ppp',
      4 => 'Posts par page',//'每页显示帖数',  //  'Posts per page',
      5 => 'Définissez le nombre de messages affichés par page',//'帖子列表中每页显示帖子数目',  //   'Set the number of posts displayed per page',
      6 => 'setting_styles_viewthread_starthreshold',
      7 => 'Mise à jour limitations étoiles ',//'星星升级阈值', //  'Upgrade stars threshold',
      8 => 'Quand étoiles atteint ce nombre seuil de fois (mis à N), N étoiles apparaissent comme une lune, N lunes apparaît comme un soleil. La valeur par défaut est 2. Mettre à 0 pour disabe cette fonction et afficher les étoiles toujours.',//'星星数在达到此阈值(设为 N)时，N 个星星显示为 1 个月亮、N 个月亮显示为 1 个太阳。默认值为 2，如设为 0 则取消此项功能，始终以星星显示',  //   'When stars reaches this threshold number of times (set to N), N stars appear as a moon, N moons appears as a sun. The default value is 2. Set to 0 for disabe this function, and display the stars always.',
      9 => 'setting_styles_viewthread_maxsigrows',
      10 => 'Signature max height (px)',//'签名最大高度(px)',  //  'Signature max height (px)',
      11 => 'Réglez la hauteur maximale autorisée d\'une signature affichée dans un post.',//'设置帖子中允许显示签名的最大高度',  //  'Set the maximum allowed height of aSignature displayed in a post.',
      12 => 'setting_styles_viewthread_sigviewcond',
      13 => 'Afficher conditions de Signature',//'签名显示条件',  //  'Signature show condition',
      14 => 'Afficher une signature uniquement si un message est plus grand que le nombre de caractères spécifié.',//'只有帖子字数大于指定数值后才显示签名',  //  'Show a signature only if message is larger than the specified number of characters.', 
      15 => 'setting_styles_viewthread_rate_on',
      16 => 'Afficher raison de score sur la page',//'是否在页面上显示评分理由', //   'Display score reason on the page',
      17 => 'setting_styles_viewthread_rate_number',
      18 => 'Nombre d\'entrées du score',//'评分条目数', //  'Number of Score entries',
      19 => 'Afficher le nombre d\'entrées de pointage récentes dans un post, définir à 0 pour ne pas Afficher.',//'在帖子中显示最近评分结果的条目数，0 为不显示', // 'Show the number of recent score entries in a post, Set 0 for not show.', 
      20 => 'setting_styles_viewthread_collection_number',
      21 => 'Nombre de collections connexes',//'相关淘专辑条目数', // 'Number of related collections', 
      22 => 'Afficher le numéro de collection d\'entrées dans le postdu sujet, 0 pour ne pas Afficher',//'在主题帖中显示相关淘专辑的条目数，0 为不显示', //  'Display the collection number of entries in the thread post, 0 for not show',
      23 => 'setting_styles_viewthread_relate_number',
      24 => 'Nombre d\'entrées liées au post',//'相关帖子条目数', //  'Number of related to post entries',
      25 => 'Définir le nombre d\'entrées liées à montrer la valeur 0 pour ne pas Afficher',//'在主题帖中显示相关帖子的条目数，0 为不显示', // 'Set how many related entries to show, set to 0 for not show', 
      26 => 'setting_styles_viewthread_relate_time',
      27 => 'Délai Cache Posts Connexes',//'相关帖子缓存时间', //'Related posts cache time',  
      28 => 'Unité: minutes, le temps de mise en cache pour afficher les messages liés au poste de sujet (0 pour pas de cache, pas recommandé!)',//'单位：分钟，在主题帖中显示相关帖子的缓存时间（0 为不缓存，不推荐）',  //   'Unit: minutes, Cache time to display related posts in the topic post (0 for no cache, not recommended!)',
      29 => 'setting_styles_viewthread_show_signature',
      30 => 'Afficher une signature',//'是否显示作者签名', //  'Show a signature',
      31 => '',
      32 => 'setting_styles_viewthread_show_face',
      33 => 'Afficher avatar de l\'utilisateur',//'是否显示作者头像',  //  'Show user avatar',
      34 => '',
      35 => 'setting_styles_viewthread_show_images',
      36 => 'Afficher des images dans un post',//'是否显示帖内图片',  //  'Show images in a post',
      37 => '',
      38 => 'setting_styles_viewthread_imagemaxwidth',
      39 => 'Largeur maximale de l\'image dans un post',//'帖内图片最大宽度',  // 'Maximum image width in a post', 
      40 => 'Si la largeur de l\'image est supérieure à cette valeur, l\'image sera automatiquement redimensionnée. La valeur doit être de type integer (nombre de pixels) et ne doit pas dépasser 1920.',//'当帖内图片宽度大于设置的值时图片宽度会被自动调节。请填写整数值，不支持百分比数值',  //  'If an image width is greater than this value, the image will be automatically resized. The value must be integer type (number of pixels) and must not exceed 1920.',
      41 => 'setting_styles_viewthread_imagelistthumb',
      42 => 'Nombre de post d\'images affichés horizontalement',//'帖内图片列表中图片横排显示条件',  //   'Number of post images displayed horizontally',
      43 => 'Définir le nombre d\'images à afficher horizontalement dans la liste d\'image du post',//'设置当帖内图片列表中图片数量满足多少张以后以横排方式显示，0 或留空为关闭横排显示',  //   'Set how many Images to be displayed horizontally within the post image list',
      44 => 'setting_styles_viewthread_zoomstatus',
      45 => 'Utilisation effet du zoom image',//'是否使用图片动态放大效果',  //  'Use image zoom effect',
      46 => 'Lorsque la largeur de l\'image est plus grande que lorsque la "largeur d\'image maximale dans un poste", vous pouvez ajouter un effet de zoom dynamique par un clic de souris. Sélectionnez "Oui" pour utiliser un effet de zoom lorsque vous cliquez sur l\'image ou sélectionnez "Non" pour seulement ouvrir l\'image dans une nouvelle fenêtre.',//'当帖内图片宽度大于“帖内图片最大宽度”时是否附加鼠标点击时的动态放大效果。选择“是”单击图片会有动态放大效果，选择“否”将会新窗口打开图片', //   'When the image width is greater than when the "Maximum image width in a post", you can add a dynamic zoom effect by mouse click. Select "Yes" to use a Zoom effect when the image clicked, or select "No" for just open the image in a new window.',
      47 => 'setting_styles_viewthread_showexif',
      48 => 'Afficher les informations EXIF des images jointes',//'图片附件显示 EXIF 信息',  //   'Display EXIF information for attached images',
      49 => 'Choisissez si vous voulez afficher les informations EXIF pour les images jointes à l\'effet de zoom dynamique',//'设置图片附件在动态放大效果中是否显示 EXIF 信息',  //   'Set whether to display the EXIF information for attached images in the dynamic zoom effect',
      50 => 'setting_styles_viewthread_vtonlinestatus',
      51 => 'Afficher l\'état en ligne',//'显示作者在线状态',  //   'Display online status',
      52 => 'Que ce soit pour montrer le statut en ligne de l\'utilisateur. Attention: cette fonctionnalité sera légèrement augmenter la charge du serveur.',//'是否显示帖子作者的在线状态，注意：“精确显示”会轻微加重服务器负担',  //   'Whether to show the poster online status. Attention: this feature will slightly increase the server loading.',
      53 => 'setting_styles_viewthread_userstatusby',
      54 => 'Afficher le titre du groupe',//'显示作者的组头衔',  //   'Show group title',
      55 => 'Choisir d\'afficher un titre de groupe dans un post.',//'设置在帖子中是否显示作者的用户组头衔',//  'Set whether to display a group title in a post.',
      56 => 'setting_styles_viewthread_postno',
      57 => 'Voir Post No.',//'帖子顺序单位',  //   'Show Post No.',/
      58 => 'Afficher le numéro ordonnée de post, comme "#1", "#2" etc.',//'设置帖子显示顺序编号的单位，如“#”将显示为 1 #',  //  'Show the post ordered number, such as "#1", "#2" etc.',
      59 => 'setting_styles_viewthread_postnocustom',
      60 => 'Post personnalisé de numérotation',//'帖子顺序名称',//  'Custom Post Numbering',
      61 => 'Régler le poste de douane de commande dans un sujet. La première ligne représente le premier poste de thread, deuxième ligne représente la première réponse, et ainsi de suite.',//'设置每篇主题所有帖子的顺序名称，每行填写一个名称，第一行代表主题帖，第二行代表主题的第一个回复，以此类推',  //  'Set the custom post ordering in a thread. First line represents the first thread post, second line represents the first reply, and so on.',
      62 => 'setting_styles_viewthread_maxsmilies',
      63 => 'Le nombre maximum de sourires par post',//'最大单一表情解析次数', //   'Maximum number of smiles per post',
      64 => 'Définir la limite de sourires peut être utilisé dans un seul poste. Plus sourires ne seront pas rendus à smilr images. Cette fonction est utile pour éviter les messages malveillants avec beaucoup de sourires. Mettre à 0 pour aucune limite.',//'在一篇帖子中解析的单一表情符号的最大次数，超过此次数限制的表情将不被解析而保留原样，以避免用户在一篇帖子中恶意发表大量表情影响正常阅读，0 为不限制', //  'Set the limit of smiles can be used in a single post. More smiles will not be rendered to smilr images. This feature is usefull for prevent malicious posts with a lot of smiles. Set to 0 for no limits.',
      65 => 'setting_styles_viewthread_author_onleft',
      66 => 'Afficher la position du nom d\'utilisateur',//'作者用户名显示位置', //   'Display user name position',
      67 => 'Position Nom d\'utilisateur n\'affecte pas les performances du forum et dépend de votre préférence seulement.',//'显示位置并不会影响论坛的性能和美观，请根据你会员的浏览习惯进行选择', // 'User name position does not affect the forum performance and depends of your preference only.', 
      68 => 'setting_styles_viewthread_customauthorinfo',
      69 => 'Réglez la position de l\'info utilisateur pour afficher',//'设置用户信息显示的位置', //  'Set a position of user info to display',
      70 => 'Lorsque vous faites défiler l\'écran, la zone d\'avatar gauche suivra le montant droit du contenu flottant, pourvoir les postes zone de très forte teneur en tête laissée en blanc', //  '当滚动屏幕时，左侧头像区域将跟随右侧帖子内容浮动，填充帖子内容区域很高时导致的左侧空白', //  
      71 => 'setting_styles_forumdisplay_disfixednv_viewthread',
      72 => 'Fermez la navigation supérieure fixée', //   '关闭顶部导航固定', 
      73 => 'Lorsque le défilement vers le haut de l\'écran, apparaît immédiatement dans le menu principal en haut de l\'écran', //   '当向上滚动屏幕时，主导航将即时出现在屏幕的顶部', //  
      74 => 'setting_styles_forumdisplay_threadguestlite',
      75 => 'Lire les modalités simplifiées visiteurs', //  '游客阅读简化模式',
      76 => 'Lorsqu\'elle est activée, les visiteurs lisent le sujet, il ne sera pas afficher les données et informations spécifiques de l\'utilisateur.', //  '启用后，游客阅读主题时，将不显示用户的具体资料信息。', //  
      77 => 'setting_styles_viewthread_close_leftinfo',
      78 => 'Fermez la barre d\'informations à gauche', //  '关闭左侧信息栏',
      79 => 'Posts champ d\'information de l\'utilisateur ne reste déconnecté par défaut', //  '帖子左侧用户信息栏是否默认关闭', //  
      80 => 'setting_styles_viewthread_close_leftinfo_userctrl',
      81 => 'À gauche barre d\'information permet à l\'utilisateur de faire le contrôle', //  '左侧信息栏允许用户控制',
      82 => 'Parcourir les sujets, si l\'utilisateur peut contrôler l\'affichage de la barre d\'informationinformations d\'état sur la gauche', //   '浏览主题时，用户是否可以控制左侧信息栏的显示状态', //  
      83 => 'setting_styles_viewthread_guestviewthumb',
      84 => 'Visiteurs de voir un petit personnage', //  '游客看小图', 
      85 => 'Visiteurs Explorer les thèmes, les images de poste au sein de la taille spécifiée sera plus petite montre, peut augmenter le nombre de membres inscrits et le volume actif', //  '游客浏览主题时，帖内的图片将以指定大小的小图显示，可提高会员注册数和活跃量', //  
      86 => 'setting_styles_viewthread_guestviewthumb_width',
      87 => 'Petite largeur de la carte', //  '小图宽度',  //  
      88 => 'Indique la largeur d\'une petite carte, la valeur par défaut est de 100', //  '指定小图的宽度,默认值为100', //  
      89 => 'setting_styles_viewthread_guestviewthumb_height',
      90 => 'Petite hauteur de l\'image', //   '小图高度', //  
      91 => 'Petite activité graphique mesure spécifié, la valeur par défaut est 100', //   '指定小图的商度，默认值为100', //  
      92 => 'setting_styles_viewthread_guesttipsinthread',
      93 => 'Posts au sein aux visiteurs invite de connexion s\'affiche', //  '帖内游客登录提示', //  
      94 => 'Visiteurs parcourant la thématique, le thème du texte de l\'invite affichée en haut', //  '游客在浏览主题时，在主题的顶部显示提示文字', //  
      95 => 'setting_styles_viewthread_guesttipsinthread_text',
      96 => 'Connexion texte de l\'invite invite', //  '登录提示提示文字', //  
      97 => 'Affichage texte invite personnalisée, la valeur par défaut est: Inscrivez-vous maintenant et faire plus d\'amis, profiter de plus de fonctionnalités qui vous permettent de facilement Communauté Fun.', //   '自定义显示提示文字,默认为：马上注册，结交更多好友，享用更多功能，让你轻松玩转社区。', //  
      98 => 'setting_styles_viewthread_imgcontent',
      99 => 'Le contenu thématique généré largeur de l\'image', //  '主题内容生成图片宽度', //  
      100 => 'Cette fonction nécessite le support de PHP GD bibliothèque, du contenu contenu doit être chinois dans un fichier de police TTF chinois dans le static/image/seccode/font/ch/Répertoire. Par défaut 100 caractères, polices de différentes largeurs légèrement différents, s\'il vous plaît contacter en fonction du réglage de l\'effet réel.', //  '本功能需要PHP支持GD库，内容里有中文需要将中文 TTF 字体文件放到 static/image/seccode/font/ch/ 目录下。默认100个汉字，字体不同宽度略有差别，请跟据实际效果调整。', //  
      101 => 'setting_styles_viewthread_fast_reply',
      102 => 'Ouvrir Poster une réponse rapide', //  '开启帖子快速回复', //  
      103 => 'Ouvrir les posts de thème apparaîtra la boîte de réponse rapide ci-dessous', //  '开启后将在主题帖下方展示快速回复框', //  
      104 => 'setting_styles_viewthread_allow_replybg',
      105 => 'Ouvrir Poster une réponse rapide boîte de fond', //  '开启帖子快速回复框背景', //  
      106 => 'Tourné vers les posts pour permettre à la boîte de réponse rapide pour ajouter l\'image de fond', //   '开启后允许给帖子快速回复框添加背景图片', //  
      107 => 'setting_styles_viewthread_global_reply_background',
      108 => 'Global Post Réponse rapide boîte de fond', //  '全局帖子快速回复框背景', //  
      109 => 'Définir l\'image de fond global Posts rapide de réponse de la boîte, réglez le forum prioritaire', //  '设置全局帖子快速回复框背景图片，版块设置优先',  //  
    ),
  ),
  251 =>
  array (
    'index' =>
    array (
      'Les paramètres Interface' => 'action=setting&operation=styles',  //  '界面设置' //  
      'Postes au sein de l\'Information de l\'Utilisateur' => 'action=setting&operation=styles&anchor=threadprofile',  //'帖内用户信息' //  
    ),
    'text' =>
    array (
      0 => 'Les paramètres Interface &raquo; ostes au sein de l\'Information de l\'Utilisateur',  //  '界面设置 &raquo; 帖内用户信息', //  '界面设置 &raquo; 帖内用户信息', //  
      1 => 'setting_styles_threadprofile_group',
      2 => 'Les paramètres du programme au groupe utilisateur',  //  '用户组方案设置', // 
      3 => 'setting_styles_threadprofile_project',
      4 => 'Présentation de la liste des programmes',  // '布局方案列表', //  
    ),
  ),
  252 =>
  array (
    'index' =>
    array (
      'Réglages du style' => 'action=setting&operation=styles',//'界面设置', //  'Style settings'
      'Invites' => 'action=setting&operation=styles&anchor=refresh',//'提示信息', //  'Prompts'
    ),
    'text' =>
    array (
      0 => 'Réglages du style &raquo; Invites',//'界面设置 &raquo; 提示信息', //   'Style settings &raquo; Prompts',
      1 => 'setting_styles_refresh',
      2 => 'Invites',//'提示信息', //   'Prompts',
      3 => 'setting_styles_refresh_refreshtime',
      4 => 'Invites afficher le temps (en secondes)',//'提示信息停留时间(秒)', // 'Prompt display time (seconds)',  
      5 => 'Une page affiche diverses invites dans un court laps de temps, généralement de 1 à 3 secondes. Conseils: un temps plus court est plus utile pour les utilisateurs expérimentés, mais pour les utilisateurs débutants peuvent être nécessaires plus longtemps pour voir les informations.',//'各种提示信息页面显示时间的长短，一般设置为 1-3。较短的提示时间让用户拥有快速的体验，但是也可能让用户无法看清信息',  //  'A page displays various prompts in a short time, usually from 1 to 3 seconds. Tips: a shorter time is more usefull for experienced users, but for newbie users may be required more long time to see the information.',
      6 => 'setting_styles_refresh_quick',
      7 => 'Voir lien de saut rapide',//'开启直接/快速跳转', //  'Show fast jump link',
      8 => 'Réglez ce pour afficher un lien direct à sauter après complété avec succès une opération. Ce lien permet d\'éliminer toute attente de temporisation automatique redirection.',//'对于站点中的某些成功的操作不显示提示信息，直接跳转到下一个页面，例如发帖，回复等等，可以节省用户等待跳转的时间',  //  'Set this for display a direct link to jump after successfully completed any operation. Such link help to eliminate any waiting of auto redirecting timeout.',
      9 => 'setting_styles_refresh_messages',
      10 => 'Afficher les informations de saut rapide',//'直接/快速跳转的信息', //   'Show fast jump info',
      11 => 'Lorsque vous mettez un lien de saut rapide plus tard, l\'information suivante sera affiché.<br>Remplissez un seul message clé pour chaque ligne.',//'当开启直接/快速跳转以后，以下信息将会直接跳转。每行填写一个信息的关键字',  //  'When you turn on a fast jump link later, the following information will be shown.<br>Fill in one message keyword per each line.',
    ),
  ),
  253 =>
  array (
    'index' =>
    array (
      'Réglages du style' => 'action=setting&operation=styles',//'界面设置', //   'Style settings'
      'Pop-up messages' => 'action=setting&operation=styles&anchor=sitemessage',//'弹出信息', //   'Pop-up messages'
    ),
    'text' =>
    array (
      0 => 'Réglages du style &raquo; Pop-up messages',//'界面设置 &raquo; 弹出信息', //   'Style settings &raquo; Pop-up messages',
      1 => 'setting_styles_sitemessage',
      2 => 'Pop-up messages',//'弹出信息', //  'Pop-up messages',
      3 => 'setting_styles_sitemessage_time',
      4 => 'Pop-up Afficher temps (en secondes)',//'弹出信息停留时间(秒)', //  'Pop-up show time (seconds)',
      5 => 'Combien de temps un message pop-up s\'affichera, habituellement 3-10 secondes. Mettre à 0 pendant près par un clic de souris.',//'各种弹出信息显示时间的长短，一般设置为 3-10，0 为鼠标点击后消失',  //  'How long time a pop-up message will be shown, usually 3-10 seconds. Set to 0 for close by mouse click.', 
      6 => 'setting_styles_sitemessage_register',
      7 => 'Page d\'inscription',//'注册页面', //  'Registration page',
      8 => 'Définissez les informations sur la pop-up de la page d\'inscription. Remplissez un message par chaque ligne. Cette information sera affichée aléatoirement à un certain nombre.',//'注册页面的弹出信息，每行填写一条信息，多条信息将随机显示',  //  'Set the information on the pop-up of registration page. Fill one message per each line. This information will randomly displayed to a number.',
      9 => 'setting_styles_sitemessage_login',
      10 => 'La page de connexion',//'登录页面', //   'Login page',
      11 => 'Définissez les informations sur la pop-up de la page de connexion. Remplissez un message par chaque ligne. Cette information sera affichée aléatoirement à un certain nombre.',//'登录页面的弹出信息，每行填写一条信息，多条信息将随机显示',  //   'Set the information on the pop-up of login page. Fill one message per each line. This information will randomly displayed to a number.',
      12 => 'setting_styles_sitemessage_newthread',
      13 => 'Nouvelle page du sujet',//'发帖页面', //  'New thread page',
      14 => 'Définissez les informations sur la pop-up de la nouvelle page de fil. Remplissez un message par chaque ligne. Cette information sera affichée aléatoirement à un certain nombre.',//'发帖页面的弹出信息，每行填写一条信息，多条信息将随机显示',  //  'Set the information on the pop-up of new thread page. Fill one message per each line. This information will randomly displayed to a number.',
      15 => 'setting_styles_sitemessage_reply',
      16 => 'Page Réponse',//'回复页面',  //   'Reply page',
      17 => 'Définissez les informations sur la pop-up de la page de réponse. Remplissez un message par chaque ligne. Cette information sera affichée aléatoirement à un certain nombre.',//'回复页面的弹出信息，每行填写一条信息，多条信息将随机显示',  //   'Set the information on the pop-up of reply page. Fill one message per each line. This information will randomly displayed to a number.',
    ),
  ),
  254 =>
  array (
    'index' =>
    array (
      'Paramètres Interface' => 'action=setting&operation=threadprofile&do=add',  //  '界面设置'   //   '界面设置'
    ),
    'text' =>
    array (
      0 => 'Paramètres Interface',  // '界面设置',
      1 => 'setting_threadprofile_tpl_tpls',
      2 => '{Marquez} Aucun paramètres dans la barre d\'information de l\'utilisateur apposées à appeler.{Marquez = paramètre} Dans les paramètres de la Marquez contenant d\'information de l\'utilisateur de coller des appels avec plusieurs paramètres","Division {Marquez}HTML{*}HTML{/Marquez} Quand un appel est affiché lorsque le contenu a une valeur {*} avant et après HTML contenu3plugin:Logo plug "Format marqué comme plug-in ajoute les informations de l\'utilisateur dans une étiquette apposée à appeler. Développeurs de plug-in dans la conception de l\'information de l\'utilisateur au sein de la nouvelle marque apposée A lire avant" Discuz! Archives Technique "dans le contenu.',   //   '{标记} 不含参数的贴内用户信息标记调用。{标记=参数} 含参数的贴内用户信息标记调用，多个参数用","分割{标记}HTML{*}HTML{/标记} 当有调用内容有值时显示 {*} 前后的 HTML 内容“plugin:插件标识”格式的标记为插件增加的贴内用户信息标记调用。插件开发人员在设计新的贴内用户信息标记前请务必仔细阅读《Discuz! 技术文库》中的内容。',
      3 => 'setting_styles_threadprofile_name',
      4 => 'Nom',  //  '名称',
    ),
  ),
  255 =>
  array (
    'index' =>
    array (
      'Optimisation des performances' => 'action=setting&operation=seo',//'性能优化'  // 'Performance Optimization'  
      'SEO' => 'action=setting&operation=seo',//'搜索引擎优化',  //  'SEO' 
    ),
    'text' =>
    array (
      0 => 'Optimisation des performances &raquo; SEO',//'性能优化 &raquo; 搜索引擎优化',  //  'Performance Optimization &raquo; SEO',
      1 => 'setting_tips',
      2 => 'Les options marquées par la ligne pointillée rouge sont liés à l\'efficacité du système, la capacité de charge et la consommation des ressources (peut améliorer ou réduire l\'efficacité). Il est recommandé de procéder à des ajustements en fonction de la situation de votre serveur.',//'以红色虚线标示的选项，表示该选项和系统效率、负载能力与资源消耗有关(提高效率、或降低效率)，建议依据自身服务器情况进行调整。',    //   'Options marked by red dotted line are related to the system efficiency, load capacity and resource consumption (can improve or reduce the efficiency). It is recommended to make adjustments according to your server situation.',
      3 => 'setting_seo_rewritecompatible',
      4 => 'Réécrire compatibilité',//'Rewrite 兼容性', //  'Rewrite compatibility',
      5 => 'Si votre serveur ne supporte pas les caractères chinois dans les règles de réécriture, s\'il vous plaît choisir "Oui." S\'il n\'y a aucun problème avec le serveur, vous pouvez choisir "Non"',//'如果你的服务器不支持 Rewrite 规则中的中文字符，请选择“是”。对于没有此问题的服务器，可以选择“否”',    //  'If your server does not support Chinese characters in Rewrite Rules, please select "Yes." If there is no problem with the server, you can choose "No"',
      6 => 'setting_seo_rewriteguest',
      7 => 'Valable uniquement pour les touristes', //  '仅对游客有效', //  
      8 => 'Sur ce point, la fonctionnalité de réécriture n\'est efficace que pour les visiteurs et les moteurs de recherche, de réduire la charge sur le serveur', //  '开启此项，则 Rewrite功能只对游客和搜索引擎有效，可减轻服务器负担',
      9 => 'setting_seo_seohead',
      10 => 'Autres informations d\'en-tête',//'其他头部信息', //   'Other header information',
      11 => 'Utilisez ce réglage pour placer du code HTML supplémentaire entre le &lt;head&gt; &lt;/head&gt; tags. Laissez ce champ vide si ce n\'est pas nécessaire.',//'如需在 &lt;head&gt;&lt;/head&gt; 中添加其他的 HTML 代码，可以使用本设置，否则请留空',    
    ),
  ),
  256 =>
  array (
    'index' =>
    array (
      'Optimisation des performances' => 'action=setting&operation=seo',//'性能优化',  //   'Performance Optimization'
      'Paramètres du cache du forum' => 'action=setting&operation=cachethread',//'论坛页面缓存设置', //  'Forum cache settings'  
    ),
    'text' =>
    array (
      0 => 'Optimisation des performances &raquo; Paramètres du cache du forum',//'性能优化 &raquo; 论坛页面缓存设置', //   'Performance Optimization &raquo; Forum cache settings',
      1 => 'setting_cachethread',
      2 => 'Paramètres du cache du forum',//'论坛页面缓存设置', //  'Forum cache settings',
      3 => 'S\'il est activé, le contenu de la page est mise en cache de fichiers statiques, qui permettra d\'améliorer grandement la vitesse du serveur. Il est recommandé d\'utiliser un serveur dédié. Pour le serveur virtuel s\'il vous plaît ajuster le cache en fonction de la situation réelle.',//'开启后，缓存页面内容到静态文件，游客访问时速度将大大提升，建议独立服务器使用，虚拟空间请根据实际情况调节缓存的有效期',  //  'If enabled, the page content will cached to static files, that will greatly enhance the server speed. It is recommended to use an dedicated server. For virtual server please adjust the cache according to the actual situation.',
      4 => 'setting_cachethread_indexlife',
      5 => 'Forum Home Cache valid time',//'缓存论坛首页有效期', // 'Forum Home Cache valid time', 
      6 => 'Réglez le Forum Accueil heure de mise à jour du cache en secondes. Mettre à 0 pour désactiver la mise en cache (Si après un arrêt du cache ne sera plus en oeuvre). La valeur recommandée est de 900. Cette fonction sert uniquement pour les invités.',//'设置论坛首页缓存更新的时间，单位为秒，0 为关闭(此处关闭以后，缓存系数将不再起作用)，建议设置为 900。此功能只针对游客', //  'Set the Forum Home cache update time in seconds. Set to 0 to turn off caching (If after a shutdown the  cache will no longer work). Recommended value is 900. This feature used only for guests.',
      7 => 'setting_cachethread_life',
      8 => 'Cache discussion période de validité',//'缓存帖子有效期', //  'Thread Cache valid period',
      9 => 'Définir la page après le temps de mise à jour du cache, en secondes. Réglez la valeur en fonction de la situation réelle, la valeur recommandée est de 900. Aussi, indiquez le poste en éditant la page forum facteur de cache.',//'设置帖子页面缓存更新的时间，单位为秒，0 为关闭。请根据实际情况进行调整，建议设置为 900。另外，请通过编辑论坛来指定帖子页面缓存系数', //   'Set the post page cache update time, in seconds. Adjust the value according to the actual situation, recommended value is 900. Also, specify the post by editing the forum page cache factor.',
      10 => 'setting_cachethread_dir',
      11 => 'Répertoire du cache',//'缓存目录', //  'Cache directory',
      12 => 'Le répertoire par défaut est data / threadcache, si vous avez besoin de spécifier un autre répertoire, assurez-vous que les permissions d\'écriture sont fixées',//'默认为 data/threadcache 目录，如果你需要将其指定为其他目录，请确定你指定的目录有可写权限',  //   'The default directory is data/threadcache, if you need to specify other directory, make sure the write permissions are set',
      13 => 'setting_cachethread_coefficient_set',
      14 => 'Définir coefficient en volume du cache',//'缓存系数批量设置', //'Set the cache volume coefficient',  
      15 => 'setting_cachethread_coefficient',
      16 => 'Mémoire cache Coefficient',//'缓存系数', //  'Cache coefficient',
      17 => 'Mise en cache les informations des membres peut précieuse diminuer la pression sur un serveur à grande échelle utilisation éventail de seuil Mémoire cache est de 0 -.. 100 Valeur recommandée est de 40, 0 pour désactiver l\'effet de Mémoire cache Si vous avez suffisamment d\'espace disque, vous pouvez essayer d\'augmenter la mémoire cache. le volume, et donc augmenter l\'effet du cache.',//'页面缓存功能可以将会员经常访问的主题临时缓存起来，缓解大型论坛服务器压力。缓存阀值范围 0 - 100，建议设置为 40 ，0 为关闭。在磁盘空间允许的情况下，适当调高缓存系数，可以提高缓存效果',     //  'Caching the members info may valuable decrease a pressure to a large-scale server. Use cache threshold range is 0 - 100. Recommended value is 40, 0 will disable cache effect. If you have enough disk space, you can try to increase the cache volume, and so increase the cache effect.',
      18 => 'setting_cachethread_coefficient_forum',
      19 => 'S\'il vous plaît choisir un forum pour mettre en place',//'请选择要设置的论坛', //   'Please select a forum to set up',
      20 => 'Cela écrasera détermine le coefficient de cache précédent. Vous pouvez maintenir enfoncée la touche CTRL pour choix multiples.',//'将会覆盖以前的缓存系数值，可以按住 CTRL 多选', //  
    ),
  ),
  257 =>
  array (
    'index' =>
    array (
      'Optimisation des performances' => 'action=setting&operation=seo',//'性能优化', //  'Performance Optimization'
      'Optimisation du serveur' => 'action=setting&operation=serveropti',//'服务器优化', //  'Server optimization' 
    ),
    'text' =>
    array (
      0 => 'Optimisation des performances &raquo; Optimisation du serveur',//'性能优化 &raquo; 服务器优化', //   'Performance Optimization &raquo; Server optimization',
      1 => 'setting_tips',
      2 => 'Les options marquées par la ligne pointillée rouge sont liés à l\'efficacité du système, la capacité de charge et la consommation des ressources (peut améliorer ou réduire l\'efficacité). Il est recommandé de procéder à des ajustements en fonction de la situation de votre serveur.',//'以红色虚线标示的选项，表示该选项和系统效率、负载能力与资源消耗有关(提高效率、或降低效率)，建议依据自身服务器情况进行调整。',  //   'Options marked by red dotted line are related to the system efficiency, load capacity and resource consumption (can improve or reduce the efficiency). It is recommended to make adjustments according to your server situation.',
      3 => 'setting_serveropti',
      4 => 'Optimisation du serveur',//'服务器优化', //   'Server optimization',
      5 => 'setting_serveropti_optimize_thread_view',
      6 => 'Que ce soit pour optimiser la mise à jour des vues de fil de discussion',//'是否优化更新主题浏览量', //  'Whether to optimize the thread Views update',
      7 => 'setting_serveropti_preventrefresh',
      8 => 'Empêcher anti-rafraîchissement pour le nombre de vues',//'查看数开启防刷新', //  'Prevent anti-refresh for views number',
      9 => 'Si vous sélectionnez "Non", chaque fil de discussion, un espace ou un blog visite va augmenter le nombre de vues du compteur et cela va augmenter la charge du serveur. Il est recommandé de choisir "Oui"',//'选择“否”，主题、空间、日志每访问一次，浏览量增1，会增大服务器压力，建议选择“是”',   // 'If select "No", every thread, space or blog visit will increase the number of views counter, and this will increase the server loading. It is recommended to select "yes"', 
      10 => 'setting_serveropti_delayviewcount',
      11 => 'Mise à jour de visualisations au compteur temporisé',//'点击数延迟更新', //   'Update views counter delay',
      12 => 'Mise à jour du sujet ou des pièces jointes de visualisations retardement peut réduire considérablement la charge du serveur. Est recommandé d\'utiliser des serveurs de la collectivité à grande échelle.',//'延迟更新主题或者附件的浏览量，可明显降低访问量很大的站点的服务器负担，建议开启本功能',  //   'Update the topic or attachments views delaying can significantly reduce the server loading. Recommended to use for large scale community servers.',
      13 => 'setting_serveropti_nocacheheaders',
      14 => 'Désactiver le cache du navigateur',//'禁止浏览器缓冲', //  'Disable browser cache',
      15 => 'Forcer un navigateur à ne pas utiliser la mémoire tampon interne, mais actualiser les pages du site mettent énormément de temps. Remarque: Cette fonction va augmenter la charge du serveur.',//'禁止浏览器对站点页面进行缓冲，可用于解决极个别浏览器内容刷新不正常的问题。注意: 本功能会加重服务器负担',  //   'Force a browser to not use internal buffer but refresh the site pages very time. Note: This feature will increase the server loading.',
      16 => 'setting_serveropti_maxonlines',
      17 => 'Maximum En-ligne',//'最大在线人数', //   'Maximum online',
      18 => 'S\'il vous plaît définir une valeur raisonnable dans la gamme 10 - 65535. Valeur recommandée = 10 * membres connectés moyenne.',//'请设置合理的数值，范围 10～65535，建议设置为平均在线人数的 10 倍左右',  //   'Please set a reasonable value in range 10 - 65535. Recommended value = 10 * average online members.',
      19 => 'setting_serveropti_onlinehold',
      20 => 'Temps d\'attente en ligne (minutes)',//'在线保持时间(分钟)', //  'Hold Online Time (minutes)',
      21 => 'Svp. veuillez définir la valeur du délai raisonnablement les grands sites doivent utiliser plus petite valeur de temps Utiliser une gamme de 5 -.. 60 minutes trop grand ou trop petit paramètres peuvent augmenter la charge du serveur.',//'请根据访问情况设置合理的数值，访问量大的站点应当调小该数值，设置范围 5 - 60，过大或者过小的设置都有可能会增大服务器资源开销',  //  'Please set the reasonable time value. Large sites should use smaller time value. Use a range of 5 - 60 minutes, too big or too small settings can increase the server loadnig.',
      22 => 'setting_serveropti_jspath',
      23 => 'Annuaire JavaScript',//'JS 文件目录', //   'JavaScript directory',
      24 => 'Définir le répertoire des fichiers JS. Le système va compresser les fichiers *. Js Définir répertoire par défaut, puis enregistrez-le dans le répertoire de cache afin d\'améliorer la vitesse.',//'设置站点 JS 文件的目录。当脚本目录为缓存目录时，系统会将默认目录中的 *.js 文件进行压缩然后保存到缓存目录以提高读取速度',  //   'Set the directory for JS files. The system will compress *.js files from default directory and then save it to the cache directory in order to improve the speed.',
      25 => 'setting_serveropti_lazyload',
      26 => 'Chargement sur image temporisée',//'开启图片延时加载', //  'on Image delay loading',
      27 => 'Lorsqu\'elle est activée, l\'image de la page dans la fenêtre du navigateur à nouveau lorsque le chargement actuel, peut réduire considérablement le poids de trafic d\'un grand site, nous vous recommandons d\'activer cette fonction ',//'当开启后，页面中的图片在浏览器的当前窗口时再加载，可明显降低访问量很大的站点的服务器负担，建议开启本功能',  //   'When turned on, the page image in the browser window again when the current load, can significantly reduce the traffic burden of a large sites, we recommend to turn on this function',
      28 => 'setting_serveropti_blockmaxaggregationitem',
      29 => 'Éléments de données cumulatives au maximum',//'参与模块聚合数据条数', //   'Maximum aggregated data items',
      30 => 'Cette valeur affecte l\'agrégation de blocs lorsque MYSQL récupérer le nombre de données d\'article. Valeur trop grande va sérieusement affecter les performances de MySQL. Trop petite La valeur va diminuer le niveau désiré de l\'agrégation des données par blocs. Mettre à 0 pour désactiver.',//'此值影响模块聚合时MYSQL检索数据的条数，设置太大将严重影响MYSQL的性能，设置太小将会影响模块聚合结果数据的理想程度。0为关闭些功能',  //   'This value affects the block aggregation when MYSQL retrieve the number of Article data. Too large value will seriously affect the performance of MYSQL. Too small value will decrease a desired level of block data aggregation. Set to 0 for disable.',
      31 => 'setting_serveropti_blockcachetimerange',
      32 => 'Bloquer intervalle de temps à jour',//'模块更新时间区间', //  'Block update time interval',
      33 => 'Définissez l\'intervalle de temps spécifié pour toutes à jour bloqué. Il est recommandé de choisir la période de visites de sites minimales, par exemple at 3:00-7:00 o\'clock.',//'设置全部模块在指定的时间区间内更新。可以根据网站的访问情况，避开访问高峰时间段。如：3点-7点',  //   'Set the specified time interval for all blockd update. It is recommended to choose the period of minimal site visits, i.e. at 3:00-7:00 o\'clock.',
      34 => 'setting_serveropti_sessionclose',
      35 => 'Que ce soit pour fermer le mécanisme de session',//'是否关闭session机制', // 'Whether to close the session mechanism', 
      36 => 'La désactivation du mécanisme de session permet de réduire considérablement la charge du serveur. Proposé pour allumer si le nombre d\'internautes est plus que 20000 (Note: durée en ligne et le nombre de visiteurs et les utilisateurs ne seront plus statistique, le forum Accueil et liste les pages fonction de la liste des utilisateurs en ligne du Forum sera indisponible)',//'关闭session机制以后，可明显降低站点的服务器负担，建议在线用户数超过2万时开启本功能（注意：游客数和用户的在线时长将不再进行统计，论坛首页和版块列表页面的在线用户列表功能将不可用）',  //  'Disabling the session mechanism can significantly reduce the the server loading. Proposed to turn on if the number of online users is more than 20000 (Note: Online duration and the number of visitors and users will no longer be statistical, The forum Home and Forum List pages online user list function will be unavailable)',
      37 => 'setting_serveropti_onlineguestsmultiple',
      38 => 'Count Online visitors',//'在线人数测算比例', //  'Count Online visitors',
      39 => 'Après la fermeture du mécanisme de session, les visiteurs en ligne totales (clients et utilisateurs, notamment) ≈ le nombre réel d\'internautes * de rapport en ligne estimé. La valeur recommandée est d\'environ 10, pour en virgule flottante (Note: le nombre réel de ligne cycle de cache de l\'utilisateur est de 600 secondes)',//'关闭session机制以后，总在线人(包括游客和用户) ≈ 实际在线用户数 * 在线人数测算比例，建议值为10左右,可为浮点数（注意：实际在线用户数缓存周期为600秒）',  //  'After closing the session mechanism, The total online visitors (Including guests and users) ≈ The actual number of online users * Estimated Online ratio. The recommended value is about 10, for floating-point (Note: the actual number of online users cache cycle is 600 seconds)',
    ),
  ),
  258 =>
  array (
    'index' =>
    array (
      'Paramètres de l\'éditeur' => 'action=setting&operation=editor',//'编辑器设置', //  'Editor Settings'
      'Global' => 'action=setting&operation=editor',//'全局', //  
    ),
    'text' =>
    array (
      0 => 'Paramètres de l\'éditeur &raquo; Global',//'编辑器设置 &raquo; 全局', //  'Editor Settings &raquo; Global',
      1 => 'setting_editor_mode_default',
      2 => 'Mode éditeur par défaut',//'默认的编辑器模式', //  'Default editor mode',
      3 => 'setting_editor_swtich_enable',
      4 => 'Activer pour changer le mode de l\'éditeur',//'是否允许切换编辑器模式', //   'Enable to switch the editor mode',
      5 => 'Choisissez si vous voulez activer ou désactiver pour changer le mode de l\'éditeur entre texte et WYSIWYG.',//'选择否将禁止用户在纯文本模式和所见即所得模式之间切换', //  'Choose whether to enable or disable to change the editor mode between plain text and WYSIWYG.',
      6 => 'setting_editor_simplemode',
      7 => 'Simple mode',//'编辑栏样式', //  'Simple mode',
      8 => 'Définir mode simple par défaut',//'设置默认的编辑栏样式', //  'Set the simple mode as default',
      9 => 'setting_editor_smthumb',
      10 => 'Largeur du sourire et de la hauteur',//'表情图片的宽高', //   'Smile width and height',
      11 => 'Quand une taille réelle de sourire dépasse la taille des vignettes, le système affiche automatiquement une vignette à la place de l\'image réelle. Plage recommandée est comprise entre 20 et 40 ans.',//'允许范围在 20～40 之间，图片实际尺寸超出设置值时将自动缩略显示',   //   'When a real smile size exceeds the thumbnal size, the system will automatically display a thumbnail instead of real image. Recommended range is between 20 and 40.',
      12 => 'setting_editor_smcols',
      13 => 'Nombre de colonnes et des sourires',//'表情列数', //  'Number of columns for smiles',
      14 => 'Combien de colonnes de sourires seront affichés. La valeur recommandée est de 8 à 12.',//'发帖页面表情显示的列数，允许范围在 8～12之间', //  'How many columns of smiles will be shown. Recommended value is from 8 to 12.',
      15 => 'setting_editor_smrows',
      16 => 'Lignes de sourires',//'表情行数', //  > 'Smiles rows',
      17 => 'Combien de lignes de sourires seront affichés.',//'发帖页面表情显示的行数', //   'How many rows of smiles will be shown.',
    ),
  ),
  259 =>
  array (
    'index' =>
    array (
      'Site modules' => 'action=setting&operation=functions',//'站点功能', //  'Site modules'
      'Blocs fonctionnels' => 'action=setting&operation=functions&anchor=curscript',//'功能模块', // 'Functional blocks' 
    ),
    'text' =>
    array (
      0 => 'Les caractéristiques du site &raquo; Blocs fonctionnels',//'站点功能 &raquo; 功能模块', //  'Site features &raquo; Functional blocks',
      1 => 'setting_functions_curscript_list',
      2 => 'Liste Module',//'功能列表', //  'Module List',
    ),
  ),
  260 =>
  array (
    'index' =>
    array (
      'Site modules' => 'action=setting&operation=functions',//'站点功能', //  
      'Liées à la gestion' => 'action=setting&operation=functions&anchor=mod',//'管理相关', //  'Management-related' 
    ),
    'text' =>
    array (
      0 => 'Les caractéristiques du site &raquo; Liées à la gestion',//'站点功能 &raquo; 管理相关', //  'Site features &raquo; Management-related',
      1 => 'setting_tips',
      2 => 'Les options marquées par la ligne pointillée rouge sont liés à l\'efficacité du système, la capacité de charge et la consommation des ressources (peut améliorer ou réduire l\'efficacité). Il est recommandé de procéder à des ajustements en fonction de la situation de votre serveur.',//'以红色虚线标示的选项，表示该选项和系统效率、负载能力与资源消耗有关(提高效率、或降低效率)，建议依据自身服务器情况进行调整。',   //   'Options marked by red dotted line are related to the system efficiency, load capacity and resource consumption (can improve or reduce the efficiency). It is recommended to make adjustments according to your server situation.',
      3 => 'setting_functions_mod_updatestat',
      4 => 'Activer les statistiques du site',//'开启站点趋势统计', //  'Enable the site statistics',
      5 => 'Les statistiques du site est très utile pour l\'Administrateur du site. Remarque: Cette fonction va légèrement augmenter la charge du système.',//'站点趋势统计可以使站长了解到站点的运作状况。注意: 本功能会轻微加重系统负担',   //   'The site statistics is very usefull for the site administrator. Note: This feature will slightly increase the system loading.',
      6 => 'setting_functions_mod_status',
      7 => 'Activer gestion Forum Statistiques',//'开启论坛管理工作统计', //  'Enable Forum Management Statistics',
      8 => 'Statistiques gestion est utile pour l\'Administrateur du site pour comprendre ce que les Modérateurs et les autres membres du personnel de gestion a effectué. Remarque: Cette fonction va légèrement augmenter la charge du système.',//'论坛管理工作统计可以使版主、管理员了解论坛版主等管理人员的工作状况。注意: 本功能会轻微加重系统负担',   //  'Management statistics is usefull for the site administrator for understand what moderators and other management staff was doing. Note: This feature will slightly increase the system loading.', 
      9 => 'setting_functions_archiver',
      10 => 'Autoriser la fonction Archiveur du Forum',//'开启论坛 Archiver 功能', //   'Allow Forum Archiver function',
      11 => 'Forum Archiveur en mesure d\'ouvrir le contenu comme un analogue de pages statiques pour les moteurs de recherche pour obtenir le contenu',//'论坛 Archiver 能够将论坛公开的内容模拟成静态页面，以便搜索引擎获取其中的内容',  //   'Forum Archiver able to open content as an analog of static pages for search engines to obtain the contents',
      12 => 'setting_functions_archiverredirect',
      13 => 'Interdire l\'Accès des utilisateurs aux Archives',//'禁止用户访问 Archiver', //   'Prohibit user access to Archiver',
      14 => 'Sélectionnez "Oui" pour passer automatiquement au contenu de la page originale, lorsqu\'un utilisateur liée à à la page d\'archivage à travers les moteurs de recherche',//'选择“是”，当用户通过搜索引擎中的链接访问 Archiver 页时将自动跳转到原始内容页',  //   'Select "Yes" for automatically jump to the original page content, when a user linked to to the Archiver page through the search engines',
      15 => 'setting_functions_mod_maxmodworksmonths',
      16 => 'Boutique temps du journal de gestion (mois)',//'管理记录保留时间(月)', //  'Store management log time (months)',
      17 => 'Définir un temps où le système se souviendra des modérateurs enregistrements du journal. La valeur par défaut est de 3 mois. Il est recommandé d\'utiliser de 3 à 6 mois.',//'系统中保留管理记录的时间，默认为 3 个月，建议在 3～6 个月的范围内取值',   //  'Set a time in which the system will remember moderators log records. Default value is 3 months. It is recommended to use from 3 to 6 months.', 
      18 => 'setting_functions_mod_losslessdel',
      19 => 'Ne pas réduire les points d\'utilisateur après N jours Supprimer le message',//'删帖不减积分时间期限(天)', //  'Do not reduce the user points after N days of Delete post',
      20 => 'Définissez le nombre de jours après qu\'une suppression par le Modérateur / Administrateur du post ne met pas à jour les points de l\'utilisateur. Cette fonctionnalité peut être utilisée pour nettoyer les messages obsolètes sans pertes de points d\'utilisateur. Mettre à 0 pour désactiver cette fonction, et toujours à jour les points de l\'utilisateur.',//'设置版主或管理员从前台删除发表于多少天以前的帖子时，不更新用户的扩展积分，可用于清理老帖子而不对作者的扩展积分造成损失。0 为不使用此功能，始终更新用户积分',  //   'Set the number of days after that a deleted by moderator/administrator post does not update the user points. This feature can be used to clean obsolete posts without losses of user points. Set to 0 for disable this feature, and always update the user points.',
      21 => 'setting_functions_mod_reasons',
      22 => 'Taux et de la raison des opérations administratives',//'管理操作理由选项', //   'Rate and Administrative operations reason',
      23 => 'Ce paramètre sera utilisé quand un utilisateur à prendre note ou effectuer certaines actions administratives, pour montrer une liste de raisons disponibles. Une ligne vide sera affiché si le séparateur "--------" a été sélectionné. L\'utilisateur peut choisir de sélectionner à partir de motifs énumérés ou entrer proprement.',//'本设定将在用户执行部分管理操作时显示，每个理由一行，如果空行则显示一行分隔符“--------”，用户可选择本设定中预置的理由选项或自行输入',  //    'This setting will used when a user make score or perform certain administrative actions, to show a list of available reasons. A blank line will displayed if the separator "--------" was selected. User can choose to select from listed reasons or enter his own.',
      24 => 'setting_functions_user_reasons',
      25 => 'Note des utilisateurs options de raison',//'用户评分理由选项', //  'User Rating reason options',
      26 => 'Ces paramètres seront affichés lorsque l\'utilisateur effectue une note. Une raison par ligne, la ligne blanche indique un séparateur "--------", les utilisateurs peuvent choisir l\'une des raisons prédéfinis ou entrer dans le leur',//'本设定将在用户执行评分时显示，每个理由一行，如果空行则显示一行分隔符“--------”，用户可选择本设定中预置的理由选项或自行输入',  //   'This settings will be displayed when the user performs a rating. One reason per line, the blank line will show a separator "--------", Users can select one of preset reasons or enter their own', 
      27 => 'setting_functions_mod_bannedmessages',
      28 => 'Cacher le contenu censuré',//'隐藏敏感帖子内容', //   'Hide censored content',
      29 => 'Choisir quoi faire si un mot censuré apparaître dans un post - Supprimer le message ou masquer les informations utilisateur.',//'选择相应项目将隐藏被删除或被禁止的用户的相关资料，在相关资料处显示被屏蔽的字样',   //  'Select what to do if a censored word apper in a post - delete post or hide user info.',
      30 => 'setting_functions_mod_warninglimit',
      31 => 'Ban auto. après N avertissements',//'用户被警告多少次自动禁言', //  'Auto Ban after N warnings',
      32 => 'Définir le nombre de fois où un utilisateur peut être averti avant l\'interdiction automatique de poster dans une période de temps',//'警告有效期结束自动解除禁言', //  'Set how many times a user may be warned before automatical ban to post in a time period',
      33 => 'setting_functions_mod_warningexpiration',
      34 => 'Avertissement période (jours)',//'警告有效期(天)', // 'Warning period (days)', 
      35 => 'La valeur par défaut est de 30 jours',//'默认为 30 天', //  'The default is 30 days',
      36 => 'setting_functions_mod_rewardexpiration',
      37 => 'Récompense de l\'expiration du fil de la discussion',//'悬赏主题有效期', //  'Reward thread expiration', 
      38 => 'Définir la récompense en jours effectifs du sujet, quand la récompense est valable même après le thème qui n\'a pas été délivré de plus de la récompense, les Modérateurs et Administrateurs ont le droit d\'accorder, au nom du thème de la récompense. 0 ou vide signifie qu\'aucun modérateur ou un administrateur de faire cela',//'设置悬赏主题的有效天数，当悬赏主题超过有效期后依然未发放悬赏，版主或管理员有权代为主题作者发放悬赏。0 或留空表示不允许版主或管理员进行此操作',  //    'Set the reward thread effective days, when the reward is valid even after the theme has not been issued more than reward, moderators or administrators have the right to grant on behalf of the theme of reward. 0 or blank means no moderator or administrator to do this',
      39 => 'setting_functions_mod_moddetail',
      40 => 'Modérateur panel indique le nombre d\'articles à auditer',//'版主面板显示审核数目', //   'Moderator panel displays the number of items to audit',
      41 => 'Réglez le panneau d\'affichage Modérateur détaillé en attendant le nombre précis de sujets, les posts et les utilisateurs',//'设置版主面板是否详细显示待审核主题、帖子及用户的具体数目', //  'Set the moderator panel detailed display pending the specific number of threads, posts and users', 
    ),
  ),
  261 =>
  array (
    'index' =>
    array (
      'Modules du Site' => 'action=setting&operation=functions',//'站点功能', //  'Site modules'
      'Valeur Sujet Hot' => 'action=setting&operation=functions&anchor=heatthread',//'主题热度', //   'Thread hot value'
    ),
    'text' =>
    array (
      0 => 'Les caractéristiques du site &raquo; Valeur Sujet Hot',//'站点功能 &raquo; 主题热度', //  'Site features &raquo; Thread hot value',
      1 => 'setting_functions_heatthread_tips',
      2 => 'La chaleur de la menace sera impliqué dans la base de présence y compris les réponses, l\'examen, la collecte, le partage, la note, il est recommandé',//'主题热度将按参与人次计算，包括回帖，点评，收藏，分享，评分，推荐',  //   'The threat heat will be involved in the attendance basis including replies, review, collection, sharing, rating, it is recommended',
      3 => 'setting_functions_heatthread_period',
      4 => 'Utilisateur période de valeur Hot (jours)',//'用户热度值周期(天)', //  'User Hot value period (days)',
      5 => 'Une période de jours, lorsque l\'utilisateur peut augmenter la valeur hot du sujet une fois seulement. 0 signifie aucun délai limité, la chaleur Hot peut être ajouté à tout moment de  +1. Pour éviter la chaleur Hot  de pinceau de l\'utilisateur, il n\'est pas recommandé de la mettre à 0. (Réponses, messages mur, commentaires, favoris, partages et ainsi de suite sont à nouveau pris en compte dans l\'action du sujet)',//'以天为单位，一个周期内某用户多次参与主题，只加一次热度。0代表不设置周期，只要参与一次，热度就加1。为避免用户刷热度，建议不要设置为0。(回复、点评、评论、收藏、分享等都算作参与主题的动作)',   //  'A period in days, when a user can increase the thread hot value only once. 0 means no limit period, the heat may be added any times to +1. To avoid the user brush heat, is not recommended to set this to 0. (Replies, wall messages, comments, favorites, sharings and so on are re counted in the thread action)',
      6 => 'setting_functions_heatthread_iconlevels',
      7 => 'Afficher un niveau de popularité au sujet ',//'热门主题显示级别', //  'Display a thread popularity level',
      8 => 'Définir la liste des icônes pour chaque niveau de chaleur Hot au sujet , la gradation par défaut est de 3 niveaux. Les valeurs de chaleur séparés par une virgule, à savoir: "50,100,200" - cela signifie que le niveau 1 si la chaleur Hot au sujet  supérieur à 50, niveau 2 lorsque la chaleur Hot plus grand que 100, niveau 3 quand une chaleur Hot de plus de 200. Laissez le champ vierge que pour ne pas afficher l\'icône de niveau',//'设置主题列表页主题图标每一级别对应的热度指数，默认为 3 个级别，请用逗号分隔，如：“50,100,200”表示当主题的热度大于 50 时为 1 级，大于 100 时为 2 级，大于 200 时为 3 级。留空表示不显示图标',   //   'Set the icon list for each thread heat level, the default gradation is for 3 levels. Separate heat values with a comma, i.e.: "50,100,200" - this means level 1 if a thread heat greater than 50, Level 2 when heat large than 100, Level 3 when a heat more than 200. Leave blank that for not display the level icon',
    ),
  ),
  262 =>
  array (
    'index' =>
    array (
      'Modules du Site ' => 'action=setting&operation=functions',//'站点功能', //  'Site modules'
      'Recommandation de la discussion' => 'action=setting&operation=functions&anchor=recommend',//'主题评价', //   'Thread recommendation'
    ),
    'text' =>
    array (
      0 => 'Les caractéristiques du site &raquo; Recommandation de la discussion',//'站点功能 &raquo; 主题评价', //   'Site features &raquo; Thread recommendation',
      1 => 'setting_functions_recommend_tips',
      2 => 'Les membres de différents groupes d\'utilisateurs peuvent voir les différents fils du sujet recommandés. Vous pouvez utiliser le réglage de "groupe" de "posts liés".',//'不同用户组会员的主题评价影响值不同，你可以在”用户组”设置的“帖子相关”中设置。',  //  'Members of different user groups can view different recommended threads. You can use "group" setting of "related posts".',
      3 => 'setting_functions_recommend_status',
      4 => 'Activer note au sujet ',//'启用主题评价', //  'Enable thread rating',
      5 => 'Note de cette discussion permettent aux utilisateurs d\'augmenter la popularité de fil du sujet. Vous pouvez définir combien de fois par jour, un utilisateur peut évaluer un sujet.',//'主题评价是让用户参与主题热度的重要指标，你可以设定用户在每天允许执行的评价次数',  //  'Thread rating enable users to increase the thread popularity. You can set how many times per day a user can rate a thread.',
      6 => 'setting_functions_recommend_addtext',
      7 => 'Positive Rate extra text',//'加分操作文字', //  'Positive Rate extra text',
      8 => 'Définir un texte supplémentaire court pour une opération de taux, comme "top", "support", etc.',//'设置评价加分的文字，不宜过长。建议使用“顶”、“支持”', //  'Set a short extra text for a rate operation, like "top", "support", etc.',
      9 => 'setting_functions_recommend_subtracttext',
      10 => 'Taux texte supplémentaire négative',//'减分操作文字', //   'Negative Rate extra text',
      11 => 'Définir un texte supplémentaire court pour une opération de taux, comme "mauvais", "contre", etc.',//'设置评价减分的文字，不宜过长。建议使用“踩”、“反对”', //   'Set a short extra text for a rate operation, like "bad", "against", etc.',
      12 => 'setting_functions_recommend_daycount',
      13 => 'Taux limite quotidienne',//'每 24 小时评价主题次数', //  'Rate daily limit', 
      14 => 'Combien de fois par jourqu\'un utilisateur puisse mettre des taux aux discussions, 0 ou vide pour ne pas limiter',//'设置用户每 24 小时可以评价多少篇主题，0 或留空为不限制', // 'How many times per day a user can rate threads, 0 or blank for not limit',
      15 => 'setting_functions_recommend_ownthread',
      16 => 'Activer les propres taux des sujets',//'是否允许评价自己的帖子', //   'Enable to rate own threads',
      17 => 'Choisissez si vous voulez autoriser les utilisateurs à voter pour leurs propres sujets. Évaluez leur propre sujet ne donnera pas de points',//'设置是否允许评价自己的主题。评价自己的主题无积分奖励', //   'Set whether to allow users to rate their own threads. Rate own thhread will not give any points',
      18 => 'setting_functions_recommend_iconlevels',
      19 => 'Icônes niveau nominal',//'评价图标显示级别', // 'Rated level icons', 
      20 => 'Définir une liste d\'icônes de niveau de notation, une icône pour chaque niveau. Recommandé d\'utiliser trois niveaux, séparés par une virgule, à savoir: "0,100,200" signifie niveau 1 pour notées de 0 à 100, niveau 2 pour valeur nominale supérieure à 100, et le niveau 2 pour évalué à plus de 200. Discussions esprit de niveau 1 et au-dessus apparaîtront dans "Les discussions recommandées" de la liste. Définir vide ou 0 pour désactiver l\'icône de niveau.',//'设置主题列表页评价图标每一级别对应的评价指数。建议为 3 个级别，请用逗号分隔，如：“0,100,200”表示当主题的评价指数大于 0 时为 1 级，大于 100 时为 2 级，大于 200 时为 3 级。1 级及以上级别的主题会在主题列表页“查看好评主题”中列出。留空或者 0 表示不显示图标',  //  'Set a list of rating level icons, one icon per each level. Recommended to use three levels, separated with a comma, i.e.: "0,100,200" means level 1 for rated from 0 to 100, level 2 for rated more than 100,and  Level 2 for rated more than 200. Threads wit a level 1 and above will appear in "Recommended threads" list. Set blank or 0 for disable level icon.',
    ),
  ),
  263 =>
  array (
    'index' =>
    array (
      'Site modules' => 'action=setting&operation=functions',//'站点功能', //  'Site modules'
      'Poster un Commentaire' => 'action=setting&operation=functions&anchor=comment',//'帖子点评', //    'Post Comment'
    ),
    'text' =>
    array (
      0 => 'Les caractéristiques du site &raquo; Poster un Commentaire',//'站点功能 &raquo; 帖子点评', //  'Site features &raquo; Post Comment',
      1 => 'setting_functions_comment_allow',
      2 => 'Méthode de commentaire',//'点评方式', //   'Comment method',
      3 => 'Définir la méthode de l\'édition du commentaire. Si cela n\'est pas choisi, à la fonction de commentaires est désactivée. Commentaire sur l\'une des méthodes doivent être définies dans les groupes d\'utilisateurs des autorisations de commentaires à "Commentaire directe" par la poste à chaque étage dans les "commentaires" lien Poster un Commentaire "réaction au sol" par post à chaque étage de la "Réponse" lien de Réponse, Commentaire sur le sol généré automatiquement. Remarque: cliquez sur le fil de la discussion "Réponse" lien ne produira pas le commentaire au sujet ',//'设置点评的发表方式，如不选择表示不启用点评功能。开启任意一种点评方式都需要在用户组中设置点评权限“直接点评”即通过帖子每楼层中的“点评”链接发表点评“楼层回复”即通过帖子每楼层中的“回复”链接发表回帖时，自动对该楼层产生点评注意：点击主题中的“回复”链接，不会对主题产生点评',              //    'Set the method of comment publishing. If this is not choosed, the Comment function will be disabled. Comment on any of the methods need to be set in the user group Comment permissions to "direct Comment" through the post every floor in the "Comments" link Post Comment "floor response" through the post every floor in the "Reply" link at Post Reply, Comment on the floor automatically generated. Note: click on the thred "Reply" link will not produce the thread comment',
      4 => 'setting_functions_comment_number',
      5 => 'Nombre de commentaires',//'点评条目数', //  'Number of comments',
      6 => 'Afficher un certain nombre de commentaires. Mettre à 0 pour ne pas calculer commentaires. Après la création d\'un fil de discussion, il est nécessaire de définir des autorisations pour des groupes d\'utilisateurs à commenter.',//'在帖子中显示点评的条目数',   //   Show a number of comments. Set to 0 for not calculate comments. After creating a thread, it is required to set permissions for user groups to comment.
      7 => 'setting_functions_comment_postself',
      8 => 'Permettre de commenter leur propres sujets',//'允许点评自己的帖子', //  'Allow to comment own threads',
      9 => 'Ces paramètres permettent de commenter vos propres sujets, mais désactiver pour voir publié par vous commentaires.',//'设置是否发帖作者能点评自己的帖子', //  'This settings enable to comment your own threads, but disable to view published by you comments.',
      10 => 'setting_functions_comment_firstpost',
      11 => 'Permettre de commenter le premier post',//'允许点评楼主帖', //   'Allow to comment the first post',
      12 => 'Choisissez si vous voulez autoriser le propriétaire pour poster un premier commentaire, les autres membres ne peuvent commenter les réponses.',//'设置是否允许直接点评楼主帖，关闭表示只能点评回帖', //  'Set whether to allow the owner to post a first comment, other members can only comment replies.',
      13 => 'setting_functions_comment_commentitem_0',
      14 => 'Préréglage du Commentaire liste générale',//'普通主题点评预置观点', //  'Preset the General Comment List',
      15 => 'Ensemble prédéfini liste d\'observation générale, affiché sous forme d\'un menu déroulant. Un commentaire par ligne. S\'il y a une ligne vide, l\'utilisateur aura la possibilité de saisir son point de vue alternatif.',//'每个观点一行。如存在空行，空行下方的内容将显示在下拉菜单中作为备选观点',   //  'Set predefined general comment list, displayed as a drop-down menu. One comment per line. If there is an empty line, a user will be enabled to enter his alternative point of view.',
      16 => 'setting_functions_comment_commentitem_1',
      17 => 'Sondage Vote commentaires prédéfinies',//'投票主题点评预置观点', //  'Poll vote predefined comments',
      18 => 'Ensemble prédéfini liste d\'observation générale, affiché sous forme d\'un menu déroulant. Un commentaire par ligne. S\'il y a une ligne vide, l\'utilisateur aura la possibilité de saisir son point de vue alternatif.',//'每个观点一行。如存在空行，空行下方的内容将显示在下拉菜单中作为备选观点',   //   'Set predefined general comment list, displayed as a drop-down menu. One comment per line. If there is an empty line, a user will be enabled to enter his alternative point of view.',
      19 => 'setting_functions_comment_commentitem_2',
      20 => 'Taux Produit aux commentaires prédéfinies',//'商品主题点评预置观点', //   'Product rate predefined comments',
      21 => 'Ensemble prédéfini liste d\'observation générale, affiché sous forme d\'un menu déroulant. Un commentaire par ligne. S\'il y a une ligne vide, l\'utilisateur aura la possibilité de saisir son point de vue alternatif.',//'每个观点一行。如存在空行，空行下方的内容将显示在下拉菜单中作为备选观点',   //  'Set predefined general comment list, displayed as a drop-down menu. One comment per line. If there is an empty line, a user will be enabled to enter his alternative point of view.',
      22 => 'setting_functions_comment_commentitem_3',
      23 => 'Récompense du vote aux commentaires prédéfinies',//'悬赏主题点评预置观点', //   'Reward vote predefined comments',
      24 => 'Ensemble prédéfini liste d\'observation générale, affiché sous forme d\'un menu déroulant. Un commentaire par ligne. S\'il y a une ligne vide, l\'utilisateur aura la possibilité de saisir son point de vue alternatif.',//'每个观点一行。如存在空行，空行下方的内容将显示在下拉菜单中作为备选观点',   //  'Set predefined general comment list, displayed as a drop-down menu. One comment per line. If there is an empty line, a user will be enabled to enter his alternative point of view.',
      25 => 'setting_functions_comment_commentitem_4',
      26 => 'Noter la discussion des commentaires prédéfinis',//'活动主题点评预置观点', //  'Thread rate predefined comments', 
      27 => 'Ensemble prédéfini liste d\'observation générale, affiché sous forme d\'un menu déroulant. Un commentaire par ligne. S\'il y a une ligne vide, l\'utilisateur aura la possibilité de saisir son point de vue alternatif.',//'每个观点一行。如存在空行，空行下方的内容将显示在下拉菜单中作为备选观点',   //   'Set predefined general comment list, displayed as a drop-down menu. One comment per line. If there is an empty line, a user will be enabled to enter his alternative point of view.',
      28 => 'setting_functions_comment_commentitem_5',
      29 => 'Débat commentaires prédéfinies',//'辩论主题点评预置观点',  //  'Debate predefined comments',
      30 => 'Ensemble prédéfini liste d\'observation générale, affiché sous forme d\'un menu déroulant. Un commentaire par ligne. S\'il y a une ligne vide, l\'utilisateur aura la possibilité de saisir son point de vue alternatif.',//'每个观点一行。如存在空行，空行下方的内容将显示在下拉菜单中作为备选观点',   //   'Set predefined general comment list, displayed as a drop-down menu. One comment per line. If there is an empty line, a user will be enabled to enter his alternative point of view.',
    ),
  ),
  264 =>
  array (
    'index' =>
    array (
      'Les caractéristiques du site' => 'action=setting&operation=functions',  // '站点功能'
      'Lecture des messages' => 'action=setting&operation=functions&anchor=threadexp', // '帖子阅读'
    ),
    'text' =>
    array (
      0 => 'Les caractéristiques du site &raquo; Lecture des messages',  //  '站点功能 &raquo; 帖子阅读',
      1 => 'setting_functions_threadexp_repliesrank',
      2 => 'Le vote est permis Réponses',  //  '启用回帖投票',
      3 => 'Activé, les utilisateurs peuvent les messages d\'un "soutien", "s\'opposer" opération.',  //  '开启后，用户可对回帖进行“支持”、“反对”操作。',
      4 => 'setting_functions_threadexp_blacklist',
      5 => 'Permettant aux utilisateurs  cacher sur liste noire',  //   '启用用户黑名单隐藏',
      6 => 'Activé, l\'utilisateur peut être ajouté à sur liste noire approche d\'autres utilisateurs, cacher les messages de l\'utilisateur spécifique.',  // '开启后，用户可以通过将其他用户加入黑名单的方法，隐藏指定用户的帖子。',  
      7 => 'setting_functions_threadexp_hotreplies',
      8 => 'Réponses recommandées',  //   '推荐回复数',  
      9 => 'Réponses système d\'évaluation en fonction de la situation et l\'autocollant ou scotché sur la première page de sujets de réponses recommandées, 0 n\'est pas affiché.',  // '系统根据回帖评价和水帖情况，在主题第一页推荐回帖，0 为不显示。',   
      10 => 'setting_functions_threadexp_filter',
      11 => 'Mots décalcomanie',  //  '水帖字数',  
      12 => 'Système pour déterminer l\'état décalcomanie messages d\'irrigation au spam, moins de la valeur du poste sera jugé comme décalcomanie, mais le traitement 0 messages.',  //  '系统判断水帖的条件，小于此数值的帖子会被判断为水帖，0 为不过滤水帖。', 
      13 => 'setting_functions_threadexp_nofilteredpost',
      14 => 'Réponse recommandée Recommander messages non aqueux',  //   '推荐回复时推荐非水帖',
      15 => 'Permis à la première page recommander recommander la réponse appropriée à un certains postes non aqueux.',  //  '启用后会在第一页推荐回复处推荐相应一定的非水帖。',
      16 => 'setting_functions_threadexp_hidefilteredpost',
      17 => 'Activer décalcomanie cacher',  //  '启用隐藏水帖', 
      18 => 'Activé le système détermine les messages sticker sera automatiquement masqués.',  //  '开启后系统判断为水帖的帖子会被自动隐藏。', 
      19 => 'setting_functions_threadexp_filterednovote',
      20 => 'Décalcomanie ne peut pas participer au vote Réponses',  //  '水帖不能参与回帖投票',
      21 => 'Ouvrir le système détermine que l\'irrigation au spam ne participera pas après au post des réponses du sondage .',  //   '开启后系统判断为水帖的帖子将不能参与回帖投票。',
    ),
  ),
  265 =>
  array (
    'index' =>
    array (
      'Site modules' => 'action=setting&operation=functions',//'站点功能',  //   'Site modules'
      'Autre' => 'action=setting&operation=functions&anchor=other',//'其他', //  'Other'
    ),
    'text' =>
    array (
      0 => 'Les caractéristiques du site &raquo; Autre',//'站点功能 &raquo; 其他',  //   'Site features &raquo; Other',
      1 => 'setting_tips',
      2 => 'Les options marquées par la ligne pointillée rouge sont liés à l\'efficacité du système, la capacité de charge et la consommation des ressources (peut améliorer ou réduire l\'efficacité). Il est recommandé de procéder à des ajustements en fonction de la situation de votre serveur.',//'以红色虚线标示的选项，表示该选项和系统效率、负载能力与资源消耗有关(提高效率、或降低效率)，建议依据自身服务器情况进行调整。',   //  'Options marked by red dotted line are related to the system efficiency, load capacity and resource consumption (can improve or reduce the efficiency). It is recommended to make adjustments according to your server situation.',
      3 => 'setting_functions_other_pwdsafety',
      4 => 'Activez le cryptage du mot de passe',//'启用登录密码加密',  //  'Enable password encryption',
      5 => 'Choisissez "Oui", si un mot de passe doit être crypté avant de transmettre au serveur',//'选择“是”，站点登录时的密码将进行加密后再传输',  //  'Select "Yes", if a password must be encrypted before transmitted to the server',
      6 => 'setting_functions_other_uidlogin',
      7 => 'Activer UID Connexion',  //  '启用UID登录',  //  
      8 => 'Sélectionnez "Oui", le site permet au journal d\'authentification d\'ouverture de session UID',  // '选择“是”，站点登录时允许通过 UID的方式登录验证', 
      9 => 'setting_functions_other_autoidselect',
      10 => 'Activer la détection automatique de compte tout en étant en train de se connecter',//'启用登录自动选择帐号', //  'Enable automatic account detection while login',  //   'Enable automatic account detection while login',
      11 => 'Choisissez "Oui" pour détecter automatiquement du compte utilisateur en première correspondance entre un nom d\'utilisateur, UID ou E-mail',//'选择“是”，站点登录时的将自动按 UID、E-mail、用户名的顺序逐一去匹配',  //   'Select "Yes" for autodetect the user account by first matching of a user name, UID or E-mail',
      12 => 'setting_functions_other_rssstatus',
      13 => 'Activé RSS',//'启用 RSS', //   'Enable RSS',
      14 => 'Choisissez "Oui" pour permettre aux utilisateurs d\'utiliser un logiciel client RSS pour recevoir les derniers messages du forum. Remarque: si le forum possède de nombreux sous-forums, cette fonctionnalité peut augmenter la charge du serveur.',//'选择“是”，将允许用户使用 RSS 客户端软件接收最新的帖子、文章更新。注意: 在分类很多的情况下，本功能可能会加重服务器负担',  //    'Select "Yes" for allow users to use RSS client software to receive the latest Forum posts. Note: If a forum has many sub-forums, this feature may increase the server loading.',
      15 => 'setting_functions_other_rssttl',
      16 => 'RSS TTL (minutes)',//'RSS TTL(分钟)', //   'RSS TTL (minutes)',
      17 => 'TTL (Time to Live) est une propriété de RSS 2.0, et permet de contrôler le temps de rafraîchissement automatique de l\'abonnement. La durée de vie plus courte donner plus d\'informations en temps réel, mais augmentent la charge du serveur. Valeur recommandée: 30 ~ 180.',//'TTL(Time to Live) 是 RSS 2.0 的一项属性，用于控制订阅内容的自动刷新时间，时间越短则资料实时性就越高，但会加重服务器负担，通常可设置为 30～180 范围内的数值',  //   'TTL (Time to Live) is a property of RSS 2.0, and used to control the automatic refresh time of subscription. The shorter TTL give more real-time information, but increase the server loading. Recommended value: 30 ~ 180.',
      18 => 'setting_functions_other_oltimespan',
      19 => 'Les utilisateurs heure de mise à jour du temps en ligne (minutes)',//'用户在线时间更新时长(分钟)', //  'Users online time update time (minutes)', 
      20 => 'Discuz! Les statistiques disponibles pour chaque utilisateur, et le temps total en ligne du mois, mis à jour du paramètre pour régler l\'heure et les utilisateurs des fréquences de temps en ligne. Par exemple, réglé sur 10, l\'utilisateur mis à jour toutes les 10 minutes tous les records en ligne. Plus la valeur de ce paramètre, du statistiques plus précises, mais plus la consommation des ressources. Réglage recommandé est de 5 à 30 gamme, 0 pour ne pas enregistrer l\'utilisateur de temps en ligne ',//'Discuz! 可统计每个用户总共和当月的在线时间，本设置用以设定更新用户在线时间的时间频率。例如设置为 10，则用户每在线 10 分钟更新一次记录。本设置值越小，则统计越精确，但消耗资源越大。建议设置为 5～30 范围内，0 为不记录用户在线时间',   //    'Discuz! Available statistics for each user, and total online time of the month, updated the setting to set the time and frequency users online time. For example, set to 10, the user updated every 10 minutes every record online. The smaller the value of this setting, the more precise statistics, but the greater the consumption of resources. Recommended setting is 5 to 30 range, 0 to not record user online time ',
      21 => 'setting_functions_other_debug',
      22 => 'Afficher les infos d\'exécution',//'显示程序运行信息', //   'Show Runtime Info',
      23 => 'Choisissez "Oui" pour afficher un délai d\'exécution et un certain nombre de requêtes de base de données en bas de page.',//'选择“是”将在页脚处显示程序运行时间和数据库查询次数',  //   'Selected "Yes" for display an execution time and a number of database queries in the footer.',
      24 => 'setting_functions_other_onlyacceptfriendpm',
      25 => 'Par défaut, les messages courts peuvent être envoyés uniquement aux amis',//'全站是否默认只接受好友短消息', //'By default short messages may be sent only to friends',  
      26 => 'Si le paramètre de message personnel sélectionner "Oui", la valeur par défaut de recevoir un message court activé uniquement à des amis',//'选择“是”将在个人短消息设置中，默认只接收好友的短消息',   //    'If the personal message setting select to "Yes", the default receive short message enabled only to friends',
      27 => 'setting_functions_other_pmreportuser',
      28 => 'Recevoir de messages brefs des informateurs(UID, séparés par des virgules)',//'接收短消息举报人(UID，多人使用英文逗号分隔)', //'Receive short message informants (UID, separated by comma)',  
      29 => 'Selon le reçu de rapport de messages courts pour alerter l\'utilisateur, peut traiter sans délai pour envoyer des messages de spam de l\'utilisateur, vierge indique ne pas activer la fonction de rapport de message court',//'根据接收的短消息举报提醒用户，可及时处理发送垃圾短信的用户，留空表示不启用短消息举报功能', //  'According to received short message report to alert the user, can promptly processing to send spam messages of the user, Blank indicates does not enable the short message report feature',
      30 => 'setting_functions_other_at_anyone',
      31 => 'Peut utiliser @username pour post à quiconque',//'发帖时可@任何人', //   'Can use @username for post to anybody',
      32 => 'Choisissez "Non" pour utilisation uniquement @  pur une liste d\amis',//'选择否表示，只有被@人在用户的收听列表和好友中时才有效', //   'Select "No" for use @ only for a list of friends',
      33 => 'setting_functions_other_chatpmrefreshtime',
      34 => 'Discussion de groupe contenu du message automatiquement d\'intervalle de rafraîchissement (en secondes)',//'群聊消息内容自动刷新间隔(秒)', //  'Group chat message content automatically refresh interval (seconds)',
      35 => 'L\'intervalle de rafraîchissement plus court pour groupe de discussion va augmenter une pression sur le serveur. Mettre à 0 pour ne pas rafraîchir',//'用于群聊消息内容刷新, 间隔时间越短对服务器压力越大, 0为不刷新', //   'The shorter Refresh interval for group chat will increase a pressure on the server. Set to 0 for not refresh',
      36 => 'setting_functions_other_collectionteamworkernum',
      37 => 'Taille de l\'équipe de la collection',//'淘帖专辑允许共同维护的人数', //   'Collection team size',
      38 => 'Le nombre de personnes qui peuvent maintenir chaque collection ensemble.',//'每个淘帖专辑可以共同参与维护的人数。', // 'The number of people who can maintain each collection together.', 
      39 => 'setting_functions_other_shortcut',
      40 => 'Ajouter un raccourci sur le bureau pour rappeler la limite intégrale',  //  '提醒添加桌面快捷的积分下限', //  
      41 => 'Supérieur ou égal à définir l\'intégrateur pour ajouter l\'utilisateur verra le raccourci sur le bureau vers le haut de la mémoire, 0, ou ne remplit pas cette fonction est désactivée', //  '大于等于设置积分的用户将看到添加桌面快捷的顶部提醒，0或不填为关闭此功能',  //  
      42 => 'setting_functions_other_closeforumorderby',
      43 => 'Désactiver la liste des sujet la page des fonctionnalités de tri',//'关闭主题列表页排序功能', //  'Turn off the topic list page sorting functionality', 
      44 => 'Choisissez "Oui" pour les utilisateurs verront uniquement le tri par défaut. Sinon, l\'utilisateur peut sélectionner le tri, qui permettra d\'augmenter la pression du système.',//'选择“是”用户将只能按默认排序查看，否则，用户自选排序将会增加系统压力。',
      45 => 'setting_functions_other_disableipnotice',
      46 => 'Disable Login security notice',//'关闭登录安全提醒',
      47 => 'If selected &quot;Yes&quot;, then the user login regions exception safety notice will be disabled',//'选择“是”将关闭用户登录地区异常安全提醒功能',
      48 => 'setting_functions_other_darkroom',
      49 => 'sur pièce sombre',  //   '开启小黑屋',   
      50 => 'Choisir "Oui" pour tourner une pièce sombre, l\'utilisateur peut naviguer dans le coin inférieur droit de pénétrer dans la chambre noire, vue a été interdit interdit l\'accès à l\'utilisateur', //  '选择“是”将开启小黑屋，用户可以在右下角导航中进入小黑屋，查看被禁言禁止访问用户',
      51 => 'setting_functions_other_global_sign',
      52 => 'Contenu de signature globale',  //  '全局签名内容',
      53 => 'Ce paramètre doit être allumé après la signature valable que si l\'utilisateur n\'a pas configuré sa signature sur le contenu de l\'affichage de signature',  //  '该设置必须开启签名后有效，如果用户没有设置签名就显示该签名内容', 
    ),
  ),
  266 =>
  array (
    'index' =>
    array (
      'Modules du Site' => 'action=setting&operation=functions',//'站点功能' //   'Site modules'
      'Réglages Guide' => 'action=setting&operation=functions&anchor=guide',//'导读设置' //  'Guide settings'
    ),
    'text' =>
    array (
      0 => 'Modules du Site &raquo; Réglages Guide',//'站点功能 &raquo; 导读设置'  //   'Site modules &raquo; Guide settings',
      1 => 'setting_functions_heatthread_guidelimit',
      2 => 'Hot limite inférieure en agrégats',//'热度值聚合下限',  //  'Heat lower limit for aggregate',
      3 => 'Fonction d\'orientation recueillis discussions chaudes quand la chaleur ig valeur supérieure à cette limite inférieure. La valeur par défaut est"3"',//'导读功能中收集热门主题时的热度值下限,默认值“3”',    //  'Guidance Function Collected Hot Threads when the heat value ig greater than this lower limit. Default value is "3"',
      4 => 'setting_functions_guide_hotdt',
      5 => 'Intervalle de temps d\'agrégation de post Hot',//'热帖聚合时间范围',  //  'Hot post aggregation time range',
      6 => 'setting_functions_guide_digestdt',
      7 => 'Résumé d\'agrégation plage de temps',//'精华聚合时间范围', //  'Digest aggregation time range', 
    ),
  ),
  267 =>
  array (
    'index' =>
    array (
      'Modules du Site' => 'action=setting&operation=functions',//'站点功能',  //  'Site modules'
      'Discussion d\'événement' => 'action=setting&operation=functions&anchor=activity',//'活动主题', //  'Event thread' 
    ),
    'text' =>
    array (
      0 => 'Les caractéristiques du site &raquo; Discussion d\'événement',//'站点功能 &raquo; 活动主题', //  'Site features &raquo; Event thread',
      1 => 'setting_functions_activity_type',
      2 => 'Types intégrés',//'内置类别', //  'Built-in types',
      3 => 'Ce paramètre sera affiché des types d\'événement lorsque l\'utilisateur qui déclenche un événement, un type par ligne',//'本设定将在用户发起活动时显示活动类别，每个类别一行',   //  'This setting will be displayed types of event when the user initiate an event, one type per line',
      4 => 'setting_functions_activity_field',
      5 => '(Cliquez ici pour ajouter une entrée de données en option) élément de données en option',//'(点此新增资料项)活动发起者可选的必填资料项',  //    '(Click here to add optional data entry) optional data item',
      6 => 'Événements initiative du client, choisissez l\'élément de données',//'用户发起活动时可选择的资料项',  //'Client-initiated events, choose the data item',   
      7 => 'setting_functions_activity_extnum',
      8 => 'Nombre de champs étendus',//'扩展资料项数量',  //'Number of extended fields',  
      9 => 'L\'initiative du client cas, le nombre d\'éléments de données personnalisés, 0 pour ne pas permettre l\'entrée de données personnalisée.',//'用户发起活动时自定义资料项数量，0为不允许自定义资料项',   //  'Client-initiated event, the number of custom data items, 0 to not allow custom data entry.',
      10 => 'setting_functions_activity_credit',
      11 => 'Utilisez les points',//'使用积分',  //  'Use the points',
      12 => 'Participation à l\'événement consommer avec ce nombre de points',//'参与消耗积分的活动时使用的积分',  // 'Participation in the event will consume with this number of points', 
      13 => 'setting_functions_activity_pp',
      14 => 'Numéro de liste utilisateur',//'列表用户数',  //   'User list number', 
      15 => 'Page de la liste utilisateur indique le nombre de personnes impliquées dans des activités',//'用户列表每页显示参与活动的人数', //   'User list page shows the number of people involved in activities',
    ),
  ),
  268 =>
  array (
    'index' =>
    array (
      'Les autorisations des utilisateurs' => 'action=setting&operation=permissions',//'用户权限',  //  'User Permissions' 
    ),
    'text' =>
    array (
      0 => 'Les autorisations des utilisateurs',//'用户权限',  //   'User Permissions',
      1 => 'setting_permissions_allowviewuserthread',
      2 => 'Permet de voir d\'autres Auteurs des discussions et posts',//'允许查看用户的主题和帖子',  //  'Allow to view other authors threads and posts',
      3 => 'Ce paramètre permet de consulter les sujets et messages publiés par d\'autres utilisateurs',//'设置是否允许查看其他用户的主题和帖子，选择相应的版块那么在个人空间将出现“主题”模块和链接',  //   'This setting allowed to view topics and posts published by other users',
      4 => 'setting_permissions_allowviewuserthread_fids',
      5 => 'Activer voir d\'autres forums',//'允许查看哪些版块的帖子', //   'Enable view other Forums',
      6 => 'Ce paramètre permet aux utilisateurs de visualiser d\'autres sujets du forum et des posts',//'设置允许查看其他用户哪些版块的主题和帖子', //  'This setting allows users to viewe other Forum topics and posts',
      7 => 'setting_permissions_allowmoderatingthread',
      8 => 'Inciter le modérateur s\'il n\'a pas examiné le sujet ou un post',//'是否提示用户自己有未审核的主题或者帖子', //  'Prompt the moderator if he have not reviewed the thread or post', 
      9 => 'Choisissez "Oui" pour avertir un modérateur pour examiner les messages en attente, de sorte qu\'il sera en mesure de voir les conseils donnés dans la page la liste des billets de chaque post en attente.',//'选择“是”，用户如果有正在审核的主题，将可以在帖子列表页看到提示',  //  'Select "Yes" for notify a moderator to review the pending posts, so he will be able to see the tips in the post list page.',
      10 => 'setting_permissions_memliststatus',
      11 => 'Activer voir la liste des membres',//'允许查看会员列表',  //  'Enable view member list',
      12 => 'setting_permissions_minpostsize',
      13 => 'Taille minimale du message (caractères)',//'帖子最小字数(字节)', //  'Minimal Post size (characters)',
      14 => 'Définir 0 pour aucune limite. Remarque: Ce paramètre n\'est pas affecté par des groupes de gestion.',//'管理组成员可通过“发帖不受限制”设置而不受影响，0 为不限制', //   'Set 0 for no limit. Note: This setting not affected for Management Groups.',
      15 => 'setting_permissions_minpostsize_mobile',
      16 => 'Client Mobile annonce le plus petit nombre de mots (octets)',  //  '手机客户端发帖最小字数(字节)',
      17 => 'Si les paramètres du client pour le téléphone seul affichage nombre minimal d\'octets, 0 pour pas un ensemble unique',  // '是否单独为手机客户端设置发帖的最小字节数，0 为不单独设置',  
      18 => 'setting_permissions_maxpostsize',
      19 => 'Taille maximale du message (caractères)',//'帖子最大字数(字节)',  //   'Maximum Post Size (characters)',
      20 => 'Remarque: Ce paramètre n\'est pas affecté par des groupes de gestion.',//'管理组成员可通过“发帖不受限制”设置而不受影响',  //  'Note: This setting not affected for Management Groups.',
      21 => 'setting_permissions_alloweditpost',
      22 => 'Activer aux utilisateurs de modifier leur propres posts',//'允许用户随时编辑的帖子类型',  // 'Enable users to edit own posts', 
      23 => 'Sélectionnés Des groupes d\'utilisateurs ne seront pas limités par "temps Modification du post" et peuvent éditer les messages à tout moment.',//'被选中的帖子类型将不受用户组中的“编辑帖子时间”限制，任何时刻都可以编辑',  //   'Selected user groups will not limited by "Edit post time", and can edit posts at any time.',
      24 => 'setting_permissions_post_append',
      25 => 'Activer ajouter la fonction de post',//'启用帖子补充功能', // 'Enable add post function', 
      26 => 'S\'il est activé, les utilisateurs ne peuvent pas éditer vos messages, mais vous pouvez ajouter du contenu',//'启用后，当用户无法编辑自己的帖子时，可以补充内容',  //  'If enabled, then users can not edit your posts, but you can add content',
      27 => 'setting_permissions_maxpolloptions',
      28 => 'Options maximum de sondage:',//'投票最大选项数:',  //  'Max Poll Options:',
      29 => 'Définir le nombre maximal d\'options de voter dans un sondage',//'设定发布投票包含的最大选项数',  //  'Set maximum number of vote options in a poll',
      30 => 'setting_permissions_editby',
      31 => 'Activer "édité par" remarque',//'编辑帖子附加编辑记录', //  'Enable "Edited by" remark',
      32 => 'Modification après 60 secondes de temps créé va ajouter une remarque "Message modifié par xxx au xxxx-xx-xx". Les administrateurs peuvent éviter cette restriction.',//'在 60 秒后编辑帖子添加“本帖由 xxx 于 xxxx-xx-xx 编辑”字样。管理员编辑不受此限制',  //  'Editing after 60 seconds from created time will add a remark "Post edited by xxx at xxxx-xx-xx". Administrators can avoid this restriction.',
      33 => 'nav_setting_rate',
      34 => 'Classements',//'评分',  //  'Ratings',
      35 => 'setting_permissions_karmaratelimit',
      36 => 'Délai de Taux (heures)',//'评分时间限制(小时)',  //  'Rate time limit (hours)',
      37 => 'Les autres utilisateurs ne peuvent pas évaluer après un temps spécifié de fil de discussion publiés. Cette limite n\'est pas atteinte pour les modérateurs et administrateurs. Mettre à 0 pour aucune limite.',//'帖子发表后超过此时间限制其他用户将不能对此帖评分，版主和管理员不受此限制，0 为不限制',  // 'Other users can not rate after specified time of thread published. This limit is not affect for Moderators and administrators. Set to 0 for no limit.', 
      38 => 'setting_permissions_modratelimit',
      39 => 'Moderator rate limit',//'版主评分限制',  //   'Moderator rate limit',
      40 => 'Les modérateurs peuvent noter les utilisateurs ordinaires et Super Modérateurs seulement dans les propres forums modérés. Administrateurs pas touchés par cette restriction, si elles disposent d\'autorisations de notation, ils peuvent noter dans tous les forums.',//'设置版主只能在自身所管辖的版块范围内对帖子进行评分。本限制只对版主有效，允许评分的普通用户及超级版主、管理员不受此限制，因此如果赋予这些用户评分权限，他们仍将可以在全版块范围内进行评分',  //   'Moderators can rate ordinary users and Super Moderators only in own moderated forums. Administrators not affected by this restriction, so if they have rating permissions, they can rate in all the forums.',
      41 => 'setting_permissions_dupkarmarate',
      42 => 'Enable duplicated rates',//'允许重复评分',  //  'Enable duplicated rates',
      43 => 'Choisissez "Oui" pour permettre aux utilisateurs de voter pour le même poste à plusieurs reprises, le défaut est "Non"',//'选择“是”将允许用户对一个帖子进行多次评分，默认为“否”',  //   'Select "Yes" to allow users to rate the same post several times, the default is "no"',
      44 => 'setting_permissions_editperdel',
      45 => 'Permettre aux utilisateurs de modifier le message marqué à supprimer',//'允许用户编辑帖子时删除帖子',  // 'Allow users to edit the post marked to delete',   //   'Allow users to edit the post marked to delete',
      46 => 'Choisissez "Oui" permettra aux utilisateurs d\'éditer le message marqué à supprimer, par défaut est "Non"',//'选择“是”将允许用户编辑帖子时删除帖子，默认为“否”',  //  'Select "Yes" will allow users to edit the post marked to delete, default is "No"',
      47 => 'setting_permissions_hideexpiration',
      48 => 'Temps maximum efficace pour le post [hide] tag',//'帖子中[hide]标签最大有效天数',  //  'Maximum effective time for the post [hide] tag',   
      49 => 'Nombre de jours de temps après l\'affichage qui le tag caché est expiré automatiquement. Réglez ou vide pour ne pas restreindre',//'距发帖日期天数超过此设置时标签自动失效，0或不填为不限制',  //   'Number of days from posting time after which the hide tag is expired automatically. Set to or empty for not restrict',
    ),
  ),
  269 =>
  array (
    'index' =>
    array (
      'Réglages Monétaire' => 'action=setting&operation=credits',//'积分设置',  // 'Money Setttings'  
      'Réglages de base' => 'action=setting&operation=credits&anchor=base',//'基本设置',  //    'Basic settings' 
    ),
    'text' =>
    array (
      0 => 'Réglages Monétaire &raquo; Réglages de base',//'积分设置 &raquo; 基本设置',  //  'Money Setttings &raquo; Base settings',
      1 => 'setting_credits_extended',
      2 => 'Rapport avec les points d\'échange',//'扩展积分设置', //  'Exchange points ratio',
      3 => 'Set exchanging coefficient for internal points and external currencies. Once the exchange rate is set, a user can exchange their points to external currencies and backward. If you do not want to use points conversion, set the exchange ratio to 0.',//'兑换比率为单项积分对应一个单位标准积分的值，例如 extcredits1 的比率为 1.5(相当于 1.5 个单位标准积分)、extcredits2 的比率为 3(相当于 3 个单位标准积分)、extcredits3 的比率为 15(相当于 15 个单位标准积分)，则 extcredits3 的 1 分相当于 extcredits2 的 5 分或 extcredits1 的 10 分。一旦设置兑换比率，则用户将可以在个人中心自行兑换各项设置了兑换比率的积分，如不希望实行积分自由兑换，请将其兑换比率设置为 0',      //  'Set exchanging coefficient for internal points and external currencies. Once the exchange rate is set, a user can exchange their points to external currencies and backward. If you do not want to use points conversion, set the exchange ratio to 0.', 
      4 => 'setting_credits',
      5 => 'Réglages Monétaire',//'积分设置', //   'Money Setttings',
      6 => 'setting_credits_trans',
      7 => 'Points Transactions',//'交易积分设置', //   'Points Transactions',
      8 => 'Utilisez les points de transactions',//'交易积分是一种可以由用户间自行转让、买卖交易、发布悬赏主题的积分类型，你可以指定一种积分作为交易积分。如果不指定交易积分，则用户间积分交易功能将不能使用。注意: 交易积分必须是已启用的积分，一旦确定请尽量不要更改，否则以往记录及交易可能会产生问题',  //  'Use the points transactions',
      9 => 'setting_credits_trans9',
      10 => 'Utilisez transfert de points ',//'积分转账使用的积分', //   'Use Points Transfer',
      11 => 'Définir pour activer le transfert de points',//'设置积分转账时使用的积分', //  'Set to enable points transfer', 
      12 => 'setting_credits_trans1',
      13 => 'Discussion Transactions Pièces jointes',//'主题(附件)买卖使用的积分', // 'Thread Attachments transactions', 
      14 => 'Discussion Transactions Pièces jointes',//'主题(附件)买卖使用的积分', //  'Thread Attachments transactions',
      15 => 'setting_credits_trans2',
      16 => 'Points de fidélité',//'悬赏使用的积分', //   'Reward points',
      17 => 'Défini pour utiliser des points de fidélité',//'设置悬赏使用的积分', //  'Set to use Reward points', 
      18 => 'setting_credits_trans3',
      19 => 'Magies, décorations utilisées des crédits',//'道具、勋章使用的积分', //   'Magics, decorations used credits',
      20 => 'Définir magie, décorations utilisées des crédits',//'设置道具、勋章中使用的积分', // 'Set magics, decorations used credits',  
      21 => 'setting_credits_trans5',
      22 => 'Points produits',//'商品主题使用的积分', //   'Products points',
      23 => 'Défini pour utiliser des points pour les discussions des produits',//'设置商品主题使用的积分', //  'Set to use points for products threads',
      24 => 'setting_credits_trans6',
      25 => 'Points de consommation Espace',//'空间消费使用的积分',  //   'Space consumption points',
      26 => 'Défini pour utiliser des points dans l\'espace consommation',//'设置在空间中消费使用的积分类型',  // 'Set to use points in space consumtion',  
      27 => 'setting_credits_trans7',
      28 => 'Les points d\'application',//'漫游应用使用的积分',  //   Application points',
      29 => 'Défini pour utiliser des points dans les applications',//'设置与漫游特定应用中的积分体系兑换使用的积分',  // 'Set to use points in applications',  
      30 => 'setting_credits_trans10',
      31 => 'Réponses incitation à utiliser les points',//'回帖奖励使用的积分',  //    'Replies incentive to use points',
      32 => 'Lorsque les utilisateurs du post peut être définie lorsque Réponses points de récompense utilisé',//'当用户发帖时可以设置的回帖奖励所使用的积分',  //  'When users post can be set when Replies reward used points', 
      33 => 'setting_credits_trans8',
      34 => 'Prévenez les utilisateurs sur les points',//'用户举报奖惩使用的积分',  //  'Notify users about points',
      35 => 'Définir pour informer les utilisateurs sur tous les points de changement sur ​​le compte utilisateur',//'设置用户举报后对用户奖惩时使用的积分',  //   'Set to notify users about any points changing on the user account', 
      36 => 'setting_credits_trans11',
      37 => 'Obtiens la limite de points à l\'étage',//'抢楼帖积分限制',  //   'Grab floor points limit', 
      38 => 'Le nombre minimum de points pour participer à ponction post à l\'étage',//'参与抢楼帖积分限制时使用的积分',  //   'Minimum number of points for participate in grab floor post',
      39 => 'setting_credits_trans12',
      40 => 'Création de groupes crédits de consommation',  //  '创建群组消耗积分',
      41 => 'Créer un groupe dans son intégralité consommable',  //  '创建群组时要消耗的积分',
      42 => 'setting_credits_tax',
      43 => 'Taxe sur les transactions',//'积分交易税',  //  'Transaction tax',
      44 => 'Définissez la valeur de taxe sur les transactions (diminution de points) pour chaque transfert de points d\'utilisateur, d\'échange, d\'achat et de vente. Cette valeur doit être du format en virgule flottante, et dans une plage entre 0 et 1. Par exemple, si la valeur est de 0,2, alors l\'utilisateur sera perte de 20% (20 points de chaque 100 points). Mettre à 0 pour désactiver la taxe.',//'积分交易税(损失率)为用户在利用积分进行转让、兑换、买卖时扣除的税率，范围为 0～1 之间的浮点数，例如设置为 0.2，则用户在转换 100 个单位积分时，损失掉的积分为 20 个单位，0 为不损失',      //    'Set the value of transaction tax (decrease points) for each user points transfer, exchange, buying and selling. The value must be of floating point format, and in a range between 0 and 1. For example, if the value is 0.2, then a user will loss 20% (20 points from each 100 points). Set to 0 for disable the tax.',
      45 => 'setting_credits_mintransfer',
      46 => 'Orientations faible',//'转账最低余额',  //   'Minimum transfer summ',
      47 => 'Nécessite un utilisateur d\'avoir cette valeur de solde minimum (points) après la mise en com du transfert. Grâce à cette fonctionnalité, vous pouvez définir restreindre les utilisateurs d\'effectuer des transactions si un solde est inférieur à cette valeur. Vous pouvez également définir une limite pour les soldes négatifs, de sorte que les transferts au sein de cette limite peut être à découvert (crédité).',//'积分转账后要求用户所拥有的余额最小数值。利用此功能，你可以设置较大的余额限制，使积分小于这个数值的用户无法转账；也可以将余额限制设置为负数，使得转账在限额内可以透支',    //    'Requires a user to have this minimum balance value (points) after comleting a transfer. Using this feature, you can set restrict users to make transactions if a balance is less than this value. Also you can set a limit for the negative balances, so transfers within this limit can be overdrawn (credited).',
      48 => 'setting_credits_minexchange',
      49 => 'Solde minimum de change',//'兑换最低余额',  // 'Minimum exchange balance',  
      50 => 'La valeur minimale de la balance utilisateur après les points nécessaires seront rachetées. Grâce à cette fonctionnalité, vous pouvez définir des restrictions pour un équilibre de l\'utilisateur, de sorte que l\'utilisateur points de moins que cette valeur ne peut pas être converti. Vous pouvez également définir une limite de solde négatif, de sorte qu\'un utilisateur peut échanger dans le découvert de quotas (crédité).',//'积分兑换后要求用户所拥有的余额最小数值。利用此功能，你可以设置较大的余额限制，使积分小于这个数值的用户无法兑换；也可以将余额限制设置为负数，使得兑换在限额内可以透支',    //    'The minimum value of user balance after the required points be redeemed. Using this feature, you can set restrictions for a user balance, so that the user points less than this value can not be converted. Also you can set a negative balance limit, so a user can exchange within the quota overdraft (credited).',
      51 => 'setting_credits_maxincperthread',
      52 => 'Unique sujet (Joindre) d\'impôt sur le revenu maximum',//'单主题(附件)最高收入',  //  'Single thread (Attach) maximum income tax',
      53 => 'Réglez la taxe maximale pour un seul fil de discussion de vente (attacher) points de revenu. Si un achat dépasse cette limite, il faudra encore déduire quelques points de l\'acheteur, mais le sujet (Joindre) points de revenu n\'augmente pas. Les restrictions appliquées que pour un fil (Joindre) achat / vente. Mettre à 0 pour aucune limite.',//'设置单一主题(附件)出售作者所得的最高税后积分收入，超过此限制后购买者将仍然被扣除相应积分，但主题(附件)作者收益将不再上涨。本限制只在主题(附件)买卖时起作用，0 为不限制',    //  'Set the maximum tax for a single sale thread (attach) income points. If a purchase exceeds this limit, it will still deduct a buyer points, but the thread (Attach) income points will not rise. The restrictions applied only for a thread (Attach) buying/selling. Set to 0 for no limit.', 
      54 => 'setting_credits_maxchargespan',
      55 => 'Sujet (Joindre) Temps de vente unique (heures)',//'单主题(附件)最高出售时限(小时)',  //  'Single thread (Attach) sale time (hours)',
      56 => 'Définir un temps (en heures) quand un sujet (Joindre) peut être en vente après la publication. Passé ce délai, un fil de discussion devient (gratuit) au point commun (Joindre), et un lecteur n\'aura pas à payer pour cela, et l\'auteur ne recevra plus de profit correspondant. Mettre à 0 pour aucune limite.',//'设置当主题(附件)被作者出售时，系统允许自主题(附件)发布时间起，其可出售的最长时间。超过此时间限制后将变为普通主题(附件)，阅读者无需支付积分购买，作者也将不再获得相应收益，以小时为单位，0 为不限制',    //    'Set a time (in hours) when a thread (Attach) may be on sale after publishing. After this time a thread become a common (free) thread (Attach), and a reader will not have to pay for it, and an author will no longer receive corresponding benefit. Set to 0 for no limit.',
    ),
  ),
  270 =>
  array (
    'index' =>
    array (
      'Réglages E-mail' => 'action=setting&operation=mail',//'邮件设置',  //  'Mail Settings'
      'Réglages' => 'action=setting&operation=mail&anchor=setting',//'设置',  // 'Settings'
    ),
    'text' =>
    array (
      0 => 'Réglages E-mail &raquo; Réglages',//'邮件设置 &raquo; 设置',  //   'Mail Settings &raquo; Settings',
      1 => 'setting_mail_setting_send',
      2 => 'Envoyer mode E-mail',//'邮件发送方式',  //  'Send mail method',
      3 => 'setting_mail_setting_delimiter',
      4 => 'Séparateur en-Tête de message',//'邮件头的分隔符',  //   'Message header separator',
      5 => 'Svp. veuillez configurer votre serveur de messagerie à des paramètres supplémentaires',//'请根据你邮件服务器的设置调整此参数',  //  'Please set your mail server to additional parameters',
      6 => 'setting_mail_setting_includeuser',
      7 => 'Adresse du destinataire contient un nom d\'utilisateur',//'收件人地址中包含用户名',  //   'Recipient address contains a user name',
      8 => 'Choisissez "Oui" si l\'adresse E-mail du destinataire contient le nom d\'utilisateur',//'选择“是”将在收件人的邮件地址中包含站点用户名',  //  'Select "Yes" if the recipient e-mail address contains the user name',
      9 => 'setting_mail_setting_silent',
      10 => 'Masquer toutes les erreurs lors du E-mail envoyer',//'屏蔽邮件发送中的全部错误提示',  //   'Hide all errors while e-mail send',
    ),
  ),
  271 =>
  array (
    'index' =>
    array (
      'Réglages E-mail' => 'action=setting&operation=mail',//'邮件设置',  //  'Mail Settings'
      'Vérification' => 'action=setting&operation=mail&anchor=check',//'检测',  //  'Check'
    ),
    'text' =>
    array (
      0 => 'Réglages E-mail &raquo; Vérification',//'邮件设置 &raquo; 检测',  //  'Mail Settings &raquo; Check',
      1 => 'setting_mail_check_test_from',
      2 => 'Test Examen',//'测试发件人',  //   'Test sender',
      3 => 'setting_mail_check_test_to',
      4 => 'Destinataire de test',//'测试收件人',  //  'Test recipient',
      5 => 'Si vous voulez tester l\'adresse e-mail contenant un nom d\'utilisateur, utilisez le format suivant: "username &lt;user@domain.com&gt;". Séparez plusieurs adresses par des virgules.',//'如果要测试包含用户名的邮件地址，格式为“username &lt;user@domain.com&gt;” 。多个邮件地址用逗号分隔',  //  'If you want to test e-mail address containing a user name, use the next format: "username &lt;user@domain.com&gt;". Separate multiple addresses by commas.',
    ),
  ),
  272 =>
  array (
    'index' =>
    array (
      'Réglages Anti-spam' => 'action=setting&operation=sec',//'防灌水设置',  // 'Anti-spam settings' 
      'Paramètres anti-spam de base' => 'action=setting&operation=sec&anchor=base',//'防灌水基本设置', //   'Basic Anti-spam settings'
    ),
    'text' =>
    array (
      0 => 'Réglages Anti-spam &raquo; Paramètres anti-spam de base',//'防灌水设置 &raquo; 防灌水基本设置',  //  'Anti-spam settings &raquo; Basic Anti-spam settings',
      1 => 'setting_sec_floodctrl',
      2 => 'Diffuser lutte contre le flood (secondes)',//'两次发表时间间隔(秒)', //  'Post Flood Control (seconds)',
      3 => 'Entre deux intervalles de post ne doit pas être inférieure à  0 pour aucune limite',//'两次发帖间隔小于此时间，0 为不限制', //   'Between two post interval must be not less than this time, 0 for no limit',
      4 => 'setting_sec_base_need_email',
      5 => 'Forcer les nouveaux utilisateurs à activer l\'E-mail',//'强制用户验证激活邮箱', //  'Force new users to activate email',
      6 => 'Les membres doivent vérifier leurs courriels avant de pouvoir poster quoi que ce soit.',//'选择是的话，用户必须验证激活自己的邮箱后，才可以进行发布操作。 ',  //  'Members must verify their mailboxes before they can post anything.', 
      7 => 'setting_sec_base_need_avatar',
      8 => 'Forcer les nouveaux utilisateurs de charger l\'Avatar',//'强制用户上传头像',  //  'Force new users to upload avatar',
      9 => 'Choisissez "Oui", pour permettre à tout post uniquement après un avatar téléchargé.',//'选择是的话，用户必须设置自己的头像后才能进行发布操作',  // 'Select Yes, for enable any posting only after an avatar uloaded.', 
      10 => 'setting_sec_base_need_friendnum',
      11 => 'Forcer les nouveaux utilisateurs d\'ajouter un minimum d\'amis',//'强制用户好友个数',  // 'Force new users to add a minimum of friends', 
      12 => 'Définissez le nombre d\'amis, nouvel utilisateur doit avoir avant qu\'il ne puisse poster quoi que ce soit.',//'设置用户必须拥有多少个好友后，才可以进行发布操作',  //  'Set the number of friends, new user must have before he can post anything.',
    ),
  ),
  273 =>
  array (
    'index' =>
    array (
      'Réglages Anti-spam' => 'action=setting&operation=sec',//'防灌水设置',  //   'Anti-spam settings'
      'Réglages Vérification code' => 'action=setting&operation=sec&anchor=seccode',//'验证码设置', //  'Verification code settings'
    ),
    'text' =>
    array (
      0 => 'Réglages Anti-spam &raquo; Réglages Vérification code',//'防灌水设置 &raquo; 验证码设置', //  'Anti-spam settings &raquo; Verification code settings',
      1 => 'setting_sec_code_tips',
      2 => 'Code d\'authentification en utilisant l\'image comme du texte, l\'image doit contenir le caractère "2346789BCEFGHJKMPQRTVWXY" 24 caractères, Et doit être une image GIF transparente, fond transparent, premier plan noir, noir pour l\'image de la couleur du premier indice. Taille de l\'image limitée, mais a suggéré que la largeur ne dépasse pas la largeur du code de vérification, 1/4, la hauteur ne dépasse pas le niveau de code de vérification. Après avoir produite dans le static/image/seccode/gif/ créer un nouveau répertoire, nom de répertoire tout, 24 ont terminé la production de GIF Image téléchargée vers le nouveau répertoire en utilisant l\'image comme fond d\'un code de validation, des images JPG à télécharger sur la production static/image/seccode/background/  bien  dans l\annuaire, Site en utilisant un code de vérification aléatoire à l\'intérieur de l\'image comme fond d\'écran pour utiliser la police TTF comme des mots de code de vérification, les polices TTF anglais pour télécharger les fichiers téléchargés à l\annuaire static/image/seccode/font/fr/ , Site au hasard en utilisant la police à l\'intérieur du fichier texte comme un code code de vérification la vérification avant d\'utiliser l\'image chinoise, nécessité de contenir les caractères chinois complets fichiers de polices TTF chinois téléchargés sur l\'annuaire static/image/seccode/font/ch/ , Site au hasard en utilisant la police à l\'intérieur du fichier texte comme un code de vérification',//'使用图片作为验证码文字，图片必须包含字符“2346789BCEFGHJKMPQRTVWXY”24 个字符，且必须为 GIF 透明图片、背景透明、前景黑色，黑色为图片的第一个索引色。图片大小不限制，但建议宽度不大于验证码宽度的 1/4，高度不大于验证码高度。制作完毕后在 static/image/seccode/gif/ 下创建一个新的子目录，目录名任意，把制作完毕的 24 个 GIF 图片上传到新子目录下使用图片作为验证码的背景，把制作好的 JPG 图片上传到 static/image/seccode/background/ 目录下，站点将随机使用里面的图片作为验证码的背景使用 TTF 字体作为验证码文字，把下载的 TTF 英文字体文件上传到 static/image/seccode/font/en/ 目录下，站点将随机使用里面的字体文件作为验证码的文字使用中文图片验证码前，需要把包含完整中文汉字的 TTF 中文字体文件上传到 static/image/seccode/font/ch/ 目录下，站点将随机使用里面的字体文件作为验证码的文字',    //    'Authentication code using picture as text, The image must contain the character "2346789BCEFGHJKMPQRTVWXY" 24 characters, And must be a transparent GIF image, Background transparent, Foreground black, Black for the color Image of the first index. Image size limited, But suggested that the width is not greater than the width of the verification code 1/4, height not more than verification code level. After produced in the static/image/seccode/gif/ create a new subdirectory, Directory name any, 24 completed the production of GIF Image uploaded to the new subdirectory using the background Image as a validation code, JPG images to upload to the production of a good static/image/seccode/background/ directory, Site will using a random verification code inside the picture as a background to use TTF font as the verification code words, English TTF fonts to download files uploaded to static/image/seccode/font/en/ directory, Site will randomly using the font inside the text file as a verification code verification code before using Chinese Image, Need to contain the complete Chinese characters Chinese TTF font files uploaded to the static/image/seccode/font/ch/ directory, Site will randomly using the font inside the text file as a verification code',
      3 => 'setting_sec_seccode_status',
      4 => 'Activer le Captcha pour',//'启用验证码', //  'Enable Captcha for',
      5 => 'Enregistrement et code de vérification de spam malveillants malicieux, sélectionnez le code d\'authentification doivent ouvrir l\'opération. Remarque: Activer le code de vérification fera partie de l\'opération devient compliqué, il est recommandé d\'ouvrir uniquement lorsque cela est nécessaire. Carte prépayée code de vérification recommandée a être secrête',//'验证码可以避免恶意注册及恶意灌水，请选择需要打开验证码的操作。注意: 启用验证码会使得部分操作变得繁琐，建议仅在必需时打开。充值卡密验证码建议一直开启',   //    'Registration and verification code from malicious malicious irrigation, select the authentication code need to open the operation. Note: Enabling verification code will make the part of the operation becomes complicated, it is recommended to open only when necessary. Prepaid card verification code recommended has been open secret',
      6 => 'setting_sec_seccode_minposts',
      7 => 'Nombres de post à éviter le Captcha',//'验证码发帖限制',//  'Post number for avoid Captcha', 
      8 => 'Nombre de messages l\'Utilisateur doit disposer pour supprimer le code de vérification. 0 signifie que TOUT les membres doivent entrer un code.',//'发帖数超过此设置的会员将不受验证码功能限制，0 表示所有会员均受限制',//  'Number of posts the user must have for remove the verification code. 0 means that ALL members must enter a code.',
      9 => 'setting_sec_seccode_type',
      10 => 'Type de Captcha ',//'验证码类型',//  'Captcha type',
      11 => 'Définir le type de code captcha. Code Chinois de vérification d\'image nécessite le soutien de l\'hôte de la bibliothèque FreeType. Pour afficher le code de vérification flash, nous recommandons que votre hébergeur supporte la bibliothèque Ming pour améliorer la sécurité.',//'设置验证码的类型。中文图片验证码需要你的主机支持 FreeType 库。要显示 Flash 验证码，建议你的主机支持 Ming 库以提高安全性验证码预览',  // 'Set the type of captcha code. Chinese image verification code require for the host support of FreeType library. To display the Flash verification code, we recommend that your host support the Ming library to improve the security.', 
      12 => 'setting_sec_seccode_width',
      13 => 'Captcha Largeur de l\'image',//'验证码图片宽度',//  'Captcha Image width', 
      14 => 'Définir une largeur de l\'image de code de vérification de gamme de 100 - 200 px.',//'验证码图片的宽度，范围在 100～200 之间',//   'Set a width of verification code image in range of 100 - 200 px.',
      15 => 'setting_sec_seccode_height',
      16 => 'Captcha hauteur de l\'image',//'验证码图片高度',//  'Captcha Image height',
      17 => 'Réglez la hauteur de l\'image du captcha dans la gamme de 50 - 80 px.',//'验证码图片的高度，范围在 30～80 之间',//  'Set a height of captcha image in range of 50 - 80 px.', 
      18 => 'setting_sec_seccode_scatter',
      19 => 'Diffusion de l\'image',//'图片打散',//  'Image scatter',
      20 => 'Entrez le diffusion (rompre) au niveau de l\'image du captcha en résulte. Réglez 0 pour ne pas utiliser la diffusion.',//'打散生成的验证码图片，输入打散的级别，0 为不打散',//  'Enter the scatter (break up) level for the resulting captcha image. Set 0 for not use scatter.',
      21 => 'setting_sec_seccode_background',
      22 => 'Image de fond aléatoire',//'随机图片背景',//   'Random background image',
      23 => 'Choisissez "Oui" à utiliser l\'image JPG hasard static/image/seccode/background/ répertoire comme fond d\'écran pour le captcha. choisir "Non" pour utiliser une couleur de fond aléatoire.',//'选择“是”将随机使用 static/image/seccode/background/ 目录下的 JPG 图片作为验证码的背景图片，选择“否”将使用随机的背景色',  //  'Select "Yes" to use random JPG image from static/image/seccode/background/ directory as a background for the captcha. Select "No" for use a random background color.', 
      24 => 'setting_sec_seccode_adulterate',
      25 => 'Graphiques Fond aléatoire',//'随机背景图形',//   'Random background graphics',
      26 => 'Choisissez "Oui" pour augmenter au hasard  graphiquement un bruit venant captcha de fond.',//'选择“是”将给验证码背景增加随机的图形',// 'Select "Yes" for increase a random graphical noice at the captcha background.', 
      27 => 'setting_sec_seccode_ttf',
      28 => 'Polices TTF aléatoires',//'随机 TTF 字体',//  'Random TTF fonts',
      29 => 'Choisissez "Oui" pour une utilisation police TTF hasard de l\'annuaire static/image/seccode/font/fr/. Choisir "Non" pour un usage polices TTF aléatoires de l\'annuaire static/image/seccode/gif/. Image de la Chine captcha utilisera police TTF hasard static/image/seccode/font/ch/ répertoire sans avoir besoin de ce paramètre.',//'选择“是”将随机使用 static/image/seccode/font/en/ 目录下的 TTF 字体文件生成验证码文字，选择“否”将随机使用 static/image/seccode/gif/ 目录中的 GIF 图片生成验证码文字。中文图片验证码将随机使用 static/image/seccode/font/ch/ 目录下的 TTF 字体文件，无需进行此设置',     //   'Select "Yes" for use random TTF font from static/image/seccode/font/en/ directory. Select "No" for use random TTF fonts from static/image/seccode/gif/ directory. Chinese image vaptcha will use random TTF font from static/image/seccode/font/ch/ directory without the need for this setting.',
      30 => 'setting_sec_seccode_angle',
      31 => 'Angle aléatoire',//'随机倾斜度',  //   'Random angle',
      32 => 'Choisissez "Oui" pour utiliser une inclinaison de texte aléatoire pour le code captcha. Ce paramètre prend effet uniquement pour les polices TTF.',//'选择“是”将给验证码文字增加随机的倾斜度，本设置只针对 TTF 字体的验证码', //  'Select "Yes" for use a random text inclination for the captcha code. This setting take effect only for TTF fonts.',
      33 => 'setting_sec_seccode_warping',
      34 => 'Random distortion',//'随机扭曲', //  'Random distortion',
      35 => 'Choisissez "Oui" pour utiliser une distorsion aléatoire (twist) dans le texte captcha, efficace uniquement pour les polices TTF.',//'选择“是”将给验证码文字增加随机的扭曲，本设置只针对 TTF 字体的验证码', // 
      36 => 'setting_sec_seccode_color',
      37 => 'Couleur aléatoire',//'随机颜色', //   'Random color',
      38 => 'Choisissez "Oui" pour utiliser du texte aléatoire et la couleur d\'arrière-plan pour le code captcha',//'选择“是”将给验证码的背景图形和文字增加随机的颜色',  //   'Select "Yes" for use random text and background color for the captcha code',
      39 => 'setting_sec_seccode_size',
      40 => 'Taille aléatoire',//'随机大小', //  'Random size',
      41 => 'Choisissez "Oui" pour utiliser une taille de texte aléatoire dans le code captcha',//'选择“是”验证码文字的大小随机显示', //  'Select "Yes" for use a random text size in captcha code',
      42 => 'setting_sec_seccode_shadow',
      43 => 'Text shadow',//'文字阴影', //  'Text shadow',
      44 => 'Choisissez "Oui" pour utiliser un effet d\'ombre dans le code captcha',//'选择“是”将给验证码文字增加阴影', // 'Select "Yes" for use a shadow effect in captcha code', 
      45 => 'setting_sec_seccode_animator',
      46 => 'Animation GIF',//'GIF 动画', //  'GIF animation',
      47 => 'Choisissez "Oui" pour une utilisation GIF animé dans le code captcha, Choisir "Non" pour utiliser un mode d\'image statique',//'选择“是”验证码将显示成 GIF 动画方式，选择“否”验证码将显示成静态图片方式',  //   'Select "Yes" for use animated GIF in the captcha code, select "No" for use a static image mode',
    ),
  ),
  274 =>
  array (
    'index' =>
    array (
      'Réglages Anti-spam' => 'action=setting&operation=sec',//'防灌水设置',  //  'Anti-spam settings'
      'Sécurité Q & R' => 'action=setting&operation=sec&anchor=secqaa',//'验证问答设置', // 'Security Q & A'  
    ),
    'text' =>
    array (
      0 => 'Réglages Anti-spam &raquo; Sécurité Q & R',//'防灌水设置 &raquo; 验证问答设置', //  'Anti-spam settings &raquo; Security Q & A',
      1 => 'setting_sec_qaa_tips',
      2 => 'Les questions de sécurité doivent être concises et sans ambiguïté, afin que les gens normale peuvent répondre correctement. Svp. veuillez vérifier et mettre à jour Questions et réponses de sécurité régulièrement pour éviter la spéculation! Pour installer une nouvelle authentification Q & R script, il est nécessaire pour vérifier et télécharger le Q & R script pour la source / class / secqaa / répertoire, vous pouvez utiliser la liste suivante pour sélectionner. Avant de concevoir de nouveaux authentification Q & A script plug-in développeurs sûr de lire le contenu "Discuz! Bibliothèque technique".',//'验证问题应该言简意赅，没有歧义，正常人都能够正确作答。请经常更新验证问答的问题及答案以防止被猜测！安装新的验证问答脚本，需将验证问答脚本程序上传到 source/class/secqaa/ 目录，然后即可在以下列表中选择使用了。插件开发人员在设计新的验证问答脚本前请务必仔细阅读《Discuz! 技术文库》中的内容。', //   'Sequrity questions should be concise, unambiguous, so normal people can answer correctly. Please verify and update Security Questions and Answers regularly to prevent speculation! To install a new authentication Q&A script it is required to verify and upload the Q&A script to the source/class/secqaa/directory, then you can use the following list for select. Before designing of new authentication Q&A script  the plug-in developers sure to read the "Discuz! Technical Library" content.',
      3 => 'setting_sec_secqaa',
      4 => 'Sécurité Q & R',//'验证问答设置',  //  'Security Q & A',
      5 => 'Questions et réponses de sécurité lors de l\'enregistrement ou l\'affichage de validation pour éviter les effets malicieux en raison de plus grande difficulté. Recommandé de régler plus de 10 questions et réponses. Note: Le code HTML pris en charge, la réponse ne doit pas être plus long que 50 caractères.',//'建议你设置 10 个以上验证问题及答案，验证问题越多，验证问答防止恶意注册或发布信息的效果越明显。问题支持 HTML 代码，答案长度不超过 50 字节', //   'Security questions and answers when registering or posting enable to prevent malicious effects because of more significant difficulty. Recommended to set more than 10 questions and answers. Note: HTML code supported, the answer mus be not longer than 50 characters.',
      6 => 'setting_sec_secqaa_status',
      7 => 'Activer la sécurité Q & R',//'启用验证问答',  //  'Enable security Q & A',
      8 => 'S\'il est activé, le système nécessitera membres pour la réponse correcte à la question de la sécurité, choisis au hasard pour poursuivre l\'opération et éviter l\'enregistrement de mauvaise foi ou de l\'édition de poste. Pour compléter l\'opération, il est nécessaire de sélectionner la question et réponse de sécurité. Remarque: L\'activation de cette fonction va augmenter la difficulté de l\'opération. Recommandé pour activer uniquement lorsque cela est nécessaire.',//'验证问答功能要求会员必须正确回答系统随机抽取的问题才能继续操作，可以避免恶意注册或发布信息，请选择需要打开验证问答的操作。注意: 启用该功能会使得部分操作变得繁琐，建议仅在必需时打开', //  'If enabled, the system will require members for correct answer for security question, randomly selected to continue operation and avoid bad faith registration or post publishing. For complete the operation it is required to select the security question and answer. Note: Enabling this feature will increase the operation difficulty. Recommended to enable only when necessary.',
      9 => 'setting_sec_secqaa_minposts',
      10 => 'Posts limite pour désactiver Q & R',//'验证问答发帖限制', //  'Posts limit for disable Q & A',
      11 => 'Ne pas utiliser la sécurité Q & R pour les membres affichés plus de fois que cette valeur. Mettre à 0 pour l\'affichage Q & R de tous les membres.',//'发帖数超过此设置的会员将不受验证问答功能限制，0 表示所有会员均受限制', //   'Do not use sequirity Q & A for members posted more times than this value. Set to 0 for show Q & A from ALL members.',
      12 => 'setting_sec_secqaa_qaa', 
      13 => 'Définir Questions et Réponses',//'验证问题及答案设置', //  'Set questions and answers',
    ),
  ),
  275 =>
  array (
    'index' =>
    array (
      'Réglages Anti-spam' => 'action=setting&operation=sec',//'防灌水设置',    // 'Anti-spam settings' 
      'Définissez le nom de la fiche d\'inscription' => 'action=setting&operation=sec&anchor=reginput',//'注册表单名称设置',   //   'Set the name of the registration form' 
    ),
    'text' =>
    array (
      0 => 'Réglages Anti-spam &raquo; Définissez le nom de la fiche d\'inscription',//'防灌水设置 &raquo; 注册表单名称设置',   //  'Anti-spam settings &raquo; Set the name of the registration form',
      1 => 'setting_sec_reginput',
      2 => 'Définissez le nom de la fiche d\'inscription',//'注册表单名称设置',   // 'Set the name of the registration form', 
      3 => 'setting_sec_reginput_username',
      4 => 'Paramètres du formulaire de nom d\'utilisateur',//'用户名表单设置',   //  'User name form settings',
      5 => 'Par défaut nom d\'utilisateur seule une combinaison de lettres et de chiffres doit commencer par une lettre. Nom du formulaire de modification, il sera plus difficile de jouer un rôle dans la RI',//'默认为 username ，只能是字母和数字的组合，必须以字母开头。修改后的表单名会使得注册机更难发挥作用',  //  'Default username, only a combination of letters and numbers must begin with a letter. Modified form name will make it more difficult to play a role in RI',
      6 => 'setting_sec_reginput_password',
      7 => 'Paramètres du formulaire de mot de passe',//'密码表单设置',   //  'Password form settings', 
      8 => 'Mot de passe par défaut, seule une combinaison de lettres et de chiffres doit commencer par une lettre. Nom du formulaire de modification, il sera plus difficile de jouer un rôle dans la RI',//'默认为 password ，只能是字母和数字的组合，必须以字母开头。 修改后的表单名会使得注册机更难发挥作用', //   'Default password, only a combination of letters and numbers must begin with a letter. Modified form name will make it more difficult to play a role in RI',
      9 => 'setting_sec_reginput_password2',
      10 => 'Répétez les paramètres de formulaire du mot de passe',//'重复密码表单设置',   //'Repeat password form settings',  
      11 => 'Par défaut password2, seule une combinaison de lettres et de chiffres doit commencer par une lettre. Nom du formulaire de modification, il sera plus difficile de jouer un rôle dans la RI',//'默认为 password2 ，只能是字母和数字的组合，必须以字母开头。 修改后的表单名会使得注册机更难发挥作用', //   'Default password2, only a combination of letters and numbers must begin with a letter. Modified form name will make it more difficult to play a role in RI',
      12 => 'setting_sec_reginput_email',
      13 => 'Paramètre Format E-mail ',//'Email表单设置',   //  'Email Form setting',
      14 => 'L\'E-mail par défaut, seule une combinaison de lettres et de chiffres doit commencer par une lettre. Nom du formulaire de modification, il sera plus difficile de jouer un rôle dans la RI',//'默认为 email ，只能是字母和数字的组合，必须以字母开头。 修改后的表单名会使得注册机更难发挥作用', //   'Default email, only a combination of letters and numbers must begin with a letter. Modified form name will make it more difficult to play a role in RI',
    ),
  ),
  276 =>
  array (
    'index' =>
    array (
      'Réglages Anti-spam' => 'action=setting&operation=sec',//'防灌水设置'   //   'Anti-spam settings'
      'Nom Formulaire d\'inscription' => 'action=setting&operation=sec&anchor=postperiodtime',//'注册表单名称设置'   // 'Registration form name' 
    ),
    'text' =>
    array (
      0 => 'Réglages Anti-spam &raquo; Définir le nom du Formulaire d\'inscription',//'防灌水设置 &raquo; 注册表单名称设置',   //   'Anti-spam settings &raquo; Set the name of the registration form',
      1 => 'setting_sec_postperiodtime',
      2 => 'Post limitation horaire',//'发帖时间段限制',   //  'Post time restriction',
      3 => 'setting_datetime_postbanperiods',
      4 => 'R/O périodes,',//'禁止发帖时间段',   //   'R/O periods,',
      5 => 'La période du jour lorsque les utilisateurs ne peuvent pas envoyer des messages. Svp. veuillez utilisez le format de 24 heures, une période de temps par ligne. La période de temps peut traverser la nuit. Laissez ce champ vide pour aucune limite. Par exemple: 23:25-5:05 signifie période quotidienne de 23:25 à 05:05 du lendemain; 9:00-14:30 désigne une période de 09:00 à 2:30PM. Note: Format non valide peut provoquer des problèmes inattendus! Si cette limitation est fixé à un groupe d\'utilisateurs, les membres du groupe affectations seront désactivés pour cette période. Tous les paramètres de temps doivent utiliser le système de site de fuseau horaire par défaut.',//'每天该时间段内用户不能发帖，请使用 24 小时时段格式，每个时间段一行，如需要也可跨越零点，留空为不限制。例如:每日晚 11:25 到次日早 5:05 可设置为: 23:25-5:05每日早 9:00 到当日下午 2:30 可设置为: 9:00-14:30注意: 格式不正确将可能导致意想不到的问题，用户组中如开启“不受时间段限制”的选项，则该组可不被任何时间段设置约束。所有时间段设置均以站点系统默认时区为准，不受用户自定义时区的影响',   //   'The period of day time when users can not post messages. Please use the 24-hour period format, one time period per line. The time period can cross the midnight. Leave blank for no limit. For example: 23:25-5:05 means daily period from 23:25 to 05:05 of the next morning; 9:00-14:30 means a period from 09:00 to 2:30PM. Note: Invalid format may cause to unexpected problems! If this restriction is set to a user group, the group members postings will be disabled for this time period. All time settings must use the site system default time zone.',
      6 => 'setting_datetime_postmodperiods',
      7 => 'Délai de Premoderation',//'发帖审核时间段',   //  'Premoderation period',
      8 => 'Toutes les offres faites dans cette période ne sera pas montré jusqu\'à modérateurs/administrateurs approuver manuellement l\'affichage.',//'每天该时间段内用户发帖不直接显示，需经版主或管理员人工审核才能发表，格式和用法同上',   //   'Any postings made in this period will not shown until moderators/administrators manually approve the posting.',
      9 => 'setting_datetime_postignorearea',
      10 => 'Aucune périodes de publication',//'发帖不受时间限制的地区列表',   // 'No posting time periods', 
      11 => 'Lorsque l\'utilisateur se trouve dans la liste d\'adresses, il peut soumettre gratuitement sans de contraintes de temps. Une région par ligne. Par exemple: Tels que "Beijing" (sans les guillemets). Blanc gauche pour ne pas réglée. Note: Pour détecter correctement l\'emplacement d\'adresse IP, veuillez télécharger le fichier qqwry.dat de base de données d\'adresses IP et de le transférer au dossier "data/ipdata/", puis renommé le fichier à "wry.dat", et retirer le fichier "tinyipdata.dat"',//'当用户处于本列表中的地址时，发帖不受时间段限制。每个地区一行，例如 "北京"(不含引号)，留空为不设置。注意：如要正确无误的判断您 IP 地址所在的地区，请到网上下载 qqwry.dat IP 地址库文件上传到 "data/ipdata/" 目录下更名为 wry.dat，同时删除 tinyipdata.dat 文件',   //   'When the user is in the address list, he can post free withou of time restrictions. One region per line. For example: Such as "Beijing" (without quotes). Left blank to not set. Note: To properly detect the IP address location please download the IP address database qqwry.dat file and upload it to the "data/ipdata/" folder, then renamed the file to "wry.dat", and remove the "tinyipdata.dat" file',
      12 => 'setting_datetime_postignoreip',
      13 => 'IP ignoré les restrictions Aucun temps de postage',//'发帖不受时间限制的IP列表',   //  'IP ignored the No Posting time restrictions',
      14 => 'Lorsque une adresse IP de l\'utilisateur est dans cette liste, la limitation d\'affichage n\'est pas appliqué. Une IP par ligne, une adresse complète ou au début de la plage IP, par exemple "192.168." (sans les guillemets) correspond à toutes les adresses IP dans la plage 192.168.0.0 ~ de 192.168.255.255. Laissez vide pour ne pas définir.',//'当用户处于本列表中的 IP 地址时，发帖不受时间段限制。每个 IP 一行，既可输入完整地址，也可只输入 IP 开头，例如 "192.168."(不含引号) 可匹配 192.168.0.0～192.168.255.255 范围内的所有地址，留空为不设置',   //  'When a user IP address is in this list, the posting time restriction is not applied. One IP per line, a full address or beginning of IP range, For example "192.168." (without quotes) matches all IP adresses in range of 192.168.0.0~192.168.255.255. Leave blank to not set.',
    ),
  ),
  277 =>
  array (
    'index' =>
    array (
      'Réglage Horaire' => 'action=setting&operation=datetime',//'时间设置',   //  'Time settings'
    ),
    'text' =>
    array (
      0 => 'Réglage Horaire',//'时间设置',   //   'Time settings',
      1 => 'setting_datetime_format',
      2 => 'Format de la date et de l\'heure',//'日期和时间格式',   //  'Date and time format',
      3 => 'setting_datetime_dateformat',
      4 => 'Default date format',//'默认日期格式',   //   'Default date format',
      5 => 'Utilisez aaaa ou aa pour l\'année, mm le mois, dd pour le jour. À savoir aaaa-mm-jj s\'affichera 2000-12-31',//'使用 yyyy(yy) 表示年，mm 表示月，dd 表示天。如 yyyy-mm-dd 表示 2000-1-1',   //   'Use yyyy or yy for the year, mm for the month, dd for the day. I.e. yyyy-mm-dd will display 2000-12-31',
      6 => 'setting_datetime_timeformat',
      7 => 'Format Heure par défaut',//'默认时间格式',   // 'Default time format', 
      8 => 'setting_datetime_dateconvert',
      9 => 'Format de l\'heure humaines',//'人性化时间格式',   //   'Human Time Format',
      10 => 'Choisissez "Oui" pour le temps d\'affichage comme "n il y a minutes", "Yesterday", "n il y a jours", etc.',//'选择“是”，站点中的时间将显示以“n分钟前”、“昨天”、“n天前”等形式显示',   //'Select "Yes" for display time like "n minutes ago", "Yesterday", "n days ago", etc.',  
      11 => 'setting_datetime_timeoffset',
      12 => 'Délai par défaut décalé',//'默认时差',   //  'Default time offset',
      13 => 'Définir décalage par rapport à GMT, heure locale (fuseau horaire)',//'当地时间与 GMT 的时差',   //   'Set your local time offset from GMT (time zone)',
      14 => 'setting_datetime_periods',
      15 => 'Réglages de la période de temps',//'时间段设置',   //  'Time period settings',
      16 => 'setting_datetime_visitbanperiods',
      17 => 'Désactiver le temps d\'accès',//'禁止访问时间段',   //  'Disable access time',
      18 => 'Définir un certain temps la période de jour lorsque les utilisateurs ne peuvent pas accéder au site. Utilisez le format horaire de 24 heures, une période de temps par ligne. si nécessaire, une fois peut traverser zéro (minuit). Laissez vide pour ne pas limiter. Par exemple: <br /> nuit près du minuit: <i>23:25-05:05</i><br />Quotidiennement 21 heures 00-2h30: <i>09:00-14:30</i><br />Remarque: Format d\'heure incorrecte peut provoquer des problèmes inattendus! À savoir Les groupes d\'utilisateurs avec l\'option "limite de temps et sans" peut avoir une des contraintes d\'accès, de temps en temps. Tous les réglages de l\'heure doivent être réglées à l\'aide du fuseau horaire par défaut du système.',//'每天该时间段内用户不能访问站点，请使用 24 小时时段格式，每个时间段一行，如需要也可跨越零点，留空为不限制。例如:每日晚 11:25 到次日早 5:05 可设置为: 23:25-5:05每日早 9:00 到当日下午 2:30 可设置为: 9:00-14:30注意: 格式不正确将可能导致意想不到的问题，用户组中如开启“不受时间段限制”的选项，则该组可不被任何时间段设置约束。所有时间段设置均以站点系统默认时区为准，不受用户自定义时区的影响',   //   'Set some time of day period when users can not access to the site. Use the 24-hours time format, one time period per line. if necessary, a time can cross zero (midnight). Leave blank to not limit. For example:<br />Nightly near the midnight: <i>23:25-05:05</i><br />Daily from 9:00 pm to 2:30: <i>09:00-14:30</i><br />Note: incorrect time format may cause some unexpected problems! I.e. User groups with "without time limit" option may have an access constraints from time to time. All the time settings must be set using the system default time zone.',
      19 => 'setting_datetime_ban_downtime',
      20 => 'Aucune période de téléchargements',//'禁止下载附件时间段',   //   'No downloads period',
      21 => 'Définir une période de temps lorsque les utilisateurs ne peuvent pas télécharger des pièces jointes',//'每天该时间段内用户不能下载附件，格式和用法同上',   //  'Set a time period when users can not download attachments',
      22 => 'setting_datetime_searchbanperiods',
      23 => 'Aucune période de recherche',//'禁止全文搜索时间段',   //   'No search period',
      24 => 'Définir une période de temps lorsque les utilisateurs ne peuvent pas utiliser la recherche en texte intégral.',//'每天该时间段内用户不能使用全文搜索，格式和用法同上',   //  'Set a time period when users can not use the full-text search.',
      25 => 'setting_attach_basic_dir',
      26 => 'Répertoire des pièces jointes',//'本地附件保存位置',   //   'Attachments Directory',
      27 => 'Définissez le chemin du serveur absolue sans se terminant slash (barre oblique) ("/"). Il doit avoir les attributs 777 et doit être accessible par Internet.',//'服务器路径，属性 777，必须为 web 可访问到的目录，结尾不加 "/"，相对目录务必以 "./" 开头',   //  
      28 => 'setting_attach_basic_url',
      29 => 'URL Pièces jointes',//'本地附件 URL 地址',   //   'Attachments URL',
      30 => 'Entrez le chemin relatif à la racine du serveur web, ou une URL absolue à partir de http://, sans terminant "/".',//'可为当前 URL 下的相对地址或 http:// 开头的绝对地址，结尾不加 "/"，不能把这个设为远程附件URL地址',   //  
      31 => 'setting_attach_image_lib',
      32 => 'Bibliothèque de traitement de l\'image',//'图片处理库类型',   //   'Image processing library',
      33 => 'Sélectionnez une bibliothèque pour gérer les vignettes et les l\'image en filigrane. GD est la bibliothèque la plus largement utilisée, mais nécessite plus de ressources. ImageMagick manger moins de ressources système, mais nécessite l\'exécution des instructions de ligne de commande. Si vous souhaitez utiliser ce programme, s\'il vous plaît aller à <a href="http://www.imagemagick.org" target="_blank">http://www.imagemagick.org</a> pour télécharger et suivez les instructions d\'installation.',//'请选择 Discuz! 用来处理缩略图和水印的图像处理库。GD 是最广泛的处理库但是使用的系统资源较多。ImageMagick 速度快系统资源占用少，但需要服务器有执行命令行命令的权限。如果你的服务器有条件安装此程序，请到 http://www.imagemagick.org 下载，安装后在下面指定安装的路径',   //  
      34 => 'setting_attach_image_impath',
      35 => 'ImageMagick installation trajectoire',//'ImageMagick 程序安装路径',   //   'ImageMagick installed path',
      36 => 'ImageMagick 6 chemin d\'installation. Si le système d\'exploitation du serveur est Windows, trajectoire N\'utilisez pas de noms de fichiers longs',//'ImageMagick 6 程序的安装路径。如果服务器的操作系统为 Windows，路径不要使用长文件名',   //  
      37 => 'setting_attach_image_thumbquality',
      38 => 'La qualité des miniatures',//'缩略图质量',  //  'Thumbnail quality',
      39 => 'Définir une qualité de l\'image miniature dans la gamme de 0 - 100 (valeur entière). Agrandir qualité donne un meilleur résultat, mais exigent une plus grande taille d\'image.',//'设置图片附件缩略图的质量参数，范围为 0～100 的整数，数值越大结果图片效果越好，但尺寸也越大',  //  Set a quality of the thumbnail image in range of 0 - 100 (integer value). Larger quality give a better result, but require greater image size.
      40 => 'setting_attach_image_disabledmobile',
      41 => 'Que ce soit pour générer une version mobile de la miniature',  //  '是否生成手机版缩略图',    //  
      42 => 'Choisissez si vous voulez générer la version mobile de chaque pièce jointe miniaturisée',  //  '设置是否为每个附件生成手机版的缩略图',  //  
      43 => 'setting_attach_image_preview',   
      44 => 'Extrait',//'预览',  //  'Preview',
      45 => 'Pas besoin d\'enregistrer les paramètres de prévisualisation',//'无需保存设置即可预览',  //  'No need to save the settings to preview',
    ),
  ),
  278 =>
  array (
    'index' =>
    array (
      'Réglages des pièces jointes' => 'action=setting&operation=attach',//'上传设置',  //   'Attachment Settings'
      'Pièces jointes Forum' => 'action=setting&operation=attach&anchor=forumattach',//'论坛附件',  //   'Forum attachments'
    ),
    'text' =>
    array (
      0 => 'Réglages des pièces jointes &raquo; Pièces jointes Forum',//'上传设置 &raquo; 论坛附件',  //  'Attachment Settings &raquo; Forum attachments',
      1 => 'setting_attach_basic_imgpost',
      2 => 'Afficher les images jointes dans un post',//'帖子中显示图片附件',  //  'Display attached images in a post',
      3 => 'Voir les images des posts ci-joints directement sans avoir besoin de cliquer sur le lien de pièce jointe',//'在帖子中直接将图片或动画附件显示出来，而不需要点击附件链接',  //   'Show the post attached images directly without the need to click the attachment link',
      4 => 'setting_attach_basic_allowattachurl',
      5 => 'Activer URL de pièce jointe pour lecteur multimédia',//'附件 URL 地址、媒体附件播放',  //  'Enable attachment URL for media player', 
      6 => 'Autoriser l\'utilisation des références de pièce jointe en forme "attach://aid" dans les posts. Soutenir cette forme permet d\'utiliser un service de médias de radiodiffusion directe.',//'开启附件 URL 地址后，上传附件的地址可通过 "attach://aid" 方式在任何帖内引用，并支持媒体类附件的直接播放，此项设置需要在 用户组 - 帖子相关 中允许用户组使用多媒体代码方可生效',  //  'Allow to use attachment reference in form "attach://aid" within posts. Supporting this form enable to use a direct broadcast media services.',
      7 => 'setting_attach_image_thumbstatus',
      8 => 'Réglages miniatures de post pour les pièces jointes',//'论坛帖子附件缩略图设置',  //  'Thumbnail settings for post attachments',
      9 => 'Définir un mode pour Afficher les images: de créer une image en miniature ou rétrécir l\'image originale à la taille spécifiée. Supportés JPG / PNG / GIF. Format GIF animé n\'est pas supporté.',//'你可以设置自动为用户上传的 JPG/PNG/GIF 图片附件添加缩略图或将图片附件缩到指定的大小。不支持动画 GIF 格式',  //   'Set a method for show images: create a thumblail image or shrink the original image to specified size. Supported JPG/PNG/GIF images. Animated GIF format is not supported.',
      10 => 'setting_attach_image_thumbwidthheight',
      11 => 'Taille de la miniature',//'缩略图大小',  //   'Thumbnail size',
      12 => 'Définir une taille de la vignette. J\'ai une image qui est inférieure à la taille, la vignette ne sera pas générée.',//'设置缩略图的大小，小于此尺寸的图片附件将不生成缩略图',  //  'Set a thumbnail size. I an image is smaller than the size, the thumbnail will not generated.',
      13 => 'setting_attach_basic_thumbsource',
      14 => 'Orientation du redimensionnement de l\'image d\'origine',//'是否直接缩放原图',  //  'Direct resize original image',
      15 => 'Choisissez "Oui" pour redimensionner l\'image originale, sans générer une vignette',//'选择“是”，将直接缩放原图，而不生成缩略图文件',  //  'Select "Yes" for resize the original image, without generating a thumbnail file',
      16 => 'setting_attach_image_thumbsourcewidthheight',
      17 => 'Image originale taille du zoom',//'原图缩放大小',  //   'Original image zoom size',
      18 => 'Définissez la taille de l\'image originale. Si jointe l\'image largeur /taille est supérieure à cette taille, l\'image sera automatiquement redimensionnée à cette taille (création de la miniature sera créée).',//'设置原图的大小，所有大于此尺寸的图片附件将缩小到指定大小', //   'Set the size of original image. If attached image width/height is greater than this size, the image will be resized automatically to this size (thumbnail created).',
      19 => 'setting_attach_antileech_expire',
      20 => 'Pièce jointe liens expiration',//'附件链接有效期',  //  'Attachment links expiration',
      21 => 'Unité: heures. Mettre à 0 ou vide pour des liens de pièce jointe  fixes. Liens périmés sont mis à jour automatiquement. Cette fonction permet d\'éviter le leech ou les sangsues de pièce jointe et des téléchargements en vrac, mais il va causer des désagréments pour les membres habituelles au téléchargement.',//'单位：小时，0 或留空表示附件链接永久有效，过期后链接自动更新。本功能可有效防止附件被盗链或附件被软件批量下载，但是会给会员的正常下载带来不便', //  'Unit: hours. Set to 0 or blank for permanent attachment links. Expired links are updated automatically. This feature can prevent the attachment leeching and bulk downloads, but it will cause inconvenience for normal members to download.',
      22 => 'setting_attach_antileech_refcheck',
      23 => 'Vérifiez le référenceur de la pièce jointe',//'下载附件来路检查',  //   'Check the attachment referer',
      24 => 'Choisissez "Oui" pour vérifier un référenceur lorsque le téléchargement des pièces jointes provenant d\'autres sites ou des sites interdits pour les téléchargements. Remarque: Cette fonction s\'affichera un message "sur pièce jointe de  l\'image", et augmentera la charge du serveur.',//'选择“是”将检查下载附件的来路，来自其他网站或站点的下载请求将被禁止。注意: 本功能在开启“帖子中显示图片附件”时，会加重服务器负担', //   'Select "Yes" for check a referer when download attachments from other sites or sites banned for downloads. Note: This feature will display a "message about image attachment", and will increase the server loading.',
    ),
  ),
  279 =>
  array (
    'index' =>
    array (
      'Réglages des pièces jointes' => 'action=setting&operation=attach',//'上传设置',   //'Attachment Settings'  
      'Pièces jointes distantes' => 'action=setting&operation=attach&anchor=remote',//'远程附件',  //  'Remote attachments'
    ),
    'text' =>
    array (
      0 => 'Réglages des pièces jointes &raquo; Pièces jointes distantes',//'上传设置 &raquo; 远程附件',  //   'Attachment Settings &raquo; Remote attachments',
      1 => 'setting_attach_remote_enabled',
      2 => 'Activer Pièces jointes distantes',//'启用远程附件', //  'Enable Remote Attachments',
      3 => 'setting_attach_remote_enabled_ssl',
      4 => 'Activer les connexions SSL',//'启用 SSL 连接', //  'Enable SSL connections',
      5 => 'Remarque: Le serveur FTP doit prendre en charge le protocole SSL',//'注意：FTP 服务器必需开启了 SSL', //   'Note: FTP server must support the SSL',
      6 => 'setting_attach_remote_ftp_host',
      7 => 'L\'adresse du serveur FTP',//'FTP 服务器地址', //  'FTP server address',
      8 => 'Saisissez l\'adresse IP du serveur FTP ou nom de domaine',//'可以是 FTP 服务器的 IP 地址或域名', //  'Enter the FTP server IP address or domain name',
      9 => 'setting_attach_remote_ftp_port',
      10 => 'Port du serveur FTP',//'FTP 服务器端口', //  'FTP server port', 
      11 => 'Par défaut est 21',//'默认为 21', //   'Default is 21',/
      12 => 'setting_attach_remote_ftp_user',
      13 => 'Compte FTP',//'FTP 帐号', //   'FTP account',
      14 => 'Le compte doit disposer des autorisations suivantes: lire le fichier, écrire des fichiers, supprimer des fichiers, créer des répertoires, sous-répertoires héritent',//'该帐号必需具有以下权限：读取文件、写入文件、删除文件、创建目录、子目录继承', //   'The account must have the following permissions: read the file, write files, delete files, create directories, subdirectories inherit',
      15 => 'setting_attach_remote_ftp_pass',
      16 => 'Mot de passe FTP',//'FTP 密码', //  'FTP password',
      17 => 'Selon les considérations de sécurité, on le verra seulement en premier et au dernier mots de passe FTP, au milieu indique les huit "*".',//'基于安全考虑将只显示 FTP 密码的第一位和最后一位，中间显示八个 * 号',   //    'According to the security considerations, will be shown only the first and the last FTP passwords, in the middle shows the eight "*".',
      18 => 'setting_attach_remote_ftp_pasv',
      19 => 'Connexion en mode passif (pasv)',//'被动模式(pasv)连接',  //  'Passive mode (pasv) connection',
      20 => 'Une connexion en mode normal est utilisé habituellement. Si vous avez des problèmes avec le téléchargement, essayez d\'utiliser ce paramètre.',//'一般情况下非被动模式即可，如果存在上传失败问题，可尝试打开此设置',   //   'A normal mode connection is used usually. If you have problems with uploading, try to use this setting.',
      21 => 'setting_attach_remote_dir',
      22 => 'Répertoire des pièces jointes distantes',//'远程附件目录',  //  'Remote Attachment directory',
      23 => 'Saisissez le chemin absolu ou relatif du répertoire Pièce jointe à distance (à partir du répertoire FTP). N\'ajoutez pas la terminaison slash "/". Répertoire FTP est ".".',//'远程附件目录的绝对路径或相对于 FTP 主目录的相对路径，结尾不要加斜杠“/”，“.”表示 FTP 主目录',  //    'Enter the Remote Attachment directory absolute or relative path (from the FTP home directory). Do not add the ending slash "/". FTP home directory is ".".',
      24 => 'setting_attach_remote_url',
      25 => 'URL d\'accès à distance',//'远程访问 URL', //  'Remote Access URL',
      26 => 'Protocoles HTTP et FTP pris en charge. Ne pas utiliser terminant slash "/". Si vous utilisez le protocole FTP, le serveur FTP doit prendre en charge le mode passif. Pour des raisons de sécurité, ne pas utiliser le compte de connexion FTP avec l\'écriture écrire / liste des autorisations.',//'支持 HTTP 和 FTP 协议，结尾不要加斜杠“/”；如果使用 FTP 协议，FTP 服务器必需支持 PASV 模式，为了安全起见，使用 FTP 连接的帐号不要设置可写权限和列表权限',  //  'HTTP and FTP protocols supported. Do not use ending slash "/". If you use the FTP protocol, the FTP server must support PASV mode. For safety reasons, do not use FTP connection account with set write/list permissions.',
      27 => 'setting_attach_remote_timeout',
      28 => 'FTP délai de transmission',//'FTP 传输超时时间',  //  'FTP transmission timeout',
      29 => 'Unit2: secondEs. Réglez 0 pour utiliser la valeur par défaut du serveur',//'单位：秒，0 为服务器默认',  //  'Unit: seconds. Set 0 for use the server default',
      30 => 'setting_attach_remote_preview',
      31 => 'Test de connexion',//'连接测试',  //  'Connection Test',
      32 => 'Aucun besoin d\'enregistrer les paramètres de test, puis enregistrer le test approuvé',//'无需保存设置即可测试，请在测试通过后再保存',  //   'No need to save the settings to test, and then save the tested',
      33 => 'setting_attach_remote_allowedexts',
      34 => 'Extensions de pièces jointes compatibles',//'允许的附件扩展名',  //  'Enabled attachment extensions',
      35 => 'Autoriser uniquement ces extensions pour les pièces jointes à distance, un par ligne, insensible à la casse. Laissez vide pour aucune limite.',//'只允许这些扩展名结尾的附件使用远程附件功能，每行一个，不区分大小写，留空为不限制',  //   'Allow only these extensions for remote attachments, one per line, case insensitive. Leave blank to no limit.',
      36 => 'setting_attach_remote_disallowedexts',
      37 => 'Désactivés Extensions de pièces jointes',//'禁止的附件扩展名',  //   'Disabled attachment extensions',
      38 => 'Interdire ces extensions pour les pièces jointes à distance, un par ligne, de la casse. Laissez vide pour aucune limite.',//'禁止这些扩展名结尾的附件使用远程附件功能，每行一个，不区分大小写，留空为不限制',  //   'Disllow these extensions for remote attachments, one per line, case insensitive. Leave blank to no limit.',
      39 => 'setting_attach_remote_minsize',
      40 => 'Taille des pièces jointes minimale',//'附件尺寸下限',  //   'Minimal attachment size',
      41 => 'Unité: Ko. Permettre de télécharger des Pièces jointes à distance uniquement si la taille est supérieure à la valeur. Mettre à 0 ou laisser vide pour aucune limite.',//'单位：KB，只有尺寸大于当前设置的附件才使用远程附件功能，0 或留空为不限制',  //  'Unit: KB. Allow to upload remote attachments only if the size is larger than the value. Set to 0 or leave blank to no limit.',
      42 => 'setting_attach_antileech_remote_hide_dir',
      43 => 'Cacher le chemin véritable de la pièce jointe',//'隐藏远程附件真实路径',  // 'Hide the real attachment path',  
      44 => 'Cette option augmente la charge du serveur, et d\'augmenter considérablement le trafic du serveur local. Choisissez si un téléchargement / upload nom de fichier joint doit être caché.',//'选择是，将加重本地服务器负担，并明显增加本地服务器流量；选择否，下载的附件与上传的附件文件名将会不一致',  //   'This option will increase the server loading, and significantly increase the local server traffic. Choose whether a download/upload attachment file name must be hidden.',
    ),
  ),
  280 =>
  array (
    'index' =>
    array (
      'Réglages des pièces jointes' => 'action=setting&operation=attach',//'上传设置',  //   'Attachment Settings'
      'Pièces jointes Espace' => 'action=setting&operation=attach&anchor=albumattach',//'空间附件', // 'Space attachments' 
    ),
    'text' =>
    array (
      0 => 'Réglages des pièces jointes &raquo; Pièces jointes Espace',//'上传设置 &raquo; 空间附件', //  'Attachment Settings &raquo; Space attachments',
      1 => 'setting_attach_album_maxtimage',
      2 => 'Images dimensions maximales',//'图片最大尺寸', //  'Image maximum dimensions',
      3 => 'Cette fonction nécessite le support de PHP GD. Si une image téléchargée a une grande taille, le système affiche l\'image en tant que réduite à ces paramètres. Par exemple, vous pouvez définir la largeur: 1024px, hauteur: 768px. Mais ne peut pas être inférieure à 300px. Mettre à 0 pour ne pas redimensionner.',//'如果用户上传一些尺寸很大的数码图片，则程序会按照本设置进行缩小该图片并显示，比如可以设置为 宽：1024px，高：768px，但都不能小于 300px。设置为 0，则不做任何处理',    //    'This feature requires the PHP GD support. If an uploaded image has a large size, the system will display the image as narrowed to this settings. As an example you can set width: 1024px, height: 768px. But can not be less than 300px. Set to 0 for do not resize.',
    ),
  ),
  281 =>
  array (
    'index' =>
    array (
      'Réglages des pièces jointes' => 'action=setting&operation=attach',//'上传设置',  //  'Attachment Settings'  
      'Portal Pièces jointes' => 'action=setting&operation=attach&anchor=portalarticle',//'门户附件', // 'Portal attachments'  
    ),
    'text' =>
    array (
      0 => 'Réglages des pièces jointes &raquo; Portal Pièces jointes',//'上传设置 &raquo; 门户附件',  //  'Attachment Settings &raquo; Portal attachments',
      1 => 'setting_attach_portal_article_img_thumb_closed',
      2 => 'Activer les vignettes pour les articles',//'开启文章图片缩略图',  //  'Enable thumbnails for articles',
      3 => 'Faire vignettes à partir d\'images d\'articles téléchargés',//'是否开启文章上传图片的缩略图',  // 'Make thumbnails from uploaded article images', 
      4 => 'setting_attach_portal_article_imgsize',
      5 => 'Taille miniature des images de l\'article',//'文章图片缩略图尺寸',  //'Thumbnail size for article images',  
      6 => 'Lorsque vous chargez une grande image, le programme générera une petite image correspondante, la taille par défaut est de 300 * 300px.',//'如果上传一些尺寸很大的数码图片，则程序会按照本设置进行生成相应的小图片，默认大小为 300*300',   //   'When you upload a large image, the program will generate a corresponding small image, the default size is 300*300px.', 
    ),
  ),
  282 =>
  array (
    'index' =>
    array (
      'Filigrane' => 'action=setting&operation=imgwater',//'水印设置',  //  'Watermark'
      'Articles' => 'action=setting&operation=imgwater&anchor=portal',//'文章',  //  'Articles'
    ),
    'text' =>
    array (
      0 => 'Filigrane &raquo; Articles',//'水印设置 &raquo; 文章',  //   'Watermark &raquo; Articles',
      1 => 'setting_imgwater_image_watermarks_portal',
      2 => 'Réglage du filigrane image de l\'article',//'文章图片水印设置', //  'Article image watermark setting',
      3 => 'setting_imgwater_image_watermarkstatus',
      4 => 'Filigrane',//'水印',  //  'Watermark',
      5 => 'Vous pouvez configurer pour ajouter le filigrane automatiquement téléchargé des images JPG / PNG / GIF. Le filigrane peut être placé dans une situation privilégiée (3x3 = 9 postes disponibles). Format GIF animé n\'est pas supporté.',//'你可以设置自动为用户上传的 JPG/PNG/GIF 图片附件添加水印，请在此选择水印添加的位置(3x3 共 9 个位置可选)。不支持动画 GIF 格式',
      6 => 'setting_imgwater_image_watermarkminwidthheight',
      7 => 'Conditions Taille du filigrane',//'水印添加条件',  //  'Watermark size conditions',
      8 => 'Si une image est plus petite que cette taille, le filigrane ne sera pas utiliser. Mettre à 0 pour un filigrane tout le temps.',//'设置水印添加的条件，小于此尺寸的图片附件将不添加水印',  //  'If an image is smaller than this size, the watermark will not used. Set to 0 in order to a watrmark allways.',
      9 => 'setting_imgwater_image_watermarktype',
      10 => 'Filigrane Type d\'image',//'水印图片类型', // 'Watermark image type',
      11 => 'Si vous définissez un filigrane GIF, le fichier static/image/common/watermark.gif sera utilisé comme filigrane. Si vous définissez un filigrane PNG, le filigrane utiliser le fichier static/image/common/watermark.png. Vous pouvez remplacer ces fichiers de filigrane à vos images préférées. Si vous définissez le texte en filigrane, l\'image sera traitée en utilisant la bibliothèque GD, et exiger pour soutenir la bibliothèque FreeType.',//'如果设置 GIF 类型的文件作为水印，水印图片为 static/image/common/watermark.gif，如果设置 PNG 类型的文件作为水印，水印图片为 static/image/common/watermark.png，你可替换水印文件以实现不同的水印效果。如果设置文本类型的水印并且使用 GD 图片处理库，那么还需要 FreeType 库支持才能使用',  // 'If you set a GIF watermark, the file static/image/common/watermark.gif will used as  watermark. If you set a PNG watermark, the watermark will use the static/image/common/watermark.png file. You can replace this watermark files with your preferrd images. If you set the TEXT watermark, the image will processed using the GD library, and require to support the FreeType library.',
      12 => 'setting_imgwater_image_watermarktrans',
      13 => 'Transparence du filigrane',//'水印融合度',  //'Watermark transparency',  
      14 => 'Définir une valeur de transparence du filigrane pour GIF Type filigrane dans la gamme 1 - 100. La plus grande valeur résulte de la transparence du filigrane inférieur. PNG Type filigrane ignorera ce paramètre avec parce que contenant ce paramètre à l\'intérieur du filigrane se déposer. Cette fonctionnalité nécessite d\'activer un type de filigrane valide.',//'设置 GIF 类型水印图片与原始图片的融合度，范围为 1～100 的整数，数值越大水印图片透明度越低。PNG 类型水印本身具有真彩透明效果，无须此设置。本功能需要开启水印功能后才有效',  //  'Set a watermark transparency value for GIF type watermark in range 1 - 100. The greater value results the lower watermark transparency. PNG type watermark will ignore this setting with because containing this parameter inside the watermark file itself. This feature require to enable a valid watermark type.',
      15 => 'setting_imgwater_image_watermarkquality',
      16 => 'JPEG qualité filigrane',//'JPEG 水印质量', 'JPEG watermark quality',
      17 => 'Réglez la qualité d\'image JPEG en résulte après le filigrane appliqué. La plage est de 0 - 100 (valeur entière). La valeur la plus élevée signifie un meilleur résultat, mais de produire une taille d\'image supérieure. Cette fonctionnalité nécessite d\'activer un type de filigrane valide.',//'设置 JPEG 类型的图片附件添加水印后的质量参数，范围为 0～100 的整数，数值越大结果图片效果越好，但尺寸也越大。本功能需要开启水印功能后才有效',  //  'Set the resulting JPEG image quality after watermark applied. The range is 0 - 100 (integer value). The larger value means a better result, but produce a larger image size. This feature require to enable a valid watermark type.',
      18 => 'setting_imgwater_image_watermarktext_text',
      19 => 'Contenu texte en filigrane',//'文本水印文字',    // 'TEXT watermark content',  
      20 => 'Si vous spécifiez une police TrueType pour la police chinoise, vous pouvez écrire un texte en filigrane dans la langue chinoise',//'如果你指定的 TrueType 字体为中文字体文件，那么你可以在文本水印中书写中文',  //  'If you specify a TrueType font for Chinese font, you can write a text watermark in Chinese language',
      21 => 'setting_imgwater_image_watermarktext_fontpath',
      22 => 'Texte en filigrane TrueType nom de fichier de police',//'文本水印 TrueType 字体文件名',  //  'TEXT watermark TrueType font file name',
      23 => 'Entrez le nom du fichier de police TTF situé dans le static/image/seccode/font/ch/ ou le répertoire static/image/seccode/font/fr/. Pour le support des caractères chinois, vous devez utiliser de polices TTF chinois, dont un des caractères chinois complets.',//'填写存放在 static/image/seccode/font/ch/ 或 static/image/seccode/font/en/ 目录下的 TTF 字体文件，支持中文字体。如使用中文 TTF 字体请使用包含完整中文汉字的字体文件',  //  'Enter the TTF font file name located in the static/image/seccode/font/ch/ or static/image/seccode/font/en/ directory. For support Chinese characters you have to use of Chinese TTF fonts including a complete Chinese characters.',
      24 => 'setting_imgwater_image_watermarktext_size',
      25 => 'Texte en filigrane taille de la police',//'文本水印字体大小',  //   'TEXT watermark font size',
      26 => 'Définir la taille du texte de police appropriée pour un filigrane',//'设置文本水印字体大小，请按照字体设置相应的大小',  //  'Set the appropriate text font size for a watermark', 
      27 => 'setting_imgwater_image_watermarktext_angle',
      28 => 'Angle de texte en filigrane',//'文本水印显示角度', //   'TEXT watermark angle',
      29 => 'Définir un angle de rotation du texte à partir de 0 degrés',//'0 度为从左向右阅读文本', //  'Set a text rotation angle starting from 0 degrees',
      30 => 'setting_imgwater_image_watermarktext_color',
      31 => 'Texte en filigrane couleur de police',//'文本水印字体颜色', //   'TEXT watermark font color',
      32 => 'Entrez une couleur de filigrane dans un format hexadécimal, c\'est à dire 99CCFF',//'输入 16 进制颜色代表文本水印字体颜色', //  'Enter a color of watermark in a hexadecimal format, i.e. 99CCFF',
      33 => 'setting_imgwater_image_watermarktext_shadowx',
      34 => 'Texte en filigrane ombre décalage horizontal',//'文本水印阴影横向偏移量', //  'TEXT watermark horizontal shadow offset',
      35 => 'Définir un décalage de l\'ombre d\'un texte en filigrane horizontal.',//'设置文本水印阴影横向偏移量，此数值不宜设置的太大', //  'Set a horizontal offset for the shadow of a text watermark.',
      36 => 'setting_imgwater_image_watermarktext_shadowy',
      37 => 'Texte en filigrane ombre décalage vertical',//'文本水印阴影纵向偏移量', //  'TEXT watermark vertical shadow offset',
      38 => 'Définir un décalage de l\'ombre d\'un filigrane texte vertical.',//'设置文本水印阴影纵向偏移量，此数值不宜设置的太大', //  'Set a vertical offset for the shadow of a text watermark.',
      39 => 'setting_imgwater_image_watermarktext_shadowcolor',
      40 => 'Couleur d\'ombre',//'文本水印阴影颜色', // 'Shadow color', 
      41 => 'Entrez une couleur d\'ombre dans un format hexadécimal, c\'est à dire 99CCFF',//'输入 16 进制颜色代表文本水印阴影字体颜色', //   'Enter a shadow color in a hexadecimal format, i.e. 99CCFF',
      42 => 'setting_imgwater_image_watermarktext_imtranslatex',
      43 => 'Texte en filigrane décalage horizontal (ImageMagick)',//'文本水印横向偏移量(ImageMagick)', //  'TEXT watermark horizontal offset (ImageMagick)',
      44 => 'Réglez le décalage horizontal de filigrane. Ce paramètre s\'applique uniquement pour la bibliothèque ImageMagick.',//'设置水印文本输出后向屏幕中央的横向的偏移值。本设置只适用于 ImageMagick 图片处理库',  //   'Set the watermark horizontal offset. This settingapplies  only for ImageMagick library.',
      45 => 'setting_imgwater_image_watermarktext_imtranslatey',
      46 => 'Texte en filigrane décalage vertical (ImageMagick)',//'文本水印纵向偏移量(ImageMagick)',    //     'TEXT watermark vertical offset (ImageMagick)',
      47 => 'Réglez le décalage vertical de filigrane. Ce paramètre s\'applique uniquement pour la bibliothèque ImageMagick.',//'设置水印文本输出后向屏幕中央的纵向的偏移值。本设置只适用于 ImageMagick 图片处理库', // 'Set the watermark vertical offset. This settingapplies  only for ImageMagick library.',
      48 => 'setting_imgwater_image_watermarktext_imskewx',
      49 => 'TEXT watermark horizontal angle (ImageMagick)',//'文本水印横向倾斜角度(ImageMagick)', //  'TEXT watermark horizontal angle (ImageMagick)',
      50 => 'Réglez l\'angle horizontal de filigrane. Ce paramètre s\'applique uniquement pour la bibliothèque ImageMagick.',//'设置水印文本横向的倾斜角度。本设置只适用于 ImageMagick 图片处理库', //   'Set the watermark horizontal angle. This settingapplies  only for ImageMagick library.',
      51 => 'setting_imgwater_image_watermarktext_imskewy',
      52 => 'Texte en filigrane inclinaison verticale (ImageMagick)',//'文本水印纵向倾斜角度(ImageMagick)', //    'TEXT watermark vertical tilt (ImageMagick)',
      53 => 'Réglez l\'angle horizontal du filigrane. Ce paramètre s\'applique uniquement pour la bibliothèque ImageMagick.',//'设置水印文本纵向的倾斜角度。本设置只适用于 ImageMagick 图片处理库', //  'Set the watermark horizontal angle. This setting applies only for ImageMagick library.',
      54 => 'setting_imgwater_preview',
      55 => 'Prévisualisation',//'预览', //  Preview',
      56 => 'S\'il vous plaît prévisualisation avant l\'affichage ',//'请先提交后再预览', //  'Please preview before the submit',
    ),
  ),
  283 =>
  array (
    'index' =>
    array (
      'Filigrane' => 'action=setting&operation=imgwater',//'水印设置', //   'Watermark'
      'Forum' => 'action=setting&operation=imgwater&anchor=forum',//'论坛', //   'Forum'
    ),
    'text' =>
    array (
      0 => 'Filigrane &raquo; Forum',//'水印设置 &raquo; 论坛', //  'Watermark &raquo; Forum',
      1 => 'setting_imgwater_image_watermarks_forum',
      2 => 'Annexés paramètres de filigrane des images Forum',//'论坛附件图片水印设置', //  'Forum attached images watermark settings',
      3 => 'setting_imgwater_image_watermarkstatus',
      4 => 'Filigrane',//'水印',  //  'Watermark',
      5 => 'Vous pouvez configurer pour ajouter filigrane automatiquement téléchargé des images JPG/PNG/GIF. Le filigrane peut être placé dans une situation privilégiée (3x3 = 9 postes disponibles). Format GIF animé n\'est pas supporté.',//'你可以设置自动为用户上传的 JPG/PNG/GIF 图片附件添加水印，请在此选择水印添加的位置(3x3 共 9 个位置可选)。不支持动画 GIF 格式',   //   'You can set up for add watermark automatically for uploaded JPG/PNG/GIF images. The watermark may be placed in a preferred position (3x3 = 9 positions available). Animated GIF format is not supported.',
      6 => 'setting_imgwater_image_watermarkminwidthheight',
      7 => 'Conditions Taille du filigrane',//'水印添加条件', //   'Watermark size conditions',
      8 => 'Si une image est plus petite que cette taille, le filigrane ne sera pas utiliser. Mettre à 0 pour un filigrane tout le temps.',//'设置水印添加的条件，小于此尺寸的图片附件将不添加水印',   // 'If an image is smaller than this size, the watermark will not used. Set to 0 in order to a watrmark allways.', 
      9 => 'setting_imgwater_image_watermarktype',
      10 => 'Filigrane Type d\'image',//'水印图片类型', //  'Watermark image type',
      11 => 'Si vous définissez un filigrane GIF, le fichier static / image / common / watermark.gif sera utilisé comme filigrane. Si vous définissez un filigrane PNG, le filigrane utiliser le fichier static/image/common/watermark.png. Vous pouvez remplacer ces fichiers de filigrane à vos images préférées. Si vous définissez le texte en filigrane, l\'image sera traitée en utilisant la bibliothèque GD, et exiger pour soutenir la bibliothèque FreeType.',//'如果设置 GIF 类型的文件作为水印，水印图片为 static/image/common/watermark.gif，如果设置 PNG 类型的文件作为水印，水印图片为 static/image/common/watermark.png，你可替换水印文件以实现不同的水印效果。如果设置文本类型的水印并且使用 GD 图片处理库，那么还需要 FreeType 库支持才能使用',    //   'If you set a GIF watermark, the file static/image/common/watermark.gif will used as  watermark. If you set a PNG watermark, the watermark will use the static/image/common/watermark.png file. You can replace this watermark files with your preferrd images. If you set the TEXT watermark, the image will processed using the GD library, and require to support the FreeType library.',
      12 => 'setting_imgwater_image_watermarktrans',
      13 => 'Transparence du filigrane',//'水印融合度', //  'Watermark transparency',
      14 => 'Définir une valeur de transparence du filigrane pour GIF Type filigrane dans la gamme 1 - 100. La plus grande valeur résulte de la transparence du filigrane inférieur. PNG Type filigrane ignorera ce paramètre avec parce que contenant ce paramètre à l\'intérieur du filigrane même présenter. Cette fonctionnalité nécessite d\'activer un type du filigrane valide.',//'设置 GIF 类型水印图片与原始图片的融合度，范围为 1～100 的整数，数值越大水印图片透明度越低。PNG 类型水印本身具有真彩透明效果，无须此设置。本功能需要开启水印功能后才有效',   // 'Set a watermark transparency value for GIF type watermark in range 1 - 100. The greater value results the lower watermark transparency. PNG type watermark will ignore this setting with because containing this parameter inside the watermark file itself. This feature require to enable a valid watermark type.', 
      15 => 'setting_imgwater_image_watermarkquality',
      16 => 'JPEG qualité filigrane',//'JPEG 水印质量', //   'JPEG watermark quality',
      17 => 'Réglez la qualité d\'image JPEG en résulte après le filigrane appliqué. La plage est de 0 - 100 (valeur entière). La valeur la plus élevée signifie un meilleur résultat, mais de produire une taille d\'image supérieure. Cette fonctionnalité nécessite d\'activer un type du filigrane valide.',//'设置 JPEG 类型的图片附件添加水印后的质量参数，范围为 0～100 的整数，数值越大结果图片效果越好，但尺寸也越大。本功能需要开启水印功能后才有效',    //  'Set the resulting JPEG image quality after watermark applied. The range is 0 - 100 (integer value). The larger value means a better result, but produce a larger image size. This feature require to enable a valid watermark type.', 
      18 => 'setting_imgwater_image_watermarktext_text',
      19 => 'Contenu texte en filigrane',//'文本水印文字', //   'TEXT watermark content',
      20 => 'Si vous spécifiez une police TrueType pour la police chinoise, vous pouvez écrire un texte en filigrane dans la langue chinoise',//'如果你指定的 TrueType 字体为中文字体文件，那么你可以在文本水印中书写中文',    //    'If you specify a TrueType font for Chinese font, you can write a text watermark in Chinese language',
      21 => 'setting_imgwater_image_watermarktext_fontpath',
      22 => 'Texte en filigrane TrueType nom de fichier de police',//'文本水印 TrueType 字体文件名', //  'TEXT watermark TrueType font file name',
      23 => 'Entrez le nom du fichier de police TTF situé dans le static/image/seccode/font/ch/ ou l\'annuaire static/image/seccode/font/fr/. Pour le support des caractères chinois, vous devez utiliser de polices TTF chinois, dont un des caractères chinois complets.',//'填写存放在 static/image/seccode/font/ch/ 或 static/image/seccode/font/en/ 目录下的 TTF 字体文件，支持中文字体。如使用中文 TTF 字体请使用包含完整中文汉字的字体文件',   //  'Enter the TTF font file name located in the static/image/seccode/font/ch/ or static/image/seccode/font/en/ directory. For support Chinese characters you have to use of Chinese TTF fonts including a complete Chinese characters.',
      24 => 'setting_imgwater_image_watermarktext_size',
      25 => 'Texte en filigrane taille de la police',//'文本水印字体大小', //   'TEXT watermark font size',
      26 => 'Définir la taille du texte de police appropriée pour un filigrane',//'设置文本水印字体大小，请按照字体设置相应的大小', // 'Set the appropriate text font size for a watermark', 
      27 => 'setting_imgwater_image_watermarktext_angle',
      28 => 'Angle de texte en filigrane',//'文本水印显示角度', //   'TEXT watermark angle',
      29 => 'Définir un angle de rotation du texte à partir de 0 degrés',//'0 度为从左向右阅读文本', //  'Set a text rotation angle starting from 0 degrees',
      30 => 'setting_imgwater_image_watermarktext_color',
      31 => 'Texte en filigrane couleur de police',//'文本水印字体颜色', //  'TEXT watermark font color',
      32 => 'Entrez une couleur de filigrane dans un format hexadécimal, c\'est à dire 99CCFF',//'输入 16 进制颜色代表文本水印字体颜色', //  'Enter a color of watermark in a hexadecimal format, i.e. 99CCFF',
      33 => 'setting_imgwater_image_watermarktext_shadowx',
      34 => 'Texte en filigrane ombre décalage horizontal',//'文本水印阴影横向偏移量', // 'TEXT watermark horizontal shadow offset', 
      35 => 'Définir un décalage de l\'ombre d\'un texte en filigrane horizontal.',//'设置文本水印阴影横向偏移量，此数值不宜设置的太大', // 'Set a horizontal offset for the shadow of a text watermark.', 
      36 => 'setting_imgwater_image_watermarktext_shadowy',
      37 => 'Texte en filigrane ombre décalage vertical',//'文本水印阴影纵向偏移量', //   'TEXT watermark vertical shadow offset',
      38 => 'Définir un décalage de l\'ombre d\'un filigrane texte vertical.',//'设置文本水印阴影纵向偏移量，此数值不宜设置的太大', //  'Set a vertical offset for the shadow of a text watermark.',
      39 => 'setting_imgwater_image_watermarktext_shadowcolor',
      40 => 'Couleur de l\'ombre',//'文本水印阴影颜色', //  'Shadow color', 
      41 => 'Entrez une couleur d\'ombre dans un format hexadécimal, c\'est à dire 99CCFF',//'输入 16 进制颜色代表文本水印阴影字体颜色', //   'Enter a shadow color in a hexadecimal format, i.e. 99CCFF',
      42 => 'setting_imgwater_image_watermarktext_imtranslatex',
      43 => 'Texte en filigrane décalage horizontal (ImageMagick)',//'文本水印横向偏移量(ImageMagick)', //  'TEXT watermark horizontal offset (ImageMagick)',
      44 => 'Réglez le décalage horizontal du filigrane. Ce paramètre s\'applique uniquement pour la bibliothèque ImageMagick.',//'设置水印文本输出后向屏幕中央的横向的偏移值。本设置只适用于 ImageMagick 图片处理库',   //  'Set the watermark horizontal offset. This settingapplies  only for ImageMagick library.', 
      45 => 'setting_imgwater_image_watermarktext_imtranslatey',
      46 => 'Texte en filigrane décalage vertical (ImageMagick)',//'文本水印纵向偏移量(ImageMagick)', //  'TEXT watermark vertical offset (ImageMagick)',
      47 => 'Réglez le décalage vertical du filigrane. Ce paramètre s\'applique uniquement pour la bibliothèque ImageMagick.',//'设置水印文本输出后向屏幕中央的纵向的偏移值。本设置只适用于 ImageMagick 图片处理库',   //    'Set the watermark vertical offset. This settingapplies  only for ImageMagick library.',
      48 => 'setting_imgwater_image_watermarktext_imskewx',
      49 => 'Texte en filigrane angle horizontal (ImageMagick)',//'文本水印横向倾斜角度(ImageMagick)',    //   'TEXT watermark horizontal angle (ImageMagick)', 
      50 => 'Réglez l\'angle horizontal du filigrane. Ce paramètre s\'applique uniquement pour la bibliothèque ImageMagick.',//'设置水印文本横向的倾斜角度。本设置只适用于 ImageMagick 图片处理库', //  'Set the watermark horizontal angle. This settingapplies  only for ImageMagick library.',
      51 => 'setting_imgwater_image_watermarktext_imskewy',
      52 => 'Texte en filigrane inclinaison verticale (ImageMagick)',//'文本水印纵向倾斜角度(ImageMagick)', //  
      53 => 'Réglez l\'angle horizontal du filigrane. Ce paramètre s\'applique uniquement pour la bibliothèque ImageMagick.',//'设置水印文本纵向的倾斜角度。本设置只适用于 ImageMagick 图片处理库', //   'Set the watermark horizontal angle. This setting applies only for ImageMagick library.',
      54 => 'setting_imgwater_preview',
      55 => 'Prévisualisation',//'预览', //  
      56 => 'Svp. veuillez prévisualiser avant d\'envoyer',//'请先提交后再预览', //   'Please preview before the submit',
    ),
  ),
  284 =>
  array (
    'index' =>
    array (
      'Filigrane' => 'action=setting&operation=imgwater',//'水印设置', //  'Watermark'
      'Espace Album' => 'action=setting&operation=imgwater&anchor=album',//'空间相册', //  'Space Album'
    ),
    'text' =>
    array (
      0 => 'Filigrane &raquo; Espace Album',//'水印设置 &raquo; 空间相册', //  'Watermark &raquo; Space Album',
      1 => 'setting_imgwater_image_watermarks_album',
      2 => 'Paramètres du filigrane photo d\'album Espace',//'空间相册图片水印设置', //  'Space album photo watermark settings',
      3 => 'setting_imgwater_image_watermarkstatus',
      4 => 'Filigrane',//'水印', //  'Watermark',
      5 => 'Vous pouvez configurer pour ajouter filigrane automatiquement téléchargé des images JPG / PNG / GIF. Le filigrane peut être placé dans une situation privilégiée (3x3 = 9 postes disponibles). Format GIF animé n\'est pas supporté.',//'你可以设置自动为用户上传的 JPG/PNG/GIF 图片附件添加水印，请在此选择水印添加的位置(3x3 共 9 个位置可选)。不支持动画 GIF 格式',  //  You can set up for add watermark automatically for uploaded JPG/PNG/GIF images. The watermark may be placed in a preferred position (3x3 = 9 positions available). Animated GIF format is not supported.
      6 => 'setting_imgwater_image_watermarkminwidthheight',
      7 => 'Conditions Taille du filigrane',//'水印添加条件', //  'Watermark size conditions',
      8 => 'Si une image est plus petite que cette taille, le filigrane ne sera pas utilisé. Mettre à 0 pour un filigrane tout le temps.',//'设置水印添加的条件，小于此尺寸的图片附件将不添加水印',  //  'If an image is smaller than this size, the watermark will not used. Set to 0 in order to a watrmark allways.',
      9 => 'setting_imgwater_image_watermarktype',
      10 => 'Filigrane Type d\'image',//'水印图片类型', //  'Watermark image type',
      11 => 'Si vous définissez un filigrane GIF, le fichier static/image/common/ watermark.gif sera utilisé comme filigrane. Si vous définissez un filigrane PNG, le filigrane utiliser le fichier static/image/common/watermark.png. Vous pouvez remplacer ces fichiers de filigrane à vos images préférées. Si vous définissez le texte en filigrane, l\'image sera traitée en utilisant la bibliothèque GD, et exiger pour soutenir la bibliothèque FreeType.',//'如果设置 GIF 类型的文件作为水印，水印图片为 static/image/common/watermark.gif，如果设置 PNG 类型的文件作为水印，水印图片为 static/image/common/watermark.png，你可替换水印文件以实现不同的水印效果。如果设置文本类型的水印并且使用 GD 图片处理库，那么还需要 FreeType 库支持才能使用',  //   'If you set a GIF watermark, the file static/image/common/watermark.gif will used as  watermark. If you set a PNG watermark, the watermark will use the static/image/common/watermark.png file. You can replace this watermark files with your preferrd images. If you set the TEXT watermark, the image will processed using the GD library, and require to support the FreeType library.',
      12 => 'setting_imgwater_image_watermarktrans',
      13 => 'Watermark transparency',//'水印融合度', //  'Watermark transparency',
      14 => 'Définir une valeur de transparence du filigrane pour GIF Type filigrane dans la gamme 1 - 100. La plus grande valeur résulte de la transparence du filigrane inférieur. PNG Type filigrane ignorera ce paramètre avec parce que contenant ce paramètre à l\'intérieur du filigrane même présenter. Cette fonctionnalité nécessite d\'activer un type du filigrane valide.',//'设置 GIF 类型水印图片与原始图片的融合度，范围为 1～100 的整数，数值越大水印图片透明度越低。PNG 类型水印本身具有真彩透明效果，无须此设置。本功能需要开启水印功能后才有效',  //   'Set a watermark transparency value for GIF type watermark in range 1 - 100. The greater value results the lower watermark transparency. PNG type watermark will ignore this setting with because containing this parameter inside the watermark file itself. This feature require to enable a valid watermark type.',
      15 => 'setting_imgwater_image_watermarkquality',
      16 => 'JPEG qualité filigrane',//'JPEG 水印质量', //   'JPEG watermark quality',
      17 => 'Réglez la qualité d\'image JPEG en résulte après le filigrane appliqué. La plage est de 0 - 100 (valeur entière). La valeur la plus élevée signifie un meilleur résultat, mais de produire une taille d\'image supérieure. Cette fonctionnalité nécessite d\'activer un type du filigrane valide.',//'设置 JPEG 类型的图片附件添加水印后的质量参数，范围为 0～100 的整数，数值越大结果图片效果越好，但尺寸也越大。本功能需要开启水印功能后才有效',  //   'Set the resulting JPEG image quality after watermark applied. The range is 0 - 100 (integer value). The larger value means a better result, but produce a larger image size. This feature require to enable a valid watermark type.',
      18 => 'setting_imgwater_image_watermarktext_text',
      19 => 'Contenu texte en filigrane',//'文本水印文字', //  'TEXT watermark content',
      20 => 'Si vous spécifiez une police TrueType pour la police chinoise, vous pouvez écrire un texte en filigrane dans la langue chinoise',//'如果你指定的 TrueType 字体为中文字体文件，那么你可以在文本水印中书写中文',  //  'If you specify a TrueType font for Chinese font, you can write a text watermark in Chinese language', 
      21 => 'setting_imgwater_image_watermarktext_fontpath',
      22 => 'Texte en filigrane TrueType nom de fichier de police',//'文本水印 TrueType 字体文件名', // 'TEXT watermark TrueType font file name', 
      23 => 'Entrez le nom du fichier de police TTF situé dans le répertoire static / image / seccode/font/ch/ ou static/image/seccode/ police/fr/. Pour le support des caractères chinois, vous devez utiliser des polices TTF chinois, dont un des caractères chinois complets.',//'填写存放在 static/image/seccode/font/ch/ 或 static/image/seccode/font/en/ 目录下的 TTF 字体文件，支持中文字体。如使用中文 TTF 字体请使用包含完整中文汉字的字体文件',  //   'Enter the TTF font file name located in the static/image/seccode/font/ch/ or static/image/seccode/font/en/ directory. For support Chinese characters you have to use of Chinese TTF fonts including a complete Chinese characters.',
      24 => 'setting_imgwater_image_watermarktext_size',
      25 => 'Texte en filigrane taille de la police',//'文本水印字体大小',  //   'TEXT watermark font size',
      26 => 'Définir la taille du texte de police appropriée pour un filigrane',//'设置文本水印字体大小，请按照字体设置相应的大小', //   'Set the appropriate text font size for a watermark',
      27 => 'setting_imgwater_image_watermarktext_angle',
      28 => 'Angle de texte en filigrane',//'文本水印显示角度', //  'TEXT watermark angle',
      29 => 'Définir un angle de rotation du texte à partir de 0 degrés',//'0 度为从左向右阅读文本', //  'Set a text rotation angle starting from 0 degrees',
      30 => 'setting_imgwater_image_watermarktext_color',
      31 => 'Texte en filigrane couleur de police',//'文本水印字体颜色', //'TEXT watermark font color',  
      32 => 'Entrez une couleur des filigrane dans un format hexadécimal, c\'est à dire 99CCFF',//'输入 16 进制颜色代表文本水印字体颜色', //  'Enter a color of watermark in a hexadecimal format, i.e. 99CCFF',
      33 => 'setting_imgwater_image_watermarktext_shadowx',
      34 => 'Texte en filigrane ombre décalage horizontal',//'文本水印阴影横向偏移量', //  'TEXT watermark horizontal shadow offset',
      35 => 'Définir un décalage de l\'ombre d\'un texte en filigrane horizontal.',//'设置文本水印阴影横向偏移量，此数值不宜设置的太大', //  'Set a horizontal offset for the shadow of a text watermark.',
      36 => 'setting_imgwater_image_watermarktext_shadowy',
      37 => 'Texte en filigrane ombre décalage vertical',//'文本水印阴影纵向偏移量', //  'TEXT watermark vertical shadow offset',
      38 => 'Définir un décalage de l\'ombre d\'un filigrane texte vertical.',//'设置文本水印阴影纵向偏移量，此数值不宜设置的太大', //  'Set a vertical offset for the shadow of a text watermark.',
      39 => 'setting_imgwater_image_watermarktext_shadowcolor',
      40 => 'Couleur de l\'ombre',//'文本水印阴影颜色', //   'Shadow color',
      41 => 'Entrez une couleur d\'ombre dans un format hexadécimal, c\'est à dire 99CCFF',//'输入 16 进制颜色代表文本水印阴影字体颜色', //  'Enter a shadow color in a hexadecimal format, i.e. 99CCFF',
      42 => 'setting_imgwater_image_watermarktext_imtranslatex',
      43 => 'Texte en filigrane décalage horizontal (ImageMagick)',//'文本水印横向偏移量(ImageMagick)',  //   'TEXT watermark horizontal offset (ImageMagick)',
      44 => 'Réglez le décalage horizontal du filigrane. Ce paramètre s\'applique uniquement pour la bibliothèque ImageMagick.',//'设置水印文本输出后向屏幕中央的横向的偏移值。本设置只适用于 ImageMagick 图片处理库',  //   'Set the watermark horizontal offset. This settingapplies  only for ImageMagick library.',
      45 => 'setting_imgwater_image_watermarktext_imtranslatey',
      46 => 'Texte en filigrane décalage vertical (ImageMagick)',//'文本水印纵向偏移量(ImageMagick)',  //  'TEXT watermark vertical offset (ImageMagick)',
      47 => 'Réglez le décalage vertical du filigrane. Ce paramètre s\'applique uniquement pour la bibliothèque ImageMagick.',//'设置水印文本输出后向屏幕中央的纵向的偏移值。本设置只适用于 ImageMagick 图片处理库',  //  'Set the watermark vertical offset. This settingapplies  only for ImageMagick library.',
      48 => 'setting_imgwater_image_watermarktext_imskewx',
      49 => 'Texte en filigrane angle horizontal (ImageMagick)',//'文本水印横向倾斜角度(ImageMagick)', //   'TEXT watermark horizontal angle (ImageMagick)',
      50 => 'Réglez l\'angle horizontal du filigrane. Ce paramètre s\'applique uniquement pour la bibliothèque ImageMagick.',//'设置水印文本横向的倾斜角度。本设置只适用于 ImageMagick 图片处理库', // 'Set the watermark horizontal angle. This settingapplies  only for ImageMagick library.', 
      51 => 'setting_imgwater_image_watermarktext_imskewy',
      52 => 'Texte en filigrane inclinaison verticale (ImageMagick)',//'文本水印纵向倾斜角度(ImageMagick)', //  
      53 => 'Réglez l\'angle horizontal du filigrane. Ce paramètre s\'applique uniquement pour la bibliothèque ImageMagick.',//'设置水印文本纵向的倾斜角度。本设置只适用于 ImageMagick 图片处理库', //  'Set the watermark horizontal angle. This setting applies only for ImageMagick library.', 
      54 => 'setting_imgwater_preview',
      55 => 'Prévisualisation',//'预览', //   'Preview',
      56 => 'Svp. veuillez prévisualiser avant d\'envoyer',//'请先提交后再预览', //   'Please preview before the submit',
    ),
  ),
  285 =>
  array (
    'index' =>
    array (
      'Paramètres de recherche' => 'action=setting&operation=search',//'搜索设置', //  'Search Settings' 
    ),
    'text' =>
    array (
      0 => 'Paramètres de recherche',//'搜索设置', //   'Search Settings',
      1 => 'setting_search_status',
      2 => 'Activer la recherche',//'开启搜索', //  'Enable search',
      3 => 'Consultez les sections que vous souhaitez activer la recherche',//'勾选你要开启的搜索栏目', //  'Check the sections you want to enable the search',
      4 => 'setting_search_srchhotkeywords',
      5 => 'Mots-clés populaires',//'热门关键词',   //  'Popular keywords',
      6 => 'Un mot clé par ligne',//'每行一个', //  'One keyword per line',
      7 => 'setting_search_srchhotkeywords',
      8 => 'Mots-clés populaires',//'热门关键词', //   'Popular keywords',
      9 => 'Un mot clé par ligne',//'每行一个', //  'One keyword per line',
      10 => 'settings_sphinx',
      11 => 'Réglages Complets de recherche textuelle Sphinx',//'Sphinx 全文检索设置', //   'Sphinx full text search settings',
      12 => 'settings_sphinx_sphinxon',
      13 => 'Activer le Sphinx',//'是否开启', //  'Enable Sphinx',
      14 => 'Avant d\'activer ce paramètre, vous devez installer et configurer le sphinx au moteur de recherche en texte intégral',//'设置是否开启 Sphinx 全文检索功能，开启前确认 Sphinx 安装及配置成功',
      15 => 'settings_sphinx_sphinxhost',
      16 => 'Définissez le nom d\'hôte du sphinx, ou une adresse de socket de service du sphinx',//'设置 Sphinx 主机名，或者 Sphinx 服务 socket 地址', //  'Set the sphinx host name, or a sphinx service socket address',
      17 => 'Entrez le sphinx complet Nom de l\'hôte: à savoir "localhost", ou remplissez l\'adresse du socket de service du sphinx comme l\'adresse absolue: à savoir /tmp/sphinx.sock.',//'填写 Sphinx 主机名：例如，本地主机填写“localhost”，或者填写 Sphinx 服务 socket 地址，必须是绝对地址：例如，/tmp/sphinx.sock',    //   'Enter the complete sphinx Host Name: i.e. "localhost", or fill out the sphinx service socket address as the absolute address: i.e. /tmp/sphinx.sock.',
      18 => 'settings_sphinx_sphinxport',
      19 => 'Port hôte du Sphinx',//'设置 Sphinx 主机端口', //   'Sphinx host port',
      20 => 'Entrez le port hôte sphinx: à savoir 3312 si le nom d\'hôte utilisé. Laisser vide si l\'adresse de socket est utilisé.',//'填写 Sphinx 主机端口：例如，3312，主机名填写 socket 地址的，则此处不需要设置',  //  'Enter the sphinx host port: i.e. 3312 if host name used. Leave empty if socket address used.',
      21 => 'settings_sphinx_sphinxsubindex',
      22 => 'Index Titre du Nom',//'设置标题索引名', //   'Index name title',
      23 => 'Entrez le titre du nom de la configuration du sphinx et un titre du principal indice nom incrément. Par exemple, "sujets, threads_minute".<br />Note: Les symboles de l\'indice doivent être séparées par des virgules ",". L\'index doit être rempli en conformité avec le fichier de configuration du sphinx',//'填写 Sphinx 配置中的标题主索引名及标题增量索引名：例如，“threads,threads_mintue”。注意：多个索引使用半角逗号 "," 隔开，必须按照 Sphinx 配置文件中的索引名填写',   //  'Enter the title of sphinx configuration name and a title of the main index name increment. For example, "threads, threads_minute".<br />Note: The index symbols must be separated by comma ",". The index must be completed in accordance with the sphinx configuration file.',
      24 => 'settings_sphinx_sphinxmsgindex',
      25 => 'Texte intégral nom d\'index',//'设置全文索引名', //  'Full-text index name',
      26 => 'Entrez le titre du nom de la configuration du sphinx et un titre du principal indice nom incrément. Par exemple, "posts, posts_minute". <br /> Note: Les symboles de l\'indice doivent être séparées par des virgules ",". L\'index doit être rempli en conformité avec le fichier de configuration du sphinx',//'填写 Sphinx 配置中的全文主索引名及全文增量索引名：例如，“posts,posts_mintue”。注意：多个索引使用半角逗号 "," 隔开，必须按照 Sphinx 配置文件中的索引名填写',   //   'Enter the title of sphinx configuration name and a title of the main index name increment. For example, "posts, posts_minute". <br /> Note: The index symbols must be separated by comma ",". The index must be completed in accordance with the sphinx configuration file.',
      27 => 'settings_sphinx_sphinxmaxquerytime',
      28 => 'Temps de Recherche maximal de requête',//'设置最大搜索时间', //  'Maximum search query time',
      29 => 'Entrez un temps de recherche maximal en millisecondes. Paramètre doit être un entier non négatif. La valeur par défaut est 0, ce qui signifie aucune restriction.',//'填写最大搜索时间，以毫秒为单位。参数必须是非负整数。默认值为 0，意思是不做限制',  //   'Enter a maximum search time in milliseconds. Parameter must be non-negative integer. The default value is 0, that means no restriction.',
      30 => 'settings_sphinx_sphinxlimit',
      31 => 'Le nombre maximum de résultats de recherche',//'设置最大返回匹配项数目', // 'Maximum number of search results',  
      32 => 'Entrez un nombre maximum de correspondances à retourner, doit être un entier non négatif, la valeur par défaut de 10000.',//'填写最大返回匹配项数目，必须是非负整数，默认值10000',  //  'Enter a maximum number of matches to return, must be non-negative integer, the default value of 10000.',
      33 => 'settings_sphinx_sphinxrank',
      34 => 'Texte intégral mode de classement de l\'index',//'设置全文索引评分模式', //  'Full-text index rank mode',
      35 => '(1) SPH_RANK_PROXIMITY_BM, le mode par défaut, en utilisant classement des phrases et BM25 rang, et sera une combinaison des deux. [Default]
						<br />(2) SPH_RANK_BM25, le mode de calcul de corrélation statistique, utilisez uniquement le calcul BM25 du rang (titulaires moteur de recherche de texte avec beaucoup plus de chose identique). Ce mode est plus rapide, mais peut contenir autre que le résultat d\'une baisse de termes de requête en qualité.
						<br />(3) SPH_RANK_NONE, Désactiver le mode de classement, qui est le mode le plus rapide. Fait que le même modèle comme une recherche booléenne. Tous les matchs sont donnés poids 1.',
    ),
  ),
  286 =>
  array (
    'index' =>
    array (
      'Réglages UCenter' => 'action=setting&operation=uc',//'UCenter 设置', //  'UCenter settings'
    ),
    'text' =>
    array (
      0 => 'Réglages UCenter',//'UCenter 设置', //  'UCenter settings',
      1 => 'setting_uc_tips',
      2 => 'Ce paramètre a été réglé automatiquement lorsque le site est installé. Dans des circonstances normales n\'ont pas besoin de changer. Avant de modifier effectuer une sauvegarde du fichier config/config_ucenter.php  pour empêcher une modifications non valides qui peuvent provoquer le site ne peut pas fonctionner.',//'本设置在站点安装时自动生成，一般情况下请不要修改，修改前请备份 config/config_ucenter.php 文件，以防止修改错误导致站点无法运行',
      3 => 'setting_uc_appid',
      4 => 'UCenter application ID',//'UCenter 应用 ID', //  'UCenter application ID',
      5 => 'L\'ID d\'application du site actuel enregistré dans UCenter. Généralement pas nécessaire de modifier.',//'该值为当前站点在 UCenter 的应用 ID，一般情况请不要改动', //   'The current site application ID registered in UCenter. Usually not required to change.',
      6 => 'setting_uc_key',
      7 => 'UCenter clé communication',//'UCenter 通信密钥', //  'UCenter communication key',
      8 => 'UCenter clé communication est pour le chiffrement d\'un transfert d\'information, il peut contenir des lettres et des chiffres, et être la même dans UCenter un Discuz! faire en sorte que les deux systèmes peuvent communiquer normalement.',//'通信密钥用于在 UCenter 和 Discuz! 之间传输信息的加密，可包含任何字母及数字，请在 UCenter 与 Discuz! 设置完全相同的通讯密钥，以确保两套系统能够正常通信',  //  UCenter communication key is for the encryption an information transfer, it can contain any letters and numbers, and be the same in UCenter an Discuz! to ensure that the two systems can normal communicate.   
      9 => 'setting_uc_api',
      10 => 'UCenter API adresse d\'accès',//'UCenter 访问地址', //   'UCenter API access address',
      11 => 'Si votre avez modifié l\'emplacement d\'UCenter, s\'il vous plaît modifier ce paramètre également. Un réglage incorrect peut entraîner le  mauvais fonctionnement du site, veuillez faire attention à cette modification.<br />Format: http://www.sitename.com/uc_server (do not add the last slash)',//'如果你的 UCenter 访问地址发生了改变，请修改此项。不正确的设置可能导致站点功能异常，请小心修改。格式: http://www.sitename.com/uc_server (最后不要加\'/\')', //    'If your have changed the UCenter location, please change this setting too. Incorrect setting may cause the site misfunction, please be careful in modification.<br />Format: http://www.sitename.com/uc_server (do not add the last slash)',
      12 => 'setting_uc_ip',
      13 => 'Adresse IP UCenter',//'UCenter IP 地址', //  'UCenter IP address',
      14 => 'Si votre serveur peut pas accéder à UCenter du nom de domaine, vous pouvez entrer l\'adresse IP du serveur UCenter',//'如果你的服务器无法通过域名访问 UCenter，可以输入 UCenter 服务器的 IP 地址',  //  'If your server can not access UCenter domain name, you can enter the UCenter server IP address',
      15 => 'setting_uc_connect',
      16 => 'UCenter type de connexion',//'UCenter 连接方式', //  'UCenter connection type',
      17 => 'Si le site UCenter est placé sur un autre serveur, vous pouvez communiquer avec elle par une télécommande bidirectionnelle. Si le UCenter est placé sur le même serveur, nous vous recommandons d\'utiliser un moyen de connexion de base de données, il sera plus rapide.',//'采用接口方式时，站点和 Ucenter 通信采用远程方式，如果你的服务器环境支持，我们推荐你使用它。数据库方式需要你站点可以连接 UCenter 数据库',  // If the Ucenter site is placed on other server, you can communicate with it by a remote way. If the UCenter is placed on the same server, we recommend you to use a database connection way, it will be more faster.
      18 => 'setting_uc_dbhost',
      19 => 'Base de données Hôte UCenter',//'UCenter 数据库服务器', //  'UCenter database host',
      20 => 'Hôte de la Database peut être en local ou distant. Si un port MySQL n\'est pas par défaut 3306, s\'il vous plaît utiliser le formulaire ci-dessous: 127.0.0.1:6033',//'可以是本地也可以是远程数据库服务器，如果 MySQL 端口不是默认的 3306，请填写如下形式：127.0.0.1:6033',
      21 => 'setting_uc_dbuser',
      22 => 'UCenter Base de données identifiant',//'UCenter 数据库用户名', //  'UCenter database username',
      23 => 'setting_uc_dbpass',
      24 => 'Mot de passe de base de données UCenter',//'UCenter 数据库密码', //   'UCenter database password',
      25 => 'setting_uc_dbname',
      26 => 'UCenter Nom Database',//'UCenter 数据库名', //  'UCenter database name',
      27 => 'setting_uc_dbtablepre',
      28 => 'Préfixe de table UCenter',//'UCenter 表前缀', //  'UCenter table prefix',
      29 => 'setting_uc_activation',
      30 => 'Permettre à d\'autres applications pour activer le membre au site',//'是否允许其他应用的会员在站点激活',   // 'Enable other applications to activate the site member', 
      31 => 'Certains membres peuvent être enregistrés par d\'autres applications, et non par ce site. Régler "Oui" si vous permettez à ces utilisateurs d\'accéder au site.',//'选择“是”，允许会员在本站点激活；选择“否”，则不允许任何第三方应用在本站点激活',
      32 => 'setting_uc_fastactivation',
      33 => 'Que ce soit pour permettre l\'activation directe',//'是否允许直接激活', //  'Whether to allow the direct activation',
      34 => 'Choisissez "Oui" pour permettre la connexion de l\'utilisateur automatiquement activé. Sélectionnez "Non" pour permettre l\'activation par l\'utilisateur uniquement lorsque l\'utilisateur remplit les paramètres du profil requis.',//'选择“是”，用户登录即自动激活；选择“否”，允许用户激活，但必须填写注册设置中的必填项',
      35 => 'setting_uc_avatarmethod',
      36 => 'Méthode d\'Avatar',//'头像调用方式', //   'Avatar method',
      37 => 'Définir une méthode pour afficher les avatars des utilisateurs',//'设置用户头像的调用方式', //  'Set a method to show user avatars',
    ),
  ),
  287 =>
  array (
    'index' =>
    array (
      'Réglages E-Commerce' => 'action=setting&operation=ec',//'电子商务', //  'E-Commerce Settings'
      'Réglages de base' => 'action=setting&operation=ec',//'基本设置', //   'Basic settings' 
    ),
    'text' =>
    array (
      0 => 'Réglages E-Commerce &raquo; Réglages de base',//'电子商务 &raquo; 基本设置', //  'E-Commerce &raquo; Base settings',
      1 => 'setting_ec_credittrade',
      2 => 'Paramètres des points d\'échange',//'积分兑换设置', //  'Points Exchange settings',
      3 => 'setting_ec_ratio',
      4 => 'Monétaire/points ratio d\'échange',//'现金/积分兑换比率', //   'Money/points exchange ratio',
      5 => 'Définir un rapport d\'échange entre la liquidité de l\'argent réel (en EURO) et des points internes. Par exemple, si est réglé sur 10, 1 EUR peut être échangé pour 10 points. Cette fonction est nécessaire pour permettre les opérations de vente avec des points, et un accès au système de paiement externe connexes (c.-à-Alipay). Mettre à 0 pour interdire l\'utilisation de la trésorerie et une conversion en points.',//'设置真实货币现金(以人民币元为单位)与站点交易积分间的兑换比率，例如设置为 10，则 1 元人民币可以兑换 10 个单位的交易积分。本功能需开启交易积分，并成功进行支付宝收款账号的相关设置后方可使用，如果禁止使用现金与交易积分的兑换功能，请设置为 0',  //   'Set an exchange ratio between the real money cash (in USD) and intenal points. For example, if is set to 10, 1 USD can be exchanged to 10 points. This feature is required to enable sale operations with points, and an access to the related external paymet system (i.e. Alipay). Set to 0 for prohibit the use of cash and a conversion to points.',
      6 => 'setting_ec_mincredits',
      7 => 'Montant minimal de points à l\'achat unique',//'单次购买最小积分数额', //  'Minimal amount of points for single purchase',
      8 => 'Réglez l\'utilisateur summ minimal à payer des points pour acheter la plus petite transaction. Mettre à 0 pour aucune limite.',//'设置用户一次支付所购买的交易积分的最小数额，单位为交易积分的单位，0 为不限制',  //  'Set the user minimal summ to pay in points for buy the smallest transaction. Set to 0 for no limit.',
      9 => 'setting_ec_maxcredits',
      10 => 'Montant maximal de points à l\'achat unique',//'单次购买最大积分数额', //  'Maximal amount of points to single purchase',
      11 => 'Réglez le summ utilisateur un maximum de payer en points pour une seule transaction. Mettre à 0 pour aucune limite.',//'设置用户一次支付所购买的交易积分的最大数额，单位为交易积分的单位，0 为不限制',  //   'Set the user maximum summ to pay in points for a single transaction. Set to 0 for no limit.',
      12 => 'setting_ec_maxcreditspermonth',
      13 => 'Plus grand nombre de points pour acheter par mois',//'每月购买最大积分数额', //  'Largest amount of points to buy per month',
      14 => 'Définir une summ maximale, l\'utilisateur peut passé à payer par chaque mois. Mettre à 0 pour aucune limite.',//'设置用户每月能够通过在线支付方式购买的交易积分的最大数额，单位为交易积分的单位，0 为不限制',  //  'Set a maximum summ the user can spent to pay per each month. Set to 0 for no limit.',
    ),
  ),
  288 =>
  array (
    'index' =>
    array (
      'Optimisation des performances' => 'action=setting&operation=seo',//'性能优化', //  'Performance Optimization'
      'Optimisation de la Mémoire' => 'action=setting&operation=memory',//'内存优化', //  'Memory Optimization'
    ),
    'text' =>
    array (
      0 => 'Optimisation des performances &raquo; Optimisation de la Mémoire',//'性能优化 &raquo; 内存优化', //  'Performance Optimization &raquo; Memory Optimization',
      1 => 'setting_memory_tips',
      2 => 'Activer la fonction d\'optimisation de mémoire permettra d\'améliorer grandement l\'exécution de l\'application et le chargement du serveur. La fonction d\'optimisation de la mémoire nécessite le module PHP serveur prend en charge certains des interfaces d\'optimisation de la mémoire, comme Memcache, eAccelerator, Alternative PHP Cache (APC), ou Xcache. Système d\'optimisation basée sur l\'environnement de serveur actuel et de l\'interface d\'optimiser la mémoire, en utilisant le paramètre principal dans le config_global.php. Vous pouvez effectuer des réglages avancés en éditant le config_global.php.',//'启用内存优化功能将会大幅度提升程序性能和服务器的负载能力，内存优化功能需要服务器系统以及PHP扩展模块支持目前支持的内存优化接口有 Memcache、eAccelerator、Alternative PHP Cache(APC)、Xcache 四种，优化系统将会依据当前服务器环境依次选用接口内存接口的主要设置位于 config_global.php 当中，你可以通过编辑 config_global.php 进行高级设置',       //    'Enable the memory optimization feature will greatly enhance an application performance and the server loading. The Memory Optimization feature requires the server PHP module supports some of memory optimization interfaces like Memcache, eAccelerator, Alternative PHP Cache (APC), or Xcache. Optimization system based on the current server environment and memory optimize interface, using the main setting in the config_global.php. You can make advanced settings by editing the config_global.php.',/
      3 => 'setting_memory_status',
      4 => 'Situation actuelle de l\'optimisation de la mémoire',//'当前内存工作状态', //   'Current status of memory optimization',
      5 => 'setting_memory_function',
      6 => 'Paramètres d\'optimisation de la mémoire',//'内存优化功能设置', //  'Memory optimization settings', 
    ),
  ),
  289 =>
  array (
    'index' =>
    array (
      'Optimisation' => 'action=setting&operation=seo',//'性能优化' //  Optimization'
      'Gestion de la mémoire cache' => 'action=setting&operation=memorydata',//'内存缓存管理' //  'Memory cache management' 
    ),
    'text' =>
    array (
      0 => 'Optimisation &raquo; Gestion de la mémoire cache',//'性能优化 &raquo; 内存缓存管理', //   'Optimization &raquo; Memory cache management',
      1 => 'setting_memorydata',
      2 => 'Gestion de la mémoire cache',//'内存缓存管理', //  'Memory cache management',
    ),
  ),
  290 =>
  array (
    'index' =>
    array (
      'Liste de Rang' => 'action=setting&operation=ranklist',//'排行榜设置', //  'Rank List' 
    ),
    'text' =>
    array (
      0 => 'Liste de Rang',//'排行榜设置', //  'Rank List',
      1 => 'setting_ranklist_status',
      2 => 'Liste de Classement est sous tension',//'是否开启排行榜', //  'Rank list is turned on',
      3 => 'setting_ranklist_index_cache_time',
      4 => 'Top Liste temps de cache',//'排行榜首页缓存时间(单位：小时)', //  'Top List cache time', 
      5 => 'Unité: Heures',//'单位：小时', //  'Unit: Hours',
      6 => 'setting_ranklist_index_select',
      7 => 'Sélectionnez la liste Page Rang temps d\' autorisation',//'排行榜首页排行时间类型', //  'Select the Rank list page leave time',
      8 => 'setting_ranklist_ignorefid',
      9 => 'Ignorer Forum/Qualifications de groupe',//'不参与排行的版块/群组', //  'Ignore Forum/Group ratings',
      10 => 'Entrez l\'ID du forum ou l\'ID groupe correspond forum et discussions de groupe n\'apparaîtront pas dans la liste. Séparez les IDs par une virgule.',//'填入版块ID或群组ID，对应版块及下面的主题将不进入排行榜，多个版块ID间请用半角逗号 "," 隔开',  //  'Enter the forum ID or group ID, Corresponds forum and group threads will not appear in the list. Separate multiple ID with a comma.',
      11 => 'setting_ranklist_block_set',
      12 => 'Liste de Classement Modules réglages détaillés',//'排行榜详细模块设置', //  'Rank List Modules detailed settings',
      13 => 'setting_ranklist_update_cache_choose',
      14 => 'Choisissez le module de mise à jour cache',//'选择更新缓存模块', // 'Choose the module to update cache', 
    ),
  ),
  291 =>
  array (
    'index' =>
    array (
      'Paramètres d\'Accès mobiles' => 'action=setting&operation=mobile',//'手机版访问设置', //  'Mobile Access settings'
    ),
    'text' =>
    array (
      0 => 'Paramètres d\'Accès mobiles',//'手机版访问设置',  //   'Mobile Access settings',
      1 => 'setting_mobile_status_tips',
      2 => 'Cette fonction fournit un moyen pratique pour afficher les pages à partir d\'appareils mobiles, cette fonctionnalité peut être utilisée dans le seul forum. Les pages téléphone prend en charge uniquement WAP 2.0 (mode XHTML) lorsque accessible sur le navigateur de votre téléphone mobile. Pour le navigateur mobile de la largeur de la vignette de l\'image est fixée à 200 pixels.',//'提供方便手机方式浏览的页面，此功能仅限论坛范围内。本手机功能页仅支持WAP2.0(XHTML方式)以上的手机浏览器进行访问。手机浏览图片宽度为200像素以内缩略图。',   //  'This feature provide a convenient way to view pages from mobile devices, this feature can be used in the forum only. The phone pages supports only WAP2.0 (XHTML mode) when accessed over the mobile phone browser. For mobile browser the image thumbnail width is set to 200 pixels.',
      3 => 'setting_mobile_status',
      4 => 'Mobiles Paramètres Globaux',//'手机版全局设置', //  'Mobile Global Settings',
      5 => 'setting_mobile_allowmobile',
      6 => 'Autoriser mobile',//'开启手机版', //   'Allow Mobile',
      7 => 'Lorsque cette fonction est activée, les utilisateurs ont accédé au nom de domaine du forum par téléphone mobile seront automatiquement redirigés vers l\'interface de la version du téléphone mobile. Pour configurer aller au: Paramètres globaux -> Paramètres du Domaine -> Les paramètres d\'application, et entrez le nom de domaine au bas de navigation: Interface ->Paramètres navigation - la navigation du bas.',//'开启本功能，用户使用手机访问论坛时，将自动跳转到手机版界面手机访问域名配置请进入: 全局 - 域名设置 - 应用域名底部导航设置请进入：界面 - 导航设置 - 底部导航',   // 'When this function enabled, users accessed the forum domain name by mobile phone will be automatically redirected to the mobile phone version interface. For configure go to: Global Settings -> Domain Settings -> Application settings, and enter the domain name at the bottom of navigation: Interface -> Navigation settings - Bottom navigation.', 
      8 => 'setting_mobile_mobileforward',
      9 => 'Autoriser la redirection automatique pour les navigateurs de téléphonie',//'开启手机浏览器自动跳转',//  'Allow automatical redirection for phone browsers',
      10 => 'Lorsqu\'un utilisateur rendront dans la collectivité des Forums des Autres pages par navigateur de votre téléphone, cette fonction redirige automatiquement l\'utilisateur à la page Index du Forum.',//'开启后用户使用手机浏览器访问社区论坛功能页以外页面时自动跳转到论坛首页进行访问',   //   'When a user visit the community forum other pages by phone browser, this function automatically redirect the the user to the Forum Index page.',
      11 => 'setting_mobile_register',
      12 => 'Autoriser l\'Inscription par mobile',//'是否允许手机版注册',//  'Allow Register by Mobile',
      13 => 'Lorsque la fonction d\'enregistrement mobile est mis hors tension, les utilisateurs ne seront pas en mesure d\'enregistrer par un téléphone, veuillez utiliser avec précaution.',//'是否开启手机版注册功能，手机注册不会对用户栏目中的注册页必填项进行检测请谨慎开启',   // 'When the Mobile registration function is turned off, users will not be able to register by a Phone, Please use carefully.', 
      14 => 'setting_mobile_seccode',
      15 => 'Que ce soit pour activer le code de vérification',//'是否开启验证码',//  'Whether to enable the verification code',
      16 => 'Si l\'enregistrement du téléphone et la connexion n\'est pas activée, un code de vérification n\'est pas utilisé. Pour régler le code de vérification aller au: Global -> Paramètres anti-spam -> Paramètres de code de vérification.',//'不开启则手机注册、登录、发布等操作均无验证码注册、登录、发布验证码开关：全局 - 防灌水设置 - 验证码设置',   //  'If the phone registration and login not enabled, a verification code is not used. For set the verification code go to: Global -> Anti-spam settings -> Verification code settimgs.',
      17 => 'setting_mobile_hotthread',
      18 => 'Que ce soit pour ouvrir les Posts hot',  //  '是否开启热帖',//  
      19 => 'Fonction "Edition Tactile", ouvrir le post scotché afficher à l\'Accueil',  //  '“触屏版”功能，开启则在首页显示热贴',
      20 => 'setting_mobile_displayorder3',
      21 => 'Que ce soit pour afficher les fils de discussions importantes scotchées',  //  '是否显示置顶贴',//  
      22 => 'Fonction "Tactile Edition», dans la page après la liste est affichée dans les fils de discussions importantes si ne s\'affiche pas en tant que scotcher',  //   '“触屏版”功能，在帖子列表页面中是否显示置顶贴，否为不显示',//  
      23 => 'setting_mobile_simpletype',
      24 => 'Disposition mobile',//'手机版式',  //  'Mobile layout',
      25 => 'La valeur par défaut est "Disponible Standard", à savoir la disposition du téléphone mobile complet. La "mise en page simple" désigne l\'ensemble des icônes a été retiré de la page de liste des sujets. Note: Les deux modes d\'émoticônes sont pris en charge, et un utilisateur peut basculer librement entre elle.',//'默认为“标准版”，标准版为全功能手机版式。“无图精简版”则为去掉小图标、主题列表日期等页面元素的精简版式注意：两种模式下表情均解析，且用户可以自由切换',   //   'The default is "Standard layout" i.e. the full-featured mobile phone layout. The "Simple Layout" means all the icons was removed from the Topic List page. Note: The two modes of emoticons are supported, and a user can freely switch between it.',
      26 => 'setting_mobile_topicperpage',
      27 => 'Rubriques par page',//'每页显示主题数',//  'Topics per page',
      28 => 'Le nombre de rubriques figurant dans la page Liste des sujets du topic, la valeur recommandée est de 10',//'主题列表页每页显示主题个数，推荐值为10',//  'The number of topics shown at the Topic List page, recommended value is 10', 
      29 => 'setting_mobile_postperpage',
      30 => 'Rubriques Posts par page',//'主题内每页显示帖数',//   'Topic Posts per page',
      31 => 'Le nombre de posts affichés sur la page Rubrique, la valeur recommandée est de 5',//'主题内每页显示的帖子数目，推荐值为5',//  'The number of posts shown at the Topic page, recommended value is 5',
      32 => 'setting_mobile_cachetime',
      33 => 'Pages vues temps de cache',//'页面浏览缓存时间',// 'Page views cache time', 
      34 => 'Régler le cache de temps en direct pour les pages vues comptoir. Vide ou 0 pour non mise en cache, unité: secondes',//'设置访问过的页面在用户手机上保存的时间，留空或0为不缓存单位：秒', //   'Set the cache live time for page views counter. Blank or 0 for no caching, unit: seconds',
      35 => 'setting_mobile_index_forumview',
      36 => 'Accueil des forums mode affichage',//'首页分区展示方式',//  'Forum Home display mode',
      37 => 'Quand est réglé sur "Echouer", le Forum accueil affiche par catégorie uniquement rubriques par défaut, et les utilisateurs doivent cliquer sur la catégorie pour afficher la liste des sous-forums de la catégorie.',//'如“收起”，论坛首页将默认只展示分区标题用户需点击分区展开该分区下的版块列表',
      38 => 'setting_mobile_come_from',
      39 => 'Mobile Style Personnalisé Origine du Post',//'手机发帖来源自定义', //  'Mobile Post Source custom style',
      40 => 'Par défaut est vide. Si vous remplissez cette rupture, les utilisateurs verront un marqueur "de mobile" dans la page du poste. Ajouter un texte ou des icônes tels que personnalisé: &lt;a href=\'http://m.x.com\'&gt; &lt;/a&gt;. Les balises HTML prises en charge: &lt;a&gt;, &lt;font&gt;, &lt;span&gt;, &lt;strong&gt;, &lt;b&gt;, &lt;img&gt;. Si vous utilisez des icônes, s\'il vous plaît assurer la hauteur de l\'image est de 16 pixels ou moins.',//'默认留空，如果填写，将在看帖页面"来自手机"处增加自定义文字或图标如:&lt;a href=\'http://m.x.com\'&gt;手机频道&lt;/a&gt;此处支持HTML标签有：&lt;a&gt;&lt;font&gt;&lt;span&gt;&lt;strong&gt;&lt;b&gt;&lt;img&gt;如果使用图标，请保证图片高度在16px(像素)以内',
      41 => 'setting_mobile_wml',
      42 => 'Que ce soit pour activer la version miniaturisée', //  '是否启用极简版',
      43 => 'La version minimalisée de certains langage WML uniquement en charge les périphériques antérieurs à parcourir le contenu du site', //  '极简版为一些仅支持WML语言的较早期的设备浏览网站的内容',
    ),
  ),
  292 =>
  array (
    'index' =>
    array (
      'Gestion Partage' => 'action=share',//'分享管理', // 'Share Management'  
    ),
    'text' =>
    array (
      0 => 'Gestion Partage',//'分享管理', //   'Share Management',
      1 => 'share_tips',
      2 => 'Gestion des lots est utilisé pour retirer les actions de l\'utilisateur. Attention: Cliquez sur le bouton Supprimer dans la page de résultats de recherche va supprimer les informations directement!',//'批量分享管理用于删除分享(share)使用。提醒：点击搜索结果页的删除按钮，将会直接删除相关信息！',  //   'Batch management is used to remove the user shares. Warning: Click on the Delete button in the search results page will delete the information directly!',
    ),
  ),
  293 =>
  array (
    'index' =>
    array ( 
      'Partage Gestion' => 'action=share',//'分享管理', //   'Share Management' 
      'Recherche' => 'action=share&search=true',//'搜索', //  'Search'
    ),
    'text' =>
    array (
      0 => 'Partage Gestion &raquo; Recherche',//'分享管理 &raquo; 搜索', //  'Share Management &raquo; Search',
      1 => 'share_search_detail',
      2 => 'Afficher la liste des actions détaillés',//'显示详细分享列表', //  'Show detailed share list',
      3 => 'share_search_perpage',
      4 => 'Articles par page',//'每页显示数', //  'Items per page',
      5 => 'share_search_icon',
      6 => 'Partage icône type',//'分享类型', //  'Share type icon',
      7 => 'share_search_uid',
      8 => 'ID Utilisateur',//'用户 UID', //   'User ID',
      9 => 'UID Séparez par une virgule ","',//'多 UID 中间请用半角逗号 "," 隔开', //  'Separate multiple UID with a comma ","',
      10 => 'share_search_user',
      11 => 'Partage nom d\'auteur',//'发表分享用户名', //  'Share author name',
      12 => 'Plusieurs noms d\'utilisateurs séparés par une virgule ","',//'多用户名中间请用半角逗号 "," 隔开', //  'Separate multiple user names by a comma ","',
      13 => 'share_search_sid',
      14 => 'Partage ID',//'分享 ID', //  'Share ID',
      15 => 'ID Séparez par une virgule ","',//'多分享 ID 中间请用半角逗号 "," 隔开', //   'Separate multiple ID with a comma ","',
      16 => 'share_search_hot',
      17 => 'Hot',//'热度', //   'Hot',
      18 => 'share_search_time',
      19 => 'Post Plage de temps',//'发表时间范围', //   'Post Time range',
      20 => 'Format est aaaa-mm-jj',//'格式 yyyy-mm-dd', //   'Format is yyyy-mm-dd',
    ),
  ),
  294 =>
  array (
    'index' =>
    array (
      'Gestion Sourire' => 'action=smilies',//'表情管理', //  'Smile management'
      'Types de Sourire' => 'action=smilies',//'表情分类', //  'Smile types' 
    ),
    'text' =>
    array (
      0 => 'Gestion Sourire &raquo; Types de Sourire',//'表情管理 &raquo; 表情分类', // 'Smile management &raquo; Smile types', 
      1 => 'smilies_tips_smileytypes',
      2 => 'Pour exporter le fichier de type sourire contenant les informations de code de sourire, s\'il vous plaît télécharger via FTP. S\'il vous plaît ne pas permettre de trop nombreux types de sourire! Et n\'oubliez pas de vérifier le type sourire longueur du nom, de sorte que la fenêtre de sourire peut affiche tous les noms de type de sourire. Vous pouvez définir un type de sourire par défaut. Pour l\'ensemble de supprimer un type de sourire s\'il vous plaît supprimer tous les sourires en dessous.',//'表情导出包含表情分类信息及表情代码信息，表情图片文件请通过 FTP 下载。请不要同时启用过多的表情分类，并控制好表情分类的名称长度，以免表情窗口无法完全显示所有表情分类。你可以在界面风格中设置默认显示的表情分类。删除表情分类请先删除该分类下所有表情。',  //  'For export the smile type file containing the smile code information, please download it via FTP. Please do not enable of too many smile types! And sure to check the smile type name length, so the smile window can displays all the smile type names. You can set a default smile type. For delete entire smile type please delete all the smiles under it.',
    ),
  ),
  295 =>
  array (
    'index' =>
    array (
      'Gestion Sourire' => 'action=smilies',//'表情管理', //  'Smile management'
      'Importer sourires' => 'action=smilies&operation=import',//'导入表情', //  'Import smiles'
    ),
    'text' =>
    array (
      0 => 'Gestion Sourire &raquo; Importer sourires',//'表情管理 &raquo; 导入表情', //   'Smile management &raquo; Import smiles',
      1 => 'smilies_tips',
      2 => 'Pour importer un type de sourire dont vous avez besoin pour télécharger des fichiers d\'image appropriés pour l\'annuaire static/image/smiley/your_smile_type/. Format pris en charge: "JPG, GIF, PNG". Nom seuls les numéros de fichiers, des lettres, de soulignement, des espaces et half-width -.&[]() symboles. Fichier longueur du nom ne peut pas dépasser 30 octets , autrement il ne sera pas reconnu. Ne réglez pas trop l\'expression, afin de ne pas prendre trop les ressources du serveur.',//'添加表情请上传表情图片到相应的表情目录中(static/image/smiley/表情目录/)。表情支持"JPG、GIF、PNG"格式的图片，文件名只允许数字、26 个英文字母、下划线、半角空格及 -.&[]() 等符号，文件名长度不能超过 30 字节，否则将会无法识别。请不要设置过多的表情，以免占用过多的服务器资源。',   //  'For import a smile type you need to upload appropriate image files to the static/image/smiley/your_smile_type/ directory. Supported format: "JPG, GIF, PNG". file name only numbers, letters, underscores, spaces, and half-width -.&[]() symbols. File name length can not exceed 30 bytes , otherwise it will be not recognized. Do not set too much expression, so as not to take up too much server resources.',
    ),
  ),
  296 =>
  array (
    'index' =>
    array (
      'Gestion du Style' => 'action=styles',//'风格管理', //  'Style Management' 
    ),
    'text' =>
    array (
      0 => 'Gestion du Style',//'风格管理', //  'Style Management',
      1 => 'styles_admin_tips',
      2 => 'Si les fichiers de style exportés stockés dans le répertoire de modèle, vous pouvez gérer ou installer le style directement.',//'如果把导出的风格文件放置在模板目录下，则可以通过风格管理直接安装风格',   //  'If the exported style files placed in the template directory, you can manage or install the style directly.',
    ),
  ),
  297 =>
  array (
    'index' =>
    array (
      'Gestion Tag' => 'action=tag',//'标签管理', //  'Tag Management' 
    ),
    'text' =>
    array (
      0 => 'Gestion Tag',//'标签管理', //  'Tag Management',
      1 => 'tagname',
      2 => 'Nom du Tag',//'标签名称', //  'Tag name',
      3 => 'feed_search_perpage',
      4 => 'Articles par page',//'每页显示数', //  'Items per page',
      5 => 'misc_tag_status',
      6 => 'Statut',//'状态', //  'Status',
    ),
  ),
  298 =>
  array (
    'index' =>
    array (
      'La gestion des Sujets du Forum' => 'action=threads',//'论坛主题管理', //  'Forum Thread Management'
      'Dernière liste' => 'action=threads',//'最新列表', //  'Latest list'
    ),
    'text' =>
    array (
      0 => 'La gestion des Sujets du Forum &raquo; Dernière liste',//'论坛主题管理 &raquo; 最新列表', //  'Forum Thread Management &raquo; Latest list',
      1 => 'threads_tips',
      2 => 'Avec la gestion des lots de cette discussion, vous pouvez rechercher des rubriques pour le lot à supprimer, déplacer, se déplacer à des catégories, supprimer, Top/Untop, résumé/undiges, ouvrir/fermer les discussions, et supprimer les pièces jointes du sujet et d\'autres opérations. S\'il vous plaît recherche de sujets en fonction de vos conditions, puis sélectionnez l\'action appropriée.',//'通过批量主题管理，你可以对搜索到的主题进行批量删除、移动、分类/取消分类、删除、置顶/取消置顶、设置/取消精华、打开/关闭以及删除主题中的附件等操作；请先根据条件搜索主题，然后选择相应的操作。',        //    'With the Thread batch management you can search topics for batch delete, move, move to categories, delete, Top/Untop, digest/undiges, open/close threads, and delete the thread attachments and other operations. Please search for threads based on your conditions, and then select the appropriate action.',
    ),
  ),
  299 =>
  array (
    'index' =>
    array (
      'La gestion des Sujets du Forum' => 'action=threads',//'论坛主题管理',  //   'Forum Thread Management'
      'Recherche' => 'action=threads&search=true',//'搜索', //  'Search'
    ),
    'text' =>
    array (
      0 => 'La gestion des Sujets du Forum &raquo; Recherche',//'论坛主题管理 &raquo; 搜索', //  'Forum Thread Management &raquo; Search',
      1 => 'threads_search_detail',
      2 => 'Voir la liste détaillée des sujets',//'显示详细主题列表', //   'Show detailed list of threads',
      3 => 'threads_search_perpage',
      4 => 'Articles par page',//'每页显示数', //   'Items per page',
      5 => 'threads_search_time',
      6 => 'Post Plage de temps',//'发表时间范围', //   'Post Time range',
      7 => 'Format est aaaa-mm-jj.Mettre à 0 pour aucune restriction',//'格式 yyyy-mm-dd，不限制请输入 0', //  'Format is yyyy-mm-dd. Set to 0 for no restrictions',
      8 => 'threads_search_user',
      9 => 'Démarrer par Utilisateur',//'主题作者', // 'Started by user',  
      10 => 'Plusieurs noms d\'utilisateurs séparés par une virgule ","',//'多用户名中间请用半角逗号 "," 隔开', // 'Separate multiple user names by a comma ","', 
      11 => 'threads_search_keyword',
      12 => 'Mots-clés',//'标题关键字', //   'Keywords',
      13 => 'Séparez les mots clés par une virgule ","',//'多关键字中间请用半角逗号 "," 隔开', //  'Separate multiple keywords by a comma ","',
      14 => 'threads_search_sort',
      15 => 'Recherche catégorie info',//'所在分类信息', //   'Search category info',
      16 => 'threads_search_viewrange',
      17 => 'Visionnages de la Gamme',//'被浏览次数介于', //  'Views range',
      18 => 'threads_search_replyrange',
      19 => 'Réponses Gamme',//'被回复次数介于', //   'Replies range',
      20 => 'threads_search_readpermmore',
      21 => 'Les autorisations de lecture supérieures',//'所需阅读权限高于', //  'Read permissions higher than',
      22 => 'threads_search_pricemore',
      23 => 'Prix supérieur',//'售价高于', //   'Price higher than',
      24 => 'threads_search_noreplyday',
      25 => 'À combien de jours sans nouvelle réponse',//'多少天内无新回复', //  'How many days of no new replies',
      26 => 'threads_search_type',
      27 => 'Rechercher dans les catégories',//'所在主题分类', //  'Search in categories',
      28 => 'threads_search_sticky',
      29 => 'Qu\'ils soient inclus posts Scotchés',//'是否包含置顶帖', //   'Whether included sticked posts',
      30 => 'threads_search_digest',
      31 => 'Contient l\'Essentiel des posts',//'是否包含精华帖', //   'Does have a digest',
      32 => 'threads_search_attach',
      33 => 'Contient pièces jointes',//'是否包含附件', //   'Contains attachments',
      34 => 'threads_rate',
      35 => 'S\'il faut inclure des notes de posts',//'是否包含评分帖', //   'Does have rating',
      36 => 'threads_highlight',
      37 => 'Ont en surbrillance les discussions',//'是否包含高亮帖', //   'Have highlighted threads',
      38 => 'threads_save',
      39 => 'Que ce soit pour inclure des projets de discussions',//'是否包含草稿', //  'Whether to include draft threads',
    ),
  ),
  300 =>
  array (
    'index' =>
    array (
      'Discussion Fractionner' => 'action=threadsplit',//'主题分表', //  'Thread Split' 
      'Gestion des sous-table' => 'action=threadsplit&operation=manage',//'分表管理', // 'Sub-table management' 
    ),
    'text' =>
    array (
      0 => 'Discussion Fractionner &raquo; Gestion des sous-table',//'主题分表 &raquo; 分表管理', //  'Thread Split &raquo; Sub-table management',
      1 => 'threadsplit_manage_tips',
      2 => 'Le sujet de la fonction de sous-table est séparée de la table de forum_thread (y compris le titre du fil de la discussion, le temps de création et d\'autres informations, mais ne comprend pas le message du sujet et des réponses) sur plusieurs tables. Quand une nouvelle table d\'archive du fil de la discussion créé, vous pouvez utiliser la fonction de déplacement du sujet à passer une discussion à une table d\'archive spécifié. Quand un sujet est déplacé à la table d\'archivage, c\'est un forum prochain sujet créé dans le domaine des archives (le nom de l\'archive peut être personnalisé), lors de la navigation dans la zone d\'archive, il sera le fil de la table d\'archivage. Discussions dans le table d\'archivage peuvent être parcourues seulement, mais pas répondu ou prévus ou gérées, mais vous pouvez le supprimer ou le déplacer vers une zone non-archivage.',//'主题分表功能可将 forum_thread 表(包含主题标题、创建时间等信息，但不包含主题及回复内容)分在多个数据表中。创建新的主题存档表后，可使用主题移动功能将特定主题移动到存档表中。移动到存档表中的主题，会在主题所在的版块下建立一个存档区(名称可自定义)，通过存档区可浏览存档表中的主题。存档表中的主题，只供浏览，不可回复、评分，不能进行管理操作，但可以删除和移动到非存档区。',                //   'The subject of the sub-table feature is separate the forum_thread table (including the thread title, creation time and other information, but does not include the thread post and replies) into multiple tables. When a new thread archive table created, you can use the thhread moving functionality to move a thread to a specified archive table. When a thread is moved to the archive table, it is a next forum thread created in the archive area (the archive name can be customized), when browsing through the archive area it will be the archive table thread. Threads in Archive table may be only browsed, but not replied or rated or managed, but you can delete it or move to a non-archive area.',
      3 => 'threadsplit_manage_table_orig',
      4 => 'Sujet info principale de la table',//'thread 主表信息', //  'Main thread table info',
      5 => 'threadsplit_manage_table_archive',
      6 => 'Archive Sujet d\'info de la table',//'thread 存档表信息', //  'Archive thread table info',
    ),
  ),
  301 =>
  array (
    'index' =>
    array (
      'Sujet Fractionner' => 'action=threadsplit',//'主题分表', //  'Thread Split' 
      'Déplacer Sujet' => 'action=threadsplit&operation=move',//'主题移动', //  'Move Thread'
    ),
    'text' =>
    array (
      0 => 'Sujet Fractionner &raquo; Déplacer Sujet',//'主题分表 &raquo; 主题移动', //  'Thread Split &raquo; Move Thread',
      1 => 'threadsplit_move_tips',
      2 => 'Discussion en fonction du déplacement est conçu pour déplacer les discussions entre la table d\'origine (forum_thread) et la nouvelle  table d\'archive à la discussion du sujet. Avant de déplacer les discussions, assurez-vous qu\'il ya au moins une table d\'archive du fil de la discussion (nom de la table forum_thread_1, forum_thread_2, etc.) Sélectionner les discussions que vous souhaitez déplacer en utilisant la recherche, puis sélectionnez la table cible pour terminer le transfert de données.',//'主题移动功能可将特定的主题在原始表(forum_thread)和新建的主题存档表之间移动。移动主题之前，请确定存在至少一个主题存档表(表名为 forum_thread_1、forum_thread_2……)。通过主题搜索筛选出要移动的主题，再选择目标表，即可完成主题数据的移动。',   //   'Thread move function is designed for move threads between the original table (forum_thread) and new thread archive table. Before move threads, make sure there is at least one thread archive table (i.e. table name forum_thread_1, forum_thread_2, etc.). Select threads you want to move using the search, and then select the target table to complete the data movement.',
      3 => 'threads_search_detail',
      4 => 'Voir la liste détaillée des discussions',//'显示详细主题列表', //   'Show detailed list of threads',
      5 => 'threads_search_sourcetable',
      6 => 'Consulter la table du sujet suivant',//'搜索以下 thread 分表', //  'Search the following thread table',
      7 => 'threads_search_forum',
      8 => 'Recherche dans les forums',//'所在版块', // 'Search in Forums', 
      9 => 'threadsplit_move_tidrange',
      10 => 'Plage ID du Sujet',//'tid 范围', //   'Thread ID range',
      11 => 'threads_search_noreplyday',
      12 => 'À combien de jours sans nouvelle réponse',//'多少天内无新回复', //   'How many days of no new replies',
      13 => 'threads_search_time',
      14 => 'Post Time range',//'发表时间范围', //  
      15 => 'Format est aaaa-mm-jj. Mettre à 0 pour aucune restriction',//'格式 yyyy-mm-dd，不限制请输入 0', //   'Format is yyyy-mm-dd. Set to 0 for no restrictions',
      16 => 'threads_search_type',
      17 => 'Rechercher dans les catégories',//'所在主题分类',  //  'Search in categories',
      18 => 'threads_search_sort',
      19 => 'Recherche catégorie info',//'所在分类信息', //   'Search category info',
      20 => 'threads_search_viewrange',
      21 => 'Visionnages Gamme',//'被浏览次数介于', //  'Views range', 
      22 => 'threads_search_replyrange',
      23 => 'Réponses Gamme',//'被回复次数介于', //  'Replies range',
      24 => 'threads_search_readpermmore',
      25 => 'Les autorisations de lecture supérieures',//'所需阅读权限高于', // 'Read permissions higher than', 
      26 => 'threads_search_pricemore',
      27 => 'Prix supérieur',//'售价高于', //  'Price higher than',
      28 => 'threads_search_keyword',
      29 => 'Mots-clés',//'标题关键字', //   'Keywords',
      30 => 'Séparez les mots clés par une virgule ","',//'多关键字中间请用半角逗号 "," 隔开', //   'Separate multiple keywords by a comma ","',
      31 => 'threads_search_user',
      32 => 'Démarré par utilisateur',//'主题作者', //  'Started by user',
      33 => 'Plusieurs noms d\'utilisateurs séparés par une virgule ","',//'多用户名中间请用半角逗号 "," 隔开', //  'Separate multiple user names by a comma ","',
      34 => 'threads_search_type',
      35 => 'Rechercher dans les catégories',//'所在主题分类', //   'Search in categories',
      36 => 'threads_search_sticky', 
      37 => 'Qu\'ils soient inclus posts Scotchés',//'是否包含置顶帖', //  'Whether included sticked posts',
      38 => 'threads_search_digest',
      39 => 'Avoir un Résumé',//'是否包含精华帖', //  'Have a digest',
      40 => 'threads_search_attach',
      41 => 'Contient pièces jointes',//'是否包含附件', //  'Contains attachments',
      42 => 'threads_rate',
      43 => 'Contient Note',//'是否包含评分帖', //  'Contains rating',
      44 => 'threads_highlight',
      45 => 'Que ce soit pour le Titre ont mis en évidence',//'是否包含高亮帖', //  'Whether to have highlighted title',
      46 => 'threads_list',
      47 => 'Liste de Discussion',//'主题列表', //  'Thread list',
      48 => 'threadsplit_move_threads_per_time',
      49 => 'Déplacez nombre de Rubriques par tranche du moment',//'每次移动主题数', //   'Move number of Topics per each time',
      50 => 'recommandé  pas plus de 1,000',//'建议不超过 1000',  //   'recommended not more than 1000',
    ),
  ),
  302 =>
  array (
    'index' =>
    array (
      'Mettre à jour mémoire cache' => 'action=tools&operation=updatecache',//'更新缓存',  //   'Update cache'
    ),
    'text' =>
    array (
      0 => 'Mettre à jour mémoire cache',//'更新缓存',  //  
      1 => 'tools_updatecache_tips',
      2 => 'Lorsque a été récupéré les données du site, mis à jour, ou quelque autre travail inhabituel, vous pouvez utiliser cette fonction pour générer le cache.
				Mettre à jour la mémoire cache peut augmenter la charge du serveur, donc s\'il vous plaît essayer d\'éviter l\'heure de pointe, lorsque de nombreuses visites des membres.
				Mémoire cache de données: mettre à jour tout le cache de données du site.
				Cache de modèles: Mettre à jour les modèles de forum, les styles et les fichiers cache lorsque vous avez modifié un modèle ou un modèle, mais il n\'y a pas d\'effet visible immédiatement.
				BRICO DIY  blocs cache: mettre à jour les blocs de BRICO, utilisés lorsque vous avez installé ou modifié un bloc de BRICO, mais n\'a pas voir un effet visuel immédiatement.
				',
    ),
  ),
  303 =>
  array (
    'index' =>
    array (
      'Groupes d\'utilisateurs' => 'action=usergroups',//'用户组',  //  'User Groups'
    ),
    'text' =>
    array (
      0 => 'Groupes d\'utilisateurs',//'用户组',  //  'User Groups',
      1 => 'usergroups_tips',
      2 => 'Les groupes d\'utilisateurs sont divisés en groupes de systèmes, les groupes membres et des groupes personnalisés. Lorsque des groupes membres déterminés au groupe d\'accéder par des points, les groupes du système et des groupes personnalisés sont artificiels et ne seront pas modifiées par le système proprement dit. Les groupes de paramètres du système et des groupes personnalisés n\'ont pas besoin de spécifier le points. Discuz! réservée huit états du système à partir de clients pour l\'Administrateur du site, lorsque les utilisateurs d\'appartenance des groupe personnalisé besoin d\'être modifié pour ajouter. Il est exigé au moins deux groupes de membres. Si le minimum de points sont négatifs (soit égale à 0), il va provoquer certains utilisateurs peuvent ne pas correspondre au groupe.',//'用户组分为系统组、会员组和自定义组，会员组以积分确定组别和权限，而系统组和自定义组是人为设定，不会由系统自行改变。系统组和自定义组的设定不需要指定积分，Discuz! 预留了从站点管理员到游客等的 8 个系统头衔，自定义组的用户需要在编辑会员时将其加入。会员用户组至少分为两组，其积分下限分别为负值(任一负数)和 0，否则将导致部分用户无法与用户组匹配的问题。',   //  'User groups are divided into system groups, Member groups and custom groups. When Member Groups determined the group access by points, the system groups and custom groups are artificial, and will not changed by the system itself. The System groups and custom groups settings do not need to specify the points. Discuz! reserved eight system statuses from Guests to the site administrator, when for the Custom group membership users need to be edited to add. It is required at least two Member Groups. If the minimum points are negative (either equal to 0), it will cause some users will can not match the Group.',
    ),
  ),
  304 =>
  array (
    'index' =>
    array (  
      'Groupes d\'utilisateurs' => 'action=usergroups',//'用户组',  //  'User Groups'
      'Réglages de base' => 'action=usergroups&operation=edit&anchor=system',//'基本设置',  // 'Basic settings' 
    ),
    'text' =>
    array (
      0 => 'Groupes d\'utilisateurs &raquo; Réglages de base',//'用户组 &raquo; 基本设置',  // 'User Groups &raquo; Base settings', 
      1 => 'usergroups_edit_system_public',
      2 => 'Groupes d\'utilisateurs publics',//'公众用户组',  //  'Public User Groups',
      3 => 'Indiquez si le groupe d\'utilisateurs est public. Remarque: Avant d\'utiliser cette fonction assurez-vous qu\'il est vraiment nécessaire, sinon l\'utilisateur une fois rejoint le groupe en lui-même, même après avoir réglé le groupe des non-public, l\'utilisateur ne peut toujours pas être retiré du groupe, sauf si vous supprimez le groupe . Pour éviter les risques de sécurité, groupes publics ne peuvent pas être associés à des groupes de gestion.',//'设置是否为公众用户组。注意: 使用此功能前请确定是否确实需要，否则用户一旦自行加入本组，即便之后再设置本组为非公众用户组，仍然无法将用户从该组中批量去除，除非删除本用户组。公众用户组不能关联管理组，以免产生安全隐患',   //   'Set whether the user group is public. Note: Before using this feature make sure it is really necessary, otherwise the user once joined the group by himself, even after setting the group of Non-public, the user still can not be removed from the the group, unless you delete the Group. To avoid security risks, Public Groups can not be associated with Manager groups.',
      4 => 'usergroups_edit_system_dailyprice',
      5 => 'Groupes chargés Prix public',//'收费公众用户组日价格',  //  'Charged Public Groups price',
      6 => 'Si vous demandez aux utilisateurs de payer des points avant de rejoindre le groupe d\'utilisateurs, saisissez le prix d\'achat du groupe, et l\'unité utilisée pour la transaction. Par exemple, si un prix est fixé à 2, puis pour acheter une permission de groupe de 30 jours, il est tenu de payer 2 * 30 = 60 points; Après la période de 30 jours l\'utilisateur sera automatiquement quitté le groupe. Si est réglé à 0, alors ce groupe est un groupe public gratuit. Remarque: Cette fonction nécessite d\'abord de fixer le groupe d\'utilisateurs que groupe public, puis a fixé le prix d\'acquisition à prendre effet.',//'如果你需要用户支付相应的交易积分后才能加入本用户组，请在此输入本组的日购买价格，单位为交易积分所使用的单位。例如设置为 2，则用户购买为期 30 天的本组权限，需要支付 2*30=60 个交易积分，30 天有效期后该用户将自动从本组中退出。如设置为 0，则本组为免费公众用户组。注意: 本功能需要首先设定用户组为公众用户组且设置了有效的交易积分后才有效',   //   'If you require users to pay points before joining the user group, enter the group purchase price, and the unit used for transaction. For example, if a price is set to 2, then to purchase a 30-day group permission it is required to pay 2 * 30 = 60 points; After 30-day period the user will automatically exited from the group. If is set to 0, then this group is a free public Group. Note: This feature requires first to set the user group as public Group, and then set the purchase price to take effect.',
      7 => 'usergroups_edit_system_minspan',
      8 => 'Délai minimum pour acheter groupes publics payants (jours)',//'收费公众用户组一次最短购买时间(天)',  //  'Minimum time to purchase chargeable public Groups (days)',
      9 => 'Définissez le nombre de jours au moins pour acheter à ce groupe d\'autorisations par une transaction de paiement, définissez la valeur supérieure ou égale à 1. Par exemple: Si un prix tous les jours est fixé à 2 et la période de du moment minimum est de 10 jours, afin que l\'utilisateur devra payer au moins 2 * 10 = 20 points pour rejoindre ce groupe et obtenir un accès au moins pendant 10 jours à ce groupe. Note: Pour que cette option soit efficace, il est nécessaire, le groupe d\'utilisateur doit être un Groupe public.',//'设置用户在购买本组权限时，一次支付价值至少多少日的交易积分，请设置为大于等于 1 的数值。例如日价格为 2，最短购买时间为 10，则用户至少需要花费 2*10=20 个交易积分，获得至少 10 天本组权限，才能加入本组。注意: 本功能需要首先设定用户组为收费公众用户组才有效',   //   'Set the number of days at least to purchase this group permissions per one payment transaction, Set the value greater than or equal to 1. For example: If a dayly price is set to 2 and minimum time period is 10 days, so the user must pay at least 2 * 10 = 20 points to join this group and obtain an access at least for 10 days to this group. Note: For this feature to be effective it is required the user group must be a public Group.',
    ),
  ),
  305 =>
  array (
    'index' =>
    array (
      'Groupes d\'utilisateurs' => 'action=usergroups',//'用户组',  //   'User Groups' 
      'Réglages de base' => 'action=usergroups&operation=edit&anchor=basic',//'基本设置',  //  'Basic settings'
    ),
    'text' =>
    array (
      0 => 'Groupes d\'utilisateurs &raquo; Réglages de base',//'用户组 &raquo; 基本设置',  //  'User Groups &raquo; Base settings',
      1 => 'usergroups_edit_basic',
      2 => 'Réglages de base',//'基本设置',  //  'Basic settings',
      3 => 'usergroups_edit_basic_title',
      4 => 'Titre du Groupe Utilisateur',//'用户组头衔',  //  'User group title',
      5 => 'usergroups_icon',
      6 => 'Icône du Groupe',//'组图标',  //  'Group icon',
      7 => 'usergroups_edit_basic_visit',
      8 => 'Autoriser l\'accès au site',//'访问站点权限',  //  'Allow access to the site',
      9 => 'Choisir "Non" totalement sera désactiver l\'accès à n\'importe quelle page du site',//'禁止访问: 禁止用户浏览本站任何页面正常访问: 当站点开放的时候，可以正常浏览页面超级访问: 即便站点关闭也可以访问，一般用于管理员或站内测试组',
      10 => 'usergroups_edit_basic_read_access',
      11 => 'Lecture des autorisations',//'阅读权限',  //  'Read permissions',
      12 => 'Établir un niveau d\'autorisation de l\'utilisateur pour les posts de navigation ou des pièces jointes, allant de 0 à 255. Mettre à 0 pour empêcher les utilisateurs de parcourir les posts ou les pièces jointes. Lorsque l\'utilisateur ayants des droits de lecture inférieur à ce paramètre (1 par défaut), l\'utilisateur ne sera pas capable de lire les posts ou de télécharger des pièces jointes.',//'设置用户浏览帖子或附件的权限级别，范围 0～255，0 为禁止用户浏览任何帖子或附件。当用户的阅读权限小于帖子或附件的阅读权限许可(默认时为 1)时，用户将不能阅读该帖子或下载该附件',    //   'Set user permission level for browse posts or attachments, ranging from 0 to 255. Set to 0 for prevent users from browsing any posts or attachments. When the user read permissions less than this setting (default is 1), then the user will not be able to read posts or download attachments.',
      13 => 'usergroups_edit_basic_max_friend_number',
      14 => 'Le nombre maximum d\'amis',//'最多好友数',  //   'Max number of friends',
      15 => 'Défini sur 0 pour illimitée',//'0为无限',  //  'Set to 0 for Unlimited',
      16 => 'usergroups_edit_basic_domain_length',
      17 => 'Longueur minimum du Nom sous-domaine',//'二级域名最短长度',  //  'Minimum length of the subdomain name',
      18 => 'Défini sur 0 pour désactiver le second noms de domaine de premier niveau. Réglez à plus de 2 caractères pour être efficace.',//'0为禁止使用二级域名，在站点开启二级域名时有效',  //   'Set to 0 for disable the second level domain names. Set to more than 2 character to be effective.',
      19 => 'usergroups_edit_basic_invisible',
      20 => 'Autoriser mode furtif',//'允许隐身',  //   'Allow Stealth mode',
      21 => 'Choisissez si vous voulez permettre à l\'utilisateur de se connecter sans apparaître dans la liste en ligne',//'设置是否允许用户登录后不显示在在线列表中',   //   'Set whether to allow user to log in without appear in the online list',
      22 => 'usergroups_edit_basic_allowtransfer',
      23 => 'Autoriser les points de transfert',//'允许积分转账',  //  'Allow transfer points',
      24 => 'Choisissez si vous voulez permettre aux utilisateurs de transférer des points à d\'autres utilisateurs. Remarque: Cette fonction est activée uniquement si les paramètres globaux "Activer le transfert de points" est activée.',//'设置是否允许用户在个人中心将自己的交易积分转让给其他用户。注意: 本功能需在 全局设置中启用交易积分后才可使用', //   'Set whether to allow users to transfer points to other users. Note: This function is enabled only if the global settings "Enable points transfer" enabled.',
      25 => 'usergroups_edit_basic_allowsendpm',
      26 => 'Autoriser à envoyer des messages privés',//'允许发送短消息',  //   'Allow to send private messages',
      27 => 'Choisissez si vous voulez autoriser les utilisateurs à envoyer des messages privés',//'设置是否允许用户发送短消息',  // 'Set whether to allow users to send private messages', 
      28 => 'usergroups_edit_pm_sendpmmaxnum',
      29 => 'Le nombre maximum de MPs en 24 heures',//'24小时内发布短消息最大数',  //  'Maximum number of PM per 24 hours',
      30 => 'Définissez le nombre maximum de messages courts publiés pour le groupe d\'utilisateurs dans les 24 heures. Il est efficace pour prévenir MP le spam en vrac. Défini sur 0 pour aucune restriction.',//'设置该用户组用户24小时内发布短消息的最大数，有效的控制短消息发布广告的情况, 0为不限制', //  'Set the maximum number of published short messages for the user group within 24 hours. It is effective for prevent bulk PM spamming. Set to 0 for no restriction.',
      31 => 'usergroups_edit_pm_sendallpm',
      32 => 'Que ce soit pour permettre d\'envoyer des MPs à tous',//'是否可以给任何人发短消息',  //   'Whether to allow to send PM to all',
      33 => 'Si vous sélectionnez Non, les utilisateurs ne peuvent envoyer / accepter des MPs à/des amis uniquement, et ne seront pas en mesure d\'envoyer des MPs à tous les autres',//'选择否的话，当对方设置为只接受好友短消息，将无法对其发送短消息',  //  'If Select No, then users can only send/accept PM to/from friends only, and will not be able to send PM to all others',
      34 => 'usergroups_edit_post_html',
      35 => 'Autoriser à utiliser le code HTML',//'允许使用 HTML 代码',  //   'Allow to use HTML code',
      36 => 'Note: Le code HTML peut causer des problèmes de sécurité, veuillez utiliser avec prudence! Recommandé pour activer uniquement en cas d\'extrême nécessité, et lui permettre seulement à l\'équipe de gestion de base.',//'注意: 开放 HTML 功能将产生安全隐患，请慎用。建议只在十分必要的情况下使用，并限制只开放给最核心的管理人员', //  'Note: HTML code will cause safety problems, please use with caution! Recommended to enable only in cases of extreme necessity, and enable it only to core management team.',
      37 => 'usergroups_edit_post_url',
      38 => 'Autoriser de publier URL extérieur',//'允许发站外URL',  //   'Allow to publish outside URL',
      39 => 'Que ce soit pour permettre à inclure l\'URL dans un message. Ce réglage permet de réduire efficacement la quantité de posts publicitaires.',//'是否允许发帖包含URL，合理的设置可以有效的减少广告帖的量', //   'Whether to allow include URL in a post. This setting can effectively reduce the amount of advertising posts.',
      40 => 'usergroups_edit_basic_allow_statdata',
      41 => 'Autoriser pour voir les statistiques du site',//'允许查看站点统计',  //   'Allow to view the site statistics',
      42 => 'usergroups_edit_basic_search_post',
      43 => 'Autoriser la recherche plein texte',//'允许全文搜索',  //  'Allow full text search',/
      44 => 'Ce paramètre n\'est efficace que sur les forums de recherche. Note: Lorsque le volume de données sont volumineuses, la recherche en texte intégral serait très coûteuse pour les ressources du serveur, donc à utiliser avec prudence.',//'此设置只对搜索论坛有效。注意: 当数据量大时，全文搜索将非常耗费服务器资源，请慎用',  //   'This setting is effective only on the search forums. Note: When the volume of data is large, the full-text search would be very expensive to the server resources, so use with caution.',
      45 => 'usergroups_edit_basic_search',
      46 => 'Autoriser utiliser la recherche',//'允许使用搜索',  //   'Allow to use search',
      47 => 'Sélectionnez les groupes d\'utilisateurs que vous souhaitez activer la recherche pour cet article',//'选择你要为此用户组开启的搜索项目',  //  'Select the user groups you want to enable the search for this item',
      48 => 'usergroups_edit_basic_reasonpm',
      49 => 'MP Avis avec un motif d\'action',//'操作理由短消息通知作者',  //   'PM notification with a reason of action',
      50 => 'Entrez le motif de cette action',//'设置用户在对他人评分或管理操作时是否强制输入理由和通知作者',  //  'Enter the action reason',
      51 => 'usergroups_edit_basic_cstatus',
      52 => 'Autoriser Titre Personnalisé',//'允许自定义头衔',  //  'Allow Custom title',
      53 => 'Choisissez si vous voulez permettre aux utilisateurs de définir leur propre titre et afficher le titre dans un post',//'设置是否允许用户设置自己的头衔名字并在帖子中显示',  //  'Set whether to allow users to set their own title and display the title in a post',
      54 => 'usergroups_edit_basic_disable_periodctrl',
      55 => 'Période de latence',//'不受时间段限制',   //  'Silent period',
      56 => 'Définir une période pendant laquelle un nouvel utilisateur ne peut pas poster n\'importe quoi',//'设置用户是否可以不受全局设置中设置的特殊时间段禁令限制',  //  'Set a period of time when new user can not post anything', 
      57 => 'usergroups_edit_basic_hour_threads',
      58 => 'Discussions limite par heure',//'会员每小时发主题数限制',  //  'Threads per hour limit',
      59 => 'Ce paramètre permet le nombre maximum de sujets publiés par heure. Utilisé pour empêcher un spam en limitant les envois par nombres. La plage est de 1 ... 255. Défini sur 0 pour aucune restriction. Cette caractéristique va légèrement augmenter la charge du serveur, et n\'est pas utilisé pour les invités',//'设置允许会员每小时最多的发主题数量，可以配合灌水预防功能进一步限制会员的发帖，可设置为 1～255 范围内的数值，0 为不限制。此功能会轻微加重服务器负担，且对游客无效',
      60 => 'usergroups_edit_basic_hour_posts',
      61 => 'Posts par heure limite',//'会员每小时发回帖数限制',  //  'Posts per hour limit',
      62 => 'Définir un nombre maximum de posts qui vont permettre aux utilisateurs de poster un post par heure. Ceci est utile pour limiter l\'irrigation ou un spam intempestif. Peut être mis à plage de valeurs 1 à 255, 0 pour aucune limite. Cette caractéristique va légèrement augmenter la charge du serveur, et non une incidence pour les invités.',//'设置允许会员每小时最多的发回帖数量，可以配合灌水预防功能进一步限制会员的发帖，可设置为 1～255 范围内的数值，0 为不限制。此功能会轻微加重服务器负担，且对游客无效',
      63 => 'usergroups_edit_basic_seccode',
      64 => 'Permettre pour utiliser le code de vérification anti-spam ou des questions et réponses de sécurité',//'启用防灌水验证码或验证问答机制',   //  'Enable to use anti-spam verification code or security questions and answers',
      65 => 'Activation de ce mécanisme anti-spam peut empêcher l\'activité anti-spam des robots ou bots, mais va augmenter la difficulté de l\'opération pour les utilisateurs habituels. Ce réglage est efficace lorsque la modification du Mot de passe aussi, si le système permet le code de vérification ou des questions de vérification et de réponses.',//'开启防灌水验证机制可以防止灌水机等，但会增加用户操作易用度。修改密码不受此限制，开启防灌水验证码或验证问答后，该设置有效',
      66 => 'usergroups_edit_basic_forcesecques',
      67 => 'Contraint de mettre les questions de sécurité',  //  '强制设置安全提问',  
      68 => 'Sélectionnez "Oui", puis ce groupe d\'utilisateurs doit être définie avant l\'ouverture de session des questions de sécurité en arrière-plan seulement après son retour, config pour le passage Globale', //  '选择“是”则此用户组的用户在登录后台前必须设置安全提问后才可登陆后台, config中为全局开关',
      69 => 'usergroups_edit_basic_disable_postctrl',
      70 => 'Post contrôle de restriction ',//'发表不受限制',  //   'Post control restriction',
      71 => 'Définir le nombre maximum de caractères de poste pour Empêcher l\'irrigation spam.',//'设置发表是否不受灌水预防和最大字数等',  //  'Set the maximum number of post characters for prevent irrigation.',
      72 => 'usergroups_edit_basic_ignore_censor',
      73 => 'Ignorer la vérification de la censure',//'忽略需要审核的关键字',  //   'Ignore the censor verification',
      74 => 'Si sélectionné &quot;Oui&quot;, alors les utilisateurs du groupe ne déclencheront pas la nécessité de revoir leur contenu',//'选择“是”则此用户组的用户在发表时不会触发需要审核的关键字',
      75 => 'usergroups_edit_basic_allowcreatecollection',
      76 => 'Admis nombre de collections',//'允许用户创建淘专辑的数量',  //   'Allowed number of collections',
      77 => 'Réglez sur "0" pour désactiver la collecte création',//'“0”表示不允许创建淘专辑',  //  'Set to "0" for disable collection creating',
      78 => 'usergroups_edit_basic_allowfollowcollection',
      79 => 'Le nombre maximum de collections suivies',//'最多允许关注淘专辑的数量',  //   'Maximum number of followed collections',
      80 => 'Autoriser les utilisateurs à ne suivre uniquement le nombre limité de collections',//'允许用户最多关注淘专辑的数量',  //  'Allow users to follow only this limited number of collections',
      81 => 'usergroups_edit_basic_close_ad',
      82 => 'Que ce soit pour fermer les impressions d\'annonces',  // '是否关闭广告展示',  //  
      83 => 'Sélectionnez "Oui", puis ce groupe d\'utilisateur ne peut pas voir d\'annonces sur le site',  //  '选择“是”则此用户组的用户在站点中看不到任何广告',
    ),
  ),
  306 =>
  array (
    'index' =>
    array (
      'Groupes d\'utilisateurs' => 'action=usergroups',//'用户组',  //  'User Groups'
      'Sujet Spécial' => 'action=usergroups&operation=edit&anchor=special',//'特殊主题',  //  'Special thread'
    ),
    'text' =>
    array (
      0 => 'Groupes d\'utilisateurs &raquo; Sujet Spécial',//'用户组 &raquo; 特殊主题',  //   'User Groups &raquo; Special thread',
      1 => 'usergroups_edit_special',
      2 => 'Sujet Spécial',//'特殊主题',  //   'Special thread',
      3 => 'usergroups_edit_special_activity',
      4 => 'Autoriser à créer des événements',//'允许发起活动',  //  'Allow to create events',
      5 => 'Choisissez "Oui" pour autoriser à ajouter de nouveaux événements',//'选择“是”允许在论坛发布活动主题',  //  'Select "Yes" for allow add new events',
      6 => 'usergroups_edit_special_poll',
      7 => 'Autoriser créer sondage',//'允许发起投票',  //  'Allow to create poll',
      8 => 'Choisissez "Oui" to allow add new polls',//'选择“是”允许在论坛发布投票主题',  //  'Select "Yes" to allow add new polls', 
      9 => 'usergroups_edit_special_vote',
      10 => 'Autoriser de voter',//'允许参与投票',  //   'Allow to vote',
      11 => 'Choisissez "Oui" pour permettre de voter les sondages sur le forum',//'选择“是”允许在论坛参与投票',  //  'Select "Yes" to allow vote the Forum polls',
      12 => 'usergroups_edit_special_reward',
      13 => 'Autoriser pour créer une récompense',//'允许发起悬赏',  //   'Allow to create reward',
      14 => 'Choisissez "Oui" pour autoriser ajouter de nouvelles récompenses',//'选择“是”允许在论坛发布悬赏主题，悬赏使用交易积分',  //   'Select "Yes" for allow add new rewards',
      15 => 'usergroups_edit_special_reward_min',
      16 => 'Points de fidélité minimum',//'最小悬赏积分',  //   'Minimum reward points',
      17 => 'Définir des points de fidélité minimales pour chaque récompense (minimum est 1)',//'每笔悬赏最少悬赏积分(最小设置为 1)',  //  'Set minimal reward points for each reward (minimum is 1)',
      18 => 'usergroups_edit_special_reward_max',
      19 => 'Points de fidélité maximum',//'最大悬赏积分',  // 'Maximum reward points',  
      20 => 'Définir des points de fidélité maximales pour chaque rendement (au maximum 32767, mis à 0 pour aucune limite)',//'每笔悬赏最多悬赏的积分，最大可设置为 65535 (不限制请输入 0)',   //   'Set maximal reward points for each reward (maximum is 32767, set to 0 for no limits)',
      21 => 'usergroups_edit_special_trade',
      22 => 'Autoriser la vente des produits',//'允许发布商品',  //   'Allow to sale products',
      23 => 'Choisissez "Oui" pour permettre aux utilisateurs de publier le fil de discussion pour la vente des produits',//'选择“是”允许在论坛发布商品主题',  //   'Select "Yes" to allow users to publish the forum thread for sale products',
      24 => 'usergroups_edit_special_trade_min',
      25 => 'Montant de transaction minimal',//'最小交易金额',  //   'Minimum transaction amount',
      26 => 'Définit le degré minimum de transaction par le commerce (unité: EURO ou des points, la valeur minimale est de 1 par transaction)',//'每笔交易的最小交易金额(单位：人民币元或积分单位，最小交易金额为 1 元)',   //  'Set the minimum transaction amount per trade (unit: USD or points, the minimal value is 1 per transaction)',
      27 => 'usergroups_edit_special_trade_max',
      28 => 'Montant maximale des transactions',//'最大交易金额',  //  'Maximum transaction amount',
      29 => 'Définit le degré de transaction au maximum par le commerce (unité: EUR ou des points, Défini sur 0 pour aucune limite)',//'设置每笔交易所能设置的最大交易金额(单位：人民币元或积分单位，不限制请输入 0)',  //  'Set the maximum transaction amount per trade (unit: USD or points, Set to 0 for no limit)',
      30 => 'usergroups_edit_special_trade_stick',
      31 => 'Le nombre maximum de produits recommandés',//'商品主题最多推荐商品数',  //   'Maximum number of recommended products',
      32 => 'Définir un certain nombre de produits recommandés à l\'Index de l\'utilisateur',//'设置用户在每篇商品主题中最多推荐商品的数目',  //   'Set a number of recommended products at user home',
      33 => 'usergroups_edit_special_debate',
      34 => 'Autoriser pour créer le débat',//'允许发起辩论',  //  'Allow to create debate',
      35 => 'Choisissez "Oui" pour autoriser ajouter nouveau débat',//'选择“是”允许在论坛发布辩论主题',  //   'Select "Yes" for allow add new debate',
      36 => 'usergroups_edit_special_rushreply',
      37 => 'Protéger les réponses cotées',//'允许发表抢楼帖',  //   'Protect quoted replies',
      38 => 'Ne permettent pas aux utilisateurs de supprimer des réponses cotées',//'抢楼帖不允许用户删除回帖',  //'Does not allow users to delete quoted replies',  
      39 => 'usergroups_edit_special_allowthreadplugin',
      40 => 'Autoriser l\'utilisation des plugins en discussion spéciale',//'允许发布扩展特殊主题',  //  'Allow use plugins in special thread',
      41 => 'Choisissez si vous voulez autoriser l\'utilisation aux modules sujet spécial',//'设置是否允许发表扩展的特殊主题',  //  'Set whether to allow use plugins in special thread',
    ),
  ),
  307 =>
  array (
    'index' =>
    array (
      'Groupes d\'utilisateurs' => 'action=usergroups',//'用户组',  //  'User Groups'  
      'Post des autorisations' => 'action=usergroups&operation=edit&anchor=post',//'帖子相关',  //   'Post permissions'
    ),
    'text' =>
    array (
      0 => 'Groupes d\'utilisateurs &raquo; Post des autorisations',//'用户组 &raquo; 帖子相关',  //  'User Groups &raquo; Post permissions',
      1 => 'usergroups_edit_post',
      2 => 'Post permissions',//'帖子相关',  //  'Post permissions',
      3 => 'usergroups_edit_post_new',
      4 => 'Autoriser de créer un nouveau Sujet',//'允许发新话题',  //  'Allow to create new thread',
      5 => 'Choisissez si vous voulez autoriser de créer un nouveau sujet. Remarque: l\'utilisateur peut ouvrir une nouvelle discussion que lorsque le groupe utilisateur ont l\autorisations de lecture est soit supérieure à 0.',//'设置是否允许发新话题。注意: 只有当用户组阅读权限高于 0 时，才能发新话题',    //   'Set whether to allow create a new thread. Note: user can create a new thread only when the user group READ permissions is higher than 0.', 
      6 => 'usergroups_edit_post_reply',
      7 => 'Autoriser de répondre à la discussion',//'允许发表回复',  //  'Allow to reply a thread',
      8 => 'Choisissez si vous voulez permettre aux utilisateurs de répondre. Remarque: l\'utilisateur peut publier une réponse que lorsque le groupe utilisateur ont l\autorisations de lecture est soit supérieure à 0.',//'设置是否允许发表回复。注意: 只有当用户组阅读权限高于 0 时，才能发表回复',  //  'Set whether to allow users to reply. Note: user can publis a reply only when the user group READ permissions is higher than 0.', 
      9 => 'usergroups_edit_post_direct',
      10 => 'Autoriser de diriger l\'affectation du post',//'允许直接发帖',  //   'Allow direct posting',
      11 => 'Que ce soit pour permettre de publier un sujet ou de répondre directement ou exiger une vérification. Ce paramètre n\'a aucun effet si le Forum ne nécessite pas de vérification.',//'是否允许直接发帖或者对回复及主题进行审核，此设置受版块审核影响，如果版块设置为不需要审核，则此设置无效',  //   'Whether to allow publish thread or reply directly or require a verification. This setting has no effect if the Forum does not require verification.',
      12 => 'usergroups_edit_post_allow_down_remote_img',
      13 => 'Autoriser de télécharger des images à distance',//'允许下载远程图片',  //  'Allow to download remote images',
      14 => 'Sauvegarder les images sous forme de fichiers locaux',//'远程图片本地化保存',  //  'Save remote images as local files',
      15 => 'usergroups_edit_post_anonymous',
      16 => 'Autoriser les post anonymes',//'允许发匿名帖',  //  'Allow anonymous postings',
      17 => 'Que ce soit pour permettre aux utilisateurs de répondre et Commencer une discussion en mode anonyme, si elle est activée dans le groupe de l\'utilisateur ou les paramètres du forum. Anonyme l\'affectation du post est différente de l\'affectation du post invité, les utilisateurs doivent se connecter avant de l\'utiliser, et les modérateurs et administrateurs peuvent visualiser le véritable auteur',//'是否允许用户匿名发表主题和回复，只要用户组或本论坛允许，用户均可使用匿名发帖功能。匿名发帖不同于游客发帖，用户需要登录后才可使用，版主和管理员可以查看真实作者',   //   'Whether to allow users to reply and create thread in anonymous mode, if it is enabled in user group or the forum settings. Anonymous posting is different from guest posting, users need to log in before use this, and moderators and administrators can view the real author.',
      18 => 'usergroups_edit_post_set_read_perm',
      19 => 'Autoriser de définir des autorisations de post',//'允许设置帖子权限',  //  'Allow to set post permissions',
      20 => 'Ce paramètre permettre aux utilisateurs de spécifier un post ayants des droits de lecture',//'设置是否允许设置帖子需要指定阅读权限才可浏览',  // 'This setting allow users to specify a post read permissions',
      21 => 'usergroups_edit_post_maxprice',
      22 => 'Sujet (pièce jointe) prix le plus élevé',//'主题(附件)最高售价',  //  'Thread (Attachment) highest price',
      23 => 'Le sujet ou la pièce jointe peut être payant. Cela signifie que le contenu du fil de la discussion ou une pièce jointe sera caché jusqu\'à ce que l\'utilisateur paie pour voir le fil de la discussion ou la pièce jointe. Réglez ici le prix maximum autorisé pour le vendeur. Défini sur 0 pour désactiver la vente. Remarque: Cette fonction prend effet uniquement si les paramètres globaux de points de négociation sont activés. Si vous souhaitez activer les Modérateurs et Administrateurs à modifier les sujets chargés (pièce jointe), de fixer le prix maximum plus élevé que pour les groupes d\'utilisateurs habituels, de sorte qu\'après la modification du prix de post ne sera pas tranché.',//'主题(附件)出售使得作者可以将自己发表的主题(附件)隐藏起来，只有当浏览者向作者支付相应的交易积分后才能查看主题(附件)内容。此处设置用户出售主题(附件)时允许设置的最高价格，0 为不允许用户出售。注意: 本功能需在 全局设置中启用交易积分后才可使用。如开启版主及管理员的出售主题(附件)功能，请将其最高价格设置为高于普通用户组允许的最高价格，这样在帖子被编辑时不至于导致主题(附件)售价的硬性下调',   //  'Thread or Attachment can be chargeable. This means the thread content or attachment will be hidden until the user pay for view the thread or attachment. Set here the maximum price allowed for seller. Set to 0 for disable sell. Note: This function take effect only if the global trading points settings enabled. If you want to enable moderators and administrators to edit charged threads (Attachment), set the maximum price higher than for ordinary user groups, so that after editing the post price will not be cutted.',
      24 => 'usergroups_edit_post_hide_code',
      25 => 'Autoriser l\'utilisation de  la balise [hide] (caché)',//'允许使用 [hide] 代码',  //  'Allow to use [hide] tag',
      26 => 'Choisissez si vous voulez autoriser à utiliser la balise [hide] dans un post',//'设置是否允许帖子中使用 [hide] 隐藏标签',  //  'Set whether to allow to use [hide] tag in a post',
      27 => 'usergroups_edit_post_mediacode',
      28 => 'Autoriser [audio], [media], [flash] et autre multimédia',//'允许使用 [audio] [media] [flash] 等多媒体代码',  //  'Allow [audio], [media], [flash] and other multimedia',
      29 => 'Choisissez si vous voulez autoriser à utiliser le [audio], [media], [flash] et autres balise multimédia',//'设置是否允许 [audio] [media] [flash] 等多媒体代码',  //  'Set whether to allow to use the [audio], [media], [flash] and other multimedia tags', 
      30 => 'usergroups_edit_post_begincode',
      31 => 'Autoriser l\'utilisation de la balise [begin]',// '允许使用 [begin] 代码',   //  'Allow to use  [begin] tag', 
      32 => 'Choisissez si vous voulez autoriser pour utiliser tag d\'animation [begin] AU Début ', // '设置是否允许帖子中使用 [begin] 开头动画标签',  //   'Set whether to allow to use [begin] Début du Tag d\'animation',
      33 => 'usergroups_edit_post_sig_bbcode',
      34 => 'Autoriser BB-Codes dans la signature',//'允许签名中使用 Discuz! 代码',  //   'Allow BB-Codes in signature',
      35 => 'Que ce soit pour permettre aux utilisateurs d\'utiliser BB-Codes dans leur signature',//'设置是否解析用户签名中的 Discuz! 代码',  //  'Whether to allow users to use BB-Codes in their signature',
      36 => 'usergroups_edit_post_sig_img_code',
      37 => 'Autoriser [img] BB-Code dans la signature',//'允许签名中使用 [img] 代码',
      38 => 'Que ce soit pour permettre aux utilisateurs d\'utiliser Code [img]  dans leur signature',//'设置是否解析用户签名中的 [img] 代码',  //   'Whether to allow users to use [img] Code in their signature',
      39 => 'usergroups_edit_post_max_sig_size',
      40 => 'Longueur maximale de la signature',//'签名文字最大长度',  //  'Maximum signature length',
      41 => 'Définir le nombre maximum de caractères dans une signature de l\'utilisateur, Défini sur 0 pour désactiver la signature',//'设置用户签名最大字节数，0 为不允许用户使用签名',  //  'Set the maximum number of characters in a user signature, Set to 0 for disable signature',
      42 => 'usergroups_edit_post_recommend',
      43 => 'Autoriser recommande Sujet',//'主题评价影响值',  //   'Allow to recomend thread',
      44 => 'Définir le nombre de fois qu\'un utilisateur peut recommander un thème. La cote du sujet est un certain nombre de recommandations d\'utilisation. Défini sur 0 pour désactiver les utilisateurs de recommander les discussions.',//'设置用户评价一次主题，对主题评价指数的影响，0 为不允许用户评价主题',  //   'Set how many times a user can recommend a theme. The thread rating is a number of user recommendations. Set to 0 for disable users to recommend threads.',
      45 => 'usergroups_edit_post_edit_time_limit',
      46 => 'Modifier le délai du post (minutes)',//'编辑帖子时间限制(分钟)',   //   'Edit Post time limit (minutes)',
      47 => 'Fixe le délai en temps après la publication, lorsque l\'utilisateur peut modifier le contenu du post. Les Modérateurs et Administrateurs peuvent éditer les messages à tout moment. Défini sur 0 pour aucune limite.',//'帖子作者发帖后超过此时间限制将不能再编辑帖，版主和管理员在 全局 - 用户权限 选择“允许用户随时编辑的帖子类型”后不受此限制，0 为不限制',   //   'Set a time period after posting, when a user can edit the post content. Moderators and administrators can edit posts at any time. Set to 0 for no limit.',
      48 => 'usergroups_edit_post_allowreplycredit',
      49 => 'Autoriser la sentence des Réponses des Récompenses',//'允许设置回帖奖励',   //  'Allow to set the Replies award',
      50 => 'Autoriser aux utilisateurs de publier certains sujets où les réponses données peuvent récompenser en points. Réponses attribuer des points disponibles dans le globale par défaut - Pour définir les points spécifiés',//'允许用户在发布主题时给予回帖者一定的扩展积分奖励。回帖奖励默认积分可在全局 - 积分设置指定',  //  'Allow users to publish certain topics where given Replies can reward points. Replies award points available in the Global by default - set the specified points',
      51 => 'usergroups_edit_post_tag',
      52 => 'Autoriser l\'utilisation des balises',//'允许使用标签',    //   'Allow to use tags',
      53 => 'Choisissez si vous voulez autoriser à utiliser les tags lors de publier une nouvelle discussion',//'设置是否允许发表新话题时使用标签',  //  'Set whether to allow to use tags when publish new thread',
      54 => 'usergroups_edit_post_allowcommentpost',
      55 => 'Autoriser un commentaire',//'帖子直接点评',    //  'Allow to Comment',
      56 => 'Choisissez si vous voulez autoriser aux utilisateurs de commenter les messages. Note: ce paramètre prend effet uniquement si les paramètres globaux permet poster des commentaires.',//'设置允许点评的范围，如不选择表示不启用点评功能注意：只有在 全局 - 站点功能 - 帖子点评 中开启直接点评功能，本设置才会生效',   //   'Set whether to allow users to comment posts. Note: this setting take effect only if the global settings allows post comments.',
      57 => 'usergroups_edit_post_allowcommentreply',
      58 => 'Autoriser Propriétaire de commenter les réponses',//'楼层回复点评',  //  'Allow Owner to comment replies',
      59 => 'Permettre à l\'utilisateur d\'afficher réponse à d\'autres réponses en cliquant sur le lien "Répondre", afin de générer automatiquement la position des commentaires. Remarque: le réglage ne prend effet que si globalement - Les caractéristiques du site - Post au sol  fonctionnalité de réponse Commentaire du message est activée. Cliquez sur le lien "Répondre" de la discussion ne produira pas le sujet des commentaires.',//'允许用户通过点击帖子中的“回复”链接回复他人回帖时，自动对该楼层产生点评注意：只有在 全局 - 站点功能 - 帖子点评 中开启楼层回复功能，本设置才会生效；点击主题中的“回复”链接，不会对主题产生点评',   //   'Allow user to post reply to other replies by clicking the "Reply" link, automatically generate the Comments position. Note: the setting takes effect Only if the Global - Site features -  Post floor Comment reply feature enabled. Click on the thread "Reply" link will not produce the thread Comments.',
      60 => 'usergroups_edit_post_allowcommentitem',
      61 => 'Autoriser voir les commentaires publiés',//'允许用户在点评时发表观点',  // 'Allow view published comments', 
      62 => 'Que ce soit pour autoriser aux utilisateurs d\'afficher leurs commentaires. Remarque: le réglage prendra effet que si le paramètre global est réglé pour permettre à voir le post commentaire après affichage.',//'允许用户在点评时发表观点注意：只有在 全局 - 站点功能 - 帖子点评 中开启直接点评功能，本设置才会生效',   //   'Whether to allow users to view their comments. Note: the setting take effect only if the global setting is set to enable view post comment after posting.',
      63 => 'usergroups_edit_post_allowat',
      64 => 'Nombre de @name utilisé',//'发帖时可 @ 其他人的数量',   //   'Number of @name used',
      65 => 'Mettre à 0 pour désactiver l\'utilisation du @name (y compris les suivis ou adeptes et amis)',//'0为不允许 @ 其他人（包括他关注的人和他的好友）',   //  'Set to 0 for disable using of @name (including the followings and friends)',
      66 => 'usergroups_edit_post_allowsetpublishdate',
      67 => 'Laissez se figer le temps de publication',//'允许设置预发帖时间',   //  'Allow to set the publish time',
      68 => 'Autoriser aux utilisateurs de définir l\'heure de départ indiquée lors de la publication de la discussion . Il peut être réglé que dans chaque demi-heure (comme 13h00 ou 13h30) pour publier automatiquement.',//'允许用户在发布主题时设置指定的发帖时间。只能设置在每半小时（如 13:00 或 13:30）自动发布。',    //  'Allow users to set the specified post time when publishing of thread. It can be set only in every half hour (such as 13:00 or 13:30) for automatically published.',
      69 => 'usergroups_edit_post_allowcommentcollection',
      70 => 'Autoriser les commentaires de collection',//'允许评论淘专辑',   //  'Allow collection comments',
      71 => 'Autoriser aux utilisateurs de commenter les discussions dans les collections.',//'允许用户在主题和淘专辑页面进行评论。',   //  'Allow users to comment threads in collections.', 
      72 => 'usergroups_edit_post_allowimgcontent',
      73 => 'Autoriser le contenu thématique image a généré',  //  '允许主题内容生成图片',
      74 => 'Posts, tous les messages seront caractères sous la forme de contenu texte généré exposition d\'image.', //   '看帖时所有帖子正文内容将以字符形式生成图片展示。',  
    ),
  ),
  308 =>
  array (
    'index' =>
    array (
      'Groupes Utilisateurs' => 'action=usergroups',//'用户组',    //    'User Groups'
      'Permissions des pièces jointes' => 'action=usergroups&operation=edit&anchor=attach',//'附件相关',    //'Attachment permissions'   
    ),
    'text' =>
    array (
      0 => 'Groupes Utilisateurs &raquo; Permissions des pièces jointes',//'用户组 &raquo; 附件相关',  //    'User Groups &raquo; Attachment permissions',
      1 => 'usergroups_edit_attach',
      2 => 'Permissions des pièces jointes',//'附件相关',  // 'Attachment permissions',  
      3 => 'usergroups_edit_attach_get',
      4 => 'Autoriser à télécharger/Voir les pièces jointes',//'允许下载附件',   //    'Allow to download/view attachments',
      5 => 'Que ce soit pour autoriser aux utilisateurs de télécharger/visualiser des pièces jointes',//'设置是否允许在没有设置特殊权限的论坛中下载附件',  // 'Whether to allow users to download/view attachments', 
      6 => 'usergroups_edit_attach_getimage',
      7 => 'Autoriser de visualiser les images',//'允许查看图片',  //  'Allow to view images',
      8 => 'Définissez si vous autoriser de voir des images dans un forum sans autorisations spéciales',//'设置是否允许在没有设置特殊权限的论坛中查看图片',  //  'Set whether to allow to view pictures in a forum without special permissions', 
      9 => 'usergroups_edit_attach_post',
      10 => 'Autoriser de télécharger des pièces jointes',//'允许上传附件',  //  'Allow to upload attachments',
      11 => 'Que ce soit pour permettre aux utilisateurs de télécharger des pièces jointes. Reportez-vous au Centre de gestion de la vérification si chargement est activé dans PHP.',//'设置是否允许上传附件到没有设置特殊权限的论坛中。需要 PHP 设置允许才有效，请参考管理中心首页',   //  'Whether to allow users to upload attachments. Refer to the Management Center for check if upload is enabled in PHP.', 
      12 => 'usergroups_edit_attach_set_perm',
      13 => 'Autoriser pour définir des autorisations de pièce jointe',//'允许设置附件权限',  //  'Allow to set attachment permissions', 
      14 => 'Que ce soit pour permettre aux utilisateurs de définir des autorisations pour la fixation de pièce jointes en téléchargement',//'设置是否允许设置附件需要指定积分以上才可下载',  //   'Whether to allow users to set permissions for download attachment',
      15 => 'usergroups_edit_image_post',
      16 => 'Autoriser de télécharger des images',//'允许上传图片',    //  'Allow to upload images',
      17 => 'Que ce soit pour permettre aux utilisateurs de télécharger des images. Reportez-vous au Centre de gestion de la vérification si chargement est activé dans PHP.',//'设置是否允许上传图片到没有设置特殊权限的论坛中。需要 PHP 设置允许才有效，请参考管理中心首页',   //   'Whether to allow users to upload images. Refer to the Management Center for check if upload is enabled in PHP.',
      18 => 'usergroups_edit_attach_max_size',
      19 => 'Taille maximale des pièces jointes (Ko)',//'论坛最大附件尺寸(单位K 1M=1024K)',  //   'Maximum attachment size (Kb)',
      20 => 'Définissez la taille maximale des pièces jointes (en Ko, 1 Mo = 1024Ko). Mettre à 0 pour aucune limite. Reportez-vous au Centre de gestion de la vérification PHP activé taille de téléchargement.',//'设置附件最大尺寸(单位K 1M=1024K)，0 为不限制，需要 PHP 设置允许才有效，请参考管理中心首页',  //   'Set the maximum attachment size (in Kb, 1Mb = 1024Kb). Set to 0 for not limit. Refer to the Management Center for check PHP enabled upload size.',
      21 => 'usergroups_edit_attach_max_size_per_day',
      22 => 'Taille maximale des pièces jointes quotidienne (Ko)',//'每天最大附件总尺寸(单位K 1M=1024K)',  //   'Daily maximum attachment size (Kb)',
      23 => 'Définir le volume des pièces jointes totale (en Ko), que les utilisateurs peuvent télécharger toutes les 24 heures. Mettre à 0 pour aucune limite. Remarque: Cette fonction va ajouter le chargement du serveur, il est recommandé d\'utiliser uniquement lorsque c\'est nécessaire.',//'设置用户每天可以上传的附件总量(单位K 1M=1024K)，0 为不限制',   //  Set the total attachments volume (in Kb), that users can upload every 24 hours. Set to 0 for no limit. Note: This feature will add the server loading, it is recommended to use only when necessary.
      24 => 'usergroups_edit_attach_max_number_per_day',
      25 => 'Le nombre maximum de pièces jointes par jour',//'每天最大附件数量',   //   'Max number of attachments per day',
      26 => 'Définissez le nombre de pièces jointes que les utilisateurs peuvent télécharger toutes les 24 heures. Mettre à 0 pour aucune limite.',//'设置用户每天可以上传的附件数量，0 为不限制',   //  'Set the number of attachments, that users may upload every 24 hours. Set to 0 for no limit.', 
      27 => 'usergroups_edit_attach_ext',
      28 => 'Activer types de pièces jointes',//'允许附件类型',   // 'Enabled attachment types',  
      29 => 'Réglez des extensions compatibles de pièces jointes, plusieurs extensions séparées par une virgule ",". Laissez ce champ vide pour aucune limite.',//'设置允许上传的附件扩展名，多个扩展名之间用半角逗号 "," 隔开，留空为不限制',   //    'Set enabled attachments extensions, separate multiple extensions with a comma ",". Leave blank for not limit.',
    ),
  ),
  309 =>
  array (
    'index' =>
    array (
      'Groupes Utilisateurs' => 'action=usergroups',//'用户组',   //    'User Groups'
      'Magie connexe' => 'action=usergroups&operation=edit&anchor=magic',//'道具相关',   //  'Magic related' 
    ),
    'text' =>
    array (
      0 => 'Groupes Utilisateurs &raquo; Magie connexe',//'用户组 &raquo; 道具相关',    //    'User Groups &raquo; Magic related',
      1 => 'usergroups_edit_magic',
      2 => 'Magie connexe',//'道具相关',   //   'Magic related',
      3 => 'usergroups_edit_magic_permission',
      4 => 'Autoriser d\'utiliser la magie',//'允许使用道具',   //   'Allow to use magic',
      5 => 'Permettre l\'utilisation de la magie, y compris la vente et les autorisations actuelles',//'是否允许使用道具，包括买卖、赠送的权限',   //  'Permit the use of magic, including the sale and present permissions',
      6 => 'usergroups_edit_magic_discount',
      7 => 'Acheter réduction de magie',//'购买道具折扣',   //   'Buy magic discount',
      8 => 'Définissez une valeur  de magie actuelle d\'achat, allant de 1 à 9. La plus petite valeur va donner moins de rabais. Mettre à 0 pour aucune remise.',//'购买道具的折扣值，范围 1~9 数值越小折扣越多，0 为不享受折扣',   //    'Set a buy magic discount value, range from 1 to 9. The smaller value will give less discount. Set to 0 for no discount.',
      9 => 'usergroups_edit_magic_max',
      10 => 'Capacité de magies empaquetés',//'道具包容量',   //    'Magic package capacity',
      11 => 'Définir un poids maximum de magie pour ce groupe d\'utilisateurs, dans la gamme de 0 à 60000.',//'本组用户带有道具的最大重量，范围 0~60000',   //   'Set a maximum weight of magic for this user group, range from 0 to 60000.',
    ),
  ),
  310 =>
  array (
    'index' =>
    array (
      'Groupes Utilisateurs' => 'action=usergroups',//'用户组',   //   'User Groups'
      'Inscription connexe' => 'action=usergroups&operation=edit&anchor=invite',//'注册相关',   //   'Registration related' 
    ),
    'text' =>
    array (
      0 => 'Groupes Utilisateurs &raquo; Inscription connexe',//'用户组 &raquo; 注册相关',   //   'User Groups &raquo; Registration related', 
      1 => 'usergroups_edit_invite',
      2 => 'Inscription connexe',//'注册相关',   //   'Registration related',
      3 => 'usergroups_edit_invite_permission',
      4 => 'Autoriser d\'acheter un code d\'invitation',//'允许购买邀请码',   //   'Allow to purchase an invitation code', 
      5 => 'Que ce soit pour permettre aux utilisateurs d\'acheter le code d\'invitation',//'是否允许购买邀请码的权限',   //  'Whether to allow users to buy invitation code', 
      6 => 'usergroups_edit_invite_send_permission',
      7 => 'Autoriser à envoyer une invitation pour l\'inscription',//'允许发送邀请注册',   //'Allow to send invitation for Registration',   
      8 => 'Que ce soit pour autoriser l\'envoi d\'un code d\'invitation par le système de messagerie du site',//'是否允许通过站点邮件系统发送邀请码',    //  'Whether to allow sending an invitation code by the site mail system', 
      9 => 'usergroups_edit_invite_price',
      10 => 'Invitation prix d\'acquisition du code',//'邀请码购买价格',   //   'Invitation code price',
      11 => 'Établir un prix de code d\'invitation, les membres des groupes d\'utilisateurs pour acheter',//'该用户组的会员购买邀请码的价格',   //    'Set a price of invitation code, the user group members to buy',
      12 => 'usergroups_edit_invite_buynum',
      13 => 'Le nombre maximum de codes d\'invitation peut être acheté dans un délai de 24 heures',//'24小时内购买邀请码最大数量',    //   'The maximum number of invitation codes can be purchased within 24 hours',
      14 => 'Le code nombre maximum d\'invitation, les membres des groupes d\'utilisateurs peuvent acheter en 24 heures, plus que ce montant ne sera pas en mesure d\'acheter',//'该用户组24小时内购买邀请码的最大数量，超过此数量将不能购买新的邀请码',   //  'The invitation code maximum number, the user group members can buy in 24 hours, more than this amount will not be able to buy',
      15 => 'usergroups_edit_invite_maxinviteday',
      16 => 'Code Invitation heure valide',//'邀请码有效期',  //   'Invitation code valid time',
      17 => 'Le délai maximal de jours, les membres du groupe d\'utilisateurs peut utiliser le code d\'invitation acheté. Après cette période, le code d\'invitation sera automatiquement expiré. Défaut est 10.',//'该用户组购买邀请码的最大有效期，超过此有效期验证码将自动失效，单位为天，默认为10',     //  'The maximum period of days, the user group members can use the purchased invitation code. After this period the invitation code will automatically expired. Default is 10.',
    ),
  ),
  311 =>
  array (
    'index' =>
    array (
      'Groupes Utilisateurs' => 'action=usergroups',//'用户组',  //  'User Groups'  
      'MPs connexes' => 'action=usergroups&operation=edit&anchor=credir',//'消息相关',  //   'PM related' 
    ),
    'text' =>
    array (
      0 => 'Groupes Utilisateurs &raquo; MPs connexes',//'用户组 &raquo; 积分相关',  //  'User Groups &raquo; Points related', 
      1 => 'usergroups_edit_credit',
      2 => 'Points connexes',//'积分相关',  //    'Points related',
      3 => 'usergroups_edit_credit_exempt_sendpm',
      4 => 'Pour diminuer points pour Envoyer un MPs',//'发短消息不扣积分',  //   'Not decrease points for Send PM',
      5 => 'usergroups_edit_credit_exempt_search',
      6 => 'Ne pas diminuer de points pour la Recherche',//'搜索不扣积分',  //   'Not decrease points for Search',
      7 => 'usergroups_edit_credit_exempt_getattch',
      8 => 'Ne diminuera pas des points pour télécharger des pièces jointes',//'下载附件不扣积分',  //   'Not decrease points for download attachments',
      9 => 'usergroups_edit_credit_exempt_attachpay',
      10 => 'Télécharger frais de montage direct',//'可直接下载收费附件',  //   'Direct download attachment fee',
      11 => 'usergroups_edit_credit_exempt_threadpay',
      12 => 'Frais de visualisation de la discussion du sujet directement',//'可直接查看收费主题',  //    'Direct thread view fee',
      13 => 'usergroups_edit_credit_allowrate',
      14 => 'Autoriser les membres à noter',//'允许参与评分',   //    'Allow to rate members',
    ),
  ),
  312 =>
  array (
    'index' =>
    array (
      'Groupes Utilisateurs' => 'action=usergroups',//'用户组',  //   'User Groups'
      'Espace connexe' => 'action=usergroups&operation=edit&anchor=home',//'空间相关',  //   'Space related'
    ),
    'text' =>
    array (
      0 => 'Groupes Utilisateurs &raquo; ',//'用户组 &raquo; 空间相关',  //   'User Groups &raquo; ',
      1 => 'usergroups_edit_home',
      2 => 'Espace connexe',//'空间相关',  //  'Space related',
      3 => 'usergroups_edit_attach_max_space_size',
      4 => 'Taille de l\'espace (Mo)',//'空间大小(单位M)',   //  'Space size (Mb)',
      5 => 'Définissez la taille maximale de l\'espace (Mo), 0 pour ne pas limiter',//'设置最大空间尺寸(单位M)，0 为不限制', //  'Set the maximum space size (Mb), 0 for not limit',
      6 => 'usergroups_edit_home_allow_blog',
      7 => 'Autoriser Publier Blog',//'发表日志', //   'Allow Publish blog',
      8 => 'usergroups_edit_home_allow_blog_mod',
      9 => 'Blogs Publiés doivent être vérifiés',//'发表日志需要审核', //  'Published blogs must be verified',
      10 => 'usergroups_edit_home_allow_doing',
      11 => 'Autoriser Écrire Acte',//'发表记录', //   'Allow Write Doing',
      12 => 'usergroups_edit_home_allow_doing_mod',
      13 => 'Actes Publiés doivent être vérifiés',//'发表记录需要审核', //  'Published doings must be verified',
      14 => 'usergroups_edit_home_allow_upload',
      15 => 'Autoriser Téléchargez l\'image',//'上传图片', //   'Allow Upload image',
      16 => 'usergroups_edit_home_allow_upload_mod',
      17 => 'Images téléchargées doivent être vérifiés',//'上传图片需要审核', //   'Uploaded images must be verified',
      18 => 'usergroups_edit_home_image_max_size',
      19 => 'Album image individuel taille maximale (en K, 1M = 1024K)',//'相册单张图片最大尺寸(单位K 1M=1024K)', //  'Album single image maximum size (in K, 1M = 1024K)',
      20 => 'Définissez la taille maximale d\'une image unique album (en K, 1M = 1024K), Mettre à 0 pour ne pas limite. Nécessaire pour définir la valeur autorisée par PHP, s\'il vous plaît se référer à Centre de gestion Accueil',//'设置相册单张图片最大尺寸(单位K 1M=1024K)，0 为不限制，需要 PHP 设置允许才有效，请参考管理中心首页',   //  'Set the maximum size of a single album image (in K, 1M = 1024K), Set to 0 for not limit. Required to set the allowed by PHP value, please refer to Management Center Home', 
      21 => 'usergroups_edit_home_allow_share',
      22 => 'Autoriser à Partager',//'发布分享', //  'Allow to Share',
      23 => 'usergroups_edit_home_allow_share_mod',
      24 => 'Partages Publiés doivent être vérifiés',//'发布分享需要审核', // 'Published shares must be verified', 
      25 => 'usergroups_edit_home_allow_poke',
      26 => 'Autoriser à envoyer des voeux',//'允许打招呼', //  'Allow to Send Greeting',
      27 => 'usergroups_edit_home_allow_friend',
      28 => 'Autoriser Ajouter un ami',//'允许加好友', //  'Allow to Add friend',
      29 => 'usergroups_edit_home_allow_click',
      30 => 'Autoriser de noter',//'允许表态', //  'Allow to Rate',
      31 => 'usergroups_edit_home_allow_comment',
      32 => 'Permettre  de Commenter',//'发表留言/评论', //  'Allow to Comment', 
      33 => 'usergroups_edit_home_allow_myop',
      34 => 'Autoriser l\'utilisation des applications',//'允许使用漫游(Manyou)应用', //  'Allow to Use Apps',
      35 => 'usergroups_edit_home_allow_video_photo_ignore',
      36 => 'Ignorer les restrictions de certification vidéo',//'不受视频认证限制', //   'Ignore restrictions of video certification',
      37 => 'usergroups_edit_home_allow_view_video_photo',
      38 => 'Laisser voir la vidéo photo certifiée conforme',//'允许查看视频认证', //  'Allow to view of video certified photo',
      39 => 'usergroups_edit_home_allow_space_diy_html',
      40 => 'Permet d\'utiliser HTML dans le module de mise en page',//'允许自定义模块使用 HTML', //  'Allow to use HTML in layout module',
      41 => 'Note: Le code HTML peut causer des problèmes de sécurité, veuillez utiliser avec prudence! Recommandé pour activer uniquement en cas d\'extrême nécessité, et lui permettre seulement à l\'équipe de gestion de base.',//'注意: 开放 HTML 功能将产生安全隐患，请慎用。建议只在十分必要的情况下使用，并限制只开放给最核心的管理人员',   //   'Note: HTML code will cause safety problems, please use with caution! Recommended to enable only in cases of extreme necessity, and enable it only to core management team.',
      42 => 'usergroups_edit_home_allow_space_diy_bbcode',
      43 => 'Autoriser l\'utilisation BBCode dans le module de mise en page',//'允许自定义模块使用 BBCODE', // 'Allow to use BBCode in layout module', 
      44 => 'Que ce soit pour permettre l\'utilisation du BBCode dans le module de mise en page pour voir les personnels style Accueil',//'设置是否解析个人主页自定义模块中的 BBCODE 代码',   //  'Whether to enable use of BBCODE in layout module for personal home style set',
      45 => 'usergroups_edit_home_allow_space_diy_imgcode',
      46 => 'Autoriser l\'utilisation des code [img] dans le module de mise en page',//'允许自定义模块使用 [img]', //  'Allow to use [img] code in layout module',
      47 => 'Que ce soit pour permettre l\'utilisation du code [img] dans le module de mise en page pour voir les personnels style Accueil',//'设置是否解析个人主页自定义模块中的 [img] 代码',   //   'Whether to enable use of [img] code in layout module for personal home style set',
    ),
  ),
  313 =>
  array (
    'index' =>
    array (
      'Groupe Utilisateurs' => 'action=usergroups',//'用户组', //  'User Groups'  
      'Groupes connexes' => 'action=usergroups&operation=edit&anchor=group',//'群组相关', //  'Groups related' 
    ),
    'text' =>
    array (
      0 => 'Groupe Utilisateurs &raquo; Groupes connexes',//'用户组 &raquo; 群组相关',  //  'User Groups &raquo; Groups related',
      1 => 'usergroups_edit_group',
      2 => 'Groupes connexes',//'群组相关',  //  'Groups related', //  
      3 => 'usergroups_edit_group_build',
      4 => 'Autorisé pour plusieurs groupes à créer',//'允许建立群组的数量', //  'Allowed number of groups to create',
      5 => 'La plage est de 1-255, fixé à 0 pour désactivé créer un groupe',//'范围1-255，0为不允许建立群组',  //  'Range is 1-255, set to 0 for disable create a group', 
      6 => 'usergroups_edit_group_buildcredits',
      7 => 'Création de groupes crédits à la consommation', //  '创建群组消耗积分',
      8 => 'Créer un groupe besoin de consommer et de nombre entier, 0 n\'est pas intégrale', //  '创建群组需要消耗和积分数量，0为不需要积分',
      9 => 'usergroups_edit_post_direct_group',
      10 => 'Autoriser orienter annonce',//'允许直接发帖', //  'Allow direct posting',
      11 => 'usergroups_edit_post_url_group',
      12 => 'Que ce soit pour permettre d\'afficher des URL externes',//'允许发站外URL',  //   'Whether to allow to post external URLs',
    ),
  ),
  314 =>
  array (
    'index' =>
    array (
      'Groupes Utilisateurs' => 'action=usergroups',//'用户组', //  'User Groups'
      'Portail connexe' => 'action=usergroups&operation=edit&anchor=portal',//'门户相关', //   'Portal related'
    ),
    'text' =>
    array (
      0 => 'Groupes Utilisateurs &raquo; Portail connexe',//'用户组 &raquo; 门户相关',  //  'User Groups &raquo; Portal related',
      1 => 'usergroups_edit_portal',
      2 => 'Portail connexe',//'门户相关',  // 'Portal related',
      3 => 'usergroups_edit_portal_allow_comment_article',
      4 => 'Nombre de mots à un commentaire',//'文章评论字数',  // 'Number of words in a comment',
      5 => 'Définissez le groupe commentaire Limite du mot d\'utilisateur publié un article. Mettre à 0 pour désactiver les commentaires de ce niveau de l\'utilisateur.',//'设置此用户组发表文章评论字数限制，设置为0将禁止此用户级发表评论', //   'Set the user group comment word limit published an article. Set to 0 for disable comments for this user level.',
      6 => 'usergroups_edit_portal_allow_post_article',
      7 => 'Permettre de Publier l\'article',//'发布文章',  // 'Allow to Publish article',
      8 => 'usergroups_edit_portal_allow_down_local_img',  
      9 => 'Autoriser de télécharger des images locales',//'允许下载本地图片',  // 'Allow to download local images',
      10 => 'Télécharger une image distante et permettre de régénérer une image locale',//'下载远程图片的同时允许重新生成一份本地图片',  // 'Download a remote image and allow to regenerate a local image',
      11 => 'usergroups_edit_portal_allow_post_article_moderate',
      12 => 'Les articles publiés doivent être vérifiés',//'发布文章需要审核',  //  'Published articles must be verified',
    ),
  ),
  315 =>
  array (
    'index' =>
    array (
      'Tags Utilisateurs' => 'action=usertag',//'用户标签' //'User Tags'  
    ),
    'text' =>
    array (
      0 => 'Tags Utilisateurs',//'用户标签'  //  'User Tags',
      1 => 'usertag_add_tips',
      2 => 'Permettre aux utilisateurs d\'ajouter plusieurs tags. Si une liste d\'utilisateurs est vide, est un add lot de tags des utilisateurs Si les balises et la liste des utilisateur dispose d\'entrée. Expliquez à l\'essentiel d\'utilisateur spécifié Fixez les étiquettes tags',//'可以给线下活动的用户批量贴标签当只填写了标签，用户列表为空时，是批量添加用户标签如果标签和用户列表都有输入，说明是给指定用户批量贴标签',  //  'Enable users to add multiple tags. If a user list is empty, is a batch add of user tags If the tags and list of user has input. Explain to the specified user bulk attach tags',
      3 => 'usertag_add_tags',
      4 => 'Tags',//'标签',  //  'Tags',
      5 => 'Vous pouvez entrer plusieurs Tags, plusieurs balises tags séparées par un espace ou une virgule',//'可以输入多个标签，多个标签可以用空格、逗号 分隔', //  'You can enter multiple tags, separate multiple tags with a space or comma',
      6 => 'usertag_add_usernames',  
      7 => 'Liste des utilisateurs',//'用户列表',  //   'User List',
      8 => 'Vous pouvez entrer plusieurs utilisateurs, un nom d\'utilisateur par ligne',//'可以输入多个用户，每行输入一个用户名', // 'You can enter multiple users, one user name per line', 
    ),
  ),
);

