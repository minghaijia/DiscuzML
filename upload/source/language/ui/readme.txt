UYGHUR/ARABIC Language Pack
for Discuz! ML 3.1 (c) 2009-2099 codersclub.org

MultiLingual version by Valery Votintsev, codersclub.org

Translated by nurqut.com Team

Code		= 'ui';
ISO Code	= 'ug_CN';
English title	= 'Uyghur';
UTF8 title	= 'Uyghur';
18x12 Icon	= 'ui.gif';
Direction	= 'rtl';
Encoding	= 'UTF-8';
