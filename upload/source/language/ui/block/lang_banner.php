<?php

/**
 *      [Discuz!] (C)2001-2099 Comsenz Inc.
 *      This is NOT a freeware, use is subject to license terms
 *
 *      $Id: lang_banner.php 27449 2012-02-01 05:32:35Z zhangguosheng $
 */

if(!defined('IN_DISCUZ')) {
	exit('Access Denied');
}

/**
 *      为使用需要而翻译，原程序开发者版权所有
 *      Translated By NurQut Team. [NTA] Powered by NURQUT(FinalDream) && UYSON
 *
 *      Auto Translated By NurQut Translation Assistant(NurQut Terjimani)
 *      Translation Time : 2012-06-02
 */

$lang = array
(
	'banner_pic' => 'رەسىم ئادرېسى',
	'banner_width' => 'رەسىم كەڭلىكى',
	'banner_height' => 'رەسىم ئىگىزلىكى',
	'banner_url' => 'رەسىم ئۇلىنىشى',
	'banner_text' => 'رەسىم چۈشەندۈرلىشى',
	'banner_atarget' => 'ئۇلىنىش ئىچىش شەكلى',
	'banner_atarget_top' => 'مۇشۇ بەتتە ئېچىش  ( يېڭى كۆزنەكتە كۆرنىدۇ )',
	'banner_atarget_blank' => 'يېڭى كۆزنەكتە ئېچىش',
	'banner_atarget_self' => 'مۇشۇ بەتتە ئېچىش',
);

