<?php

/**
 *      [Discuz!] (C)2001-2099 Comsenz Inc.
 *      This is NOT a freeware, use is subject to license terms
 *
 *      $Id: lang_albumlist.php 27449 2012-02-01 05:32:35Z zhangguosheng $
 */

if(!defined('IN_DISCUZ')) {
	exit('Access Denied');
}

/**
 *      为使用需要而翻译，原程序开发者版权所有
 *      Translated By NurQut Team. [NTA] Powered by NURQUT(FinalDream) && UYSON
 *
 *      Auto Translated By NurQut Translation Assistant(NurQut Terjimani)
 *      Translation Time : 2012-06-02
 */

$lang = array
(
	'albumlist_aids' => 'بەلگىلەنگەن ئالبۇم',
	'albumlist_aids_comment' => 'بەلگىلەنگەن ئالبۇمنىڭ (ID(fid سىنى كىرگۈزۈڭ ، كۆپ بولسا ( , ) ئارقىلىق ئايرىڭ',
	'albumlist_uids' => 'ئەزا UID',
	'albumlist_uids_comment' => 'بەلگىلەنگەن ئەزانىڭ (ID(uid سىنى كىرگۈزۈڭ ، كۆپ بولسا ( , ) ئارقىلىق ئايرىڭ',
	'albumlist_catid' => 'بەلگىلەنگەن تۈر',
	'albumlist_catid_comment' => 'ئالبۇم تەۋە بولغان سېستىما ئالبۇم تۈرىنى تاللاڭ ، كۆپ تاللىسىڭىزمۇ بولىدۇ',
	'albumlist_startrow' => 'دەسلەپكى ئۇچۇر قۇر سانى',
	'albumlist_startrow_comment' => 'دەسلەپكى ئۇچۇر قۇر سانى كىرگۈزۈش كىرەك بولسا ، كونكىرىتنى قىممەتنى كىرگۈزۈڭ ، 0 دىسىڭىز بىرىنجى قۇردىن باشلىنىدۇ',
	'albumlist_titlelength' => 'بەلگىلەنگەن ئالبۇم ئسمىنىڭ ئۇزۇنلىقى',
/*vot*/	'albumlist_titlelength_comment'	=> 'Set the max length of alubms name',//'指定相册名称最大长度',
	'albumlist_orderby' => 'رەسىم تىزىلىش شەكىلى',
	'albumlist_orderby_comment' => 'قايسى سۆز بۆلىكى ياكى شەكىلىگە ئاساسەن رەسىم تىزىلىشىنى بەلگىلەڭ',
	'albumlist_orderby_dateline' => 'يوللانغان ۋاقىتنىڭ تەتۈرى بويىچە',
	'albumlist_orderby_picnum' => 'رەسىم سانىنىڭ تەتۈرى بويىچە',
	'albumlist_orderby_updatetime' => 'يېڭىلانغان ۋاقىتنىڭ تەتۈرى بويىچە',
);

