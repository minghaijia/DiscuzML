<?php

/**
 *      [Discuz!] (C)2001-2099 Comsenz Inc.
 *      This is NOT a freeware, use is subject to license terms
 *
 *      $Id: lang_grouplist.php 27449 2012-02-01 05:32:35Z zhangguosheng $
 */

if(!defined('IN_DISCUZ')) {
	exit('Access Denied');
}

/**
 *      为使用需要而翻译，原程序开发者版权所有
 *      Translated By NurQut Team. [NTA] Powered by NURQUT(FinalDream) && UYSON
 *
 *      Auto Translated By NurQut Translation Assistant(NurQut Terjimani)
 *      Translation Time : 2012-06-02
 */

$lang = array
(
	'grouplist_gtids' => 'گۇرۇپپا تۈرى',
	'grouplist_gtids_comment' => 'گۇرۇپپا تۈرىنى تاللاڭ',
	'grouplist_fids' => 'بەلگىلەنگەن گۇرۇپپا',
	'grouplist_fids_comment' => 'بەلگىلەنگەن گورۇپپىنىڭ ID سىنى كىرگۈزۈڭ ، كۆپ بولسا پەش ( , ) ئارقىلىق ئايرىڭ',
	'grouplist_startrow' => 'دەسلەپكى ئۇچۇر قۇر سانى',
	'grouplist_startrow_comment' => 'دەسلەپكى ئۇچۇر قۇر سانى كىرگۈزۈش كىرەك بولسا ، كونكىرىتنى قىممەتنى كىرگۈزۈڭ ، 0 دىسىڭىز بىرىنجى قۇردىن باشلىنىدۇ',
	'grouplist_titlelength' => 'گۇرۇپپا ئسمىنىڭ ئۇزۇنلىقىنى بەلگىلەڭ',
/*vot*/	'grouplist_titlelength_comment'		=> 'Set the group name max length',//'设置群组名称最大长度',
	'grouplist_summarylength' => 'تونۇشتۇرۇش ئۇزۇنلىقى',
	'grouplist_summarylength_comment' => 'گۇرۇپپا تونۇشتۇرلىشىنىڭ ئۇزۇنلىقىنى بەلگىلەڭ',
	'grouplist_orderby' => 'گۇرۇپپا تىزىلىش شەكىلى',
	'grouplist_orderby_comment' => 'قايسى سۆز بۆلىكى ياكى شەكىلىگە ئاساسەن تىزىلىشىنى بەلگىلەڭ',
	'grouplist_orderby_dateline' => 'قۇرۇلغان ۋاقىتنىڭ تەتۈرى بويىچە',
	'grouplist_orderby_posts' => 'بارلىق يازما سانىنىڭ تەتۈرى بويىچە',
	'grouplist_orderby_todayposts' => 'بۈگۈنكى يازما سانىنىڭ تەتۈرى بويىچە',
	'grouplist_orderby_threads' => 'تېما سانىنىڭ تەتۈرى بويىچە',
	'grouplist_orderby_level' => 'دەرىجىنىڭ تەتۈرى بويىچە',
	'grouplist_orderby_membernum' => 'ئەزالار سانىنىڭ تەتۈرى بويىچە',
	'grouplist_orderby_activity' => 'قىزغىنلىقىنىڭ تەتۈرى بويىچە',
	'grouplist_orderby_commoncredits' => 'ئورتاق جۇغلانمىسىنىڭ تەتۈرى بويىچە',
	'grouplist_orderby_displayorder' => 'سۈكۈتتىكى تەرتىپى بويىچە',
);

