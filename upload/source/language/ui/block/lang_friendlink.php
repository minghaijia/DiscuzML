<?php

/**
 *      [Discuz!] (C)2001-2099 Comsenz Inc.
 *      This is NOT a freeware, use is subject to license terms
 *
 *      $Id: lang_friendlink.php 27449 2012-02-01 05:32:35Z zhangguosheng $
 */

if(!defined('IN_DISCUZ')) {
	exit('Access Denied');
}

/**
 *      为使用需要而翻译，原程序开发者版权所有
 *      Translated By NurQut Team. [NTA] Powered by NURQUT(FinalDream) && UYSON
 *
 *      Auto Translated By NurQut Translation Assistant(NurQut Terjimani)
 *      Translation Time : 2012-06-02
 */

$lang = array
(
	'friendlink_content' => 'كۆرگەزمە شەكلى',
	'friendlink_content_both' => 'logo بىلەن خەت',
	'friendlink_content_logo' => 'پەقەت logo لا',
	'friendlink_content_text' => 'پەقەت خەتلا',
	'friendlink_type' => 'ئۇلىنىش گورۇپپىسى',
	'friendlink_type_group1' => 'گۇرۇپپا 1',
	'friendlink_type_group2' => 'گۇرۇپپا 2',
	'friendlink_type_group3' => 'گۇرۇپپا 3',
	'friendlink_type_group4' => 'گۇرۇپپا 4',
	'friendlink_titlelength' => 'بېكەت ئىسمى ئۇزۇنلىقى',
	'friendlink_titlelength_comment' => 'بېكەت ئسمىنىڭ ئۇزۇنلىقىنى بەلگىلەڭ',
	'friendlink_summarylength' => 'قىسقىچە تونۇشتۇرۇش ئۇزۇنلىقى',
	'friendlink_summarylength_comment' => 'قىسقىچە تونۇشتۇرۇش ئۇزۇنلىقىنى بەلگىلەڭ',
);

