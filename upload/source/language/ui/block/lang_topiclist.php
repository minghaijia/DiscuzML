<?php

/**
 *      [Discuz!] (C)2001-2099 Comsenz Inc.
 *      This is NOT a freeware, use is subject to license terms
 *
 *      $Id: lang_topiclist.php 27449 2012-02-01 05:32:35Z zhangguosheng $
 */

if(!defined('IN_DISCUZ')) {
	exit('Access Denied');
}

/**
 *      为使用需要而翻译，原程序开发者版权所有
 *      Translated By NurQut Team. [NTA] Powered by NURQUT(FinalDream) && UYSON
 *
 *      Auto Translated By NurQut Translation Assistant(NurQut Terjimani)
 *      Translation Time : 2012-06-02
 */

$lang = array
(
	'topiclist_topicids' => 'بەلگىلەنگەن مەخسۇس تېما',
	'topiclist_topicids_comment' => 'بەلگىلەنگەن مەخسۇس تېمىنىڭ (ID(topicid سىنى كىرگۈزۈڭ ، كۆپ بولسا پەش ( , ) ئارقىلىق ئايرىڭ',
	'topiclist_uids' => 'قۇرغۇچى UID',
	'topiclist_uids_comment' => 'بەلگىلەنگەن مەخسۇس تېما قۇرغۇچىنىڭ (ID(uid سىنى كىرگۈزۈڭ ، كۆپ بولسا پەش ( , ) ئارقىلىق ئايرىڭ',
	'topiclist_startrow' => 'دەسلەپكى ئۇچۇر قۇر سانى',
	'topiclist_startrow_comment' => 'دەسلەپكى ئۇچۇر قۇر سانى كىرگۈزۈش كىرەك بولسا ، كونكىرىتنى قىممەتنى كىرگۈزۈڭ ، 0 دىسىڭىز بىرىنجى قۇردىن باشلىنىدۇ',
	'topiclist_titlelength' => 'ماۋزۇ ئۇزۇنلىقى',
	'topiclist_titlelength_comment' => 'بەلگىلەنگەن مەخسۇس تېمىنىڭ ماۋزۇ ئۇزۇنلىقى',
	'topiclist_summarylength' => 'تونۇشتۇرۇش ئۇزۇنلىقى',
	'topiclist_summarylength_comment' => 'مەخسۇس تېما تونۇشتۇرلۇش ئۇزۇنلىقى',
	'topiclist_picrequired' => 'مۇقاۋىسىز مەخسۇس تېمىلارنى سۈزۈش',
	'topiclist_picrequired_comment' => 'مۇقاۋىسىز رەسىملىك مەخسۇس تېمىلارنى سۈزەمسىز',
	'topiclist_orderby' => 'مەخسۇس تېما تىزىلىش تەرتىپى',
	'topiclist_orderby_comment' => 'قايسى سۆز بۆلىكى ياكى شەكىلىگە ئاساسەن تىزىلىشىنى بەلگىلەڭ',
	'topiclist_orderby_dateline' => 'يوللانغان ۋاقىتنىڭ تەتۈرى بويىچچە',
	'topiclist_orderby_viewnum' => 'كۆرۈش سانىنىڭ تەتۈرى بويىچە',
);

