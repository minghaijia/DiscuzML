<?php

/**
 *      [Discuz!] (C)2001-2099 Comsenz Inc.
 *      This is NOT a freeware, use is subject to license terms
 *
 *      $Id: lang_interthread.php 27449 2012-02-01 05:32:35Z zhangguosheng $
 */

if(!defined('IN_DISCUZ')) {
	exit('Access Denied');
}

/**
 *      为使用需要而翻译，原程序开发者版权所有
 *      Translated By NurQut Team. [NTA] Powered by NURQUT(FinalDream) && UYSON
 *
 *      Auto Translated By NurQut Translation Assistant(NurQut Terjimani)
 *      Translation Time : 2012-06-02
 */

$lang = array
(
	'interthread_name' => 'مۇنبەر / گۇرۇپ يازما ئۇقتۇرۇش ئىستونى ئېلانى',
	'interthread_desc' => 'كۆرسىتىش ئۇسۇلى:يازما ئارىسى ئۇقتۇرۇش ئېلانى يازما ۋە بىرىنجى ئىنكاس ئارىسىدا كۆرۈنىدۇ،480*60 چوڭلۇقتىكى رەسىم ياكى Flash شەكلىدە بولىدۇ.<br />بەت يۈزىدە بىردىن كۆپ بولغان يازما ئارىسى ئۇقتۇرۇش ئېلانى بولغان ئەھۋالدا سېستىما ئاپتوماتىك ئارىسىدىن بىرنى تاللاپ كۆرسىتىدۇ. <br />قىممەت تەھلىلى: يازما ۋە ئىنكاسنى ئايرىش خۇسۇسىيىتىگە ئىگە،ئېلان چوڭ لېكىن يازما مەزمۇنىغا تەسىر يەتكۈزمەيدۇ،زىيارەتچىلەرگە ئاساسەن تەسىر يەتكۈزمەيدۇ',
	'interthread_fids' => 'قويۇدىغان سەھىپە',
	'interthread_fids_comment' => 'ئېلان قويىدىغان مۇنبەر سەھىپىسى،ئېلان قويۇش رايونى دائىرىسىدە «مۇنبەر» بولغان ئەھۋالدا كۈچكە ئىگە',
	'interthread_groups' => 'قويىدىغان گۇرۇپ تۈرى',
	'interthread_groups_comment' => 'ئېلان قويىدىغان گۇرۇپ تۈرى تەڭشىكى،ئېلان قويۇش دائىرىسى «گۇرۇپ» نى ئۆزئىچىگە ئالغان ئەھۋال ئاستىدا كۈچكە ئىگە',
	'interthread_pnumber' => 'ئېلان قويۇش قەۋىتى',
	'interthread_pnumber_comment' => 'تاللاش تۈرى 1# 2# 3# ... يازمىنىڭ قەۋەت سانىنى ئىپادىلەيدۇ،CTRL كونۇپكىسىنى بېسىپ تۇرۇش ئارقىلىق كۆپ تاللاشقا بولىدۇ،كۆڭۈلدىكى ئەھۋالدا 1 - قەۋەتتىلا كۆرۈنىدۇ',
);

