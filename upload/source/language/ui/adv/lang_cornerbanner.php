<?php

/**
 *      [Discuz!] (C)2001-2099 Comsenz Inc.
 *      This is NOT a freeware, use is subject to license terms
 *
 *      $Id: lang_cornerbanner.php 27449 2012-02-01 05:32:35Z zhangguosheng $
 */

if(!defined('IN_DISCUZ')) {
	exit('Access Denied');
}

/**
 *      为使用需要而翻译，原程序开发者版权所有
 *      Translated By NurQut Team. [NTA] Powered by NURQUT(FinalDream) && UYSON
 *
 *      Auto Translated By NurQut Translation Assistant(NurQut Terjimani)
 *      Translation Time : 2012-06-02
 */

$lang = array
(
	'cornerbanner_name' => 'پۈتۈن رايون ئوڭ ئاستى بۇرجەك ئېلانى',
	'cornerbanner_desc' => 'كۆرسىتىش ئۇسۇلى:بەتنىڭ ئوڭ تەرەپ ئاستى بۇرجەك رايونىدا كۆرۈنىدۇ،بەت يۈزىدە بىردىن ئارتۇق بەت بېشى ئۇقتۇرۇش ئىستونى بولغان ئەھۋالدا سېستىما ئىچىدىن خالىغان بىرىنى كۆرسىتىدۇ.<br /> قىممەت تەھلىلى: بەتتىكى قىممەتلىك،مۇھىم ئۇچۇر،داڭلىق ماركىلارنى تونۇشتۇرۇشتىكى ئېلان تۈرلىرىنىڭ بىرى',
	'cornerbanner_index' => 'باش بەت',
	'cornerbanner_fids' => 'قويۇش بۆلىكى',
	'cornerbanner_fids_comment' => 'ئېلان قويىدىغان مۇنبەر سەھىپىسى،ئېلان قويۇش رايونى دائىرىسىدە «مۇنبەر» بولغان ئەھۋالدا كۈچكە ئىگە',
	'cornerbanner_groups' => 'قويىدىغان گۇرۇپ تۈرى',
	'cornerbanner_groups_comment' => 'ئېلان قويىدىغان گۇرۇپ تۈرى تەڭشىكى،ئېلان قويۇش دائىرىسى «گۇرۇپ» نى ئۆزئىچىگە ئالغان ئەھۋال ئاستىدا كۈچكە ئىگە',
	'cornerbanner_animator' => 'ھەركەت ئۈنۈمى',
	'cornerbanner_animator_comment' => 'ھەركەت ھالىتىدە كۆرۈنسۇنمۇ؟',
	'cornerbanner_category' => 'دەرۋازا قانىلىغا قويۇش',
	'cornerbanner_category_comment' => 'ئېلان قويىدىغان قانال تۈرى تەڭشىكى ، ئېلان قويىدىغان دائىرىدە «دەرۋازا» بولغان ئەھۋالدا كۈچكە ئىگە',
	'cornerbanner_disableclose' => 'ئېلاننىڭ ئۇلىنىشىنى تاقاش',
	'cornerbanner_disableclose_comment' => 'ناۋادا ئېلان كودى ئىچىدە تاقاش مەشغۇلاتى ئېلىپ بېرىلغان بولسا ، سېستىما تەڭشىكىدىن تاقىسىڭىزمۇ بولىدۇ',
	'cornerbanner_show' => 'كۆرسىتىش',
	'cornerbanner_hidden' => 'يوشۇرۇش',
);

