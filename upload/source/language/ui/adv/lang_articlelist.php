<?php

/**
 *      [Discuz!] (C)2001-2099 Comsenz Inc.
 *      This is NOT a freeware, use is subject to license terms
 *
 *      $Id: lang_articlelist.php 27449 2012-02-01 05:32:35Z zhangguosheng $
 */

if(!defined('IN_DISCUZ')) {
	exit('Access Denied');
}

/**
 *      为使用需要而翻译，原程序开发者版权所有
 *      Translated By NurQut Team. [NTA] Powered by NURQUT(FinalDream) && UYSON
 *
 *      Auto Translated By NurQut Translation Assistant(NurQut Terjimani)
 *      Translation Time : 2012-06-02
 */

$lang = array
(
	'articlelist_name' => 'دەرۋازا ماقالە ئېلانى',
	'articlelist_desc' => 'كۆرسىتىش ھالىتى: ماقالە ئېلانى قانال ماقالە تۈرىدىكى ماقالە تىزىملىكى بېتىدە كۆرسىتىلىدۇ، چوققا قىسمى ۋە ئاستى قىسمى قاتارلىق 2 قويۇش ئورنىغا بۆلۈنىدۇ.',
	'articlelist_position' => 'قويۇش ئورنى',
	'articlelist_position_comment' => '4 ئورۇنغا بۆلۈنىدۇ،چوققا ۋە ئاستى قىسمىدا 2دىن زىچ كۆرنۈش ئورنى بار',
	'articlelist_position_up1' => 'چوققا قىسمى(ئۈستى)',
	'articlelist_position_up2' => 'چوققا قىسمى(ئاستى)',
	'articlelist_position_down1' => 'ئاستى قىسمى(ئۈستى)',
	'articlelist_position_down2' => 'ئاستى قىسمى(ئاستى)',
	'articlelist_category' => 'ئېلان قويۇلىدىغان قانال تۈرى',
	'articlelist_category_comment' => 'ئېلان قويۇلىدىغان قانال تۈرى تەڭشىكى',
);

