<?php

/**
 *      [Discuz!] (C)2001-2099 Comsenz Inc.
 *      This is NOT a freeware, use is subject to license terms
 *
 *      $Id: lang_article.php 27449 2012-02-01 05:32:35Z zhangguosheng $
 */

if(!defined('IN_DISCUZ')) {
	exit('Access Denied');
}

/**
 *      为使用需要而翻译，原程序开发者版权所有
 *      Translated By NurQut Team. [NTA] Powered by NURQUT(FinalDream) && UYSON
 *
 *      Auto Translated By NurQut Translation Assistant(NurQut Terjimani)
 *      Translation Time : 2012-06-02
 */

$lang = array
(
	'article_name' => 'دەرۋازا ماقالە ئېلانى',
	'article_desc' => 'كۆرسىتىش ھالىتى: ماقالە ئېلانى بولسا ماقالە بېتىدە كۆرۈنىدۇ،مەزمۇن رايونى ئايلانما ئېلانى ۋە مۇناسىۋەتلىك ئوقۇلما ئۈستى قىسمىدىن ئىبارەت 2 خىل بولىدۇ.',
	'article_position' => 'قويۇش ئورنى',
	'article_position_comment' => '3 ئورۇنغا بۆلۈنگەن بولۇپ،ئىچىدىكى مۇناسىۋەتلىك ئوقۇلما ئۈستى قىسمىدا 2خىل زىچ كۆرۈنۈش ئورنى بار',
	'article_position_float' => 'مەزمۇن رايونى ئايلانمىسى',
	'article_position_up' => 'مۇناسىۋەتلىك ئوقۇلما ئۈستى (ئۈستى)',
	'article_position_down' => 'مۇناسىۋەتلىك ئوقۇلما ئۈستى (ئاستى)',
	'article_category' => 'ئېلان قويۇلىدىغان قانال تۈرى',
	'article_category_comment' => 'ئېلان قويۇلىدىغان قانال تۈرى تەڭشىكى',
);

