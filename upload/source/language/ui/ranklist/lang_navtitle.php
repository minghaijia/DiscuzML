<?php

/**
 *      [Discuz!] (C)2001-2099 Comsenz Inc.
 *      This is NOT a freeware, use is subject to license terms
 *
 *      $Id: lang_navtitle.php 31175 2012-07-24 02:35:29Z liulanbo $
 *
 *      This file is automatically generate
 */

if(!defined('IN_DISCUZ')) {
	exit('Access Denied');
}

$lang = array(
	'ranklist_title_member_credit' => 'ئەزالار جۇغلانما قاتارى',
	'ranklist_title_member_bid' => 'ئەزالار رىقابەت قاتارى',
	'ranklist_title_member_girl' => 'خانىملار قاتارى',
	'ranklist_title_member_boy' => 'ئەپەندىلەر قاتارى',
	'ranklist_title_member_friend' => 'دوست سانى قاتارى',
	'ranklist_title_member_invite' => 'تەكلىپ قاتارى',
	'ranklist_title_member_post' => 'تېما سانى قاتارى',
	'ranklist_title_member_blog' => 'كۈندىلىك خاتىرە سانى قاتارى',
	'ranklist_title_member_onlinetime' => 'توردىكىلەر قاتارى',

	'ranklist_title_thread_reply' => 'كۆپ ئىنكاسلىق تېمىلار تىزىملىكى',
	'ranklist_title_thread_view' => 'ئاۋات تېما قاتارى',
	'ranklist_title_thread_share' => 'كۆپ بەھرىلەنگەن تېمىلار تىزىملىكى',
	'ranklist_title_thread_favorite' => 'كۆپ ساقلىۋېلىنغان تېمىلار تىزىملىكى',
	'ranklist_title_thread_heat' => 'يازما ئاۋاتلىق قاتارى',

	'ranklist_title_blog_heat' => 'ئاۋات كۈندىلىك خاتىرە تىزىملىكى',
	'ranklist_title_blog_reply' => 'كۆپ باھالىق كۈندىلىك خاتىرە تىزىملىكى',
	'ranklist_title_blog_view' => 'ئاۋات كۈندىلىك خاتىرە تىزىملىكى',
	'ranklist_title_blog_share' => 'كۆپ بەھرىلەنگەن كۈندىلىك خاتىرە تىزىملىكى',
	'ranklist_title_blog_favorite' => 'كۆپ ساقلىۋېلىنغان كۈندىلىك خاتىرە تىزىملىكى',
	'ranklist_title_blog_1' => 'كۈندىلىك خاتىرە كۆرۈلۈش قاتارى',
	'ranklist_title_blog_2' => 'بلوگ «چۆچۈش» قاتارى',
	'ranklist_title_blog_3' => 'بلوگ «قوللاش» قاتارى',
	'ranklist_title_blog_4' => 'بلوگ «گۈل» قاتارى',
	'ranklist_title_blog_5' => 'بلوگ «تۇخۇم» قاتارى',

	'ranklist_title_poll_heat' => 'ئاۋات سايلام تىزىملىكى',
	'ranklist_title_poll_favorite' => 'كۆپ ساقلىۋېلىنغان سايلام تىزىملىكى',
	'ranklist_title_poll_share' => 'كۆپ بەھرىلەنگەن سايلام تىزىملىكى',

	'ranklist_title_activity_heat' => 'ئاۋات پائالىيەتلەر تىزىملىكى',
	'ranklist_title_activity_favorite' => 'كۆپ ساقلىۋېلىنغان پائالىيەتلەر تىزىملىكى',
	'ranklist_title_activity_share' => 'كۆپ بەھرىلەنگەن پائالىيەتلەر تىزىملىكى',

	'ranklist_title_picture_heat' => 'ئاۋات رەسىملەر تىزىملىكى',
	'ranklist_title_picture_share' => 'كۆپ بەھرىلەنگەن رەسىملەر تىزىملىكى',
	'ranklist_title_picture_1' => 'چىرايلىق رەسىملەر قاتارى',
	'ranklist_title_picture_2' => 'سوقۇش رەسىمى تەرتىپى',
	'ranklist_title_picture_3' => 'ھەيران قېلىش رەسىمى تەرتىپى',
	'ranklist_title_picture_4' => 'گۈل رەسىمى تەرتىپى',
	'ranklist_title_picture_5' => 'تۇخۇم رەسىمى تەرتىپى',

	'ranklist_title_forum_thread' => 'كۆپ تېما يوللانغان سەھىپىلەر تىزىملىكى',
	'ranklist_title_forum_post' => 'كۆپ ئىنكاس يوللانغان سەھىپىلەر تىزىملىكى',
	'ranklist_title_forum_post_30' => 'سەھىپە 30 كۈنلۈك تېما يوللىنىش قاتارى',
	'ranklist_title_forum_post_24' => 'سەھىپە 24 سائەتلىك تېما يوللىنىش قاتارى',

	'ranklist_title_group_credit' => 'جۇغلانمىسى كۆپ گۇرۇپپىلار تىزىملىكى',
	'ranklist_title_group_member' => 'گۇرۇپپا ئەزا سانى قاتارى',
	'ranklist_title_group_thread' => 'گۇرۇپپا تېما سانى قاتارى',
	'ranklist_title_group_post' => 'گۇرۇپپا ئىنكاس قاتارى',
	'ranklist_title_group_post_30' => 'گۇرۇپپا 30 كۈنلۈك تېما يوللىنىش قاتارى',
	'ranklist_title_group_post_24' => 'گۇرۇپپا 24 سائەتلىك تېما يوللىنىش قاتارى',

	'ranklist_title_app_today' => 'بۈگۈنكى ئاۋات قوللانچىلار',
	'ranklist_title_app_all' => 'ئەڭ مودا قوللانچىلار',
);

