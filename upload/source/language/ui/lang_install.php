<?php

/**
 *      [Discuz!] (C)2001-2099 Comsenz Inc.
 *      This is NOT a freeware, use is subject to license terms
 *
 *      $Id: lang_install.php by Valery Votintsev, codersclub.org
 */

define('UC_VERNAME', 'International Version');

$lang = array(
	'SC_GBK'		=> 'Simplified Chinese GBK encoding',//'简体中文版',
	'TC_BIG5'		=> 'Traditional Chinese BIG5 encoding',//'繁体中文版',
	'SC_UTF8' => 'ئۇيغۇرچە UTF8 نۇسخىسى(نۇرقۇت ئەتىرىتى)',
	'TC_UTF8'		=> 'Traditional Chinese UTF8 encoding',//'繁体中文 UTF8 版',
	'EN_ISO'		=> 'ENGLISH ISO8859',
	'EN_UTF8'		=> 'ENGLISH UTF-8',

	'title_install' => SOFT_NAME.' ئورنىتىش يىتەكچىسى',
	'agreement_yes' => 'قوشۇلىمەن',
	'agreement_no' => 'قوشۇلمايمەن',
	'notset' => 'چەكلىمە يوق',

	'message_title' => 'ئەسكەرتى ئۇچۇرى',
	'error_message' => 'خاتالىق ئۇچۇرى',
	'message_return' => 'قايتىش',
	'return' => 'قايتىش',
	'install_wizard' => 'ئورنىتىش يىتەكچىسى',
	'config_nonexistence' => 'سەپلىمە ھۆججىتى تېپىلمىدى',
	'nodir' => 'مۇندەرىجە تېپىلمىدى',
	'redirect'		=> 'Browser will automatically redirect the page, without a human intervention.<br>Except when your browser does not support frames, please click here',//'浏览器会自动跳转页面，无需人工干预。<br>除非当您的浏览器没有自动跳转时，请点击这里',
	'auto_redirect'		=> 'Browser will automatically redirect the page, without a human intervention.',//'浏览器会自动跳转页面，无需人工干预',
	'database_errno_2003'	=> 'Can not connect to the database, check whether the database is run and the database server address is correct.',//'无法连接数据库，请检查数据库是否启动，数据库服务器地址是否正确',
	'database_errno_1044'	=> 'Unable to create a new database, please check the database name is correct.',//'无法创建新的数据库，请检查数据库名称填写是否正确',
	'database_errno_1045'	=> 'Can not connect to the database, check the database user name and password are correct.',//'无法连接数据库，请检查数据库用户名或者密码是否正确',
	'database_errno_1064'	=> 'SQL Syntax error',//'SQL 语法错误',

	'dbpriv_createtable'	=> 'No CREATE TABLE permission, can not continue installation.',//'没有CREATE TABLE权限，无法继续安装',
	'dbpriv_insert'		=> 'No INSERT permission, can not continue installation.',//'没有INSERT权限，无法继续安装',
	'dbpriv_select'		=> 'No SELECT privileges, can not continue installation.',//'没有SELECT权限，无法继续安装',
	'dbpriv_update'		=> 'No UPDATE permissions, can not continue installation.',//'没有UPDATE权限，无法继续安装',
	'dbpriv_delete'		=> 'No DELETE permissions, can not continue installation.',//'没有DELETE权限，无法继续安装',
	'dbpriv_droptable'	=> 'No DROP TABLE permissions to install.',//'没有DROP TABLE权限，无法安装',

	'db_not_null'		=> 'UCenter database already installed, continue the installation will clear the old data.',//'数据库中已经安装过 UCenter, 继续安装会清空原有数据。',
	'db_drop_table_confirm'	=> 'To continue the installation it is required to clear all the old data, are you sure you want to continue?',//'继续安装会清空全部原有数据，您确定要继续吗?',

	'writeable' => 'يازغىلى بولىدۇ',
	'unwriteable' => 'يازغىلى بولمايدۇ',
	'old_step' => 'ئارقىغا قايتىش',
	'new_step' => 'داۋاملاشتۇرۇش',

	'database_errno_2003'	=> 'Can not connect to the database, check whether the database is run and the database server address is correct.',//'无法连接数据库，请检查数据库是否启动，数据库服务器地址是否正确',
	'database_errno_1044'	=> 'Unable to create a new database, please check the database name is correct.',//'无法创建新的数据库，请检查数据库名称填写是否正确',
	'database_errno_1045'	=> 'Can not connect to the database, check the database user name and password are correct.',//'无法连接数据库，请检查数据库用户名或者密码是否正确',
	'database_connect_error'	=> 'Database connection error.',//'数据库连接错误',

	'step_title_1' => 'ئورنىتىش مۇھىتىنى تەكشۈرۈش',
	'step_title_2' => 'ئىجرا مۇھىتى سەپلەش',
	'step_title_3' => 'ساندان قۇرۇش',
	'step_title_4' => 'ئورنىتىش',
	'step_env_check_title' => 'ئورنىتىشنى باشلاش',
	'step_env_check_desc' => 'مۇھىت ۋە مۇندەرىجە ھوقۇق چەكلىمىسىنى تەكشۈرۈش',
	'step_db_init_title' => 'ساندان ئورنىتىش',
	'step_db_init_desc' => 'ساندان ئورنىتىۋاتىدۇ',

	'step1_file' => 'ھۆججەت ۋە مۇندەرىجە',
	'step1_need_status' => 'ھازىرلايدىغان شەرت',
	'step1_status' => 'ھازىرقى ھالىتى',
	'not_continue' => 'قىزىل رەڭلىك خاتالىق بار ئورۇنلارنى تۈزىتىپ ئاندىن داۋاملاشتۇرۇڭ',

	'tips_dbinfo' => 'ساندان ئۇچۇرلىرىنى تولدۇرۇڭ',
	'tips_dbinfo_comment' => '',
	'tips_admininfo' => 'مۇنبەر مەسئۇلى ئۇچۇرلىرىنى تولدۇرۇڭ',
	'step_ext_info_title' => 'ئورنىتىش تامام',
	'step_ext_info_comment' => 'بۇ يەرنى چېكىپ كىرىڭ',

	'ext_info_succ' => 'ئورنىتىش تامام',
	'install_submit' => 'تاپشۇرۇش',
	'install_locked' => 'ئورنىتىش چەكلەنگەن، سىز بىر قېتىم ئورنىتىپ بولغان، ناۋادا قايتىدىن ئورناتماقچى بولسىڭىز مۇلازىمېتىردىن بۇ ھۆججەتنى ئۆچۈرۋىتىڭ:<br /> '.str_replace(ROOT_PATH, '', $lockfile),
	'error_quit_msg' => 'سىز ئۈستىدىكى مەسىلىنى ھەل قىلغاندىن كېيىن، ئورنىتىشنى داۋاملاشتۇرالايسىز',

	'step_app_reg_title' => 'ئىجرا مۇھىتى سەپلەش',
	'step_app_reg_desc' => 'مۇلازىمېتىر مۇھىتىنى تەكشۈرۈش ۋە UCenter تەڭشىكى',
	'tips_ucenter' => 'مۇناسىۋەتلىكUCenter ئۇچۇرلىرىنى تولدۇرۇڭ',
	'tips_ucenter_comment'	=> 'UCenter is the Comsenz inc. core service program. Discuz! Board and other Comsenz applications rely on this program. If you have already installed UCenter, please fill in the information below. Otherwise, please go to <a href="http://www.discuz.com/" target="blank">Comsenz Products</a> to download and install UCenter, and then continue.',//'UCenter 是 Comsenz 公司产品的核心服务程序，Discuz! Board 的安装和运行依赖此程序。如果您已经安装了 UCenter，请填写以下信息。否则，请到 <a href="http://www.discuz.com/" target="blank">Comsenz 产品中心</a> 下载并且安装，然后再继续。',
	
	'advice_mysql_connect'		=> 'Please check the mysql module is loaded correctly.',//'请检查 mysql 模块是否正确加载',
	'advice_gethostbyname'		=> 'PHP configuration is not allowed the <b>gethostbyname</b> function. Please contact the server administrator to resolve this problem.',//'是否php配置中禁止了gethostbyname函数。请联系空间商，确定开启了此项功能',
	'advice_file_get_contents'	=> 'This function require the <b>allow_url_fopen</b> option to <b>On</b> in php.ini. Please contact the server administrator to resolve this problem.',//'该函数需要 php.ini 中 allow_url_fopen 选项开启。请联系空间商，确定开启了此项功能',
	'advice_xml_parser_create'	=> 'This function require the PHP support for XML. Please contact the server administrator to resolve this problem.',//'该函数需要 PHP 支持 XML。请联系空间商，确定开启了此项功能',
	'advice_fsockopen'		=> 'This function require the <b>allow_url_fopen</b> option to be <b>On</b> in php.ini. Please contact the server administrator to resolve this problem.',//'该函数需要 php.ini 中 allow_url_fopen 选项开启。请联系空间商，确定开启了此项功能',
	'advice_pfsockopen'		=> 'This function requires to allow_url_fopen in php.ini. Please contact the hosting provider to set this value',//'该函数需要 php.ini 中 allow_url_fopen 选项开启。请联系空间商，确定开启了此项功能',
	'advice_stream_socket_client'	=> 'Whether enabled the stream_socket_client function in PHP configuration',//'是否 PHP 配置中禁止了 stream_socket_client 函数',
	'advice_curl_init'		=> 'Whether enabled the curl_init function in PHP configuration',//'是否 PHP 配置中禁止了 curl_init 函数',

	'ucurl'				=> 'UCenter URL',//'UCenter 的 URL',
	'ucpw'				=> 'UCenter Founder Password',//'UCenter 创始人密码',
	'ucip'				=> 'UCenter IP address',//'UCenter 的IP地址',
	'ucenter_ucip_invalid'		=> 'Invalid format, please fill in the correct IP address',//'格式错误，请填写正确的 IP 地址',
	'ucip_comment'			=> 'In most cases you can leave this empty',//'绝大多数情况下您可以不填',

	'tips_siteinfo'			=> 'Please fill in the site information',//'请填写站点信息',
	'sitename'			=> 'Site Name',//'站点名称',
	'siteurl'			=> 'Site URL',//'站点 URL',

	'forceinstall' => 'مەجبۇرىي ئورنىتىش',
	'dbinfo_forceinstall_invalid' => 'ھازىرقى سانداندا ئوخشاش بولغان ئۇچۇر جەدۋىلى مەۋجۇت، سىز «جەدۋەل ئالدى ئۇلانمىسى»نى ئۆزگەرتىش ئارقىلىق كونا ساندان جەدۋىلىنىڭ ئۆچۈرۈلۈپ كېتىشىدىن ساقلانسىڭىز بولىدۇ، ياكى مەجبۇرىي قاچىلاشنى تاللىسىڭىزمۇ بولىدۇ. مەجبۇرىي قاچىلىسىڭىز كونا ساندان تازىلىنىپ كېتىدۇ، ئەسلىگە كەلتۈرگىلى بولمايدۇ.',

	'click_to_back' => 'ئارقىغا يېنىش',
	'adminemail' => 'سىستېما Email',
	'adminemail_comment' => 'سىستېما خاتالىق دوكلاتى ئەۋەتىلىدۇ',
	'dbhost_comment' => 'ساندان مۇلازىمېتىر ئادرېسى، ئادەتتە localhost',
	'tablepre_comment' => 'بىر ساندانغا مۇنبەردىن بىرنەچچىنى ئورناتقاندا بۇنى ئۆزگەرتىڭ',
	'forceinstall_check_label' => 'ئۇچۇرلارنى ئۆچۈرۈپ مەجبۇرىي ئورنىتىمەن',

	'uc_url_empty'			=> 'You have to fill in the UCenter URL',//'您没有填写 UCenter 的 URL，请返回填写',
	'uc_url_invalid'		=> 'UCenter URL format is invalid',//'URL 格式错误',
	'uc_url_unreachable'		=> 'UCenter URL address is unreachable, please check',//'UCenter 的 URL 地址可能填写错误，请检查',
	'uc_ip_invalid'			=> 'Can not resolve the domain name, please fill in the site IP address',//'无法解析该域名，请填写站点的 IP',
	'uc_admin_invalid'		=> 'UCenter administrator password invalid, please re-fill',//'UCenter 创始人密码错误，请重新填写',
	'uc_data_invalid'		=> 'UCenter communication failure. Check the UCenter URL address is correct',//'通信失败，请检查 UCenter 的URL 地址是否正确 ',
	'uc_dbcharset_incorrect'	=> 'UCenter database character set is inconsistent with the current application character set',//'UCenter 数据库字符集与当前应用字符集不一致',
	'uc_api_add_app_error'		=> 'Adding to UCenter application error',//'向 UCenter 添加应用错误',
	'uc_dns_error'			=> 'UCenter DNS resolution error. Please return and fill in the UCenter IP address',//'UCenter DNS解析错误，请返回填写一下 UCenter 的 IP地址',

	'ucenter_ucurl_invalid'		=> 'UCenter URL is empty or wrong format, please check',//'UCenter 的URL为空，或者格式错误，请检查',
	'ucenter_ucpw_invalid'		=> 'UCenter administrator password is blank, or formatting errors, please check',//'UCenter 的创始人密码为空，或者格式错误，请检查',
	'siteinfo_siteurl_invalid'	=> 'The site URL is blank, or formatting errors, please check',//'站点URL为空，或者格式错误，请检查',
	'siteinfo_sitename_invalid'	=> 'The site name is empty or wrong format, please check',//'站点名称为空，或者格式错误，请检查',
	'dbinfo_dbhost_invalid'		=> 'Database server is empty, or wrong format, please check',//'数据库服务器为空，或者格式错误，请检查',
	'dbinfo_dbname_invalid'		=> 'Database name is empty, or wrong format, please check',//'数据库名为空，或者格式错误，请检查',
	'dbinfo_dbuser_invalid'		=> 'Database user name is blank, or format error, please check',//'数据库用户名为空，或者格式错误，请检查',
	'dbinfo_dbpw_invalid'		=> 'Database password is blank, or format error, please check',//'数据库密码为空，或者格式错误，请检查',
	'dbinfo_adminemail_invalid'	=> 'The site system email address is empty, or format error, please check',//'系统邮箱为空，或者格式错误，请检查',
	'dbinfo_tablepre_invalid'	=> 'Table prefix is blank, or format error, please check',//'数据表前缀为空，或者格式错误，请检查',
	'admininfo_username_invalid'	=> 'Administrator user name is blank, or format error, please check',//'管理员用户名为空，或者格式错误，请检查',
	'admininfo_email_invalid'	=> 'Administrator Email is blank, or format error, please check',//'管理员Email为空，或者格式错误，请检查',
	'admininfo_password_invalid' => 'مۇنبەر مەسئۇلى پارولى بوش قالسا بولمايدۇ، تولدۇرۇڭ!',
	'admininfo_password2_invalid' => 'ئىككى قېتىملىق پارول بىردەك ئەمەس، تەكشۈرۈڭ!',

/*vot*/	'install_dzfull'		=> 'ھەممىنى يېڭىدىن ئورنىتىش Discuz! X (UCenter Server نىمۇ ئورنىتىدۇ)',
/*vot*/	'install_dzonly'		=> 'پەقەت Discuz! X نىلا ئورنىتىش (ئالدىن ئورنىتىپ بولغان UCenter Server لازىم)',

	'username' => 'مۇنبەر مەسئۇلى ئىسمى',
	'email' => 'مۇنبەر مەسئۇلى Email',
	'password' => 'مۇنبەر مەسئۇلى پارولى',
	'password_comment' => 'مۇنبەر مەسئۇلى پارولى بوش قالسا بولمايدۇ',
	'password2' => 'پارولنى قايتا كىرگۈزۈڭ',

	'admininfo_invalid'		=> 'Administrator information is not complete, check the administrator usernamet, password, email',//'管理员信息不完整，请检查管理员账号，密码，邮箱',
	'dbname_invalid'		=> 'Database name is empty, please fill in the database name',//'数据库名为空，请填写数据库名称',
	'tablepre_invalid'		=> 'Table prefix is blank or format error, please check',//'数据表前缀为空，或者格式错误，请检查',
	'admin_username_invalid'	=> 'Illegal user name! User name length should not be more than 15 English characters, and can not contain special characters, like Chinese letters or numbers',//'非法用户名，用户名长度不应当超过 15 个英文字符，且不能包含特殊字符，一般是中文，字母或者数字',
	'admin_password_invalid'	=> 'Password and the above discrepancies, please re-enter',//'密码和上面不一致，请重新输入',
	'admin_email_invalid'		=> 'The e-mail address used is invalid or the format is invalid, please change to other address',//'Email 地址错误，此邮件地址已经被使用或者格式无效，请更换为其他地址',
	'admin_invalid'			=> 'You did not fill in complete administrator information, please carefully fill in each item',//'您的信息管理员信息没有填写完整，请仔细填写每个项目',
	'admin_exist_password_error'	=> 'This user already exists. If you want to set this user as an administrator, please enter the correct password for the user, or replace the administrator name',//'该用户已经存在，如果您要设置此用户为论坛的管理员，请正确输入该用户的密码，或者请更换论坛管理员的名字',

	'tagtemplates_subject'		=> 'Title',//'标题',
	'tagtemplates_uid'		=> 'User ID',//'用户 ID',
	'tagtemplates_username'		=> 'Posted by',//'发帖者',
	'tagtemplates_dateline'		=> 'Date',//'日期',
	'tagtemplates_url'		=> 'Templates URL',//'主题地址',

	'uc_version_incorrect'		=> 'Your UCenter server version is too old. Please upgrade the UCenter service with the latest version. Download address: http://www.comsenz.com/.',//'您的 UCenter 服务端版本过低，请升级 UCenter 服务端到最新版本，并且升级，下载地址：http://www.comsenz.com/ 。',
	'config_unwriteable'		=> 'Setup Wizard can not write the configuration file. Enable the config.inc.php write permissions (666 or 777)',//'安装向导无法写入配置文件, 请设置 config.inc.php 程序属性为可写状态(777)',

	'install_in_processed'		=> 'Installing ...',//'正在安装...',
	'install_succeed'		=> 'Installation successfully completed! Click here to enter your Discuz! X2',//'安装成功，点击进入',
	'install_cloud'			=> 'After successful installation, Welcome to the opening Discuz! Cloud platform<br>Discuz! Cloud platform dedicated to help website owners to increase their websites traffic, enhance the ability of Web site operators, and increase a website revenue.<br>Discuz! Cloud platform currently provides a free QQ Internet, Tencent analysis, Cloud search, QQ Group Community,Roaming,SOSO emoticon services.Discuz! Cloud platform will continue to provide more quality services to the project.<br>Before open the Discuz! Platform make sure that your website (Discuz!, UCHome or SupeSite) has been upgraded to Discuz! X2.5.',//'安装成功，欢迎开通Discuz!云平台<br>Discuz!云平台致力于帮助站长提高网站流量，增强网站运营能力，增加网站收入。<br>Discuz!云平台目前免费提供了QQ互联、腾讯分析、纵横搜索、社区QQ群、漫游应用、SOSO表情服务。Discuz!云平台将陆续提供更多优质服务项目。<br>开通Discuz!平台之前，请确保您的网站（Discuz!、UCHome或SupeSite）已经升级到Discuz!X2.5。',
	'to_install_cloud'		=> 'Open Admin-Center',//'到后台开通',
	'to_index' => 'ھازىرچە ئاچمايمەن، مۇنبەرگە كىرەي',

	'init_credits_karma'	=> 'Reputation',//'威望',//!!! The same in install_var.php
	'init_credits_money'	=> 'Points',//'金钱',//!!! The same in install_var.php

	'init_postno0'		=> '#1',//'楼主',//!!! The same in install_var.php 
	'init_postno1'		=> '#2',//'沙发',    //!!! The same in install_var.php
	'init_postno2'		=> '#3',//'板凳',   //!!! The same in install_var.php
	'init_postno3'		=> '#4',//'地板',   //!!! The same in install_var.php

	'init_support'		=> 'Digg',//'支持',   //!!! The same in install_var.php
	'init_opposition'	=> 'Bury',//'反对',//!!! The same in install_var.php

	'init_group_0'	=> 'Member',//'会员',
	'init_group_1'	=> 'Administrator',//'管理员',
	'init_group_2'	=> 'Super Moderator',//'超级版主',
	'init_group_3'	=> 'Moderator',//'版主',
	'init_group_4'	=> 'R/O Member',//'禁止发言',
	'init_group_5'	=> 'Banned',//'禁止访问',
	'init_group_6'	=> 'IP Banned',//'禁止 IP',
	'init_group_7'	=> 'Guest',//'游客',
	'init_group_8'	=> 'Wait for verification',//'等待验证会员',
	'init_group_9'	=> 'Newbie',//'乞丐',
	'init_group_10'	=> 'Junior',//'新手上路',
	'init_group_11'	=> 'Member',//'注册会员',
	'init_group_12'	=> 'Middle Member',//'中级会员',
	'init_group_13'	=> 'Senior Member',//'高级会员',
	'init_group_14'	=> 'Gold Member',//'金牌会员',
	'init_group_15'	=> 'Veteran',//'论坛元老',

	'init_rank_1'	=> 'Beginner',//'新生入学',
	'init_rank_2'	=> 'Apprentice',//'小试牛刀',
	'init_rank_3'	=> 'Intern',//'实习记者',
	'init_rank_4'	=> 'Freelance writer',//'自由撰稿人',
	'init_rank_5'	=> 'Distinguished Writer',//'特聘作家',

	'init_cron_1'	=> 'Empty today\'s post count',//'清空今日发帖数',
	'init_cron_2'	=> 'Empty this month\'s online time',//'清空本月在线时间',
	'init_cron_3'	=> 'Daily data cleaning',//'每日数据清理',
	'init_cron_4'	=> 'Birthday statistics and e-mail subscriptions',//'生日统计与邮件祝福',
	'init_cron_5'	=> 'Topic reply notifications',//'主题回复通知',
	'init_cron_6'	=> 'Daily bulletin clean up',//'每日公告清理',
	'init_cron_7'	=> 'Time-limited operation clean-up',//'限时操作清理',
	'init_cron_8'	=> 'Promotion messages clean-up',//'论坛推广清理',
	'init_cron_9'	=> 'Monthly topic clean-up',//'每月主题清理',
	'init_cron_10'	=> 'Daily update X-Space users',//'每日 X-Space更新用户',
	'init_cron_11'	=> 'Weekly topic update',//'每周主题更新',

	'init_bbcode_1'	=> 'So that the contents of horizontal scrolling, the effect is similar to the marquee HTML tags, Note: This effect only valid under Internet Explorer browser.',//'使内容横向滚动，这个效果类似 HTML 的 marquee 标签，注意：这个效果只在 Internet Explorer 浏览器下有效。',
	'init_bbcode_2'	=> 'Embedded Flash animation',//'嵌入 Flash 动画',
	'init_bbcode_3'	=> 'Show QQ online status, click to this icon and chat with him/her',//'显示 QQ 在线状态，点这个图标可以和他（她）聊天',
	'init_bbcode_4'	=> 'Superscript',//'上标',
	'init_bbcode_5'	=> 'Subscript',//'下标',
	'init_bbcode_6'	=> 'Embedded Windows Media Audio',//'嵌入 Windows media 音频',
	'init_bbcode_7'	=> 'Embedded Windows Media Audio or video',//'嵌入 Windows media 音频或视频',

	'init_qihoo_searchboxtxt'		=> 'Input keywords for quick search this forum',//'输入关键词,快速搜索本论坛',
	'init_threadsticky'			=> 'Stick object: Global top, Category Top, This forum top',//'全局置顶,分类置顶,本版置顶',

	'init_default_style'			=> 'Default Style',//'默认风格',
	'init_default_forum'			=> 'Default Forum',//'默认版块',
	'init_default_template'			=> 'Default template',//'默认模板套系',
	'init_default_template_copyright'	=> 'Sing Imagination (Beijing) Technology Co., Ltd.',//'北京康盛新创科技有限责任公司',

	'init_dataformat'	=> 'Y-m-d',//'Y-n-j',
	'init_modreasons'	=> 'Advertising/SPAM\r\nMalicious/Hacking\r\nIllegal content\r\nOfftopic\r\nRepeated post\r\n\r\nI agree\r\nExcellent article\r\nOriginal content',//'广告/SPAM\r\n恶意灌水\r\n违规内容\r\n文不对题\r\n重复发帖\r\n\r\n我很赞同\r\n精品文章\r\n原创内容',
	'init_userreasons'	=> 'Powerfull!\r\nUsefull\r\nVery nice\r\nThe best!\r\nInteresting',//'很给力!\r\n神马都是浮云\r\n赞一个!\r\n山寨\r\n淡定',
	'init_link'		=> 'Discuz! Official forum',//'Discuz! 官方论坛',
	'init_link_note'	=> 'To provide the latest Discuz! Product news, software downloads and technical exchanges',//'提供最新 Discuz! 产品新闻、软件下载与技术交流',

	'init_promotion_task'	=> 'Website promotion task',//'网站推广任务',
	'init_gift_task'	=> 'Init Gift Task',//'红包类任务',
	'init_avatar_task'	=> 'Avatar Task',//'头像类任务',

	'license'	=> '<div class="license"><h1>ئۇيغۇرچە نۇسخسىنى ئورنىتىشتىن بۇرۇن ئىشلىتىش كېلىشىمىنى تەپسىلىي ئوقۇڭ:</h1>

<p>ئالدى بىلەن سىزنىڭ نۇرقۇت ئەتىرىتى ئۆزلەشتۈرگەن Discuz!X2.5 پىروگىراممىسىنى ئىشلەتكىنىڭىزگە كۆپ رەھمەت ! مەزكۇر پىروگىراممنىڭ بارلىق ھوقۇقى (c) 2001-2012 بېيجىڭ كاڭشېڭ شىنچۇاڭ پەن-تېخنىكا چەكلىك مەسئۇلىيەت شىركىتىگە تەۋە بولۇپ نۇرقۇت ئەتىرىتى مەزكۇر پىروگىراممىنى ئۈگىنىش ۋە تەتقىق قىلىش مەقسىتىدە ئۇيغۇرچىغا ئۆزلەشتۈردى.<p>

<p>مەزكۇر پىروگىراممىنى ئورنىتىپ ئىشلىتىشتىن بۇرۇن چوقۇم تۆۋەندىكى قائىدىلەرگە بويسۇنىشىڭىز كېرەك.<p>

<p>1. دۆلەتنىڭ بىخەتەرلىكىگە بۇزغۇنچىلىق قىلىدىغان، دۆلەت مەخپىيەتلىكىنى ئاشكارىلايدىغان، كوللېكتىپ ۋە ئاممىنىڭ قانۇنىي ھوقۇق-مەنپەئەتىگە دەخلى-تەرز قىلىدىغان تۆۋەندىكىدەك مەزمۇن، يازمىلارنى مەزكۇر پىروگىراممىدىن پايدىلىنىپ ئېچىلغان تور بېكەتتە ئېلان قىلىشقا، باشقا ئورۇندىن كۆچۈرۈپ چاپلاشقا بولمايدۇ:<p>

<p>(1)ئاساسى قانۇن، جىنايى ئىشلار قانۇنى، مەمۇر قانۇنغا خىلاپ يازمىلار؛<p>
	
<p>(2)ھۆكۈمەت ۋە سوتسىيالىستىك تۈزۈمگە قارشى يازمىلار؛<p>

<p>(3)دۆلەتنى پارچىلايدىغان، دۆلەت بىرلىگىگە بۇزغۇنچىلىق قىلىدىغان يازمىلار؛<p>

<p>(4)مىللىي ئۆچمەنلىك، مىللىي كەمسىتىش تۈسىنى ئالغان ۋە مىللەتلەر ئىتتىپاقلىقىغا بۇزغۇنچىلىق قىلىدىغان يازمىلار؛<p>

<p>(5)ھەقىقەتنى بۇرمىلاپ ئىغۋاگەرچىلىك قىلىدىغان، جەمئىيەتنى قالايمىقانلاشتۇرۇدىغان يازمىلار؛<p>

<p>(6)خۇراپىي، بىدئەت، قىمار ئويناش، تېررورلۇققا مۇناسىۋەتلىك يازمىلار؛<p>

<p>(7)شەخسىنىڭ غۇرۇرىغا تېگىدىغان، ھاقارەتلەيدىغان ۋە باشقا يامان غەرەزدىكى يازمىلار؛<p>

<p>(8)دۆلەت ئاپپاراتلىرىنىڭ ئىناۋىتىگە بۇزغۇنچىلىق قىلىدىغان يازمىلار؛<p>

<p>(9)رۇخسەتسىز ئېلان قىلىنغان سودا ئېلانى خاراكتېرىدىكى يازمىلار؛<p>

<p>(10)دۆلەتنىڭ قانۇن-سىياسىتىگە خىلاپ باشقا يازمىلار؛<p>

<p>ﺋﻪﮔﻪﺭ ﻳﯘﻗﯘﺭﺩﯨﻜﯩﻠﻪﺭﮔﻪ ﺋﺎﺋﯩﺖ ﻣﻪﺯﻣﯘﻥ ﺑﺎﻳﻘﺎﻟﺴﺎ ۋە ياكى ﺑﯩﺮﻩﺭ ﻣﻪﺳﯩﻠﻪ ﭼﯩﻘﺴﺎ ، مەزكۇر پىروگىراممىنى ئىشلەتكۈچى ﺋﯚﺯﻯ ﻣﻪﺳﺌﯘﻝ ﺑﻮﻟﯩﺪﯗ، نۇرقۇت تورى ھېچقانداق ﻣﻪﺳﺌﯘﻟﻴﻪﺗﻨﻰ ﺋﯚﺯ ﺋﯜﺳﺘﯩﮕﻪ ﺋﺎﻟﻤﺎﻳﺪﯗ.<p>
<p>2. مەزكۇر پىروگىراممىنى ئىشلەتكەن بېكەت باشلىقلىرى ئەمگىكىمىزگە تولۇق ھۆرمەت قىلىشى، ئۇيغۇر كومپيۇتېرچىلىقىنىڭ بىردەكلىكىنى ساقلىشى، ئېقىمغا، بىرلىككە قارشى يازمىلارنى يوللىماسلىقى كىرەك. ئۇيغۇر تور بېكەت باشلىقلىرى بىر-بىرىنى ھۆرمەتلىشى ھەمدە مەزكۇر پىروگىراممىدىن پايدىلىنىپ ئېچىلغان تور بېكەتتە ئېلان قىلغان ھەر قانداق يازمىغا ئۆزى مەسئۇل بولۇشى،  قائىدە-تەرتىپىگە ۋە مۇنازىرە ئەخلاقىغا ئالاھىدە دىققەت قىلىشى كېرەك.<p>
<p>3.مەزكۇر پىروگىراممىنى ئىشلەتكەن ﮬﻪﺭ ﻗﺎﻧﺪﺍﻕ بىر تور بېكەت باشلىقى ﮬﻪﺭﻗﺎﻧﺪﺍﻕ ﺷﻪﻛﯩﻠﺪﯨﻜﻰ ﺩﯙﻟﻪﺕ ﻗﺎﻧﯘﻧﻰ ﯞﻩ ﺋﯩﻨﺘﯩﺰﺍﻡ ـ ﭘﯩﺮﯨﻨﺴﯩﭙﻠﯩﺮﯨﻐﺎ ﻗﺎﺭﺷﻰ ﮬﻪﺭىكەت،  ﭘﺎﺋﺎﻟﯩﻴﻪﺗﻠﻪﺭﻧﻰ ﺗﻮﺳﯘﺵ ﯞﻩ ﻣﻪﻟﯘﻡ ﻗﯩﻠﯩﺶ ﻣﻪﺟﺒﯘﺭﯨﻴﯩﺘﻰ ﺑﺎﺭ.<p>
<p>4. ئەگەر سىز ئەمگىكىمىزگە ھۆرمەت قىلماي مەزكۇر پىروگىراممىدىكى ئۇلىنىش ئادرېسىمىزنى ئۆچۈرۋەتسىڭىز ياكى ئۆزگەرتىۋەتسىڭىز سىزنىڭ نۇرقۇت تورىدىكى ئەزالىق ھېساباتىڭىز توڭلىتىلغاندىن باشقا تور بېكىتىڭىزگە قارىتا مەلۇم چەكلەش تەدبىرىنى قوللىنىمىز، ئەگەر مۇشۇ سەۋەپلىك كېلىپ چىققان ھەرقانداق ئاقىۋەتنى تورىمىز ئۆز ئۈستىگە ئالمايدۇ.<p>
<p>5. ھەرقانداق تور بېكەت باشلىقىنىڭ نۇرقۇت ئەتىرىتى ئۆزلەشتۈرگەن پىروگىراممىدىن پايدىلىنىپ نۇرقۇت ئەتىرىتىمىزنىڭ ئىناۋىتىگە تەسىر يېتىدىغان ئىشلار بىلەن شۇغۇللانسا ، نۇرقۇت ئەتىرىتى مەسئۇلىيەتنى سۈرۈشتۈرۈش ھوقوقى بار ، ئېغىر بولغانلارنى بىۋاستە ئالاقىدار ئورۇنلارنىڭ بىر تەرەپ قىلىشىغا تاپشۇرىمىز.<p>
<p>6. ئەگەر سىز تور بېكىتىمىزگە كېلىپ تور بېكىتىمىزگە بەلگىلىك تۆھپە قوشسىڭىز ، تور بېكىتىمىز سىزنى ئالاھىدە مۇلازىمەت ياكى مۇكاپات بىلەن تەمىنلەيدۇ.<p>
<p>7.تور بېكىتىمىزنىڭ بۇندىن كىيىنكى تەرەققىياتى ئۈچۈن  تور بېكىتىمىزنى دوسىتلىرىڭىزغا تەۋسىيە قىلىشنى ئۇنۇتماڭ !<p>
<p>بىزنىڭ شوئارىمىز- مەقسىتىمىز بىر،مەنزىلىمىز بىر!<p>
<p>ھۆرمەت بىلەن نۇرقۇت ئەتىرىتى     تور ئادرېسى:http://www.nurqut.com<p></div>

</div>',

	'uc_installed'		=> 'You have installed the UCenter. If you need to re-install, delete the data/install.lock file',//'您已经安装过 UCenter，如果需要重新安装，请删除 data/install.lock 文件',
	'i_agree'		=> 'I have read and agree to all the elements of the Terms',//'我已仔细阅读，并同意上述条款中的所有内容',
	'supportted' => 'قوللايدۇ',
	'unsupportted' => 'قوللىمايدۇ',
	'max_size'		=> 'Supported / Max Size',//'支持/最大尺寸',
	'project' => 'تۈر',
	'ucenter_required' => 'Discuz! كېتەرلىك شەرت',
	'ucenter_best' => 'Discuz! ئۈچۈن تەۋسىيە',
	'curr_server' => 'مۇلازىمىتېر',
	'env_check' => 'مۇھىت تەكشۈرۈش',
	'os' => 'مەشغۇلات سىستېمىسى',
	'php' => 'PHP نەشرى',
	'attachmentupload' => 'ھۆججەت يۈكلەش',
	'unlimit' => 'چەكلىمە يوق',
	'version' => 'نەشرى',
	'gdversion' => 'GD ئامبىرى',
	'allow' => 'بولىدۇ ',
	'unix' => 'Unix تىپىدىكى',
	'diskspace' => 'دىسكا بوشلۇقى',
	'priv_check' => 'ھۆججەت، مۇندەرىجە ھوقۇق چەكلمىسى',
	'func_depend' => 'تاينىشچان فۇنكىسىيەلەرنى تەكشۈرۈش',
	'func_name' => 'فۇنكىسىيە نامى',
	'check_result' => 'تەكشۈرۈش نەتىجىسى',
	'suggestion' => 'تەۋسىيە',
	'advice_mysql' => ' mysql بۆلەك نورمال قوزغالدىمۇ-يوق تەكشۈرۈپ بېقىڭ',
	'advice_fopen'		=> 'This function require the <b>allow_url_fopen</b> option to be <b>On</b> in php.ini. Please contact the server administrator to resolve this problem.',//'该函数需要 php.ini 中 allow_url_fopen 选项开启。请联系空间商，确定开启了此项功能',
	'advice_file_get_contents'	=> 'This function require the <b>allow_url_fopen</b> option to be <b>On</b> in php.ini. Please contact the server administrator to resolve this problem.',//'该函数需要 php.ini 中 allow_url_fopen 选项开启。请联系空间商，确定开启了此项功能',
	'advice_xml'			=> 'This function require the PHP support for XML. Please contact the server administrator to resolve this problem.',//'该函数需要 PHP 支持 XML。请联系空间商，确定开启了此项功能',
	'none' => 'يوق',

	'dbhost' => 'ساندان مۇلازىمېتىرى',
	'dbuser' => 'ساندان ئىشلەتكۈچى',
	'dbpw' => 'ساندان پارولى',
	'dbname' => 'ساندان نامى',
	'tablepre' => 'جەدۋەل ئالدى ئۇلانمىسى',

	'ucfounderpw' => 'قۇرغۇچى پارولى',
	'ucfounderpw2' => 'پارولنى قايتا كىرگۈزۈڭ',

	'init_log'		=> 'Initialize log',//'初始化记录',
	'clear_dir'		=> 'Clear directory',//'清空目录',
	'select_db'		=> 'Select the database',//'选择数据库',
	'create_table'		=> 'Create table',//'建立数据表',
	'succeed'		=> 'Success',//'成功 ',

	'install_data'			=> 'Data installed successfully',//'正在安装数据',
	'install_test_data'		=> 'Install regional data',//'正在安装附加数据',

	'method_undefined'		=> 'Undefined method',//'未定义方法',
	'database_nonexistence'		=> 'Database object does not exist',//'数据库操作对象不存在',
	'skip_current' => 'ئاتلاپ ئۆتۈپ كېتىش',
	'topic' => 'مەخسۇس تېما',
/*3.1*/	'install_finish'		=> 'Installation successfully completed! Click here to enter your Discuz! X',//'您的论坛已完成安装，点此访问',

//---------------------------------------------------------------
// Added by Valery Votintsev
// 2 vars for language select:
	'welcome'			=> 'Welcome to Discuz! X Installation!',//'欢迎到Discuz！ X安装！',
	'select_language'		=> '<b>Select the installation language</b>:',//'<b>选择安装语言</b>',
//vot !!!Translate to Chinese!!!
//vot	'regiondata'			=> 'Add regions data',//'Add location data',
//vot	'regiondata_check_label'	=> 'Install additional regional data (countries/regions/cities)',//'Install additional regional data (countries/regions/cities)',
//vot	'install_region_data'		=> 'Install regional data',//'Install regional data',
	'php_version_too_low'		=> 'PHP version is too low',
	'php_version_too_low_comment'	=> 'For normal functioning Discuz! requires for more new version of PHP',
	'mbstring'			=> 'MBstring Library',//'MBstring 库',
	'ext_info'			=> 'Additionally you can install many interesting and usefull plugins and templates from the Cloud Application Center:',//'另外，你可以从云计算应用中心的许多有趣的和有用的插件和模板安装：',//'另外，你可以從雲計算應用中心的許多有趣的和有用的插件和模板安裝：',
//---------------------------------------------------------------

);

$msglang = array(
	'config_nonexistence'	=> 'Your config.inc.php file does not exist. Can not continue the installation, please use the FTP to upload the file and try again.',//'您的 config.inc.php 不存在, 无法继续安装, 请用 FTP 将该文件上传后再试。',
);

