<?php

/**
 *      [Discuz!] (C)2001-2099 Comsenz Inc.
 *      This is NOT a freeware, use is subject to license terms
 *
 *      $Id: lang_swfupload.php 27449 2012-02-01 05:32:35Z zhangguosheng $
 */

if(!defined('IN_DISCUZ')) {
	exit('Access Denied');
}

$lang = "
<okbtn>muqimlash</okbtn>
<ctnbtn>dawamlashturush</ctnbtn>
<fileName>höjjet ismi</fileName>
<size>höjjet chongliqi</size>
<stat>yollinish ehwali</stat>
<browser>körüsh</browser>
<delete>öchürüsh</delete>
<return>qaytish</return>
<upload>yollash</upload>
<okTitle>yollash tamam</okTitle>
<okMsg>höjjet yollash tamam</okMsg>
<uploadTitle>yolliniwatidu</uploadTitle>
<uploadMsg1>jeméiy</uploadMsg1>
<uploadMsg2>dane höjjet saqlawatidu,inchi höjjet yolliniwatidu</uploadMsg2>
<uploadMsg3>dane höjjet</uploadMsg3>
<bigFile>höjjet chong</bigFile>
<uploaderror>yollash meghlup boldi</uploaderror>
";

