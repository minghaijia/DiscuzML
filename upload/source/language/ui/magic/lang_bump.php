<?php

/**
 *      [Discuz!] (C)2001-2099 Comsenz Inc.
 *      This is NOT a freeware, use is subject to license terms
 *
 *      $Id: lang_bump.php 27449 2012-02-01 05:32:35Z zhangguosheng $
 */

if(!defined('IN_DISCUZ')) {
	exit('Access Denied');
}

/**
 *      为使用需要而翻译，原程序开发者版权所有
 *      Translated By NurQut Team. [NTA] Powered by NURQUT(FinalDream) && UYSON
 *
 *      Auto Translated By NurQut Translation Assistant(NurQut Terjimani)
 *      Translation Time : 2012-06-02
 */

$lang = array
(
	'bump_name'			=> 'Bump Card',//'提升卡',
	'bump_forum' => 'بۇ كارتىنى ئىشلىتىشكە بولىدىغان سەھىپىلەر',
	'bump_desc' => 'خالىغان بىر تېمىنى كۆتۈرۈشكە بولىدۇ',
	'bump_info' => 'بەلگىلەنگەن تېمىنى كۆتۈرۈشكە بولىدۇ، تېمىنىڭ ID نومۇرىنى تولدۇرۇڭ',
	'bump_info_nonexistence' => 'كۆتۈرمەكچى بولغان تېمىنى بەلگىلەڭ',
	'bump_succeed' => 'تېما كۆتۈرۈش مەشغۇلاتىڭىز مۇۋەپپەقىيەتلىك تاماملاندى.',
	'bump_info_noperm' => 'كەچۈرۈڭ، تېما تۇرۇشلۇق سەھىپىدە مەزكۇر كارتىنى ئىشلىتىش چەكلەنگەن.',

	'bump_notification' => 'تېمىڭىز {subject} گە {actor}   {magicname} نى ئىشلەتتى، <a href="forum.php?mod=viewthread&tid={tid}"> كۆرۈپ بېقىڭ!</a>',
);

