<?php

/**
 *      [Discuz!] (C)2001-2099 Comsenz Inc.
 *      This is NOT a freeware, use is subject to license terms
 *
 *      $Id: lang_friendnum.php 27449 2012-02-01 05:32:35Z zhangguosheng $
 */

if(!defined('IN_DISCUZ')) {
	exit('Access Denied');
}

/**
 *      为使用需要而翻译，原程序开发者版权所有
 *      Translated By NurQut Team. [NTA] Powered by NURQUT(FinalDream) && UYSON
 *
 *      Auto Translated By NurQut Translation Assistant(NurQut Terjimani)
 *      Translation Time : 2012-06-02
 */

$lang = array
(
	'friendnum_name' => '好友增容卡',
	'friendnum_desc' => 'دوست قوشۇش چەكلىمە سانىنى ئاشۇرۇشقا بولىدۇ',
	'friendnum_info' => 'چەكلىمىدىن سىرت  {num}  دانە دوست قوشۇشقا بولىدۇ',
	'friendnum_addnum' => 'دوست سانىنى ئاشۇرۇش',
);

