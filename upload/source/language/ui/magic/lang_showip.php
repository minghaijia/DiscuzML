<?php

/**
 *      [Discuz!] (C)2001-2099 Comsenz Inc.
 *      This is NOT a freeware, use is subject to license terms
 *
 *      $Id: lang_showip.php 27449 2012-02-01 05:32:35Z zhangguosheng $
 */

if(!defined('IN_DISCUZ')) {
	exit('Access Denied');
}

/**
 *      为使用需要而翻译，原程序开发者版权所有
 *      Translated By NurQut Team. [NTA] Powered by NURQUT(FinalDream) && UYSON
 *
 *      Auto Translated By NurQut Translation Assistant(NurQut Terjimani)
 *      Translation Time : 2012-06-02
 */

$lang = array
(
	'showip_name'			=> 'Show IP Card',//'窥视卡',
	'showip_desc' => 'بەلگىلەنگەن ئەزانىڭ IP ئادرېسىنى كۆرۈشكە بولىدۇ',
	'showip_targetuser' => 'كىمنىڭ IP ئادرېسىنى كۆرمەكچىسىز',
	'showip_info_nonexistence' => 'ئابونت ئىسمىنى كىرگۈزۈڭ',
	'showip_ip_message' => '{username} نىڭ IP ئادرېسى: {ip}',
	'showip_info_noperm' => 'كەچۈرۈڭ، بۇ ئەزانىڭ IP ئادرېسىنى كۆرۈشكە ھوقۇقىڭىز يەتمەيدۇ',

	'showip_notification' => 'باشقا ئەزالار سىزگە {magicname} ئىشلىتىۋاتىدۇ',
);

