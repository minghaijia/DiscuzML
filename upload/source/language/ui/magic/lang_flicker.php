<?php
/**
 *      [Discuz!] (C)2001-2099 Comsenz Inc.
 *      This is NOT a freeware, use is subject to license terms
 *
 *      $Id: lang_downdateline.php 27449 2012-02-01 05:32:35Z zhangguosheng $
 */

if(!defined('IN_DISCUZ')) {
	exit('Access Denied');
}

/**
 *      为使用需要而翻译，原程序开发者版权所有
 *      Translated By NurQut Team. [NTA] Powered by NURQUT(FinalDream) && UYSON
 *
 *      Auto Translated By NurQut Translation Assistant(NurQut Terjimani)
 *      Translation Time : 2012-06-02
 */

$lang = array
(
	'flicker_name'		=> 'Flicker',//'彩虹炫',
	'flicker_desc' => 'خاتىرە ۋە ئالبوم باھا سۆزىدە رەڭدار ھەسەن-ھۈسەن ئۈنۈمى ئىشلىتىش',
	'flicker_info' => 'خاتىرە ۋە ئالبوم باھا سۆزىدە رەڭدار ھەسەن-ھۈسەن ئۈنۈمى ئىشلىتىش',
	'flicker_succeed' => 'رەڭدار ھەسەن-ھۈسەن ئىشلىتىش مۇۋەپپەقىيەتلىك بولدى.',
);