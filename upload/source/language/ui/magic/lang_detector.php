<?php

/**
 *      [Discuz!] (C)2001-2099 Comsenz Inc.
 *      This is NOT a freeware, use is subject to license terms
 *
 *      $Id: lang_detector.php 27449 2012-02-01 05:32:35Z zhangguosheng $
 */

if(!defined('IN_DISCUZ')) {
	exit('Access Denied');
}

/**
 *      为使用需要而翻译，原程序开发者版权所有
 *      Translated By NurQut Team. [NTA] Powered by NURQUT(FinalDream) && UYSON
 *
 *      Auto Translated By NurQut Translation Assistant(NurQut Terjimani)
 *      Translation Time : 2012-06-02
 */

$lang = array
(
	'detector_name'	=> 'Detector',//'探测器',
	'detector_desc' => 'قىزىل بولاق كۆمۈلگەن ئەزالار بوشلۇقىنى تەكشۈرۈش',
	'detector_num' => 'ئەڭ يۇقىرى تەكشۈرۈش سانى ',
	'detector_info' => 'قىزىل بولاق كۆمۈلگەن ئەزالار بوشلۇقىنى تەكشۈرۈش. ئاڭ كۆپ بولغاندا {num} دانە',
);

