<?php

/**
 *      [Discuz!] (C)2001-2099 Comsenz Inc.
 *      This is NOT a freeware, use is subject to license terms
 *
 *      $Id: lang_misc.php 27449 2012-02-01 05:32:35Z zhangguosheng $
 */

if(!defined('IN_DISCUZ')) {
	exit('Access Denied');
}

/**
 *      为使用需要而翻译，原程序开发者版权所有
 *      Translated By NurQut Team. [NTA] Powered by NURQUT(FinalDream) && UYSON
 *
 *      Auto Translated By NurQut Translation Assistant(NurQut Terjimani)
 *      Translation Time : 2012-06-02
 */

$lang = array
(
	'group_normal_member' => 'ئادەتتىكى ئەزا قىلىش',
	'group_goaway' => 'توپتىن چىقىرىۋېتىش',
	'group_demise_message_title' => 'نى{forum}سىزگە ئۆتكۈزۈپ بەردى.',
	'group_demise_message_body' => '{forum}نى سىزگە ئۆتكۈزۈپ بەردىم，[url={siteurl}forum.php?mod=group&fid={fid}]بۇ يەردىن كۆرۈڭ[/url]',
	'group_join' => 'كىرىش{groupname}توپقا',
);

